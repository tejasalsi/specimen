CURRENT STATS:
System memory usage:
  SOff/Norm: 2 samples:
    Cached: 201MB min, 230MB avg, 258MB max
    Free: 72MB min, 94MB avg, 116MB max
    ZRam: 0.00 min, 0.00 avg, 0.00 max
    Kernel: 141MB min, 142MB avg, 143MB max
    Native: 61MB min, 61MB avg, 61MB max

Per-Package Stats:
  * com.specimen / u0a330 / v1:
      * com.specimen / u0a330 / v1:
               TOTAL: 78% (8.2MB-111MB-184MB/7.0MB-109MB-182MB over 20)
                 Top: 78% (8.2MB-111MB-184MB/7.0MB-109MB-182MB over 20)
          (Last Act): 0.04%
            (Cached): 6.8% (125MB-125MB-125MB/123MB-123MB-123MB over 1)
      * com.specimen.util.notifications.RegistrationIntentService:
        Process: com.specimen
            Running count 10 / time 0.09%
            Started count 5 / time 0.02%
          Executing count 10 / time 0.08%

Summary:
  * com.specimen / u0a330 / v1:
           TOTAL: 78% (8.2MB-111MB-184MB/7.0MB-109MB-182MB over 20)
             Top: 78% (8.2MB-111MB-184MB/7.0MB-109MB-182MB over 20)
      (Last Act): 0.04%
        (Cached): 6.8% (125MB-125MB-125MB/123MB-123MB-123MB over 1)

Run time Stats:
  SOff/Norm: +9m2s622ms
  SOn /Norm: +16m37s615ms (running)
      TOTAL: +25m40s237ms

Memory usage:
  Kernel : 142MB (16 samples)
  Native : 61MB (16 samples)
  Persist: 230MB (20 samples)
  Top    : 144MB (84 samples)
  ImpFg  : 92MB (75 samples)
  ImpBg  : 39MB (43 samples)
  Service: 326MB (209 samples)
  Receivr: 125KB (117 samples)
  Home   : 76MB (14 samples)
  LastAct: 48KB (21 samples)
  CchAct : 109MB (16 samples)
  CchEmty: 184MB (149 samples)
  Cached : 230MB (16 samples)
  Free   : 94MB (16 samples)
  TOTAL  : 1.7GB
  ServRst: 21KB (7 samples)

          Start time: 2015-11-28 15:12:13
  Total elapsed time: +25m40s257ms (partial) libart.so

AGGREGATED OVER LAST 24 HOURS:
System memory usage:
  SOff/Norm: 21 samples:
    Cached: 180MB min, 295MB avg, 452MB max
    Free: 41MB min, 86MB avg, 180MB max
    ZRam: 0.00 min, 0.00 avg, 0.00 max
    Kernel: 126MB min, 135MB avg, 143MB max
    Native: 61MB min, 63MB avg, 65MB max
  SOn /Norm: 24 samples:
    Cached: 233MB min, 327MB avg, 416MB max
    Free: 42MB min, 84MB avg, 249MB max
    ZRam: 0.00 min, 0.00 avg, 0.00 max
    Kernel: 125MB min, 135MB avg, 146MB max
    Native: 60MB min, 62MB avg, 65MB max

Per-Package Stats:
  * com.specimen / u0a330 / v1:
      * com.specimen / u0a330 / v1:
               TOTAL: 32% (8.1MB-136MB-235MB/6.9MB-133MB-229MB over 226)
                 Top: 32% (8.1MB-136MB-235MB/6.9MB-133MB-229MB over 226)
          (Last Act): 0.45% (154MB-163MB-172MB/145MB-155MB-165MB over 2)
            (Cached): 2.6% (40MB-126MB-168MB/40MB-123MB-161MB over 10)
      * com.specimen.util.notifications.GCMListenerService:
        Process: com.specimen
            Running count 2 / time 0.00%
            Started count 1 / time 0.00%
          Executing count 2 / time 0.00%
      * com.specimen.util.notifications.RegistrationIntentService:
        Process: com.specimen
            Running count 55 / time 0.02%
            Started count 28 / time 0.00%
          Executing count 55 / time 0.02%

Summary:
  * com.specimen / u0a330 / v1:
           TOTAL: 32% (8.1MB-136MB-235MB/6.9MB-133MB-229MB over 226)
             Top: 32% (8.1MB-136MB-235MB/6.9MB-133MB-229MB over 226)
      (Last Act): 0.45% (154MB-163MB-172MB/145MB-155MB-165MB over 2)
        (Cached): 2.6% (40MB-126MB-168MB/40MB-123MB-161MB over 10)

Run time Stats:
  SOff/Norm: +8h11m59s13ms
  SOn /Norm: +4h16m9s407ms
      TOTAL: +12h28m8s420ms

Memory usage:
  Kernel : 135MB (315 samples)
  Native : 63MB (315 samples)
  Persist: 233MB (545 samples)
  Top    : 158MB (1769 samples)
  ImpFg  : 113MB (1775 samples)
  ImpBg  : 30MB (2210 samples)
  Service: 338MB (5383 samples)
  Receivr: 161KB (4981 samples)
  Home   : 51MB (268 samples)
  LastAct: 9.4MB (950 samples)
  CchAct : 52MB (184 samples)
  CchCAct: 394 (65 samples)
  CchEmty: 195MB (2619 samples)
  Cached : 306MB (315 samples)
  Free   : 85MB (315 samples)
  TOTAL  : 1.7GB
  ServRst: 20KB (1312 samples)

          Start time: 2015-11-27 15:53:34
  Total elapsed time: +23h44m20s235ms (partial) libart.so

AGGREGATED OVER LAST 3 HOURS:
System memory usage:
  SOff/Norm: 12 samples:
    Cached: 180MB min, 263MB avg, 387MB max
    Free: 43MB min, 81MB avg, 142MB max
    ZRam: 0.00 min, 0.00 avg, 0.00 max
    Kernel: 131MB min, 138MB avg, 143MB max
    Native: 61MB min, 62MB avg, 65MB max
  SOn /Norm: 6 samples:
    Cached: 233MB min, 291MB avg, 390MB max
    Free: 43MB min, 112MB avg, 209MB max
    ZRam: 0.00 min, 0.00 avg, 0.00 max
    Kernel: 134MB min, 138MB avg, 141MB max
    Native: 60MB min, 62MB avg, 64MB max

Per-Package Stats:
  * com.specimen / u0a330 / v1:
      * com.specimen / u0a330 / v1:
               TOTAL: 73% (8.1MB-124MB-232MB/6.9MB-122MB-229MB over 156)
                 Top: 73% (8.1MB-124MB-232MB/6.9MB-122MB-229MB over 156)
          (Last Act): 0.02%
            (Cached): 2.6% (40MB-117MB-135MB/40MB-115MB-132MB over 6)
      * com.specimen.util.notifications.RegistrationIntentService:
        Process: com.specimen
            Running count 47 / time 0.06%
            Started count 24 / time 0.01%
          Executing count 47 / time 0.06%

Summary:
  * com.specimen / u0a330 / v1:
           TOTAL: 73% (8.1MB-124MB-232MB/6.9MB-122MB-229MB over 156)
             Top: 73% (8.1MB-124MB-232MB/6.9MB-122MB-229MB over 156)
      (Last Act): 0.02%
        (Cached): 2.6% (40MB-117MB-135MB/40MB-115MB-132MB over 6)

Run time Stats:
  SOff/Norm: +1h39m18s499ms
  SOn /Norm: +1h46m36s229ms
      TOTAL: +3h25m54s728ms

Memory usage:
  Kernel : 138MB (126 samples)
  Native : 62MB (126 samples)
  Persist: 234MB (180 samples)
  Top    : 133MB (567 samples)
  ImpFg  : 114MB (528 samples)
  ImpBg  : 33MB (704 samples)
  Service: 331MB (1791 samples)
  Receivr: 153KB (1571 samples)
  Home   : 86MB (86 samples)
  LastAct: 0.92MB (121 samples)
  CchAct : 79MB (53 samples)
  CchEmty: 204MB (798 samples)
  Cached : 277MB (126 samples)
  Free   : 97MB (126 samples)
  TOTAL  : 1.7GB
  ServRst: 11KB (87 samples)

          Start time: 2015-11-28 11:58:07
  Total elapsed time: +3h39m46s410ms (partial) libart.so
