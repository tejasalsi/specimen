package com.specimen.authentication;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;

import com.specimen.widget.JustifyTextView;

/**
 * Created by Tejas on 6/1/2015.
 * Contains privacy statement.
 */
public class PrivacyAndTermsConditionsFragment extends DialogFragment {

    public static PrivacyAndTermsConditionsFragment newInstance() {

        return new PrivacyAndTermsConditionsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Translucent_NoTitleBar);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_policy_and_conditions, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        JustifyTextView lblPrivacyAndTermsContent = (JustifyTextView) view.findViewById(R.id.lblContent);
        ImageView closeButton = (ImageView) view.findViewById(R.id.closeTnC);
        TextView privacyTitle = (TextView) view.findViewById(R.id.privacyTitle);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        lblPrivacyAndTermsContent.setMovementMethod(new ScrollingMovementMethod());

        if ("PRIVACY".equals(getTag().toString())) {
            //getDialog().setTitle(R.string.privacy_title);
            privacyTitle.setText("Privacy Policy");
            lblPrivacyAndTermsContent.setText(
                    Html.fromHtml(getActivity().getResources().
                            getString(R.string.terms_and_conditions_content)));
        } else {
            //getDialog().setTitle(R.string.terms_and_conditions_title);
            privacyTitle.setText("Terms & Conditions");
            lblPrivacyAndTermsContent.setText(
                    Html.fromHtml(getActivity().getResources().
                            getString(R.string.terms_and_conditions_content)));
        }

        return view;
    }
}
