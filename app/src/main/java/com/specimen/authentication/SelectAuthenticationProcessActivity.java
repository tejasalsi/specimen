package com.specimen.authentication;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.authentication.IAuthenticationFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.LoginBannerResponse;
import com.specimen.core.response.UserResponse;
import com.specimen.home.HomeActivity;
import com.specimen.survey.SurveyActivity;
import com.specimen.util.PerformSocialLogin;
import com.specimen.util.notifications.RegistrationIntentService;
import com.specimen.widget.ProgressView;
import com.specimen.widget.swipeablecards.view.CardContainer;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by root on 12/8/15.
 */
public class SelectAuthenticationProcessActivity extends BaseActivity implements IResponseSubscribe {

    public static final int LOGIN = 1;
    public static final int SIGNUP = 2;

    public static final String FACEBOOK_LOGIN = "fb";
    public static final String GOOGLE_LOGIN = "gp";

    final String TAG = "SelectAuthenticationProcessActivity";

    ViewPager userAuthenticatePager;
    DisplayMetrics metrics;
    ImageAdapter adapter;
    ProgressView mProgressView;

    // Facebook Login
    CallbackManager callbackManager;
    FBLogin fbLogin;

    // Google plus login
    GoogleApiClient mGoogleApiClient;
    GooglePlusLogin gPlusLogin;

    List<String> mLoginBannerImagesStringArray;
    List<String> mClonedLoginBannerImagesStringList;

    int banner_image_counter;
    int save_counter_value;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectauthenticationprocess);


        ((TextView) findViewById(R.id.txtTop)).setTypeface(Typeface.createFromAsset(getAssets(), "fonts/GILB____.TTF"));
        ((TextView) findViewById(R.id.txtTopNextLine)).setTypeface(Typeface.createFromAsset(getAssets(), "fonts/GILB____.TTF"));

        // Restore from saved instance state
        // [START restore_saved_instance_state]
        if (savedInstanceState != null) {
            gPlusLogin.mIsResolving = savedInstanceState.getBoolean(GooglePlusLogin.KEY_IS_RESOLVING);
            gPlusLogin.mShouldResolve = savedInstanceState.getBoolean(GooglePlusLogin.KEY_SHOULD_RESOLVE);
        }

        mProgressView = (ProgressView) findViewById(R.id.progressView);
        metrics = getResources().getDisplayMetrics();

        Intent notificationIntent = new Intent(this, RegistrationIntentService.class);
        startService(notificationIntent);

        final IApplicationFacade applicationFacade = (IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, TAG);
        if (applicationFacade.getUserResponse() != null) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
//            finish();
        } else {
            initFB();
            initGPLUS();

            userAuthenticatePager = (ViewPager) findViewById(R.id.selectuthenticationprocessViewPager);
            userAuthenticatePager.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
            findViewById(R.id.btnSignup).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent loginIntent = new Intent(SelectAuthenticationProcessActivity.this, UserAuthenticationActivity.class);
                    loginIntent.putExtra("LOGIN", SIGNUP);
                    startActivity(loginIntent);
                    finish();
                }
            });

            TextView lblLogin = (TextView) findViewById(R.id.lblLogin);
            String str = lblLogin.getText().toString();
            SpannableString spannableString = SpannableString.valueOf(str);
            spannableString.setSpan(new StyleSpan(Typeface.BOLD), 15, spannableString.length(), 0);
            lblLogin.setText(spannableString);
            lblLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent loginIntent = new Intent(SelectAuthenticationProcessActivity.this, UserAuthenticationActivity.class);
                    loginIntent.putExtra("LOGIN", LOGIN);
                    startActivity(loginIntent);
                    finish();
                }
            });

            findViewById(R.id.imgFb).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fbLogin.initFBLogin();
                }
            });

            findViewById(R.id.imgGplus).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gPlusLogin.signInWithGplus();
                }
            });

            mProgressView.setAnimation();
            mProgressView.setVisibility(View.VISIBLE);
            ((IAuthenticationFacade) IOCContainer.getInstance()
                    .getObject(ServiceName.AUTHENTICATION_SERVICE, TAG)).loginBanner();
        }
    }

    void initFB() {
        fbLogin = new FBLogin(this);
        fbLogin.signInWithFB();
        callbackManager = fbLogin.getCallbackManager();
    }

    void initGPLUS() {
        gPlusLogin = new GooglePlusLogin(this);
        if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient = null;
        } else {
            mGoogleApiClient = gPlusLogin.getGoogleApiClient();
        }
    }

    void setBannerImageTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (banner_image_counter == save_counter_value) {
                    save_counter_value = 0;
                }
                userAuthenticatePager.getAdapter().notifyDataSetChanged();
                userAuthenticatePager.setCurrentItem(save_counter_value);
                ++save_counter_value;
                setBannerImageTimer();
            }
        }, 3000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (null != mGoogleApiClient)
            mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }
    }

    // [START on_save_instance_state]
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(GooglePlusLogin.KEY_IS_RESOLVING, gPlusLogin.mIsResolving);
        outState.putBoolean(GooglePlusLogin.KEY_SHOULD_RESOLVE, gPlusLogin.mShouldResolve);
    }
    // [END on_save_instance_state]

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        callbackManager.onActivityResult(requestCode, responseCode, data);

        if (requestCode == GooglePlusLogin.RC_SIGN_IN) {
            // If the error resolution was not successful we should not resolve further.
            if (responseCode != RESULT_OK) {
                gPlusLogin.mShouldResolve = false;
            }

            gPlusLogin.mIsResolving = false;
            mGoogleApiClient.connect();
        }
    }
    // [END on_activity_result]

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == GooglePlusLogin.RC_PERM_GET_ACCOUNTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                gPlusLogin.getProfileInformation();
            } else {
            }
        }
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        mProgressView.setVisibility(View.GONE);
        if (!tag.equals(TAG)) {
            return;
        }

        if (response instanceof UserResponse) {
            UserResponse userResponse = (UserResponse) response;
            if (null != userResponse) {
                if ("Failure".equals(userResponse.getStatus())) {
                    Toast.makeText(SelectAuthenticationProcessActivity.this, "" + userResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(SelectAuthenticationProcessActivity.this, SurveyActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
//                    finish();
                }
            }
        }

        if (response instanceof LoginBannerResponse) {
            LoginBannerResponse loginBannerResponse = (LoginBannerResponse) response;
            if (null != loginBannerResponse && null != loginBannerResponse.getData() && 0 < loginBannerResponse.getData().size()) {
                mLoginBannerImagesStringArray = loginBannerResponse.getData();
                mClonedLoginBannerImagesStringList = new ArrayList<>();
                for (int i = 0; i < 300; i++) {
                    for (int copy = 0; copy < mLoginBannerImagesStringArray.size(); copy++) {
                        mClonedLoginBannerImagesStringList.add(mLoginBannerImagesStringArray.get(copy));
                    }
                }
                banner_image_counter = mClonedLoginBannerImagesStringList.size();
                adapter = new ImageAdapter(SelectAuthenticationProcessActivity.this);
                userAuthenticatePager.setAdapter(adapter);

                setBannerImageTimer();
            }
        }
    }

    @Override
    public void onFailure(Exception error) {

    }

    public class ImageAdapter extends PagerAdapter {

        Context context;

        ImageAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return mClonedLoginBannerImagesStringList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = new ImageView(context);
            Picasso.with(context).load(mClonedLoginBannerImagesStringList.get(position))
                    .placeholder(R.drawable.pdp_banner_placeholder)
                    .fit().centerCrop()
                    .into(imageView);

            container.addView(imageView, 0);


            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }
    }

    public void socialLoginAPI(final String loginType, final String email, final String firstName, final String accessToken) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SelectAuthenticationProcessActivity.this);
        String deviceId = sharedPreferences.getString("gcm_device_id", "");
        ((IAuthenticationFacade) IOCContainer.getInstance()
                .getObject(ServiceName.AUTHENTICATION_SERVICE, TAG))
                .socialLogin(loginType, email, firstName, accessToken, deviceId);
    }
}