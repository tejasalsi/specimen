package com.specimen.authentication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.authentication.IAuthenticationFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.UserResponse;
import com.specimen.survey.SurveyActivity;
import com.specimen.util.PerformSocialLogin;
import com.specimen.util.TokenGenerator;
import com.specimen.widget.ProgressView;
import com.specimen.widget.UserPerformedErrors;

import java.util.regex.Pattern;

/**
 * Created by Tejas on 6/1/2015.
 * User will enter login credentials here.
 */
public class LogInFragment extends BaseFragment implements IResponseSubscribe, View.OnClickListener {

    private Button fbLoginBtn, gPlusLoginBtn;
    private Button logInImageButton;
    private EditText editEmail, editPassword;
    private TextView textForgotPassword, lblErrorMsg;
    private ProgressView logInLoader;
    private UserPerformedErrors userPerformedErrors;
    public final String TAG = "LOGIN";
    AppCompatActivity mActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (AppCompatActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup logInView = (ViewGroup) inflater.inflate(R.layout.fragment_login, container, false);

        logInImageButton = (Button) logInView.findViewById(R.id.image_loginBtn);
        fbLoginBtn = (Button) logInView.findViewById(R.id.fb_loginbtn);
        gPlusLoginBtn = (Button) logInView.findViewById(R.id.gplus_loginbtn);
        editEmail = (EditText) logInView.findViewById(R.id.edit_signupemail);
        editPassword = (EditText) logInView.findViewById(R.id.edit_Password);
        textForgotPassword = (TextView) logInView.findViewById(R.id.text_forgotpassword);
        lblErrorMsg = (TextView) logInView.findViewById(R.id.lblErrorMsg);
        logInLoader = (ProgressView) logInView.findViewById(R.id.progressBar);

        fbLoginBtn.setOnClickListener(this);
        logInImageButton.setOnClickListener(this);
        gPlusLoginBtn.setOnClickListener(this);
        textForgotPassword.setOnClickListener(this);

        TokenGenerator tokenGenerator = new TokenGenerator();
        Log.d("Token:", tokenGenerator.generateToken());

        return logInView;
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (!tag.equals("LOGIN")) {
            return;
        }

        if (response instanceof UserResponse) {
            if (null != ((UserResponse) response).getData() && !"Failure".equals(response.getStatus())) {
                logInLoader.setVisibility(View.GONE);

                Intent intent = new Intent(mContext, SurveyActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                mContext.finish();

            } else {
                userPerformedErrors = new UserPerformedErrors(lblErrorMsg);
                userPerformedErrors.setErrorMessage(response.getMessage().toUpperCase());
                logInLoader.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        } else {
            IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
        }
    }

    @Override
    public void onFailure(Exception error) {
        logInLoader.setVisibility(View.GONE);
        Log.e("Onerror", error.toString());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_loginBtn:
                String email = editEmail.getText().toString();
                String password = editPassword.getText().toString();
                logInLoader.setVisibility(View.VISIBLE);
                userPerformedErrors = new UserPerformedErrors(lblErrorMsg);
                if (TextUtils.isEmpty(email)) {
                    userPerformedErrors.setErrorMessage("PLEASE ENTER EMAIL ID");
                    logInLoader.setVisibility(View.GONE);
                } else if (!isValidEmail(email.trim())) {
                    userPerformedErrors.setErrorMessage("PLEASE ENTER VALID EMAIL ID");
                    logInLoader.setVisibility(View.GONE);
                } else if (TextUtils.isEmpty(password)) {
                    userPerformedErrors.setErrorMessage("PLEASE ENTER PASSWORD");
                    logInLoader.setVisibility(View.GONE);
                } else {
                    lblErrorMsg.setText("");
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
                    String gcmDeviceId = sharedPreferences.getString("gcm_device_id", "");
                    ((IAuthenticationFacade) IOCContainer.getInstance()
                            .getObject(ServiceName.AUTHENTICATION_SERVICE, TAG)).signIn(email, password, gcmDeviceId);
                    ((IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, "FBLogin"))
                            .setOfflineResponse("profile_img_url", "");
                }
                break;
            case R.id.fb_loginbtn:
                ((UserAuthenticationActivity) mActivity).getFbLogin().initFBLogin();
                break;
            case R.id.gplus_loginbtn:
                ((UserAuthenticationActivity) mActivity).getGPlusLogin().signInWithGplus();
                break;
            case R.id.text_forgotpassword:
                FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.userauthentication_container, new ForgetPasswordFragment(), "FORGOT_PASSWORD");
                transaction.addToBackStack("FORGOT_PASSWORD");
                transaction.commit();
                break;

        }
    }

    private boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
}
