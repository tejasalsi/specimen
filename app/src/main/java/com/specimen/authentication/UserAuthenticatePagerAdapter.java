package com.specimen.authentication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.specimen.authentication.LogInFragment;
import com.specimen.authentication.SignUpFragment;


/**
 * Created by Tejas on 6/22/2015.
 */
public class UserAuthenticatePagerAdapter extends FragmentStatePagerAdapter {

    AppCompatActivity mActivity;

    public UserAuthenticatePagerAdapter(FragmentManager fm, AppCompatActivity activity) {
        super(fm);
        this.mActivity = activity;
    }

    @Override
    public Fragment getItem(int position) {

        if (position == 0)
            return new SignUpFragment();
        else
            return new LogInFragment();

    }

    @Override
    public int getCount() {
        return 2;
    }
}
