package com.specimen.authentication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.authentication.IAuthenticationFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.UserResponse;
import com.specimen.survey.SurveyActivity;
import com.specimen.widget.ProgressView;
import com.specimen.widget.UserPerformedErrors;

import java.util.regex.Pattern;

/**
 * Created by Tejas on 6/1/2015.
 * User can signup from here.
 */
public class SignUpFragment extends BaseFragment implements IResponseSubscribe {

    public static final String TAG = "SIGNUP";

    private EditText editName, editEmail, editPassword, editMobile;
    private Button facebookLogin, gPlusLogIn;
    private Button signupBtnImage;
    private ProgressView signUpLoader;
    private Button checkBoxTAndC;
    private UserPerformedErrors userPerformedErrors;
    TextView lblErrorMsg;
    AppCompatActivity mActivity;

    boolean isPrivacyChecked;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (AppCompatActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup signUpView = (ViewGroup) inflater.inflate(R.layout.fragment_signup, container, false);
        editName = (EditText) signUpView.findViewById(R.id.edit_signupname);
        editEmail = (EditText) signUpView.findViewById(R.id.edit_signupemail);
        editPassword = (EditText) signUpView.findViewById(R.id.edit_signuppassword);
        editMobile = (EditText) signUpView.findViewById(R.id.edit_signupmobile);
        signupBtnImage = (Button) signUpView.findViewById(R.id.image_loginBtn);
        facebookLogin = (Button) signUpView.findViewById(R.id.fb_loginbtn);
        gPlusLogIn = (Button) signUpView.findViewById(R.id.gplus_loginbtn);
        lblErrorMsg = (TextView) signUpView.findViewById(R.id.lblErrorMsg);
        checkBoxTAndC = (Button) signUpView.findViewById(R.id.checkBoxTC);
        signUpLoader = (ProgressView) signUpView.findViewById(R.id.progressBar);

        signupBtnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpLoader.setVisibility(View.VISIBLE);
                String name = editName.getText().toString();
                String email = editEmail.getText().toString();
                String password = editPassword.getText().toString();
                String mobile = editMobile.getText().toString();

                userPerformedErrors = new UserPerformedErrors(lblErrorMsg);
                if (TextUtils.isEmpty(name)) {
                    signUpLoader.setVisibility(View.GONE);
                    userPerformedErrors.setErrorMessage("PLEASE ENTER NAME");
                } else if (TextUtils.isEmpty(email)) {
                    signUpLoader.setVisibility(View.GONE);
                    userPerformedErrors.setErrorMessage("PLEASE ENTER EMAIL ID");
                } else if (!isValidEmail(email.trim())) {
                    signUpLoader.setVisibility(View.GONE);
                    lblErrorMsg.setText("PLEASE ENTER VALID EMAIL ID");
                } else if (TextUtils.isEmpty(password)) {
                    signUpLoader.setVisibility(View.GONE);
                    lblErrorMsg.setText("PLEASE ENTER PASSWORD");
                } else if (mobile.length() < 10 || mobile.length() > 17) {
                    signUpLoader.setVisibility(View.GONE);
                    lblErrorMsg.setText("PLEASE ENTER VALID MOBILE NUMBER");
                } else if (password.length() < 6) {
                    signUpLoader.setVisibility(View.GONE);
                    lblErrorMsg.setText("Password should not be less than six characters".toUpperCase());
                } else if (!isPrivacyChecked) {
                    signUpLoader.setVisibility(View.GONE);
                    lblErrorMsg.setText("Please accept the Privacy and Terms and Conditions".toUpperCase());
                } else {
                    lblErrorMsg.setText("");
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
                    String deviceId = sharedPreferences.getString("gcm_device_id", "");
                    ((IAuthenticationFacade) IOCContainer.getInstance()
                            .getObject(ServiceName.AUTHENTICATION_SERVICE, TAG))
                            .signUp(name, email, password, mobile, deviceId);
                }

            }
        });

        checkBoxTAndC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPrivacyChecked) {
                    isPrivacyChecked = false;
                    v.setBackgroundResource(R.drawable.checkbox_unchecked);
                } else {
                    isPrivacyChecked = true;
                    v.setBackgroundResource(R.drawable.checkbox_checked);
                }
            }
        });

        facebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((UserAuthenticationActivity) mActivity).getFbLogin().initFBLogin();
            }
        });

        gPlusLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((UserAuthenticationActivity) mActivity).getGPlusLogin().signInWithGplus();
            }
        });

        signUpView.findViewById(R.id.lblPrivacy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialogFragment = PrivacyAndTermsConditionsFragment.newInstance();
                dialogFragment.show(getChildFragmentManager(), "PRIVACY");
            }
        });

        signUpView.findViewById(R.id.lblTermsAndConditions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialogFragment = PrivacyAndTermsConditionsFragment.newInstance();
                dialogFragment.show(getChildFragmentManager(), "TnC");
            }
        });

        return signUpView;
    }

    private boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        signUpLoader.setVisibility(View.GONE);
        if (response instanceof UserResponse) {
            if (tag.equals(TAG) && response != null) {
                if (null != ((UserResponse) response).getData() && !"Failure".equals(response.getStatus())) {
                    Intent intent = new Intent(mActivity, SurveyActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    mActivity.finish();
                } else {
                    lblErrorMsg.setText("" + response.getMessage());
//                    Toast toast = new Toast(getActivity());
//                    userPerformedErrors = new UserPerformedErrors(toast);
//                    userPerformedErrors.setErrorMessageToast(getActivity(), ""+response.getMessage(), null, 0, Toast.LENGTH_SHORT);
                }
            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        signUpLoader.setVisibility(View.GONE);
//        Toast.makeText(getActivity(), "Something went wrong, try again later", Toast.LENGTH_SHORT).show();
        lblErrorMsg.setText("Something went wrong, try again later");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        } else {
            IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }
}
