package com.specimen.authentication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.authentication.IAuthenticationFacade;
import com.specimen.core.model.User;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.UserResponse;
import com.specimen.widget.ProgressView;

import java.util.regex.Pattern;

/**
 * Created by Tejas on 6/15/2015.
 */

public class ForgetPasswordFragment extends BaseFragment implements IResponseSubscribe {

    final String TAG = "ForgetPassword";
    ProgressView mProgressView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final ViewGroup forgotPasswordView = (ViewGroup) inflater.inflate(R.layout.fragment_forgotpassword, container, false);
        final TextView lblErrorMsg = (TextView) forgotPasswordView.findViewById(R.id.lblErrorMsg);
        mProgressView = (ProgressView) forgotPasswordView.findViewById(R.id.progressView);
        forgotPasswordView.findViewById(R.id.btnResetpassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = ((EditText) forgotPasswordView.findViewById(R.id.edit_resetpassword_email)).getText().toString();
                if (TextUtils.isEmpty(email) || !isValidEmail(email)) {
                    lblErrorMsg.setText("Please enter valid email id.".toUpperCase());
                } else {
                    mProgressView.setVisibility(View.VISIBLE);
                    ((IAuthenticationFacade) IOCContainer.getInstance().getObject(ServiceName.AUTHENTICATION_SERVICE, TAG)).resetPassword(email);
                }
            }
        });
        return forgotPasswordView;
    }

    private boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        mProgressView.setVisibility(View.GONE);
        if (!tag.equals(TAG))
            return;

        if (response instanceof UserResponse) {
            if ("Failure".equals(response.getStatus())) {
                Toast.makeText(mContext, "" + response.getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, "Password has been sent to your registered email id.", Toast.LENGTH_LONG).show();
                mContext.onBackPressed();
            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        Toast.makeText(mContext, "Server encountered an error...", Toast.LENGTH_SHORT).show();
    }
}
