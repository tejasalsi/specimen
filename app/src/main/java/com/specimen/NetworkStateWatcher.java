package com.specimen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.specimen.listener.INetworkStateListener;

import java.util.ArrayList;

public class NetworkStateWatcher extends BroadcastReceiver {

    private static NetworkStateWatcher instance = null;

    private ArrayList<INetworkStateListener> iNetworkStateListenerlist;
    public static int isConnectedToNetwork = -1;

    private NetworkStateWatcher() {
        iNetworkStateListenerlist = new ArrayList<>();
    }

    public static NetworkStateWatcher getInstance() {
        if (instance == null) {
            instance = new NetworkStateWatcher();
        }
        return instance;
    }

    public void register(INetworkStateListener stateListener) {
        iNetworkStateListenerlist.add(stateListener);
    }

    public void unregister(INetworkStateListener stateListener) {
        iNetworkStateListenerlist.remove(stateListener);
    }

    public void onReceive(Context context, Intent intent) {


        String action = intent.getAction();

        if (!action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {

            return;
        }

        boolean noConnectivity = intent.getBooleanExtra(
                ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
        if (noConnectivity) {
            isConnectedToNetwork = 0;
        } else {
            isConnectedToNetwork = 1;
        }


        for (INetworkStateListener networkStateListener : iNetworkStateListenerlist) {
            if (noConnectivity) {
                isConnectedToNetwork = 0;
                networkStateListener.onNetworkDisconnect();
            } else {
                isConnectedToNetwork = 1;
                networkStateListener.onNetworkConnect();
            }

        }


//        Toast.makeText(context, "Specimen..." + mState.toString(), Toast.LENGTH_SHORT).show();
//        showToast(context);
    }


}