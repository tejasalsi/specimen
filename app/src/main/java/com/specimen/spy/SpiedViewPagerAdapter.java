package com.specimen.spy;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by santoshbangera on 26/06/15.
 */
public class SpiedViewPagerAdapter extends FragmentPagerAdapter {
    private final ArrayList<Fragment> fragments;
    //    int PAGE_COUNT;
    Context context;
    private ArrayList<String> titles;

    public SpiedViewPagerAdapter(Context context, FragmentManager fm, ArrayList<String> tabTitles, ArrayList<Fragment> fragmentArrayList) {
        super(fm);
        this.context = context;
        this.titles = tabTitles;
        this.fragments = fragmentArrayList;
    }

    @Override
    public Fragment getItem(int position) {

        return fragments.get(position);
    }


    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return titles.get(position);
    }
}
