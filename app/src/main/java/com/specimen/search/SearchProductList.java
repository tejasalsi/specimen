package com.specimen.search;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.model.PageRequest;
import com.specimen.core.model.Product;
import com.specimen.core.model.Query;
import com.specimen.core.model.SuggestionEntity;
import com.specimen.core.model.TopCategory;
import com.specimen.core.product.IProductFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.ProductListResponse;
import com.specimen.product.ProductAdapter;
import com.specimen.search.events.EditSearchString;
import com.specimen.search.events.ISearchEvents;
import com.specimen.search.events.SearchProduct;
import com.specimen.search.footerEvent.IToggleState;
import com.specimen.search.footerEvent.ProductFooterEvent;
import com.specimen.util.BusProvider;
import com.specimen.widget.DividerItemDecoration;
import com.specimen.widget.ProgressView;
import com.specimen.widget.SpacesItemDecoration;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

/**
 * Created by Rohit on 9/30/2015.
 */
public class SearchProductList extends BaseFragment {

    private static final String TAG = "SearchProductList";

    private SearchTopPanel searchTopPanel;
    private FooterPanel footerPanel;

    private RecyclerView productRecyclerView;

    ProductAdapter productAdapter;

    private ProgressView progressView;

    private ArrayList<Product> productsList;

    String topCategoryId, subcategoryId;

    private TextView noResultView;

    private ProductFooterEvent productFooterEventState;
    private GridLayoutManager gridLayoutManager;

    private DividerItemDecoration listItemDecor;
    private SpacesItemDecoration gridDecor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_productlist, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);
    }


    private void init(View view) {
        noResultView = (TextView) view.findViewById(R.id.no_result_txt);
        productRecyclerView = (RecyclerView) view.findViewById(R.id.vertical_recycler_view);

        progressView = (ProgressView) view.findViewById(R.id.progressBar);

        listItemDecor = new DividerItemDecoration(getActivity());
        gridDecor = new SpacesItemDecoration(5);

        productsList = new ArrayList<>();

        topCategoryId = Query.getInstance().getTopCategoryId();
        subcategoryId = Query.getInstance().getSubCategoryId();

        searchTopPanel = new SearchTopPanel(mContext, topCategoryId, subcategoryId);
        searchTopPanel.init(view);

        productFooterEventState = new ProductFooterEvent();
        footerPanel = new FooterPanel(mContext, productFooterEventState, topCategoryId);
        footerPanel.init(view);
        footerPanel.setIsCallFromSearch(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setView();
    }

    private void setView() {

        searchTopPanel.setView(getArguments());

        setproductRecyclerView();

    }


    private void setproductRecyclerView() {

        productRecyclerView.setHasFixedSize(true);

        gridLayoutManager = new GridLayoutManager(mContext, 1, LinearLayoutManager.VERTICAL, false);
        productRecyclerView.setLayoutManager(gridLayoutManager);
        productAdapter = new ProductAdapter(mContext, R.layout.product_item_layout, productsList);

        productRecyclerView.setAdapter(productAdapter);

    }


    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        BusProvider.getInstance().register(this);
        searchTopPanel.onResume();
        footerPanel.onResume();
        fetchProduct();
    }


    private void fetchProduct() {
        if (subcategoryId != null) {
            ((IProductFacade) IOCContainer.getInstance().getObject(ServiceName.PRODUCT_SERVICE, TAG))
                    .getProduct(new PageRequest().setLimit(36), subcategoryId, TAG);
        } else if (topCategoryId != null) {

            ((IProductFacade) IOCContainer.getInstance().getObject(ServiceName.PRODUCT_SERVICE, TAG))
                    .getProduct(new PageRequest().setLimit(36), topCategoryId, TAG);

        } else {
            ((IProductFacade) IOCContainer.getInstance().getObject(ServiceName.PRODUCT_SERVICE, TAG))
                    .getProduct(new PageRequest().setLimit(36));
        }

        if (progressView != null) {

            progressView.setVisibility(View.VISIBLE);

        }

    }


    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (!tag.equals(TAG)) {
            return;
        }


        progressView.setVisibility(View.GONE);

        if (response instanceof ProductListResponse) {
            ProductListResponse productListResponse = (ProductListResponse) response;

            productsList.addAll(productListResponse.getProductResponseData().getProducts());
            productAdapter.notifyDataSetChanged();

            if (productsList.size() > 0) {
                noResultView.setVisibility(View.GONE);
                productRecyclerView.setVisibility(View.VISIBLE);
            } else {
                noResultView.setVisibility(View.VISIBLE);
                productRecyclerView.setVisibility(View.GONE);
            }

        }
    }


    @Override
    public void onFailure(Exception error) {
        Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
        Log.e(TAG, error.toString());
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();

        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
        BusProvider.getInstance().unregister(this);
        clearProductList();

        searchTopPanel.onPause();
        footerPanel.onPause();
    }

    private void clearProductList() {
        productsList.clear();
        productAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void upDateProductList(ISearchEvents update) {

        if (update instanceof SearchProduct) {
            clearProductList();

            topCategoryId = Query.getInstance().getTopCategoryId();
            subcategoryId = Query.getInstance().getSubCategoryId();

            fetchProduct();
        } else if (update instanceof EditSearchString) {
            mContext.onBackPressed();
        }
    }


    @Subscribe
    public void toggleProductView(Boolean isGrid) {
        if (!isGrid) {
            gridLayoutManager.setSpanCount(1);
            productAdapter.setResourceLayout(R.layout.product_item_layout);
            productRecyclerView.removeItemDecoration(gridDecor);
            productRecyclerView.addItemDecoration(listItemDecor);

        } else {
            gridLayoutManager.setSpanCount(2);
            productAdapter.setResourceLayout(R.layout.product_item_grid_layout);
            productRecyclerView.removeItemDecoration(listItemDecor);
            productRecyclerView.addItemDecoration(gridDecor);
        }
        productRecyclerView.setAdapter(productAdapter);

    }

    @Subscribe
    public void updateProduct(IToggleState integer) {
        productsList.clear();
        productAdapter.notifyDataSetChanged();
        fetchProduct();
    }

    @Subscribe
    public void toggleCategory(TopCategory topCategory) {
        topCategoryId = "" + topCategory.getId();
        Query.getInstance().setTopCategoryId(topCategoryId);
    }

}
