package com.specimen.search;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.core.model.Query;
import com.specimen.core.model.TopCategory;
import com.specimen.filter.FilterProductFragment;
import com.specimen.search.footerEvent.FilterToggle;
import com.specimen.search.footerEvent.IToggleState;
import com.specimen.search.footerEvent.ProductFooterEvent;
import com.specimen.search.footerEvent.SorterToggle;
import com.specimen.sort.SortFragment;
import com.specimen.util.BusProvider;
import com.squareup.otto.Subscribe;

/**
 * Created by Rohit on 10/7/2015.
 */
public class FooterPanel implements View.OnClickListener {

    public static boolean lastToggleState = false;

    String topCategoryId;
    Button productViewToggle;
    private ProductFooterEvent footerEvent;
    private RelativeLayout containFragmentHolder;
    private BaseActivity mContext;
    private Drawable gridIcon, listIcon;

    private boolean isCallFromSearch;

    private static View mView;

    public FooterPanel(BaseActivity mContext, ProductFooterEvent event, String topCategoryId) {
        this.footerEvent = event;
        this.topCategoryId = topCategoryId;
        this.mContext = mContext;
        lastToggleState = false;
    }

    public boolean isCallFromSearch() {
        return isCallFromSearch;
    }

    public void setIsCallFromSearch(boolean isCallFromSearch) {
        this.isCallFromSearch = isCallFromSearch;
    }

    public void init(View view) {
        mView = view;
        containFragmentHolder = (RelativeLayout) view.findViewById(R.id.fragment_container);
        view.findViewById(R.id.filterButton).setOnClickListener(this);
        view.findViewById(R.id.sortButton).setOnClickListener(this);
        productViewToggle = (Button) view.findViewById(R.id.productViewToggle);
        productViewToggle.setOnClickListener(this);

        gridIcon = view.getContext().getResources().getDrawable(R.drawable.ic_grid);
        listIcon = view.getContext().getResources().getDrawable(R.drawable.ic_list);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.filterButton:
                //TODO implement

                toggleFilter();

                break;


            case R.id.sortButton:
                //TODO implement

                toggleSorter();

                break;

            case R.id.productViewToggle:
                //TODO implement

                toggelListing();

                break;
        }

    }

    private void toggelListing() {

        footerEvent.isFilterClicked = !footerEvent.isFilterClicked;
        footerEvent.isSorterClicked = !footerEvent.isSorterClicked;

        if (footerEvent.isFilterClicked) {
            footerEvent.isFilterClicked = false;
            if(footerEvent.integerStack.size() > 0) {
                mContext.onBackPressed();
                footerEvent.integerStack.pop();
            }
        }

        if (footerEvent.isSorterClicked) {
            footerEvent.isSorterClicked = false;
            if(footerEvent.integerStack.size() > 0) {
                mContext.onBackPressed();
                footerEvent.integerStack.pop();
            }
        }

        footerEvent.isGridView = lastToggleState = !footerEvent.isGridView;

        if (!footerEvent.isGridView) {
            productViewToggle.setText("GRID");
            //productViewToggle.setCompoundDrawables(gridIcon, null, null, null);
            productViewToggle.setCompoundDrawablesWithIntrinsicBounds(gridIcon, null, null, null);
        } else {
            productViewToggle.setText("LIST");
            //productViewToggle.setCompoundDrawables(listIcon, null, null, null);
            productViewToggle.setCompoundDrawablesWithIntrinsicBounds(listIcon, null, null, null);

        }

        BusProvider.getInstance().post(footerEvent.isGridView);

    }

    private void toggleSorter() {
        footerEvent.isSorterClicked = !footerEvent.isSorterClicked;

        if (footerEvent.isFilterClicked) {
            footerEvent.isFilterClicked = false;
            mContext.onBackPressed();
            footerEvent.integerStack.pop();
        }

        if (footerEvent.isSorterClicked) {
            mContext.switchFragment(new SortFragment(), true, R.id.fragment_container, R.anim.slide_in_from_bottom, R.anim.slide_out_to_bottom);
            containFragmentHolder.setVisibility(View.VISIBLE);
            footerEvent.integerStack.push(1);
        } else {
            mContext.onBackPressed();
            footerEvent.integerStack.pop();
        }

    }

    private void toggleFilter() {
        footerEvent.isFilterClicked = !footerEvent.isFilterClicked;

        if (footerEvent.isSorterClicked) {
            footerEvent.isSorterClicked = false;
            mContext.onBackPressed();
            footerEvent.integerStack.pop();
        }

        if (footerEvent.isFilterClicked) {

            Query.getInstance().backupQuery();

            Bundle bundle = new Bundle();
            bundle.putString("topCategory", topCategoryId);
            bundle.putBoolean("isFromSearch", isCallFromSearch);
            FilterProductFragment fragment = new FilterProductFragment();
            fragment.setArguments(bundle);

            mContext.switchFragment(fragment, true, R.id.fragment_container, R.anim.slide_in_from_bottom, R.anim.slide_out_to_bottom);
            containFragmentHolder.setVisibility(View.VISIBLE);

            footerEvent.integerStack.push(1);

        } else {
            mContext.onBackPressed();
            footerEvent.integerStack.pop();
        }
    }


    public void onResume() {
        BusProvider.getInstance().register(this);
    }

    public void onPause() {
        BusProvider.getInstance().unregister(this);
    }

    /**
     * Handle call by respective fragment.
     *
     * @param iToggleState
     */
    @Subscribe
    public void toggleFooter(IToggleState iToggleState) {

        Log.d("TEST", "*******DEMO********");
        if (iToggleState instanceof FilterToggle) {
            toggleFilter();
        } else if (iToggleState instanceof SorterToggle) {
            toggleSorter();
        }

    }


    /**
     * Once you select category.
     *
     * @param topCategory
     */
    @Subscribe
    public void toggleCategory(TopCategory topCategory) {
        topCategoryId = "" + topCategory.getId();
        onBackPressed();
    }

    /**
     * It handle backpress button click
     *
     * @return false if not handle by footerPanel
     * true  else handle by this panel.
     */

    public boolean onBackPressed() {

        if (footerEvent.integerStack.size() > 0) {
            int top = footerEvent.integerStack.peek();
            if (top == 2) {
                toggleSorter();
            } else if (top == 1) {
                toggleFilter();
            }

            return true;
        }
        return false;
    }

    public static void hideFootePanel(boolean flag) {
        if (null != mView) {
            if (flag) mView.setVisibility(View.VISIBLE);
            else mView.setVisibility(View.GONE);
        }

    }


}



