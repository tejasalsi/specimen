package com.specimen.search;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.category.ICategoryFacade;
import com.specimen.core.filter.IFilterFacade;
import com.specimen.core.model.Category;
import com.specimen.core.model.Query;
import com.specimen.core.model.Subcategory;
import com.specimen.core.model.SuggestionEntity;
import com.specimen.core.model.TopCategory;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.CategoryListResponse;
import com.specimen.search.events.EditSearchString;
import com.specimen.search.events.SearchProduct;
import com.specimen.util.BusProvider;
import com.specimen.widget.SpacesItemDecoration;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rohit on 10/5/2015.
 */
public class SearchTopPanel implements IResponseSubscribe {

    private LinearLayout searchPanel;

    private RecyclerView searchTextRecyclerView;
    private RecyclerView categoryRecyclerView;
    private Context mContext;

    private ArrayList<Category> categoryList;

    SearchTextViewAdapter searchTextViewAdapter;
    SearchHeaderCategoryViewAdapter categoryAdapter;

    CategoryListResponse categoryListResponse;


    String topCategoryId, subcategoryId;

    private String TAG = "SearchProductList";
    private ArrayList<SuggestionEntity> entities;

    public SearchTopPanel(Context mContext, String topCategoryId, String subcategoryId) {
        this.mContext = mContext;
        this.topCategoryId = topCategoryId;
        this.subcategoryId = subcategoryId;
    }

    public void init(View view) {
        searchPanel = (LinearLayout) view.findViewById(R.id.searchPanel);
        searchTextRecyclerView = (RecyclerView) view.findViewById(R.id.search_text_recycler_view);
        categoryRecyclerView = (RecyclerView) view.findViewById(R.id.horizontal_recycler_view);
        categoryList = new ArrayList<>();
    }

    public void setView(Bundle bundle) {

        //TODO : Attach Adapter
        entities = bundle.getParcelableArrayList("search_lable");

        setSearchPanel();

        setSearchTextRecyclerView();

        setcategoryRecyclerView();


    }

    private void setSearchPanel() {
        searchPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusProvider.getInstance().post(new EditSearchString());
            }
        });
    }


    private void setSearchTextRecyclerView() {

        //TODO : Attach Adapter

        if (entities == null)
            return;

        searchTextRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        searchTextRecyclerView.setLayoutManager(layoutManager);

        searchTextViewAdapter = new SearchTextViewAdapter(mContext, entities);
        searchTextRecyclerView.setAdapter(searchTextViewAdapter);

        searchTextViewAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SuggestionEntity entity = (SuggestionEntity) v.getTag();
                updateSearchText(entity);
            }
        });

        searchTextRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusProvider.getInstance().post(new EditSearchString());
            }
        });

    }

    private void updateSearchText(SuggestionEntity entity) {

        if (topCategoryId != null && topCategoryId.equals(entity.getSug_id())) {
            topCategoryId = null;
        }

        if (subcategoryId != null && subcategoryId.equals(entity.getSug_id())) {
            subcategoryId = null;
        }

        if (entity.getString().equals(Query.getInstance().getSearchString())) {
            Query.getInstance().setSearchString(null);
        }

        /**
         * Please Do not optimise code :
         * Need to set State of Query Object.
         */
        Query.getInstance().setTopCategoryId(topCategoryId);
        Query.getInstance().setSubCategoryId(subcategoryId);

        entities.remove(entity);
        refreshSearchPanel();
    }


    private void setcategoryRecyclerView() {
        categoryRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        categoryRecyclerView.setLayoutManager(layoutManager);

        //TODO : Attach Adapter
        categoryAdapter = new SearchHeaderCategoryViewAdapter(mContext, categoryList);
        categoryRecyclerView.setAdapter(categoryAdapter);
//        categoryRecyclerView.addItemDecoration(new SpacesItemDecoration(5));
        RecyclerView.ItemDecoration decor = new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.right = 5;
                outRect.top = 5;
            }

        };
        categoryRecyclerView.addItemDecoration(decor);
        categoryAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Category category = (Category) v.getTag();
                updateCategory(category);
            }
        });
    }

    private void updateCategory(Category category) {

        SuggestionEntity suggestionEntity;


        if (topCategoryId == null) {

            topCategoryId = String.valueOf(category.getId());

            Query.getInstance().setTopCategoryId(topCategoryId);

            suggestionEntity = getEntity(category.getName(), topCategoryId);
            entities.add(suggestionEntity);

            /**
             * onChange Of top Category Filter must be refresh. Hence need to clear Cache.
             */

            ((IFilterFacade) IOCContainer.getInstance().getObject(ServiceName.FILTER_SERVICE, "")).clearFilterResponse();

        } else if (subcategoryId == null) {

            subcategoryId = String.valueOf(category.getId());
            Query.getInstance().setSubCategoryId(subcategoryId);
            suggestionEntity = getEntity(category.getName(), subcategoryId);
            entities.add(suggestionEntity);

        }


        refreshSearchPanel();
    }

    private void refreshSearchPanel() {

        searchTextViewAdapter.notifyDataSetChanged();

        if (entities.size() == 0) {
            searchPanel.setVisibility(View.GONE);
        } else {
            searchPanel.setVisibility(View.VISIBLE);
        }


        categoryList.clear();
        categoryAdapter.notifyDataSetChanged();

        fetchCategory();

        BusProvider.getInstance().post(new SearchProduct());
    }

    @NonNull
    private SuggestionEntity getEntity(String string, String sug_id) {
        SuggestionEntity subEntity = new SuggestionEntity();
        subEntity.setString(string);
        subEntity.setSug_id(sug_id);
        return subEntity;
    }


    private void updatecategoryRecyclerView() {
        //TODO : Update categories.
        if (topCategoryId == null) {

            categoryList.addAll(categoryListResponse.getData());

        } else if (subcategoryId == null) {

            ArrayList<TopCategory> topCategory = (ArrayList) categoryListResponse.getData();

            for (TopCategory temp_category : topCategory) {
                //TODO: Update List
                if (String.valueOf(temp_category.getId()).equals(topCategoryId)) {
                    categoryList.clear();


                    List<Subcategory> subcategory = temp_category.getSubcategory();
                    if (subcategory != null)
                        categoryList.addAll(subcategory);
                    categoryAdapter.notifyDataSetChanged();
                }
            }
        }
    }


    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (!tag.equals(TAG)) {
            return;
        }

        if (response instanceof CategoryListResponse) {
            categoryListResponse = (CategoryListResponse) response;
            updatecategoryRecyclerView();
        }
    }

    @Override
    public void onFailure(Exception error) {
        Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_LONG).show();
    }

    private void fetchCategory() {

        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);

        if (subcategoryId == null) {

            categoryRecyclerView.setVisibility(View.VISIBLE);
            ((ICategoryFacade) IOCContainer.getInstance().getObject(ServiceName.CATEGORY_SERVICE, TAG)).getCategoryList();

        } else {

            categoryRecyclerView.setVisibility(View.GONE);

        }


    }

    public void onResume() {
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        BusProvider.getInstance().register(this);
        fetchCategory();
    }


    public void onPause() {
        categoryAdapter.notifyDataSetChanged();
        categoryList.clear();
        BusProvider.getInstance().unregister(this);
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Subscribe
    public void toggleCategory(TopCategory topCategory) {
        updateCategory(topCategory);
    }
}
