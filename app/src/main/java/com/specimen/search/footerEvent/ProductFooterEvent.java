package com.specimen.search.footerEvent;

import java.util.Stack;

/**
 * Created by Rohit on 10/7/2015.
 */
public class ProductFooterEvent {

    public boolean isFilterClicked;
    public boolean isSorterClicked;

    public boolean isGridView;

    public Stack<Integer> integerStack=new Stack<>();
}
