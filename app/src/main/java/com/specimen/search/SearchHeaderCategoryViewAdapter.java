package com.specimen.search;

import java.util.List;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.specimen.R;
import com.specimen.core.model.Category;


public class SearchHeaderCategoryViewAdapter extends RecyclerView.Adapter<SearchHeaderCategoryViewAdapter.SearchHeaderCategoryViewViewHolder> implements View.OnClickListener {

    private List<Category> mDataSet;

    private Context context;
    private LayoutInflater layoutInflater;
    private View.OnClickListener onClickListener;

    public SearchHeaderCategoryViewAdapter(Context context, List<Category> input) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.mDataSet = input;
        this.onClickListener = this;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public SearchHeaderCategoryViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.searched_strings_view, null);
        SearchHeaderCategoryViewViewHolder holder = new SearchHeaderCategoryViewViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SearchHeaderCategoryViewViewHolder holder, int position) {

        holder.setView(mDataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    @Override
    public void onClick(View v) {

    }

    public class SearchHeaderCategoryViewViewHolder extends RecyclerView.ViewHolder {

        private Button btnSearchString;

        public SearchHeaderCategoryViewViewHolder(View view) {
            super(view);
            btnSearchString = (Button) view.findViewById(R.id.btnSearchString);
        }

        public void setView(final Category object) {
            btnSearchString.setText(object.getName());
            btnSearchString.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setTag(object);
                    onClickListener.onClick(v);
                }
            });

        }
    }

}
