package com.specimen;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

//import com.newrelic.agent.android.NewRelic;
import com.specimen.authentication.AppIntroductionActivity;
import com.specimen.widget.ProgressView;

/**
 * Created by Tejas on 6/1/2015.
 * This will be the Splash Screen.
 */
public class SplashActivity extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
    private ProgressView progressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        checkDP();


        relicClientInitialization();
        progressView = (ProgressView) findViewById(R.id.progressView);
        progressView.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                progressView.setVisibility(View.GONE);
                Intent i = new Intent(SplashActivity.this, AppIntroductionActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onBackPressed() {

    }

    void relicClientInitialization() {
//        NewRelic.withApplicationToken(
//                "AA6093c8f7bb51b464fc57bde7fbe1d8806f7bdd60"
//        ).start(this.getApplication());
    }

    public int checkDP() {
        switch (getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                Toast.makeText(SplashActivity.this, "DENSITY_LOW", Toast.LENGTH_SHORT).show();
                return DisplayMetrics.DENSITY_LOW;
            case DisplayMetrics.DENSITY_MEDIUM:
                Toast.makeText(SplashActivity.this, "DENSITY_MEDIUM", Toast.LENGTH_SHORT).show();
                return DisplayMetrics.DENSITY_MEDIUM;
            case DisplayMetrics.DENSITY_HIGH:
                Toast.makeText(SplashActivity.this, "DENSITY_HIGH", Toast.LENGTH_SHORT).show();
                return DisplayMetrics.DENSITY_HIGH;
            case DisplayMetrics.DENSITY_XHIGH:
                Toast.makeText(SplashActivity.this, "DENSITY_XHIGH", Toast.LENGTH_SHORT).show();
                return DisplayMetrics.DENSITY_XHIGH;
            case DisplayMetrics.DENSITY_XXHIGH:
                Toast.makeText(SplashActivity.this, "DENSITY_XXHIGH", Toast.LENGTH_SHORT).show();
                return DisplayMetrics.DENSITY_XXHIGH;
            case DisplayMetrics.DENSITY_XXXHIGH:
                Toast.makeText(SplashActivity.this, "DENSITY_XXXHIGH", Toast.LENGTH_SHORT).show();
                return DisplayMetrics.DENSITY_XXXHIGH;
        }

        return 0;
    }
}
