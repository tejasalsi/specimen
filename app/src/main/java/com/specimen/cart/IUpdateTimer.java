package com.specimen.cart;

/**
 * Created by root on 7/10/15.
 */
public interface IUpdateTimer {
    void updateTimer(long updatedTimerValue);
}
