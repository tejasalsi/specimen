package com.specimen.cart;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.renderscript.Type;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.R;
import com.specimen.core.model.CartMetadata;
import com.specimen.core.model.CartProduct;
import com.specimen.cart.IUpdateTimer;
import com.specimen.product.productdetails.ProductDetailsActivity;
import com.specimen.util.ImageUtils;
import com.specimen.util.NumberFormatsUtil;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Swapnil on 8/10/2015.
 */
public class CartProductsAdapter extends HeaderRecyclerViewAdapterV2 {

    private List<CartProduct> productItemList;
    private Context mContext;
    private String mTrunkDiscount, mTotalTax, mTotalValue,mCouponDiscount,mSubTotal,mTrunkTotal;    // mTotalSaving;
    private String isCouponApplied;
    private static Timer mTimer = null;
    ArrayList<IUpdateTimer> iUpdateTimer;

    public void setOnCartClickListners(OnCartComponentsClickListener onCartClickListners) {
        this.onCartClickListners = onCartClickListners;
    }

    private OnCartComponentsClickListener onCartClickListners;
    private DisplayMetrics matrix;

    public CartProductsAdapter(Context context, List<CartProduct> feedItemList, CartMetadata cartMetadata) {
        this.productItemList = feedItemList;
        this.mContext = context;
        this.mTrunkDiscount = cartMetadata.getTotal_discount();
        this.mTotalTax = cartMetadata.getTotal_tax();
        this.mTotalValue = cartMetadata.getTotal();
        //this.mTotalSaving = cartMetadata.getTotalSaving();
        this.mCouponDiscount = cartMetadata.getCoupon_code();
        this.isCouponApplied = cartMetadata.getCoupon_code();
        this.mSubTotal = cartMetadata.getSubtotal();
        this.mTotalTax = cartMetadata.getTotal_tax();
        this.mTrunkTotal = cartMetadata.getTrunk_total();
        mTotalTax = cartMetadata.getTotal_tax();
        this.matrix = mContext.getResources().getDisplayMetrics();
        iUpdateTimer = new ArrayList<>();
        useTimer();
    }

    @Override
    public boolean useHeader() {
        return false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_row_header, null);

        return new CustomHeaderView(view);
    }

    @Override
    public void onBindHeaderView(RecyclerView.ViewHolder holder, int position) {
        final CustomHeaderView mHeaderView = (CustomHeaderView) holder;
        mHeaderView.tvItemTotal.setText(mTotalValue);
        mHeaderView.tvItemCount.setText("ITEMS (" + productItemList.size() + ")");
    }

    @Override
    public boolean useFooter() {
        return true;
    }

    @Override
    public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_row_footer, null);

        return new CustomFooterView(view);
    }

    @Override
    public void onBindFooterView(RecyclerView.ViewHolder holder, int position) {
        final CustomFooterView mFooterView = (CustomFooterView) holder;
        mFooterView.tvTotalPayableText.setText(mContext.getResources().getString(R.string.rupee_symbole) + " " + mTotalValue);

        mFooterView.couponDiscountText.setText((isCouponApplied.isEmpty()) ? "NOT APPLIED" : "- "+mContext.getString(R.string.rupee_symbole) + " " +mCouponDiscount);

        mFooterView.btnApplyPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCartClickListners.onApplyPromocode(mFooterView.edtPromoCode.getText().toString());
            }
        });
//        mFooterView.btnCheckOut.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onCartClickListners.checkout();
//            }
//        });
         mFooterView.tvTrunkTotal.setText(mContext.getString(R.string.rupee_symbole) + " " + mTrunkTotal);
        mFooterView.tvTaxCollected.setText(mContext.getString(R.string.rupee_symbole) + " " + mTotalTax);

        if (mTrunkDiscount.isEmpty()) {
            mFooterView.llTrunkDiscount.setVisibility(View.GONE);
            mFooterView.llSubTotal.setVisibility(View.GONE);

        } else {
            mFooterView.tvTrunkDiscountText.setText("- "+mContext.getString(R.string.rupee_symbole) + " " + mTrunkDiscount);
            mFooterView.tvSubtotal.setText(mContext.getString(R.string.rupee_symbole)  + " " + mSubTotal);
        }


    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_row_items, null);

        return new CustomViewHolder(view);
    }

    @Override
    public void onBindBasicItemView(RecyclerView.ViewHolder holder, final int position) {
        final CustomViewHolder mHolder = (CustomViewHolder) holder;
        final CartProduct productItem = productItemList.get(position);
        mHolder.setCurrentItem(productItem);
        mHolder.setIsDealProduct();
        mHolder.tvProductTitle.setText(productItem.getName().toUpperCase());

        mHolder.tvProductSize.setText("Size : " + productItem.getSizes().get(0).getLabel());
        mHolder.tvProductQty.setText(mContext.getString(R.string.cart_qty) + " " + productItem.getQuantity());
//        mHolder.tvItemPrice.setText(mContext.getString(R.string.rupee_symbole) + productItem.getPrice());
//        mHolder.tvItemTotal.setText(productItem.getQuantity() + " " + mContext.getString(R.string.cart_item_total) + " " + productItem.getItemTotal());
//        mHolder.tvTax.setText(mContext.getString(R.string.cart_tax) + " : " + productItem.getTax());
//        mHolder.tvDiscount.setText(mContext.getString(R.string.cart_discount) + " : " + productItem.getDiscount());
//        mHolder.tvProductTotal.setText("TOTAL " + mContext.getResources().getString(R.string.rupee_symbole) + productItem.getTotal() + "");
        if (null != productItem.getSpecialprice()) {
            mHolder.tvProductTotal.setText(mContext.getResources().getString(R.string.rupee_symbole) + " " + productItem.getSpecialprice());
            if (productItem.getDiscount() == null) {
                mHolder.cartProductDiscount.setText("");
            } else {
                mHolder.cartProductDiscount.setText("(" + productItem.getDiscount() + "% discounted)");
            }
        } else {
            mHolder.tvProductTotal.setText(mContext.getResources().getString(R.string.rupee_symbole) + " " + productItem.getTotal());
            mHolder.cartProductDiscount.setText("");
        }

        mHolder.tvProductQty.setTag(position);
        mHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(mContext)
                        .setTitle("Delete product")
                        .setMessage("Are you sure you want to delete product from cart?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                onCartClickListners.onProductDelete(productItem.getId(), productItem.getSizes().get(0).getChild_product_id());
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
//                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });
        mHolder.ivQtyAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String qty = "" + ((productItem.getQuantity()) + 1);
                if (isProductAvailable(productItem.getSizes().get(0).getStock_count(), Integer.parseInt(qty))) {
                    onCartClickListners.onQuantityUpdateListener(productItem.getId(), qty, productItem.getSizes().get(0).getChild_product_id());
                } else {
                    Toast.makeText(mContext, "Product not available", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mHolder.ivQtyMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String qty = "" + (productItem.getQuantity() - 1);
             /*   if (isProductAvailable(productItem.getSizes().get(0).getStock_count(), Integer.parseInt(qty))) {
                    onCartClickListners.onQuantityUpdateListener(productItem.getId(), qty, productItem.getSizes().get(0).getChild_product_id());
                } else {
                    Toast.makeText(mContext, "Product quantity cannot be zero", Toast.LENGTH_SHORT).show();
                }*/

               if((productItem.getQuantity() - 1) > 0) {
                 //  if ((productItem.getQuantity() - 1) > productItem.getSizes().get(0).getStock_count()) {
                 //      Toast.makeText(mContext, "Product quantity is greater than available stock", Toast.LENGTH_SHORT).show();
                 //  } else {
                       onCartClickListners.onQuantityUpdateListener(productItem.getId(), qty, productItem.getSizes().get(0).getChild_product_id());

                  // }
               }
                else
               {
                   Toast.makeText(mContext, "Product quantity cannot be zero", Toast.LENGTH_SHORT).show();

               }
            }
        });

        if (productItem.getImage() != null && productItem.getImage().size() > 0) {
            Picasso.with(mContext)
                    .load(ImageUtils.getProductImageUrl(productItem.getImage().get(0), matrix))
                    .placeholder(R.drawable.cart_img_placeholder)
                    .resize(200, 240)
                    .into(mHolder.ivProductImage);
        } else {
            Picasso.with(mContext)
                    .load(R.drawable.cart_img_placeholder)
                    .resize(200, 240)
                    .into(mHolder.ivProductImage);
        }
        mHolder.ivProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ProductDetailsActivity.class);
                intent.putExtra("PRODUCT_ID", productItem.getId());
                v.getContext().startActivity(intent);
            }
        });

        if (mHolder.getIsDealProduct()) {
            iUpdateTimer.add(mHolder);
        } else {
            if (iUpdateTimer.contains(mHolder)) {
                iUpdateTimer.remove(mHolder);
            }
        }

        if (productItem.getSizes().get(0).getStock_count() == 0) {
            mHolder.tvProductOutOfStock.setVisibility(View.VISIBLE);
            mHolder.tvProductOutOfStock.setText("OUT OF STOCK");
        }
        else if(productItem.getQuantity() > productItem.getSizes().get(0).getStock_count())
        {
            mHolder.tvProductOutOfStock.setVisibility(View.VISIBLE);
            mHolder.tvProductOutOfStock.setText(""+productItem.getSizes().get(0).getStock_count()+ " are left in stock.");
        }
        else {
            mHolder.tvProductOutOfStock.setVisibility(View.INVISIBLE);
        }



    }

    public boolean isProductAvailable(int stockCount, int curruntQuantity) {
        return curruntQuantity > 0 && curruntQuantity <= stockCount;
    }

    @Override
    public int getBasicItemCount() {
        return productItemList.size();
    }

    @Override
    public int getBasicItemType(int position) {
        return position;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements IUpdateTimer {
        protected ImageView ivProductImage;
        protected TextView tvProductTitle;
        protected TextView tvProductSize;
        protected TextView tvProductQty;
        protected TextView tvProductOutOfStock;
        //        protected TextView tvItemPrice;
//        protected TextView tvItemTotal;
//        protected TextView tvTax;
        //        protected TextView tvDiscount;
        protected TextView tvProductTotal;
        protected TextView cartProductDiscount;
        protected Button ivQtyAdd;
        protected Button ivQtyMinus;
        protected ImageView ivDelete;
        protected TextView tvCountDownTimer;
        private CartProduct currentItem;
        private boolean isDealProduct;


        public CustomViewHolder(View view) {
            super(view);
            this.ivProductImage = (ImageView) view.findViewById(R.id.cartproductImg);
            this.tvProductTitle = (TextView) view.findViewById(R.id.cartproductTitle);
            this.tvProductSize = (TextView) view.findViewById(R.id.cartproductSize);
            this.tvProductQty = (TextView) view.findViewById(R.id.cartProductQty);
            this.cartProductDiscount = (TextView) view.findViewById(R.id.cartProductDiscount);
            this.tvProductOutOfStock = (TextView) view.findViewById(R.id.tvProductOutOfStock);
//            this.tvItemPrice = (TextView) view.findViewById(R.id.cartProductPrice);
//            this.tvItemTotal = (TextView) view.findViewById(R.id.cartProductQtyPrice);
//            this.tvTax = (TextView) view.findViewById(R.id.cartProductTax);
//            this.tvDiscount = (TextView) view.findViewById(R.id.cartPorductDiscount);
            this.tvProductTotal = (TextView) view.findViewById(R.id.cartProductTotal);
            this.ivQtyAdd = (Button) view.findViewById(R.id.ivQtyPlus);
            this.ivQtyMinus = (Button) view.findViewById(R.id.ivQtyMinus);
            this.ivDelete = (ImageView) view.findViewById(R.id.ivDelete);
            this.tvCountDownTimer = (TextView) view.findViewById(R.id.tvCountDownTimer);
            this.tvCountDownTimer.setTypeface(null,Typeface.BOLD);
        }

        public void setIsDealProduct() {
            String constantTimeString = "00:00:00";
            if (currentItem == null || currentItem.getRemaining_time() == null || currentItem.getRemaining_time().isEmpty() || currentItem.getRemaining_time().equals(constantTimeString)) {
                isDealProduct = false;
            } else {
                isDealProduct = true;
            }

        }

        public boolean getIsDealProduct() {
            return isDealProduct;
        }

        @Override
        public void updateTimer(long updatedTimerValue) {
            final String constantTimeString = "00:00:00";
            final String[] remainingTime = (!TextUtils.isEmpty(currentItem.getRemaining_time()) ?
                    currentItem.getRemaining_time().split(":") : constantTimeString.split(":"));
            long hoursInmillis = Long.parseLong(remainingTime[0]) * (60 * 60 * 1000);
            long minsInMillis = Long.parseLong(remainingTime[1]) * (60 * 1000);
            long secsInMillis = Long.parseLong(remainingTime[2]) * 1000;

            final long dealTimeInMillis = hoursInmillis + minsInMillis + secsInMillis;
            final long updatedTimeInMillis = dealTimeInMillis - (updatedTimerValue * 1000);

            if (NumberFormatsUtil.isNegative(updatedTimeInMillis)) {
                //Activity need to hit server for update
                if (getIsDealProduct()) {
                    cancleTimer();
                    onCartClickListners.onRefreshCart();
                }
            } else {
                long sec = (updatedTimeInMillis / 1000) % 60;
                long hr = (updatedTimeInMillis / (1000 * 60 * 60));
                long min = (updatedTimeInMillis / (1000 * 60) % 60);
                String strMin = "" + min, strSec = "" + sec;
                if (sec < 10) {
                    strSec = "0" + strSec;
                }
                if (min < 10) {
                    strMin = "0" + strMin;
                }
                tvCountDownTimer.setText("NEXT PRICE IN " + strMin + ":" + strSec);
            }
        }

        public void setCurrentItem(CartProduct currentItem) {
            this.currentItem = currentItem;
        }
    }

    public class CustomFooterView extends RecyclerView.ViewHolder {

        protected TextView tvTaxCollected;
        protected TextView tvTrunkDiscountText;
        protected TextView couponDiscountText;
        protected TextView tvTotalPayableText;
        //        protected Button btnCheckOut;
        protected EditText edtPromoCode;
        protected Button btnApplyPromo;
        protected LinearLayout llTrunkDiscount;
        protected LinearLayout llSubTotal;
        protected TextView totalTxt;
        protected TextView tvTrunkTotal;
        protected TextView tvSubtotal;

        public CustomFooterView(View itemView) {
            super(itemView);
            tvTrunkTotal = (TextView)itemView.findViewById(R.id.trunkTotal);

            tvTrunkDiscountText = (TextView) itemView.findViewById(R.id.trunkDiscount);
            tvSubtotal = (TextView)itemView.findViewById(R.id.tvSubTotal);
            tvTaxCollected = (TextView)itemView.findViewById(R.id.taxCollected);
            couponDiscountText = (TextView) itemView.findViewById(R.id.couponDiscountText);
            tvTotalPayableText = (TextView) itemView.findViewById(R.id.totalPayableText);
//            btnCheckOut = (Button) itemView.findViewById(R.id.btnCheckout);
            edtPromoCode = (EditText) itemView.findViewById(R.id.promocodeEdit);
            btnApplyPromo = (Button) itemView.findViewById(R.id.btnApplyCode);
            llTrunkDiscount = (LinearLayout) itemView.findViewById(R.id.llTrunklDiscount);
            tvTotalPayableText.setTypeface(null, Typeface.BOLD);
            totalTxt = (TextView)itemView.findViewById(R.id.txtview_total);
            totalTxt.setTypeface(null, Typeface.BOLD);
            llSubTotal = (LinearLayout)itemView.findViewById(R.id.llSubTotal);

        }
    }

    public class CustomHeaderView extends RecyclerView.ViewHolder {

        protected TextView tvItemCount;
        protected TextView tvItemTotal;

        public CustomHeaderView(View itemView) {
            super(itemView);

            tvItemCount = (TextView) itemView.findViewById(R.id.tvHeaderCount);
            tvItemTotal = (TextView) itemView.findViewById(R.id.tvHeaderTotal);

        }
    }

    /**
     *
     */
    private Handler messageHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            CartActivity.currentTimeInMillis++;
            for (IUpdateTimer iUpdate : iUpdateTimer) {
                iUpdate.updateTimer(CartActivity.currentTimeInMillis);
            }
        }
    };

    /**
     *
     */
    private void useTimer() {
        cancleTimer();
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                messageHandler.sendEmptyMessage(0);
            }
        }, 0, 1 * 1000);
    }

    public void cancleTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            CartActivity.currentTimeInMillis = 0;
        }
    }

}
