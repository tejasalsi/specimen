package com.specimen.cart;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.citruspayment.paymentaddress.PaymentConfirmDeliveryAddressActivity;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.cart.ICartFacade;
import com.specimen.core.model.Product;
import com.specimen.core.model.ProductSizesEntity;
import com.specimen.core.model.Query;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.CartDetailsResponse;
import com.specimen.home.HomeActivity;
import com.specimen.util.material_design_utils.HidingScrollListener;
import com.specimen.util.material_design_utils.Utils;
import com.specimen.widget.ProgressView;

import java.util.List;

public class CartActivity extends BaseFragment implements OnCartComponentsClickListener {

    public static int currentTimeInMillis;

    private final String TAG = "CART_ACTIVITY";
    private final String PROMO_CODE_TAG = "PROMO_CODE";

    private final String TAG_CART_STOCK = "CART_STOCK";

    private RecyclerView mListViewProducts;
    private CartProductsAdapter mCartAdapter;
    private TextView tvHeaderItemCount, tvHeaderTotal;
    private LinearLayoutManager mLayoutManager;
    private ProgressView progressView;
    private CartDetailsResponse cartResponse;
    private Button btnCheckout;
    private LinearLayout mToolbarContainer;
    private int mToolbarHeight;
    private View view;
    private boolean isCartItemQuantityEmpty;
    private boolean isBuyNowButtonClicked;
    private boolean isCheckOutClicked = false;
    private boolean isPromocodeAppliedClicked;
    private String productName;
    private boolean isPromocodeApplied = false;

//    @Override
//    protected void callAfterInitialization() {
//        getComponents();
//    }
//
//    @Override
//    protected int getContentView() {
//        return R.layout.activity_cart;
//    }
//
//    @Override
//    protected View getCurrentSelectedTab() {
//        return trunk_tab;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.activity_cart, container, false);
        mToolbarContainer = (LinearLayout) view.findViewById(R.id.toolbarContainer);
       Toolbar toolbar = (Toolbar)mToolbarContainer.findViewById(R.id.cart_toolbar);
        toolbar.setBackgroundResource(R.drawable.toolbar_custom_bg);
        mToolbarHeight = Utils.getToolbarHeight(mContext);
     /*   view.findViewById(R.id.btnCheckout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkout();
            }
        });*/
        progressView = (ProgressView) view.findViewById(R.id.progressBar);
        btnCheckout = (Button) view.findViewById(R.id.btnCheckout);
        btnCheckout.setEnabled(false);
        isCheckOutClicked = false;
        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, TAG_CART_STOCK)).getCartStock();
               // progressView.setVisibility(View.VISIBLE);
                cancelTimer();
                onRefreshCart();
                isBuyNowButtonClicked = true;
                isCheckOutClicked = true;
                //btnCheckout.setEnabled(false);
               // btnCheckout.setFocusable(false);
               // checkout();
            }
        });
        getComponents(view);
        return view;
    }

    // Initialize all components in activity
    private void getComponents(View view) {
        mListViewProducts = (RecyclerView) view.findViewById(R.id.rvProducts);
        tvHeaderItemCount = (TextView) view.findViewById(R.id.tvHeaderCount);
        tvHeaderItemCount.setTypeface(null, Typeface.BOLD);

        tvHeaderTotal = (TextView) view.findViewById(R.id.tvHeaderTotal);
        tvHeaderTotal.setTypeface(null, Typeface.BOLD);
        mListViewProducts.setHasFixedSize(true);
        int paddingTop = Utils.getToolbarHeight(mContext) + Utils.getTabsHeight(mContext) - 5;
        mListViewProducts.setPadding(mListViewProducts.getPaddingLeft(), paddingTop, mListViewProducts.getPaddingRight(), mListViewProducts.getPaddingBottom());
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mListViewProducts.setLayoutManager(mLayoutManager);

        mListViewProducts.addOnScrollListener(new HidingScrollListener(mContext) {

            @Override
            public void onMoved(int distance) {
                mToolbarContainer.setTranslationY(-distance);
            }

            @Override
            public void onShow() {
                mToolbarContainer.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void onHide() {
                mToolbarContainer.animate().translationY(-mToolbarHeight).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, TAG)).getCart();
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        cancelTimer();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        progressView.setVisibility(View.GONE);

        try {

            if (tag.equals(TAG) || tag.equals(PROMO_CODE_TAG)) {
                if (response instanceof CartDetailsResponse) {

                    if (tag.equals(PROMO_CODE_TAG)) {
                        isPromocodeAppliedClicked = false;
                        if(!cartResponse.getData().getMetadata().getCoupon_code().isEmpty())
                        {
                            Toast.makeText(mContext, "Coupon code is already applied.", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(mContext, "" + response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    cartResponse = (CartDetailsResponse) response;

                    MainActivity.recursiveApiCall();

                    if (null != cartResponse.getData() && null != cartResponse.getData().getProducts() && cartResponse.getData().getProducts().size() != 0) {

                        view.findViewById(R.id.rvProducts).setVisibility(View.VISIBLE);
                        view.findViewById(R.id.linearNoProductFoundView).setVisibility(View.GONE);

                        view.findViewById(R.id.btnContinueShopping).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                MainActivity.pressHomeTabExplicitely();
                            }
                        });

                        mListViewProducts.setVisibility(View.VISIBLE);
                        view.findViewById(R.id.linearCheckout).setVisibility(View.VISIBLE);
                        mCartAdapter = new CartProductsAdapter(getActivity(), cartResponse.getData().getProducts(), cartResponse.getData().getMetadata());

                        int lastFirstVisiblePosition = ((LinearLayoutManager) mListViewProducts.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                        mCartAdapter.setOnCartClickListners(this);
                        mListViewProducts.setAdapter(mCartAdapter);
                        mLayoutManager.scrollToPositionWithOffset(lastFirstVisiblePosition, 20);
                       isCartItemQuantityEmpty = false;
                        if (null != cartResponse.getData().getProducts()) {

                            for (Product product : cartResponse.getData().getProducts()) {
                                List<ProductSizesEntity> productSizes = product.getSizes();
                                int quantity = product.getQuantity();
                             //   for (int i = 0 ;i < productSizes.size(); i++)
                               // {

                                    if (productSizes.get(0).getStock_count() == 0 || quantity > productSizes.get(0).getStock_count()) {
                                        isCartItemQuantityEmpty = true;
                                        break;
                                    }
                             //   }

                            }
                        }

                        tvHeaderTotal.setText("TOTAL: " + getResources().getString(R.string.rupee_symbole) + " " + cartResponse.getData().getMetadata().getTotal());

                        if (1 < cartResponse.getData().getProducts().size()) {
                            tvHeaderItemCount.setText("ITEMS (" + cartResponse.getData().getProducts().size() + ")");
                        } else {
                            tvHeaderItemCount.setText("ITEM (" + cartResponse.getData().getProducts().size() + ")");
                        }

                        MainActivity.setCartCount(cartResponse.getData().getProducts().size());

                        if (isBuyNowButtonClicked) {
                            if (isCartItemQuantityEmpty) {
                                Toast.makeText(mContext, "Any of the product added in your trunk is out of stock. Please delete it from trunk.", Toast.LENGTH_SHORT).show();
                                btnCheckout.setEnabled(false);
                                btnCheckout.setFocusable(false);
                                isCheckOutClicked = false;
                            } else {
                                if(isCheckOutClicked)
                                {
                                    checkout();
                                }
                                btnCheckout.setEnabled(true);
                                btnCheckout.setFocusable(true);
                            }

                            //checkout();

                        } else {
                            btnCheckout.setEnabled(true);
                            btnCheckout.setFocusable(true);
                        }

                    } else {
                        isCartItemQuantityEmpty = false;

                        view.findViewById(R.id.rvProducts).setVisibility(View.GONE);
                        view.findViewById(R.id.linearNoProductFoundView).setVisibility(View.VISIBLE);

                        view.findViewById(R.id.btnContinueShopping).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(mContext, MainActivity.class);
                                intent.putExtra("tag", MainActivity.SHOP);
                                startActivity(intent);
                            }
                        });

//                    Toast.makeText(mContext, "No product found in trunk. Continue Shopping", Toast.LENGTH_SHORT).show();
                        view.findViewById(R.id.topBar).setVisibility(View.GONE);
                        if (mCartAdapter != null) {
                            mCartAdapter.notifyDataSetChanged();
                            tvHeaderTotal.setText("TOTAL " + getResources().getString(R.string.rupee_symbole) + " 0");
                            tvHeaderItemCount.setText("0 Item");
                            mListViewProducts.setVisibility(View.GONE);
                            isBuyNowButtonClicked = true;
                            btnCheckout.setEnabled(false);
                            btnCheckout.setFocusable(false);
                            MainActivity.setCartCount(0);
                        }
                    }
                } else {
//                Toast.makeText(mContext, "" + response.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
   /*         if(tag.equalsIgnoreCase(TAG_CART_STOCK))
            {
                if (response instanceof CartDetailsResponse) {

                    cartResponse = (CartDetailsResponse)response;

                    String isInStock = cartResponse.getData().getStock();
                    if(isInStock.equalsIgnoreCase("false"))
                    {
                        isCartItemQuantityEmpty = true;
                        productName = cartResponse.getData().getProductName();
                    }
                }
            }*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onFailure(Exception error) {

        try {
            isPromocodeAppliedClicked = false;
            progressView.setVisibility(View.GONE);
            isBuyNowButtonClicked = false;
            btnCheckout.setEnabled(true);
            btnCheckout.setFocusable(true);
//        Toast.makeText(mContext, "Server encountered an error.", Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onQuantityUpdateListener(String productId, String quantity, String childId) {
        cancelTimer();
        ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, TAG)).updateQuantity(productId, quantity, childId);
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onApplyPromocode(String promocode) {

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        if (null != promocode && 0 < promocode.length()) {
            isPromocodeAppliedClicked = true;
            isBuyNowButtonClicked = true;
            btnCheckout.setEnabled(false);
            btnCheckout.setFocusable(false);

            cancelTimer();

            ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, PROMO_CODE_TAG)).applyPromoCode(promocode);

            progressView.setVisibility(View.VISIBLE);

        } else {
            Toast.makeText(getActivity(), "Please enter valid coupon code.", Toast.LENGTH_SHORT).show();
        }
    }

    //    @Override
    public void checkout() {

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        isCheckOutClicked = false;
        if (!isCartItemQuantityEmpty) {
            Intent intent = new Intent();
            intent.setClass(mContext, PaymentConfirmDeliveryAddressActivity.class);
            intent.putExtra("TOTAL", cartResponse.getData().getMetadata().getTotal());
            intent.putExtra("CART_PRODUCT_COUNT", "" + cartResponse.getData().getProducts().size());
            startActivity(intent);
        } else {
           Toast.makeText(mContext, "Any of the product added in your trunk is out of stock. Please delete it from trunk.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRefreshCart() {
        ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, TAG)).getCart();
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onProductDelete(String productId, String childId) {
        cancelTimer();
        ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, TAG)).deleteFromCart(productId, childId);
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelTimer();
    }

    private void cancelTimer() {
        isPromocodeAppliedClicked = false;
        currentTimeInMillis = 0;
        if (mCartAdapter != null)
            mCartAdapter.cancleTimer();
    }

}
