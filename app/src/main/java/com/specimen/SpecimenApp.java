package com.specimen;

import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.support.multidex.MultiDex;

import com.specimen.core.IOCContainer;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Tejas on 6/1/2015.
 * Application class for Specimen.
 */
public class SpecimenApp extends Application {

    private IOCContainer iocContainer;

    public SpecimenApp() {
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.iocContainer = IOCContainer.getInstance();
        this.iocContainer.init(this);

        IOCContainer.BaseURL = com.specimen.BuildConfig.ENV;
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/gil____.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

        IntentFilter inf = new IntentFilter();
        inf.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(NetworkStateWatcher.getInstance(), inf);
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        iocContainer = null;
        unregisterReceiver(NetworkStateWatcher.getInstance());
    }


}
