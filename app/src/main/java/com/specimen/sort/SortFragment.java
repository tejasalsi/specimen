package com.specimen.sort;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.widget.DividerItemDecoration;

import java.util.Arrays;

/**
 * Created by Tejas on 6/15/2015.
 */
public class SortFragment extends BaseFragment {

    RecyclerView sortOptionList;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sort, container, false);

        sortOptionList = (RecyclerView) view.findViewById(R.id.list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        sortOptionList.setLayoutManager(linearLayoutManager);
        String[] list = (getActivity().getResources().getStringArray(R.array.sort_option));
        String[] code = (getActivity().getResources().getStringArray(R.array.sort_code));

        sortOptionList.setAdapter(new SortAdapter(R.layout.list_sort_row, Arrays.asList(list), Arrays.asList(code)));

        sortOptionList.addItemDecoration(new DividerItemDecoration(getActivity()));

        return view;
    }


}


