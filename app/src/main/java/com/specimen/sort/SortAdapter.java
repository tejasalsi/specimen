package com.specimen.sort;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.Query;
import com.specimen.search.footerEvent.SorterToggle;
import com.specimen.util.BusProvider;

import java.util.List;

/**
 * Created by Jitendra Khetle on 03/07/15.
 */
public class SortAdapter extends RecyclerView.Adapter<SortAdapter.StringHolder> {
    List<String> options;
    int resourceLayout;
    List<String> code;

    public SortAdapter(int resourceLayout, List<String> objects, List<String> code) {
        this.resourceLayout = resourceLayout;
        this.options = objects;
        this.code = code;
    }


    @Override
    public StringHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resourceLayout, null);
        StringHolder productHolder = new StringHolder(view);
        productHolder.setClickListener();
        return productHolder;
    }

    @Override
    public void onBindViewHolder(StringHolder holder, int position) {

        String optionValue = options.get(position);

        holder.textView.setText(optionValue);
        holder.textView.setTag(code.get(position));

        String sorter = Query.getInstance().getSorter();
        if (sorter != null && sorter.equals(code.get(position))) {
//            holder.textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.select_check, 0);
            holder.checker.setSelected(true);
            holder.checker.setVisibility(View.VISIBLE);

        } else {
            holder.checker.setSelected(false);
            holder.checker.setVisibility(View.GONE);
//            holder.textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }


    @Override
    public int getItemCount() {
        return (null != options ? options.size() : 0);
    }


    public class StringHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;
        ImageView checker;

        public StringHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.text1);
            this.checker = (ImageView) itemView.findViewById(R.id.itemCheck);
        }

        public void setClickListener() {
            super.itemView.setOnClickListener(this);
            textView.setOnClickListener(this);
            checker.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String sorter = Query.getInstance().getSorter();

            if (sorter != null && sorter.equals(textView.getTag().toString())) {
                Query.getInstance().setSorter(null);
//                BusProvider.getInstance().post("");
            } else {
                String sort_by = textView.getTag().toString();
                Query.getInstance().setSorter(sort_by);
//                BusProvider.getInstance().post(sort_by);
            }

            /**
             * Post SorterToggle object to notify that sorter option has clicked
             */

            BusProvider.getInstance().post(new SorterToggle());
//            boolean b = textView.setCompoundDrawablesWithIntrinsicBounds().getVisibility() == View.GONE;
//            if (b) {
//
//            } else {
//                Query.getInstance().setSorter(null);
//                BusProvider.getInstance().post("");
//            }
            notifyDataSetChanged();

        }
    }
}
