package com.specimen.look;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.R;
import com.specimen.core.model.Look;
import com.specimen.core.model.Product;
import com.specimen.product.ProductHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rohit on 8/24/2015.
 */
public class LookDetailProductListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int HEADER = 0;
    private static final int LIST_ITEM = 1;
    private final DisplayMetrics matrix;
    Context context;
    LayoutInflater inflater;
    List<Product> products;
    Look looks;
    LookHolder lookHolder;

    public LookDetailProductListingAdapter(Context context, ArrayList<Product> objects) {
        this.context = context;
        this.products = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        matrix = context.getResources().getDisplayMetrics();
    }

    public void setHeader(Look header) {
        this.looks = header;
    }

    public LookHolder getLookHolder () {
        return lookHolder;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.looksitem, null);
            return lookHolder = new LookHolder(view, matrix, true);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item_layout, null);
            return new ProductHolder(view, matrix);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ProductHolder) {
            if (looks != null) {
                position = position - 1;
            }
            Product product = products.get(position);
            ((ProductHolder) holder).getView(product);
        } else {
            ((LookHolder) holder).getView(looks);
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0 && looks != null) {
            return HEADER;
        }
        return LIST_ITEM;
    }

    @Override
    public int getItemCount() {
        return looks != null ? products.size() + 1 : products.size();
    }
}
