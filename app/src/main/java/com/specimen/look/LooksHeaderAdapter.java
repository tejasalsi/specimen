package com.specimen.look;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.Subcategory;

import java.util.ArrayList;

/**
 * Created by Jitendra Khetle on 03/07/15.
 */
public class LooksHeaderAdapter extends RecyclerView.Adapter<LooksHeaderAdapter.LookHeaderHolder> {
    Context context;
    ArrayList<Subcategory> subcategories;
    int resourceLayout;
    int mSelectedPosition = -1;

    public LooksHeaderAdapter(Context context, int resource, ArrayList<Subcategory> objects) {
        this.context = context;
        this.resourceLayout = resource;
        this.subcategories = objects;
    }

    public void setResourceLayout(int resourceLayout) {
        this.resourceLayout = resourceLayout;
    }

    @Override
    public LookHeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(resourceLayout, null);

        return new LookHeaderHolder(view);
    }

    @Override
    public void onBindViewHolder(LookHeaderHolder holder, int position) {
        Subcategory looks = subcategories.get(position);

        if (position == mSelectedPosition) {
            holder.textView.setBackgroundColor(holder.textView.getContext().getResources().getColor(R.color.specimen_green));
        } else {
            holder.textView.setBackgroundColor(holder.textView.getContext().getResources().getColor(R.color.specimen_background_dark_grey));
        }

        holder.getView(looks);
    }

    public void setSelectedItemPosition(int position) {
        mSelectedPosition = position;
    }

    @Override
    public int getItemCount() {
        return (null != subcategories ? subcategories.size() : 0);
    }


    public class LookHeaderHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public LookHeaderHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.txt_name);
        }

        public void getView(Subcategory subcategory) {
            Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/GILB____.TTF");
            textView.setTypeface(face);
            textView.setText(subcategory.getName().toUpperCase());
        }
    }
}
