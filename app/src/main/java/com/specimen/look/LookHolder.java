package com.specimen.look;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.model.Look;
import com.specimen.core.model.Look;
import com.specimen.core.spy.ISpyFacade;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

public class LookHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private static final String TAG = "LookHolder";
    public ImageView lookImage;
    public TextView lookName, lookViewsText;

    public ImageView spiedImage;


    Look looks;
    Context context;
    DisplayMetrics matrix;
    boolean isDetailPageView;

    public LookHolder(View itemView, DisplayMetrics matrix) {
        super(itemView);

        this.lookImage = (ImageView) itemView.findViewById(R.id.looksItemImage);
        this.lookName = (TextView) itemView.findViewById(R.id.lookTitle);
        this.lookViewsText = (TextView) itemView.findViewById(R.id.lookViewsText);
        this.spiedImage = (ImageView) itemView.findViewById(R.id.spied_icon_image);

        this.matrix = matrix;
        this.context = itemView.getContext();
        this.isDetailPageView = false;
    }

    public LookHolder(View itemView, DisplayMetrics matrix, boolean isDetailPageView) {
        this(itemView, matrix);
        this.isDetailPageView = isDetailPageView;

//        if (isDetailPageView) {
//            spiedImage.setVisibility(View.GONE);
//        } else {
//            spiedImage.setVisibility(View.VISIBLE);
//        }
    }

    public void getView(Look looks) {

        this.looks = looks;

        if (looks.getImage() != null && looks.getImage().size() > 0) {
            Picasso.with(context)
                    .load(ImageUtils.getProductImageUrl(looks.getImage().get(0), matrix))
                    .placeholder(R.drawable.pdp_banner_placeholder)
                    //.resize(matrix.widthPixels, matrix.heightPixels)
                    //.centerInside()
                    .fit()
                    .into(lookImage);
        } else {
            Picasso.with(context)
                    .load(R.drawable.pdp_banner_placeholder)
                    .resize(matrix.widthPixels, matrix.heightPixels)
                    .centerInside()
                    .into(lookImage);
        }

        if(!TextUtils.isEmpty(looks.getName()))
        lookName.setText(looks.getName().toUpperCase());
        lookViewsText.setText("" + looks.getView_count() + " VIEWS");
        spiedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(spiedImage, "rotationX", 90f, 0.0f);
                objectAnimator.setDuration(1000);
                objectAnimator.setStartDelay(0);
                objectAnimator.start();
                LookHolder.this.onClick(v);
            }
        });

        if (!isDetailPageView) {
            lookImage.setOnClickListener(this);
        }

        if (!looks.getIsSpied()) {
            spiedImage.setImageResource(R.drawable.spi_circle_black);

        } else {
            spiedImage.setImageResource(R.drawable.prodlist_spi_enabled);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.spied_icon_image:

                toggleSpiedState();
                break;

            case R.id.looksItemImage:
                Intent intent = new Intent(v.getContext(), LookDetailActivity.class);
                intent.putExtra("look", looks);
                v.getContext().startActivity(intent);
                break;

        }
    }

    private void toggleSpiedState() {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(spiedImage, "rotationX", 90f, 0.0f);
        objectAnimator.setDuration(600);
        //objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setStartDelay(0);
        objectAnimator.start();
        if (!looks.getIsSpied()) {
            spiedImage.setImageResource(R.drawable.prodlist_spi_enabled);
            looks.setIsSpied(true);
            addToSpied();


        } else {
            spiedImage.setImageResource(R.drawable.spi_circle_black);
            looks.setIsSpied(false);
            removeFromSpied();
        }
        spiedImage.invalidate();
    }

    private void removeFromSpied() {
        ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).removeSpied(looks.getId());
    }

    private void addToSpied() {
        ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).setSpied(looks.getId());
    }
}