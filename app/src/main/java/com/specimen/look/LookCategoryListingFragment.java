package com.specimen.look;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.look.ILookFacade;
import com.specimen.core.model.Query;
import com.specimen.core.model.Subcategory;
import com.specimen.core.model.TopCategory;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.LookResponse;
import com.specimen.listener.RecyclerItemClickListener;
import com.specimen.widget.SpacesItemDecoration;

import java.util.ArrayList;

/**
 * Created by Intelliswift on 8/20/2015.
 */
public class LookCategoryListingFragment extends LooksListingFragment implements IResponseSubscribe {

    private static final String TAG = "LookCategoryListingFragment";
    ArrayList<Subcategory> subcategories;
    private String topCategoryId;
    private View looksInflatedView;

    public LookCategoryListingFragment() {
        super();
    }

    public static LookCategoryListingFragment newInstance(TopCategory topCategory) {

        LookCategoryListingFragment fragment = new LookCategoryListingFragment();
        Bundle bundle = new Bundle();
        bundle.putString("topCategory", "" + topCategory.getId());
        bundle.putParcelableArrayList("subcategories", (ArrayList<Subcategory>) topCategory.getSubcategory());

        fragment.setArguments(bundle);

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            subcategories = getArguments().getParcelableArrayList("subcategories");
            topCategoryId = getArguments().getString("topCategory");
            looksesList = new ArrayList<>();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        looksInflatedView = super.onCreateView(inflater, container, savedInstanceState);

        headerRecyclerView.setVisibility(View.VISIBLE);
        headerRecyclerView.setHasFixedSize(true);

        LinearLayoutManager horizontalLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        headerRecyclerView.setLayoutManager(horizontalLinearLayoutManager);

        final LooksHeaderAdapter looksHeaderAdapter = new LooksHeaderAdapter(getActivity(), R.layout.look_header_textview, subcategories);
        RecyclerView.ItemDecoration decor = new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.right = 5;
                outRect.top = 5;
            }

        };
//        headerRecyclerView.addItemDecoration(new SpacesItemDecoration(5));
        headerRecyclerView.addItemDecoration(decor);
        headerRecyclerView.setAdapter(looksHeaderAdapter);

        headerRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // do whatever
                looksHeaderAdapter.setSelectedItemPosition(position);
                looksHeaderAdapter.notifyDataSetChanged();
                Query.getInstance().setSubCategoryId(subcategories.get(position).getId() + "");
                setView();
            }
        }));
        Query.getInstance().totalResetQuery();
        Query.getInstance().setSubCategoryId(topCategoryId);
        return looksInflatedView;
    }

    private void setView() {
        looksesList.clear();
        looksAdapter.notifyDataSetChanged();
        fetchLooks();
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        setView();
    }

    private void fetchLooks() {
        progressView.setVisibility(View.VISIBLE);
        ((ILookFacade) IOCContainer.getInstance().getObject(ServiceName.LOOK_SERVICE, TAG)).fetchLooks(Query.getInstance());
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
//        super.onSuccess(response, tag);
        progressView.setVisibility(View.GONE);
        if (response instanceof LookResponse) {
            LookResponse lookResponse = (LookResponse) response;
            looksesList.addAll(lookResponse.getData().getLooks());
            looksAdapter.notifyDataSetChanged();

            if(null == looksesList || 0 == looksesList.size()) {
                looksInflatedView.findViewById(R.id.recycler_view).setVisibility(View.GONE);
                looksInflatedView.findViewById(R.id.linearNoProductFoundView).setVisibility(View.VISIBLE);
            } else {
                looksInflatedView.findViewById(R.id.recycler_view).setVisibility(View.VISIBLE);
                looksInflatedView.findViewById(R.id.linearNoProductFoundView).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        super.onFailure(error);
        progressView.setVisibility(View.GONE);
        if(null == looksesList || 0 == looksesList.size()) {
            looksInflatedView.findViewById(R.id.recycler_view).setVisibility(View.GONE);
            looksInflatedView.findViewById(R.id.linearNoProductFoundView).setVisibility(View.VISIBLE);
        } else {
            looksInflatedView.findViewById(R.id.recycler_view).setVisibility(View.VISIBLE);
            looksInflatedView.findViewById(R.id.linearNoProductFoundView).setVisibility(View.GONE);
        }
    }
}
