package com.specimen.trustcircle;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.TrustCircleResponse;
import com.specimen.core.trustcircle.ITrustCircleFacade;
import com.specimen.widget.UserPerformedErrors;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Tejas on 8/19/2015.
 */
public class AddMateForm extends BaseFragment {

    private EditText editMatename, editEmail, editMobile;
    private ImageView btnAddMate, btnAddFromAddressBook;
    private TextView errorText;
    private final int PICK_CONTACT = 100;
    private Cursor cursor, emailCursor, phoneCursor;
    private Toast contactToast;
    private UserPerformedErrors userPerformedErrors;
    private final String ADD_TRUSTEE_TAG = "ADD_TRUSTEE";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup formView = (ViewGroup) inflater.inflate(R.layout.fragment_addamate, container, false);
        ((TrustCircleActivity) getActivity()).setToolbarTitle(R.string.addamate_title);
        contactToast = new Toast(getActivity());

        editMatename = (EditText) formView.findViewById(R.id.matename_edit);
        editEmail = (EditText) formView.findViewById(R.id.mateEmail_edit);
        editMobile = (EditText) formView.findViewById(R.id.mateMobile_edit);
        errorText = (TextView) formView.findViewById(R.id.error_txt);
        errorText.setText("");
        userPerformedErrors = new UserPerformedErrors(errorText, contactToast);

        btnAddMate = (ImageView) formView.findViewById(R.id.add_mate_btn);
        btnAddFromAddressBook = (ImageView) formView.findViewById(R.id.add_from_address_book_btn);

        btnAddMate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                if (TextUtils.isEmpty(editMatename.getText().toString())) {
                    userPerformedErrors.setErrorMessage("Please enter valid mate's name.".toUpperCase());
                    return;
                }

                if (TextUtils.isEmpty(editMatename.getText().toString()) || !isValidEmail(editEmail.getText().toString().trim())) {
                    userPerformedErrors.setErrorMessage("Please enter valid email id.".toUpperCase());
                    return;
                }

                if (TextUtils.isEmpty(editMobile.getText().toString()) || editMobile.getText().length() < 10) {
                    userPerformedErrors.setErrorMessage("Please enter valid mobile no.".toUpperCase());
                    return;
                }

                String name = editMatename.getText().toString();
                String email = editEmail.getText().toString();
                String number = editMobile.getText().toString();
                ((ITrustCircleFacade) IOCContainer.getInstance()
                        .getObject(ServiceName.TRUST_CIRCLE_SERVICE, ADD_TRUSTEE_TAG)).addTrustee(email, name, number);


//                    ShowMatesFragment showMatesFragment = new ShowMatesFragment();
//                    Bundle mateBundle = new Bundle();
//                    mateBundle.putString("mate_name", editMatename.getText().toString());
//                    showMatesFragment.setArguments(mateBundle);
//                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    List<Fragment> askMateFragments = fragmentManager.getFragments();
//                    if (askMateFragments.contains(fragmentManager.findFragmentByTag("SHOW_MATES")))
//                        fragmentManager.popBackStackImmediate();
//                    else {
//                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                        transaction.replace(R.id.askamate_container, showMatesFragment, "SHOW_MATES");
//                        transaction.commit();
//                    }
//                if (isComingFromPDP) {
//                    //do nothing for now
//                } else {
//
//                    //transaction.addToBackStack("SHOW_MATES");
//                }
            }
        });

        btnAddFromAddressBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });
        return formView;
    }

    private boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        editMatename.setText(null);
        editMobile.setText(null);
        editEmail.setText(null);
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    cursor = getActivity().getContentResolver().query(contactData, null, null, null, null);
                    if (null != cursor && cursor.moveToFirst()) {
                        String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        String emailIdOfContact = null;
                        String phone = null;
                        getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                        emailCursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + id, null, null);
                        phoneCursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                        while (emailCursor.moveToNext()) {
                            emailIdOfContact = emailCursor.getString(emailCursor
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        }
                        while (phoneCursor.moveToNext()) {
                            phone = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            if(phone != null && !phone.isEmpty())
                            {
                                break;
                            }
                        }
                        phoneCursor.close();
                        if (name == null && (emailIdOfContact == null || phone == null)) {
                            userPerformedErrors.setErrorMessageToast(getActivity(), "Not a valid Contact", null, 0, Toast.LENGTH_SHORT);
                        } else {
                            editMatename.setText(name);
                            if (emailIdOfContact != null) {
                                editEmail.setText(emailIdOfContact);
                                errorText.setText("");
                            }
                            if (phone != null) {
                                editMobile.setText(phone);
                                errorText.setText("");
                            }
                            if (emailIdOfContact == null && phone == null)
                                userPerformedErrors.setErrorMessage("User must have at least Mobile or Email");
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (emailCursor != null && phoneCursor != null && cursor != null) {
            emailCursor.close();
            phoneCursor.close();
            cursor.close();
        }
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (response instanceof TrustCircleResponse) {

            ShowMatesFragment showMatesFragment = new ShowMatesFragment();

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            List<Fragment> askMateFragments = fragmentManager.getFragments();

            if (askMateFragments.contains(fragmentManager.findFragmentByTag("SHOW_MATES"))) {
                fragmentManager.popBackStackImmediate();
            } else {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.askamate_container, showMatesFragment, "SHOW_MATES");
                transaction.commit();
            }

            Toast.makeText(mContext, "" + response.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onFailure(Exception error) {
//        Log.d("Add Mate:", error.getMessage());
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
        mContext.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public AddMateForm() {
        super();
    }
}
