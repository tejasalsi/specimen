package com.specimen.trustcircle;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.model.TrusteesEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 15/8/15.
 */
public class TrustCircleActivity extends BaseActivity implements ShowMatesFragment.OnGetProductId {

    List<TrusteesEntity> trustees;
    Toolbar toolbar;
    TextView toolbarTitle;
    private String productId;
    boolean isFromNotification;
    public static List<Integer> selectedMates = new ArrayList<>();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        selectedMates.clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_askamate);
        setToolbar();
        productId = getIntent().getStringExtra("product_id");
        isFromNotification = getIntent().getBooleanExtra("NOTIFICATION", false);

        if (isFromNotification && null != productId) {
            MateAlertFragment mateAlertFragment = new MateAlertFragment();
            Bundle bundle = new Bundle();
            bundle.putString("product_id", productId);
            mateAlertFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().add(R.id.askamate_container, mateAlertFragment, "MATE_ALERT").commit();
        } else {
            if (productId != null) {
                ShowMatesFragment showMatesFragment = new ShowMatesFragment();
                //showMatesFragment.setProductId(productId);
                getSupportFragmentManager().beginTransaction().add(R.id.askamate_container, showMatesFragment, "SHOW_MATES").commit();
            } else
                getSupportFragmentManager().beginTransaction().add(R.id.askamate_container, new TrustCircleHome()).commit();
        }
    }

    @Override
    public void onBackPressed() {
        if(isFromNotification) {
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setToolbarTitle(R.string.askamate_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setToolbarTitle(int toolbarTitleStringResourceId) {
        toolbarTitle.setText(toolbarTitleStringResourceId);
    }

    public void hideToolbar() {
        toolbar.setVisibility(View.GONE);
    }

    public void visibleToolbar() {
        toolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public String getProductId() {
        return productId;
    }
}
