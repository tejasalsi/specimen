package com.specimen.trustcircle;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.model.Like;
import com.specimen.core.model.LikeNotificationResponse;
import com.specimen.core.response.APIResponse;
import com.specimen.core.trustcircle.ITrustCircleFacade;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tejas on 9/3/2015.
 */
public class MateAlertFragment extends BaseFragment {

    private ImageView ivProductImg, ivClose;
    private TextView tvProductName, tvProductPrice, tvTotalLikes, tvMate1, tvMate2, tvMate3, tvMate4, tvMate5;
    private String TAG = "ALERT";
    Bundle bundle;
    private List<TextView> tvLikes;
    private DisplayMetrics matrix;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((TrustCircleActivity) getActivity()).hideToolbar();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View alertView = inflater.inflate(R.layout.fragment_askmate_alert, container, false);

        bundle = getArguments();
        ivProductImg = (ImageView) alertView.findViewById(R.id.mateAlertProductImage);
        ivClose = (ImageView) alertView.findViewById(R.id.close_mate_alert);
        matrix = mContext.getResources().getDisplayMetrics();

        tvProductName = (TextView) alertView.findViewById(R.id.mate_alert_product_name);
        tvProductPrice = (TextView) alertView.findViewById(R.id.tvProductPrice);
        tvTotalLikes = (TextView) alertView.findViewById(R.id.tvCountOfLikes);
        tvMate1 = (TextView) alertView.findViewById(R.id.tvMate1);
        tvMate2 = (TextView) alertView.findViewById(R.id.tvMate2);
        tvMate3 = (TextView) alertView.findViewById(R.id.tvMate3);
        tvMate4 = (TextView) alertView.findViewById(R.id.tvMate4);
        tvMate5 = (TextView) alertView.findViewById(R.id.tvMate5);

        tvLikes = new ArrayList<>();
        tvLikes.add(tvMate1);
        tvLikes.add(tvMate2);
        tvLikes.add(tvMate3);
        tvLikes.add(tvMate4);
        tvLikes.add(tvMate5);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.onBackPressed();
            }
        });
//        TextView TV = (TextView)findViewById(R.id.mytextview01);
//
//        Spannable wordtoSpan = new SpannableString("I know just how to whisper, And I know just how to cry,I know just where to find the answers");
//
//        wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLUE), 15, 30, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//        TV.setText(wordtoSpan);
        return alertView;

    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        ((ITrustCircleFacade) IOCContainer.getInstance()
                .getObject(ServiceName.TRUST_CIRCLE_SERVICE, TAG)).trusteeProductLikeNotification(bundle.getString("product_id"));
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        ((TrustCircleActivity) getActivity()).visibleToolbar();
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (response instanceof LikeNotificationResponse) {
            LikeNotificationResponse likeNotificationResponse = (LikeNotificationResponse) response;
            try {
                if (null == likeNotificationResponse || (null == likeNotificationResponse.getData().getCount())) {
                    Toast.makeText(mContext, "You have not shared this product with friends.", Toast.LENGTH_SHORT).show();
                    //mContext.onBackPressed();
                    return;
                }

                Picasso.with(mContext)
                        .load(ImageUtils.getProductImageUrl(likeNotificationResponse.getData().getProduct().getImage().get(0), matrix))
                        .resize(300, 480)
                        .centerInside()
//                    .placeholder(R.drawable.hand_pick_product_placeholder)
                        .placeholder(R.drawable.product_list_placeholder)
                        .into(ivProductImg);

                tvProductName.setText(likeNotificationResponse.getData().getProduct().getName().toUpperCase());
                tvProductPrice.setText(getResources().getString(R.string.rupee_symbole) + " " + likeNotificationResponse.getData().getProduct().getPrice());
                String likeCount = (!TextUtils.isEmpty(likeNotificationResponse.getData().getLikeCount()) ? likeNotificationResponse.getData().getLikeCount() : "0");
                //     tvTotalLikes.setText(likeCount +" of "+likeNotificationResponse.getData().getCount()+ " people like this product.");
                String totalCount = (null != likeNotificationResponse.getData().getCount()) ? likeNotificationResponse.getData().getCount().toString() : "0";
                Spannable totalLikesCount = new SpannableString(likeCount + " of " + totalCount + " people like this product.");
                totalLikesCount.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.specimen_green)), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                totalLikesCount.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.specimen_green)), 5, 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                tvTotalLikes.setText(totalLikesCount);

                String name = "";
                String status = "";
                Like like;
                for (int index = 0; index < likeNotificationResponse.getData().getLikes().size(); index++) {
                    like = likeNotificationResponse.getData().getLikes().get(index);
                    name = like.getName().toUpperCase();
                    if (!TextUtils.isEmpty(like.getLike())) {
                        tvLikes.get(index).setVisibility(View.VISIBLE);
                        status = (like.getLike().equals("1") ? " like " : " didn't liked ");

                        Spannable likedText = new SpannableString(name + status + "this product.");

                        likedText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.specimen_green)), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        tvLikes.get(index).setText(likedText);
                    }
                }

            } catch (NullPointerException nE) {
                Toast.makeText(mContext, "Our servers are working on this,please try again after some time", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void onFailure(Exception error) {

    }
}
