package com.specimen.trustcircle;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.model.LikeNotificationResponse;
import com.specimen.core.model.TrusteesEntity;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.TrustCircleResponse;
import com.specimen.core.trustcircle.ITrustCircleFacade;
import com.specimen.widget.ProgressView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tejas on 8/20/2015.
 */
public class ShowMatesFragment extends BaseFragment implements View.OnClickListener, IResponseSubscribe {

    private ImageView addMate1, addMate2, addMate3, addMate4, addMate5, notificationBell,
            deleteMate1, deleteMate2, deleteMate3, deleteMate4, deleteMate5;
    private TextView mateName1, mateName2, mateName3, mateName4, mateName5;
    private Button sendMateBtn, okMateBtn;
    private ProgressView progressView;

    private List<TrusteesEntity> mates;
    private final String DELETE_MATE_TAG = "DELETE_MATE";
    private final String SEND_MATE_TAG = "SEND_TO_MATE";
    private final String TAG = "TRUST_CIRCLE";

    private OnGetProductId onGetProductId;
    private String productId;
    //    private int matesIndex = 100;
    private List<TextView> mateNames;
    private List<ImageView> matesBackground, deleteMatesIcon;
    private String mProductId = "";

    private ArrayList<String> mTrustees;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup showMatesView = (ViewGroup) inflater.inflate(R.layout.fragment_mates, container, false);
        ((TrustCircleActivity) getActivity()).setToolbarTitle(R.string.askamate_title);

        addMate1 = (ImageView) showMatesView.findViewById(R.id.mate1);
        addMate2 = (ImageView) showMatesView.findViewById(R.id.mate2);
        addMate3 = (ImageView) showMatesView.findViewById(R.id.mate3);
        addMate4 = (ImageView) showMatesView.findViewById(R.id.mate4);
        addMate5 = (ImageView) showMatesView.findViewById(R.id.mate5);

        mateName1 = (TextView) showMatesView.findViewById(R.id.mate1name);
        mateName2 = (TextView) showMatesView.findViewById(R.id.mate2name);
        mateName3 = (TextView) showMatesView.findViewById(R.id.mate3name);
        mateName4 = (TextView) showMatesView.findViewById(R.id.mate4name);
        mateName5 = (TextView) showMatesView.findViewById(R.id.mate5name);

        deleteMate1 = (ImageView) showMatesView.findViewById(R.id.deleteMate1);
        deleteMate2 = (ImageView) showMatesView.findViewById(R.id.deleteMate2);
        deleteMate3 = (ImageView) showMatesView.findViewById(R.id.deleteMate3);
        deleteMate4 = (ImageView) showMatesView.findViewById(R.id.deleteMate4);
        deleteMate5 = (ImageView) showMatesView.findViewById(R.id.deleteMate5);

        progressView = (ProgressView) showMatesView.findViewById(R.id.progressView);
        progressView.setVisibility(View.VISIBLE);
        ((ITrustCircleFacade) IOCContainer.getInstance()
                .getObject(ServiceName.TRUST_CIRCLE_SERVICE, TAG)).getTrustCircle(onGetProductId.getProductId());


        productId = onGetProductId.getProductId();
        mateNames = new ArrayList<>();
        matesBackground = new ArrayList<>();
        deleteMatesIcon = new ArrayList<>();

        mateNames.add(mateName1);
        mateNames.add(mateName2);
        mateNames.add(mateName3);
        mateNames.add(mateName4);
        mateNames.add(mateName5);

        matesBackground.add(addMate1);
        matesBackground.add(addMate2);
        matesBackground.add(addMate3);
        matesBackground.add(addMate4);
        matesBackground.add(addMate5);

        deleteMatesIcon.add(deleteMate1);
        deleteMatesIcon.add(deleteMate2);
        deleteMatesIcon.add(deleteMate3);
        deleteMatesIcon.add(deleteMate4);
        deleteMatesIcon.add(deleteMate5);

        if (mates != null) {
            refreshMates();
        }

        /*if (productId == null) {
            if(getArguments() != null) {
                String firstMate = getArguments().getString("mate_name");
                if (firstMate != null) {
                    if (firstMate.length() > 5) {
                        mateName1.setText(firstMate.toString().substring(0, 5) + "...");
                    } else
                        mateName1.setText(firstMate);
                    mateName1.setVisibility(View.VISIBLE);
                    Drawable mataNameBackground = getResources().getDrawable(R.drawable.mate_green);
                    addMate1.setImageDrawable(mataNameBackground);
                }
            }
        }*/

        sendMateBtn = (Button) showMatesView.findViewById(R.id.mateSendBtn);
        okMateBtn = (Button) showMatesView.findViewById(R.id.mateOkBtn);
        notificationBell = (ImageView) showMatesView.findViewById(R.id.img_bell);
        sendMateBtn.setOnClickListener(this);

        if (productId != null) {
            sendMateBtn.setVisibility(View.VISIBLE);
            notificationBell.setVisibility(View.VISIBLE);
        } else {
            sendMateBtn.setVisibility(View.GONE);
            notificationBell.setVisibility(View.GONE);
        }


        addMate1.setOnClickListener(this);
        addMate2.setOnClickListener(this);
        addMate3.setOnClickListener(this);
        addMate4.setOnClickListener(this);
        addMate5.setOnClickListener(this);

        deleteMate1.setOnClickListener(this);
        deleteMate2.setOnClickListener(this);
        deleteMate3.setOnClickListener(this);
        deleteMate4.setOnClickListener(this);
        deleteMate5.setOnClickListener(this);

        notificationBell.setOnClickListener(this);

        return showMatesView;
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
//        iterateMates();
    }

    private boolean iterateMates(int mateCountId) {

        for (int i = 0; i < TrustCircleActivity.selectedMates.size(); i++) {
            if (mateCountId == TrustCircleActivity.selectedMates.get(i)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.mate1:
                checkMateAvailable(0);
                break;
            case R.id.mate2:
                checkMateAvailable(1);
                break;
            case R.id.mate3:
                checkMateAvailable(2);
                break;
            case R.id.mate4:
                checkMateAvailable(3);
                break;
            case R.id.mate5:
                checkMateAvailable(4);
                break;
            case R.id.mateOkBtn:
                //invalidate view...
                ((ITrustCircleFacade) IOCContainer.getInstance()
                        .getObject(ServiceName.TRUST_CIRCLE_SERVICE, DELETE_MATE_TAG)).deleteTrustee(mates.get(0).getTrustee_email());
                break;

            case R.id.deleteMate1:
                removeMate(0);
                break;
            case R.id.deleteMate2:
                removeMate(1);
                break;
            case R.id.deleteMate3:
                removeMate(2);
                break;
            case R.id.deleteMate4:
                removeMate(3);
                break;
            case R.id.deleteMate5:
                removeMate(4);
                break;
            case R.id.mateSendBtn:
                if (mates != null) {
                    mTrustees = new ArrayList<>();
                    for (int i = 0; i < mates.size(); i++) {
                        if (deleteMatesIcon.get(i).getVisibility() == View.VISIBLE) {
                            mTrustees.add(mates.get(i).getTrustee_email());
                        }
                    }
                    if (mTrustees.size() > 0) {
                        ((ITrustCircleFacade) IOCContainer.getInstance()
                                .getObject(ServiceName.TRUST_CIRCLE_SERVICE, SEND_MATE_TAG)).sendProducttoTrustCircle(onGetProductId.getProductId(), mTrustees);
                    } else {
                        Toast.makeText(getActivity(), "Please select at least one mate.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Trust is important!, first add it to your circle", Toast.LENGTH_SHORT).show();
                }
                //API to be implemented for send in core
                break;
            case R.id.img_bell:
                /*FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                MateAlertFragment mateAlertFragment = new MateAlertFragment();
                Bundle bundle = new Bundle();
                bundle.putString("product_id", onGetProductId.getProductId());
                mateAlertFragment.setArguments(bundle);
                transaction.replace(R.id.askamate_container, mateAlertFragment, "MATE_ALERT");
                transaction.addToBackStack("MATE_ALERT");
                transaction.commit();*/
                ((ITrustCircleFacade) IOCContainer.getInstance()
                        .getObject(ServiceName.TRUST_CIRCLE_SERVICE, TAG)).trusteeProductLikeNotification(onGetProductId.getProductId());
                break;
        }
    }

    public void checkMateAvailable(int count) {
        if (mateNames.get(count).getVisibility() == View.GONE) {
            gotoAddMateForm();
        } else {
            selectMate(count);
        }
    }

    public void selectMate(int count) {
        if (onGetProductId.getProductId() != null) {
            if (deleteMatesIcon.get(count).getVisibility() == View.VISIBLE) {
                deleteMatesIcon.get(count).setVisibility(View.INVISIBLE);
                TrustCircleActivity.selectedMates.add(count);
            } else {
                deleteMatesIcon.get(count).setImageResource(R.drawable.mate_added_checked);
                deleteMatesIcon.get(count).setVisibility(View.VISIBLE);
                for (int i = 0; i < TrustCircleActivity.selectedMates.size(); i++) {
                    if (TrustCircleActivity.selectedMates.get(i) == count) {
                        TrustCircleActivity.selectedMates.remove(i);
                    }
                }
            }
        }
    }

    private void removeMate(final int mateIndex) {
        if (onGetProductId.getProductId() != null) {
        } else {
            new AlertDialog.Builder(mContext)
                    .setTitle("Delete A Mate")
                    .setMessage("Are you sure you want to delete a mate?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ((ITrustCircleFacade) IOCContainer.getInstance()
                                    .getObject(ServiceName.TRUST_CIRCLE_SERVICE, DELETE_MATE_TAG)).deleteTrustee(mates.get(mateIndex).getTrustee_email());
                            mates.remove(mateIndex);
                            deleteMatesIcon.get(mateIndex).setVisibility(View.GONE);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
//                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    private void gotoAddMateForm() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.askamate_container, new AddMateForm(), "SHOW_FORM");
//        if (productId != null) {
        transaction.addToBackStack("SHOW_FORM");
//        } else {
//            transaction.addToBackStack("SHOW_FORM");
//        }
        transaction.commit();
    }

    private void refreshMates() {
        Drawable mataNameBackground = getResources().getDrawable(R.drawable.mate_green);
        Drawable mataNormalBackground = getResources().getDrawable(R.drawable.add_mate_cricle_btn);
        for (int i = 0; i < 5; i++) {
            if (mates != null && mates.size() > i) {
                TrusteesEntity trustee = mates.get(i);
                if (trustee.getTrustee_name().length() > 5)
                    mateNames.get(i).setText(trustee.getTrustee_name().toUpperCase().substring(0, 6) + "...");
                else
                    mateNames.get(i).setText(trustee.getTrustee_name().toUpperCase());

                mateNames.get(i).setVisibility(View.VISIBLE);
                matesBackground.get(i).setImageDrawable(mataNameBackground);
                if (onGetProductId.getProductId() != null && onGetProductId.getProductId().length() > 0) {
                    if (mates.get(i).getFeedback_given().trim().equalsIgnoreCase("true")) {
                        matesBackground.get(i).setImageResource(R.drawable.grey_green_bg_mate);
                        matesBackground.get(i).setOnClickListener(null);
                    } else {
                        if (null != TrustCircleActivity.selectedMates && 0 < TrustCircleActivity.selectedMates.size()) {
                            if (iterateMates(i)) {
                                deleteMatesIcon.get(i).setVisibility(View.INVISIBLE);
                            } else {
                                deleteMatesIcon.get(i).setVisibility(View.VISIBLE);
                                deleteMatesIcon.get(i).setImageResource(R.drawable.mate_added_checked);
                            }
                        } else {
                            deleteMatesIcon.get(i).setVisibility(View.VISIBLE);
                            deleteMatesIcon.get(i).setImageResource(R.drawable.mate_added_checked);
                        }
                    }
                } else {
                    deleteMatesIcon.get(i).setVisibility(View.VISIBLE);
                }
            } else {
                mateNames.get(i).setVisibility(View.GONE);
                deleteMatesIcon.get(i).setVisibility(View.GONE);
                matesBackground.get(i).setImageDrawable(mataNormalBackground);
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


        try {
            onGetProductId = (OnGetProductId) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnGetTrustees");
        }
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        progressView.setVisibility(View.GONE);
        if (response instanceof TrustCircleResponse) {
            TrustCircleResponse trustCircleResponse = (TrustCircleResponse) response;
            if (trustCircleResponse.getData() == null) {
                Toast.makeText(getActivity(), trustCircleResponse.getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                if (trustCircleResponse.getData().size() > 0) {
                    mates = trustCircleResponse.getData();
                }
                refreshMates();
            }
        }
        if (tag.equals(DELETE_MATE_TAG)) {
            Toast.makeText(mContext, "" + response.getMessage(), Toast.LENGTH_SHORT).show();
        } else if (tag.equals(SEND_MATE_TAG)) {
            Toast.makeText(mContext, "" + response.getMessage(), Toast.LENGTH_SHORT).show();
            progressView.setVisibility(View.VISIBLE);
            ((ITrustCircleFacade) IOCContainer.getInstance()
                    .getObject(ServiceName.TRUST_CIRCLE_SERVICE, TAG)).getTrustCircle(onGetProductId.getProductId());
            getActivity().onBackPressed();
        }
        if (response instanceof LikeNotificationResponse) {
            LikeNotificationResponse likeNotificationResponse = (LikeNotificationResponse) response;
            try {
                if (null == likeNotificationResponse || (null == likeNotificationResponse.getData().getCount())) {
                    Toast.makeText(mContext, "You have not shared this product with friends.", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    MateAlertFragment mateAlertFragment = new MateAlertFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("product_id", onGetProductId.getProductId());
                    mateAlertFragment.setArguments(bundle);
                    transaction.replace(R.id.askamate_container, mateAlertFragment, "MATE_ALERT");
                    transaction.addToBackStack("MATE_ALERT");
                    transaction.commit();
                }
            } catch (NullPointerException nE) {
                Toast.makeText(mContext, "Our servers are working on this,please try again after some time", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onFailure(Exception error) {
        progressView.setVisibility(View.GONE);
    }

//    public void setProductId(String productId) {
//        mProductId = productId;
//        onGetProductId.getProductId();
//    }

    /**
     * Callback for getting product id from PDP
     */
    public interface OnGetProductId {
        String getProductId();
    }
}
