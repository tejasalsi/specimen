package com.specimen.listener;

/**
 * Created by Rohit on 10/30/2015.
 */
public interface INetworkStateListener {

    void onNetworkConnect();

    void onNetworkDisconnect();

}
