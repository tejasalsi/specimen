package com.specimen.product.callback;

/**
 * Created by Intelliswift on 7/28/2015.
 */
public interface IPageChangeDelegate {

    void pageVisible(boolean isVisibleToUser);
}
