package com.specimen.product;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.R;
import com.specimen.core.model.Curated;
import com.specimen.core.model.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Created by Jitendra Khetle on 03/07/15.
 */
public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int HEADER = 0, ROW = 1;

    public static String CURRENT_REGISTER_BUS_EVENT_TAG = null;

    private final DisplayMetrics matrix;
    Context context;
    LayoutInflater inflater;
    List<Product> products;
    int resourceLayout;

    /**
     * Save the state of each entry Item
     * with position, Entry Type, Index of header in header Listing (Curated object Index)
     */
    private ConcurrentSkipListMap<Integer, EntryObjectState> headerPositionMapping;

    private List<Curated> curatedList;

    public ProductAdapter(Context context, int resource, ArrayList<Product> objects) {
        this.context = context;
        this.resourceLayout = resource;
        this.products = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        matrix = context.getResources().getDisplayMetrics();
        headerPositionMapping = new ConcurrentSkipListMap<>();
    }

    public void setResourceLayout(int resourceLayout) {
        this.resourceLayout = resourceLayout;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.curated_layout, null);
            return new CuratedHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(resourceLayout, null);
            return new ProductHolder(view, matrix);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case ROW:
                /**
                 * Calculate the Object position in given list
                 * which may differ after adding Header Row in List.
                 */
//                Log.e("ROW", "POSITION :-" + position + " , CuratedIndex :-" + headerPositionMapping.get(position).curatedHeaderListIndex);
                position = position - (headerPositionMapping.get(position).curatedHeaderListIndex + 1);
             //  if(!products.isEmpty() ) {
                   if(position < products.size()){
                   ((ProductHolder) holder).getView(products.get(position));
                   }
                break;
            case HEADER:
                /**
                 * Get Header Object.
                 */
//                Log.e("HEADER", "POSITION :-" + position + " , CuratedIndex :-" + headerPositionMapping.get(position).curatedHeaderListIndex);
                ((CuratedHolder) holder).getView(curatedList.get(headerPositionMapping.get(position).curatedHeaderListIndex));
                break;
        }
    }

    public void clear() {
        products.clear();
        if (null != curatedList)
            curatedList.clear();
        headerPositionMapping.clear();
    }

    @Override
    public int getItemCount() {
        return (null != products ? (curatedList != null ? curatedList.size() : 0) + products.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {

        /**
         * Check For already parse position
         */
        if (headerPositionMapping.containsKey(position)) {
            return headerPositionMapping.get(position).entryType;
        }


        if (curatedList != null && curatedList.size() > 0) {

            int curatedProductCount = 0;

            for (Curated curated : curatedList) {

                int curatedHeaderListIndex = curatedList.indexOf(curated);

                /**
                 * Check It this position to place header.
                 */
                if (position == curatedProductCount) {
                    headerPositionMapping.put(position, new EntryObjectState(HEADER, curatedHeaderListIndex));
                    return HEADER;
                }

                /**
                 * Condition satisfy when already place header for this position
                 */
                if (position < curatedProductCount) {
                    headerPositionMapping.put(position, new EntryObjectState(ROW, curatedHeaderListIndex - 1));
                    return ROW;
                }

                /**
                 * Calculate next Header Position in list
                 */
                if (curated != null && curated.getCount() != 0) {
                    curatedProductCount = curatedProductCount + curated.getCount() + 1;
                }


            }


            /**
             * Work when all header get placed on respective position & nothing to process.
             */
            headerPositionMapping.put(position, new EntryObjectState(ROW, curatedList.size() - 1));
            return ROW;
        }


        /**
         * Work when Header List not present (Curated)
         */

        headerPositionMapping.put(position, new EntryObjectState(ROW, -1));
        return ROW;
    }

    public void setHeader(List<Curated> header) {
        this.curatedList = header;
    }

    private class EntryObjectState {

        public EntryObjectState(int entryType, int curatedHeaderListIndex) {
            this.entryType = entryType;
            this.curatedHeaderListIndex = curatedHeaderListIndex;
        }

        private int entryType;
        private int curatedHeaderListIndex;
    }
}
