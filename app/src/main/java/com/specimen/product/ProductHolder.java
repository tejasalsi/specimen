package com.specimen.product;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.cart.ICartFacade;
import com.specimen.core.model.Product;
import com.specimen.core.model.ProductSizesListParable;
import com.specimen.core.spy.ISpyFacade;
import com.specimen.home.OnSizeSelectedListener;
import com.specimen.product.productdetails.ProductDetailsActivity;
import com.specimen.ui.dialog.SizeDialog;
import com.specimen.util.ImageUtils;
import com.specimen.util.ShareToPinterest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener, OnSizeSelectedListener {

    //    private static final String TAG = "ProductHolder";
    private ShareToPinterest mShareToPinterest;
    public ImageView productImage;
    public TextView productName;
    public TextView productPrice;
    public TextView scratched_priceText;
    public ImageView spiedImage;
    public ImageView trunkImage;

    public View scrachView;
    Product product;
    Context context;
    DisplayMetrics matrix;

    public static Product tempProduct;

    public ProductHolder(View itemView, DisplayMetrics matrix) {
        super(itemView);

        this.productImage = (ImageView) itemView.findViewById(R.id.product_image);
        this.productName = (TextView) itemView.findViewById(R.id.product_name);
        this.productPrice = (TextView) itemView.findViewById(R.id.product_price);
        this.scratched_priceText = (TextView) itemView.findViewById(R.id.scratched_priceText);
        this.scrachView = itemView.findViewById(R.id.scratched_layout);

        this.spiedImage = (ImageView) itemView.findViewById(R.id.spied_icon_image);
        try {
            this.trunkImage = (ImageView) itemView.findViewById(R.id.trunk_image);
        } catch (Exception ignored) {

        }
        this.matrix = matrix;
        this.context = itemView.getContext();
        mShareToPinterest = new ShareToPinterest(context);
    }

    public void getView(final Product product) {

        this.product = product;
        if (product.getImage() != null && product.getImage().size() > 0) {
            Picasso.with(context)
                    .load(ImageUtils.getProductImageUrl(product.getImage().get(0), matrix))
                    .placeholder(R.drawable.product_list_placeholder)
                    .resize(300, 480)
                    .centerInside()
                    .into(productImage);
        } else {
            Picasso.with(context).load(R.drawable.product_list_placeholder).into(productImage);
        }
        productName.setText(product.getName().toUpperCase());

        spiedImage.setOnClickListener(this);

        if (!product.getIsSpied()) {
            spiedImage.setImageResource(R.drawable.spi_circle_black);

        } else {
            spiedImage.setImageResource(R.drawable.prodlist_spi_enabled);
        }
        String rsSymbol = context.getResources().getString(R.string.Rs);
        String specialPrice = product.getSpecialprice();
        if (specialPrice != null) {
            if ("0".equals(specialPrice)) {
                productPrice.setTextColor(context.getResources().getColor(android.R.color.black));
                productPrice.setText(rsSymbol + product.getPrice());
                scrachView.setVisibility(View.GONE);
                scratched_priceText.setText(null);
            } else {
                scrachView.setVisibility(View.VISIBLE);
                scratched_priceText.setText(rsSymbol + product.getPrice());
                productPrice.setText(rsSymbol + specialPrice);
                productPrice.setTextColor(context.getResources().getColor(R.color.specimen_green));
            }
        } else {
            productPrice.setTextColor(context.getResources().getColor(android.R.color.black));
            productPrice.setText(rsSymbol + product.getPrice());
            scrachView.setVisibility(View.GONE);
            scratched_priceText.setText(null);
        }
        productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ProductDetailsActivity.class);
                intent.putExtra("PRODUCT_ID", product.getId());
                v.getContext().startActivity(intent);
            }
        });

        if (trunkImage != null) {
            trunkImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (product.getQauntity() == 0) {
//                        Toast.makeText(context, "Product is out of stock", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
                    tempProduct = product;

                    if (product.getSizes() != null) {
                        int size = product.getSizes().size();
                        List<Boolean> checkProductSizesIfBlank = new ArrayList<>();
                        for (int i = 0; i < size; i++) {
                            if (product.getSizes().get(i).getStock_count() == 0) {
                                checkProductSizesIfBlank.add(true);
                            }
                        }

                        if (0 < checkProductSizesIfBlank.size() && size == checkProductSizesIfBlank.size()) {
                            Toast.makeText(context, "Apologies, this product is out of stock.", Toast.LENGTH_SHORT).show();
                        } else {
                            SizeDialog sizeDialog = new SizeDialog();
                            sizeDialog.setOnSizeSelectedListner(ProductHolder.this);
                            Bundle bundle = new Bundle();
                            ProductSizesListParable productSizesListParable = new ProductSizesListParable(product.getSizes());
//                          ProductSizesListParable productSizesListParable = new ProductSizesListParable(product.getSize());
                            bundle.putParcelable("product", productSizesListParable);
                            bundle.putString("productId", product.getId());
                            bundle.putString("attributeId", product.getSizes().get(0).getAttribute_id());
                            bundle.putString("tag", "");
                            sizeDialog.setArguments(bundle);
                            sizeDialog.show(((BaseActivity) v.getContext()).getSupportFragmentManager(), "");
                        }
                    } else {
                        Toast.makeText(context, "Apologies, this product is out of stock.", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.spied_icon_image:
                toggleSpiedState();
//                Toast.makeText(v.getContext(), getAdapterPosition() + "", Toast.LENGTH_SHORT).show();

                break;
            case R.id.trunk_image:
                // TODO : first show select size screen
//                ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, TAG)).addToCart("1", "1", "10");
                break;
        }
    }

    private void toggleSpiedState() {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(spiedImage, "rotationX", 90f, 0.0f);
        objectAnimator.setDuration(600);
        //objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setStartDelay(0);
        objectAnimator.start();
//        if (0 == product.getIsSpied()) {
        if (!product.getIsSpied()) {
            spiedImage.setImageResource(R.drawable.prodlist_spi_enabled);
//            product.setIsSpied(1);
            product.setIsSpied(true);
            addToSpied();
            if(!product.getImage().isEmpty()) {
                mShareToPinterest.postToPinterest(product.getImage().get(0).getOriginal().getUrl()); //make changes in respective spy buttons
            }

        } else {
            spiedImage.setImageResource(R.drawable.spi_circle_black);
//            product.setIsSpied(0);
            product.setIsSpied(false);

            removeFromSpied();
        }
        spiedImage.invalidate();
    }


    private void removeFromSpied() {
//        ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).removeSpied(product.getId());
        ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, "")).removeSpied(product.getId());
    }

    private void addToSpied() {
//        ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).setSpied(product.getId());
        ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, "")).setSpied(product.getId());
    }

    @Override
    public void onSizeSelected(String productId) {

//        if(ProductAdapter.CURRENT_REGISTER_BUS_EVENT_TAG.equals("SpyActivity")) {
//            ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, ProductAdapter.CURRENT_REGISTER_BUS_EVENT_TAG))
//                    .addToCart(product.getId(), "1", sizeId,
////                        product.getSizes().get(0).getAttribute_id());
//                            product.getSize().get(0).getAttribute_id());
//        }
    }
}