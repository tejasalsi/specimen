package com.specimen.product.productdetails;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.ImageEntity;
import com.specimen.core.model.Look;
import com.specimen.core.model.ProductDetailResponseData;
import com.specimen.core.model.ProductSizesListParable;
import com.specimen.core.model.Rating;
import com.specimen.core.product.IProductFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddOrRemoveFromWishListResponse;
import com.specimen.core.response.CartDetailsResponse;
import com.specimen.core.response.ProductDetailsResponse;
import com.specimen.core.response.SpyAndCartItemCountResponse;
import com.specimen.core.spy.ISpyFacade;
import com.specimen.core.spyandcartitemcount.ISpyAndCartItemCountFacade;
import com.specimen.home.OnSizeSelectedListener;
import com.specimen.home.deal.DealFragment;
import com.specimen.home.deal.IUpdateTimer;
import com.specimen.look.LookDetailActivity;
import com.specimen.trustcircle.TrustCircleActivity;
import com.specimen.ui.dialog.SizeDialog;
import com.specimen.util.GaugeView;
import com.specimen.util.ImageUtils;
import com.specimen.util.MyTagHandler;
import com.specimen.util.NumberFormatsUtil;
import com.specimen.util.StoreAndShareProductBitmapUtil;
import com.specimen.widget.CirclePageIndicator;
import com.specimen.widget.NiddleAnimation;
import com.specimen.widget.ProgressView;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by root on 5/8/15.
 */
public class ProductDetailsActivity extends BaseActivity implements IResponseSubscribe, OnSizeSelectedListener {

    private final String TAG = "ProductDetailsActivity";
    private ViewPager mViewPager;
    private ProgressView mProgressView;
    private Button btnSpy;
    private TextView textHR, textMIN, textSec, textFriends, tvSKU;
    private TextView lblCartCount;
    private ProductDetailResponseData productDetailsResponseData;
    private DisplayMetrics metrics;

    private List<ImageEntity> imageEntityList;
    private String productId;
    private String currentPrice;
    private int remainingStock;
    private String originalPrice;
    private int hrs = 0, mins = 0, secs = 0;
    private boolean isDealProduct;
    private long nCounter = 0;
    private Timer mTimer;

    private Animation animatorSet;
    private GaugeView mGaugeView;

    //Just for testing
    private float degree = -225;
    private float sweepAngleControl = 0;
    private float sweepAngleFirstChart = 1;
    private float sweepAngleSecondChart = 1;
    private float sweepAngleThirdChart = 1;

    PopupWindow popupWindow;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        findViewById(R.id.scrPDP).setVisibility(View.GONE);

        lblCartCount = (TextView) findViewById(R.id.lblCartCount);
        animatorSet = AnimationUtils.loadAnimation(this, R.anim.shake);

        productId = getIntent().getStringExtra("PRODUCT_ID");
        isDealProduct = getIntent().getBooleanExtra("IS_DEAL_PRODUCT", false);
        mGaugeView = (GaugeView) findViewById(R.id.tickerGaugeNiddle);
        tvSKU = (TextView) findViewById(R.id.tvSKU);

        if (isDealProduct) {
            remainingStock = getIntent().getIntExtra("REMAINING_STOCK", 0);
            currentPrice = getIntent().getStringExtra("CURRENT_PRICE");
            originalPrice = getIntent().getStringExtra("ORIGINAL_PRICE");
            hrs = Integer.parseInt(getIntent().getStringExtra("HOURS"));
            mins = Integer.parseInt(getIntent().getStringExtra("MINUTES"));
            secs = Integer.parseInt(getIntent().getStringExtra("SECONDS"));
            startRunningGauge();
        }

        textFriends = (TextView) findViewById(R.id.textFriends);
        mGaugeView.setRotateDegree(degree);

        String friendsCount = ((IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, "")).getOfflineResponse("fbFriendsCount");
        textFriends.setText((TextUtils.isEmpty(friendsCount) ? "0" : friendsCount) + " ARE FRIENDS");

        Log.d(TAG, "PDP Product Id : " + productId);

        mProgressView = (ProgressView) findViewById(R.id.progressView);
        mProgressView.setVisibility(View.VISIBLE);
        mProgressView.setAnimation();
        ((IProductFacade) IOCContainer.getInstance().getObject(ServiceName.PRODUCT_SERVICE, TAG)).getProductDetail(productId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        ((ISpyAndCartItemCountFacade) IOCContainer.getInstance().getObject(ServiceName.SPYANDCART_ITEM_COUNT_SERVICE, TAG)).getTotalSpyCartCount();
    }

    @Override
    protected void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);

        if (null != popupWindow) {
            popupWindow.dismiss();
        }
    }

    public void recursiveDealAPICall() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ((IProductFacade) IOCContainer.getInstance().getObject(ServiceName.PRODUCT_SERVICE, TAG)).getProductDetail(productId);
            }
        }, (1000 * 180)); // 3 minutes delay (takes millis)
    }

    void setNiddleAnimation() {
        NiddleAnimation niddleAnimation = new NiddleAnimation(0, 200, 0, 500);
        niddleAnimation.setInterpolator(new LinearInterpolator());
        niddleAnimation.setDuration(3000);
        niddleAnimation.setFillAfter(true);
        niddleAnimation.setRepeatCount(Animation.INFINITE);
    }

    void openBannerImages() {
        DialogFragment pdpImageDialogFragment = new PDPImageDialogFragment(productDetailsResponseData.getImages());
        pdpImageDialogFragment.show(getSupportFragmentManager(), "");
    }

    void setBannerImages() {
        metrics = getResources().getDisplayMetrics();
        imageEntityList = productDetailsResponseData.getImages();
        mViewPager = (ViewPager) findViewById(R.id.pdpBannerImgPager);
        if (null != imageEntityList && 0 < imageEntityList.size()) {
            mViewPager.setAdapter(new ImageAdapter(this));
            CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.circle_pageIndicator);
           if(imageEntityList.size() == 1)
           {
               indicator.setVisibility(View.INVISIBLE);
           }
            else
           {
               indicator.setViewPager(mViewPager);
               indicator.setSnap(true);
           }

        } else {
            Toast.makeText(ProductDetailsActivity.this, "Server encountered an error while retrieving images", Toast.LENGTH_SHORT).show();
        }
    }

    void buttonAddToTrunk() {
        TextView lblProductPrice = (TextView) findViewById(R.id.lblProductPrice);
        Button btnAddCart = (Button) findViewById(R.id.btnBuyNow);
        if (isDealProduct) {
            if (null == currentPrice) {
                btnAddCart.setText("BUY " + getResources().getString(R.string.rupee_symbole) + " " + originalPrice);
                lblProductPrice.setText("BUY " + getResources().getString(R.string.rupee_symbole) + " " + originalPrice);
            } else {
                btnAddCart.setText("BUY " + getResources().getString(R.string.rupee_symbole) + " " + currentPrice);
                lblProductPrice.setText("BUY " + getResources().getString(R.string.rupee_symbole) + " " + currentPrice);
            }
        } else {
            if (null != productDetailsResponseData.getSpecialprice() || !TextUtils.isEmpty(productDetailsResponseData.getSpecialprice())) {
                btnAddCart.setText("BUY " + getResources().getString(R.string.rupee_symbole) + " " + productDetailsResponseData.getSpecialprice());
                lblProductPrice.setText("BUY " + getResources().getString(R.string.rupee_symbole) + " " + productDetailsResponseData.getSpecialprice());
            } else {
                btnAddCart.setText("BUY " + getResources().getString(R.string.rupee_symbole) + " " + productDetailsResponseData.getPrice());
                lblProductPrice.setText("BUY " + getResources().getString(R.string.rupee_symbole) + " " + productDetailsResponseData.getPrice());
            }
        }

        btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (productDetailsResponseData.getSizes() != null) {
                    int size = productDetailsResponseData.getSizes().size();
                    List<Boolean> checkProductSizesIfBlank = new ArrayList<>();
                    for (int i = 0; i < size; i++) {
                        if (productDetailsResponseData.getSizes().get(i).getStock_count() == 0) {
                            checkProductSizesIfBlank.add(true);
                        }
                    }

                    if (0 < checkProductSizesIfBlank.size() && size == checkProductSizesIfBlank.size()) {
                        Toast.makeText(ProductDetailsActivity.this, "Apologies, this product is out of stock.", Toast.LENGTH_SHORT).show();
                    } else {
                        SizeDialog sizeDialog = new SizeDialog();
                        sizeDialog.setOnSizeSelectedListner(ProductDetailsActivity.this);
                        Bundle bundle = new Bundle();
                        ProductSizesListParable productSizesListParable = new ProductSizesListParable(productDetailsResponseData.getSizes());
                        bundle.putParcelable("product", productSizesListParable);
                        bundle.putString("productId", productDetailsResponseData.getId());
                        bundle.putString("attributeId", productDetailsResponseData.getSizes().get(0).getAttribute_id());
                        bundle.putString("tag", TAG);
                        sizeDialog.setArguments(bundle);
                        sizeDialog.show(getSupportFragmentManager(), TAG);


                    }
                } else {
                    Toast.makeText(ProductDetailsActivity.this, "Apologies, this product is out of stock.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (productDetailsResponseData.getQty() == null || TextUtils.isEmpty(productDetailsResponseData.getQty())) {
            btnAddCart.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
            btnAddCart.setTextColor(Color.BLACK);
            btnAddCart.setEnabled(false);
        }

        if (productDetailsResponseData.getQty() != null && !TextUtils.isEmpty(productDetailsResponseData.getQty()) && 0 == Integer.parseInt(productDetailsResponseData.getQty())) {
            btnAddCart.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
            btnAddCart.setTextColor(Color.BLACK);
            btnAddCart.setEnabled(false);
        }

    }

    void buttonAskMate() {
        ImageView askMateButton = (ImageView) findViewById(R.id.imgAskaMate);
        askMateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent askMateIntent = new Intent(ProductDetailsActivity.this, TrustCircleActivity.class);
                askMateIntent.putExtra("product_id", productId);
                startActivity(askMateIntent);
                finish();
            }
        });
    }

    void buttonSpy() {
        btnSpy = (Button) findViewById(R.id.btnSpy);

        if (!TextUtils.isEmpty(productDetailsResponseData.getIsSpied()) && "true".equalsIgnoreCase(productDetailsResponseData.getIsSpied())) {
            btnSpy.setBackgroundResource(R.drawable.spy_btn_green);


            }
            else
            {
                btnSpy.setBackgroundResource(R.drawable.spy_btn_hover);
            }


            btnSpy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //mProgressView.setVisibility(View.VISIBLE);
                  //  ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).setSpied(productId);
                    // Total SPY

                    if (!TextUtils.isEmpty(productDetailsResponseData.getIsSpied()) && "true".equalsIgnoreCase(productDetailsResponseData.getIsSpied()))

                    {
                            ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).removeSpied(productId);

                    }
                        else
                        {

                           ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).setSpied(productId);
                    }



                }
            });
        }


    void setRatings() {
        // set feedback
        int feedbackCount = (productDetailsResponseData.getFeeabackCount() != null
                && !TextUtils.isEmpty(productDetailsResponseData.getFeeabackCount()))
                ? Integer.parseInt(productDetailsResponseData.getFeeabackCount()) : 0;

        if (0 == feedbackCount || 1 == feedbackCount) {
            ((TextView) findViewById(R.id.lblFeedbackCountFromCustomers))
                    .setText("FROM " + feedbackCount + " CUSTOMER");
        } else {
            ((TextView) findViewById(R.id.lblFeedbackCountFromCustomers))
                    .setText("FROM " + feedbackCount + " CUSTOMERS");
        }

        Rating rating = productDetailsResponseData.getRating();

        LinearLayout linear_style = (LinearLayout) findViewById(R.id.linear_style);
        LinearLayout linear_fit = (LinearLayout) findViewById(R.id.linear_fit);
        LinearLayout linear_tailoring = (LinearLayout) findViewById(R.id.linear_tailoring);
        LinearLayout linear_fabric = (LinearLayout) findViewById(R.id.linear_fabric);

        if (null != rating) {
            int styleRatingCount = rating.getStyle();
            int fitRatingCount = rating.getFit();
            int tailoringRatingCount = rating.getTailoring();
            int fabricRatingCount = rating.getFabric();

            for (int i = 0; i < styleRatingCount; i++) {
                setRatingImages((ImageView) linear_style.getChildAt(i));
            }

            for (int i = 0; i < fitRatingCount; i++) {
                setRatingImages((ImageView) linear_fit.getChildAt(i));
            }

            for (int i = 0; i < tailoringRatingCount; i++) {
                setRatingImages((ImageView) linear_tailoring.getChildAt(i));
            }

            for (int i = 0; i < fabricRatingCount; i++) {
                setRatingImages((ImageView) linear_fabric.getChildAt(i));
            }
        }
    }

    void setRatingImages(final ImageView imgView) {
        imgView.setBackgroundResource(R.drawable.bulb_green);
    }

    void setGoesWithProducts() {
        ImageView imgGoesWith = (ImageView) findViewById(R.id.imgGoesWith);
        final Look lookEntity = productDetailsResponseData.getLook();
        if (null != lookEntity) {
            if (null != lookEntity.getImage() && 0 != lookEntity.getImage().size()) {
                findViewById(R.id.linear_goeswith).setVisibility(View.VISIBLE);

                TextView lblGoeswithDescription = ((TextView) findViewById(R.id.lblGoeswithDescription));
                lblGoeswithDescription.setText(lookEntity.getDescription());
                lblGoeswithDescription.setVisibility(View.VISIBLE);

                Picasso.with(this)
                        .load(ImageUtils.getFullSizeImage(lookEntity.getImage().get(0), metrics))
                        .placeholder(R.drawable.banner_explore_placeholder)
                        .fit()
                        .into(imgGoesWith);

                imgGoesWith.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(v.getContext(), LookDetailActivity.class);
                        intent.putExtra("look", lookEntity);
                        v.getContext().startActivity(intent);
                        finish();
                    }
                });
            } else {
                findViewById(R.id.linear_goeswith).setVisibility(View.GONE);
            }
        }
//        else if (null != productDetailsResponseData.getIdentical()) {
//            Product product = productDetailsResponseData.getIdentical();
//            if (null != product) {
//                findViewById(R.id.linear_goeswith).setVisibility(View.VISIBLE);
//                ((TextView) findViewById(R.id.lblGoeswithDescription)).setText(productDetailsResponseData.getIdentical().getName());
//
//                if (product.getImage() != null && product.getImage().size() > 0) {
//                    Picasso.with(this).load(ImageUtils.getProductImageUrl(productDetailsResponseData.getIdentical().getImage().get(0), metrics)).placeholder(R.drawable.product_list_placeholder).into(imgGoesWith);
//                } else {
//                    Picasso.with(this).load(R.drawable.product_list_placeholder).into(imgGoesWith);
//                }
//                imgGoesWith.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                });
//            }
//        }
    }

    void setToolbarOptions() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.imgTrunk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent trunkIntent = new Intent();
                trunkIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                trunkIntent.setClass(ProductDetailsActivity.this, MainActivity.class);
                trunkIntent.putExtra("tag", MainActivity.TRUNK);
                startActivity(trunkIntent);
//                finish();
            }
        });

        findViewById(R.id.imgAskaMate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent trunkIntent = new Intent();
                trunkIntent.setClass(ProductDetailsActivity.this, TrustCircleActivity.class);
                trunkIntent.putExtra("product_id", productId);
                startActivity(trunkIntent);
                finish();
            }
        });

        findViewById(R.id.imgShare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView imageView = (ImageView) mViewPager.findViewWithTag("specimen" + mViewPager.getCurrentItem());
                StoreAndShareProductBitmapUtil bitmapUtil = new StoreAndShareProductBitmapUtil(ProductDetailsActivity.this);
                Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                bitmapUtil.storeImage(bitmap, true);
                ((IProductFacade) IOCContainer.getInstance().getObject(ServiceName.PRODUCT_SERVICE, "productsharecount")).productsharecount(productId);
            }
        });
    }

    void setNotes() {
        String notes = productDetailsResponseData.getNotes();

        ((TextView) findViewById(R.id.lblNOtesContent)).setText(Html.fromHtml(notes));

        String details = productDetailsResponseData.getDetails();
        final TextView lblDetails = (TextView) findViewById(R.id.lblDetails);

        if (!TextUtils.isEmpty(details)) {
            lblDetails.setText(Html.fromHtml(details, null, new MyTagHandler()));
        } else {
            lblDetails.setText("Description not available.");
        }

        final ImageView lblCollapse = (ImageView) findViewById(R.id.lblCollapse);

        lblCollapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lblDetails.getVisibility() == View.VISIBLE) {
                    lblDetails.setVisibility(View.GONE);
                    lblCollapse.setImageResource(R.drawable.details_btn_add);

                } else {
                    lblDetails.setVisibility(View.VISIBLE);
                    lblCollapse.setImageResource(R.drawable.details_btn_remove);
                }
            }
        });
    }

    void setUIbinding() {
        if (TextUtils.isEmpty(productDetailsResponseData.getName())) {
            Toast.makeText(ProductDetailsActivity.this, "Error occurred while showing product details.", Toast.LENGTH_SHORT).show();
            return;
        }

        ((TextView) findViewById(R.id.lblProductName)).setText(productDetailsResponseData.getName());

        // set notes
        setNotes();

        // Total SPY
        ((TextView) findViewById(R.id.lblTotalSpy))
                .setText((!TextUtils.isEmpty(productDetailsResponseData.getSpied_count()) ?
                        productDetailsResponseData.getSpied_count() : "0") + " SPYING");

        // Total VIEWS
        ((TextView) findViewById(R.id.lbl_open_deal_TotalViews))
                .setText((!TextUtils.isEmpty(productDetailsResponseData.getView_count()) ?
                        productDetailsResponseData.getView_count() : "0") + " VIEWS");

        tvSKU.setText("SKU : " + productDetailsResponseData.getSku());
        // button buy now
        buttonAddToTrunk();

        //button ask a mate
        buttonAskMate();

        // button spy
        buttonSpy();

        // set banner image
        setBannerImages();

        // set Ratings
        setRatings();

        // set goes with section
        setGoesWithProducts();

        setToolbarOptions();

        if (isDealProduct) {
            // Set price
            findViewById(R.id.linear_price).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.lblOriginalPrice))
                    .setText(getResources().getString(R.string.rupee_symbole) + " " + productDetailsResponseData.getPrice());
            ((TextView) findViewById(R.id.lblTodaysLowPrice))
                    .setText(getResources().getString(R.string.rupee_symbole) + " " + productDetailsResponseData.getLow_price());
            ((TextView) findViewById(R.id.lblTodaysHighPrice))
                    .setText(getResources().getString(R.string.rupee_symbole) + " " + productDetailsResponseData.getHigh_price());

            // set stock
            ((TextView) findViewById(R.id.lblRemainingStock))
                    .setText(remainingStock + " LEFT");

            // set timer animation
            findViewById(R.id.timerGuageLayout).setVisibility(View.VISIBLE);

            ImageView imageView = (ImageView) findViewById(R.id.imgUpDownPrice);
            imageView.setVisibility(View.VISIBLE);

            if (null != currentPrice || !TextUtils.isEmpty(currentPrice)) {
                DecimalFormat df = new DecimalFormat();
                DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                symbols.setDecimalSeparator(',');
                symbols.setGroupingSeparator(' ');
                df.setDecimalFormatSymbols(symbols);

                Number number1, number2;
                try {
                    currentPrice = currentPrice.replaceAll(",","");
                    originalPrice = originalPrice.replaceAll(",","");

                    number1 = df.parse(currentPrice);
                    number2 = df.parse(originalPrice);

                    double doubleCurrentPrice = number1.doubleValue();
                    double doubleOriginalPrice = number2.doubleValue();

                    if (doubleCurrentPrice > doubleOriginalPrice) {
                        imageView.setImageResource(R.drawable.arrow_red_down);
                    } else {
                        imageView.setImageResource(R.drawable.arrow_green_up);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                imageView.setImageResource(R.drawable.arrow_green_up);
            }

            doTimerTask();

            setNiddleAnimation();

            recursiveDealAPICall();
        }
    }

    void setDealProductAnimation(long updatedTimerValue) {

        textHR = ((TextView) findViewById(R.id.hrtxt_pdp));
        textMIN = ((TextView) findViewById(R.id.mintxt_pdp));
        textSec = ((TextView) findViewById(R.id.sectxt_pdp));
        final String constantTimeString = "00:00:00";


        long hoursInmillis = hrs * (60 * 60 * 1000);
        long minsInMillis = mins * (60 * 1000);
        long secsInMillis = secs * 1000;

        final long dealTimeInMillis = hoursInmillis + minsInMillis + secsInMillis;
        final long updatedTimeInMillis = dealTimeInMillis - (updatedTimerValue * 1000);

        if (NumberFormatsUtil.isNegative(updatedTimeInMillis)) {
            textHR.setText("00");
            textMIN.setText("00");
            textSec.setText("00");
        } else {
            long sec = (updatedTimeInMillis / 1000) % 60;
            long hr = (updatedTimeInMillis / (1000 * 60 * 60));
            long min = (updatedTimeInMillis / (1000 * 60) % 60);
            textSec.setText("" + String.format("%02d", sec));
            textMIN.setText("" + String.format("%02d", min));
            textHR.setText("" + String.format("%02d", hr));

            ObjectAnimator secObjectAnimator = ObjectAnimator.ofFloat(textSec, "rotationX", 90f, -90f);
            secObjectAnimator.setDuration(1000);
            secObjectAnimator.setInterpolator(new LinearInterpolator());
            secObjectAnimator.start();

            if (sec == 59) {
                ObjectAnimator minObjectAnimator = ObjectAnimator.ofFloat(textMIN, "rotationX", 90f, 0.0f);
                minObjectAnimator.setDuration(800);
                minObjectAnimator.setInterpolator(new LinearInterpolator());
                minObjectAnimator.start();
            }
            if (min == 59) {
                ObjectAnimator hrObjectAnimator = ObjectAnimator.ofFloat(textHR, "rotationX", 90f, 0.0f);
                hrObjectAnimator.setDuration(800);
                hrObjectAnimator.setInterpolator(new LinearInterpolator());
                hrObjectAnimator.start();
            }

        }

    }

    private Handler messageHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            nCounter++;

            setDealProductAnimation(nCounter);

        }
    };

    public void doTimerTask() {
//        long hoursInmillis = hrs * (60 * 60 * 1000);
//        long minsInMillis = mins * (60 * 1000);
//        long secsInMillis = secs * 1000;

//        final long dealTimeInMillis = hoursInmillis + minsInMillis + secsInMillis;

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                messageHandler.sendEmptyMessage(0);
            }
        }, 0, 1000);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        mProgressView.setVisibility(View.GONE);
        if (!tag.equals(TAG)) {
            return;
        }

        if (response instanceof CartDetailsResponse) {
//            Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
            findViewById(R.id.imgTrunk).startAnimation(animatorSet);
            MainActivity.setCartCount(((CartDetailsResponse) response).getData().getshoppingcart_count());
            setCartCount(((CartDetailsResponse) response).getData().getshoppingcart_count());
            ((ISpyAndCartItemCountFacade) IOCContainer.getInstance().getObject(ServiceName.SPYANDCART_ITEM_COUNT_SERVICE, TAG)).getTotalSpyCartCount();
        }

        if (response instanceof ProductDetailsResponse) {
            findViewById(R.id.scrPDP).setVisibility(View.VISIBLE);
            productDetailsResponseData = ((ProductDetailsResponse) response).getData();
            Log.d(TAG, productDetailsResponseData.toString());
            setUIbinding();
        }
        if (response instanceof AddOrRemoveFromWishListResponse) {
            Toast.makeText(getApplicationContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
            MainActivity.setSpyCount(((AddOrRemoveFromWishListResponse) response).getData().getSpy_count());

            if (!TextUtils.isEmpty(productDetailsResponseData.getIsSpied()) && "true".equalsIgnoreCase(productDetailsResponseData.getIsSpied()))
            {
                btnSpy.setBackgroundResource(R.drawable.spy_btn_hover);
                ((TextView) findViewById(R.id.lblTotalSpy))
                        .setText((Integer.parseInt(productDetailsResponseData.getSpied_count())) + " SPYING");
            }
            else
            {
                ((TextView) findViewById(R.id.lblTotalSpy))
                        .setText((Integer.parseInt(productDetailsResponseData.getSpied_count()) + 1) + " SPYING");
                btnSpy.setBackgroundResource(R.drawable.spy_btn_green);
            }
            ((IProductFacade) IOCContainer.getInstance().getObject(ServiceName.PRODUCT_SERVICE, TAG)).getProductDetail(productId);

        }

        if (response instanceof SpyAndCartItemCountResponse) {
            SpyAndCartItemCountResponse spyAndCartItemCountResponse = (SpyAndCartItemCountResponse) response;
            Log.i(TAG, "spy count : " + spyAndCartItemCountResponse.getData().getSpy_count());
            Log.i(TAG, "cart count : " + spyAndCartItemCountResponse.getData().getShoppingcart_count());

            if (!TextUtils.isEmpty(spyAndCartItemCountResponse.getData().getShoppingcart_count())) {
                MainActivity.setCartCount(Integer.parseInt(spyAndCartItemCountResponse.getData().getShoppingcart_count()));
                setCartCount(Integer.parseInt(spyAndCartItemCountResponse.getData().getShoppingcart_count()));
            }

            MainActivity.setSpyCount(spyAndCartItemCountResponse.getData().getSpy_count());
        }
    }

    @Override
    public void onFailure(Exception error) {
        Log.e(TAG, "" + error.getMessage());
        mProgressView.setVisibility(View.GONE);
        findViewById(R.id.relativePDPMain).setVisibility(View.GONE);
    }

    @Override
    public void onSizeSelected(String sizeId) {
//        mProgressView.setVisibility(View.VISIBLE);
//        ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, TAG))
//                .addToCart(productDetailsResponseData.getId(), "1", sizeId,
//                        productDetailsResponseData.getSizes().get(0).getAttribute_id());

        View addView = LayoutInflater.from(ProductDetailsActivity.this).inflate(R.layout.animated_add_cart_alert_view, null);
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
//        ((RelativeLayout)findViewById(R.id.relativePDPMain)).addView(addView, params);

        popupWindow = new PopupWindow(
                addView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        Button btnDismiss = (Button) addView.findViewById(R.id.imgCancelPopup);
        btnDismiss.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (null != popupWindow)
                    popupWindow.dismiss();
            }
        });
        popupWindow.showAsDropDown(findViewById(R.id.imgTrunk), 50, (int) findViewById(R.id.imgTrunk).getY());
        // Closes the popup window when touch outside.
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(null);
        popupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });

        ((TextView) addView.findViewById(R.id.lblAnimatedCartProductName)).setText("" + productDetailsResponseData.getName());



        if (null != imageEntityList && 0 < imageEntityList.size()) {
            Picasso.with(ProductDetailsActivity.this)
                    .load(ImageUtils.getFullSizeImage(imageEntityList.get(0), metrics))
                    .placeholder(R.drawable.pdp_banner_placeholder)
                    .resize(metrics.widthPixels, metrics.heightPixels)
                    .centerInside()
                    .into((ImageView) addView.findViewById(R.id.imgAnimatedCart));
        }

        if (isDealProduct) {
            if (null == currentPrice) {
                ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + getResources().getString(R.string.rupee_symbole) + " " + originalPrice);
            } else {
                ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + getResources().getString(R.string.rupee_symbole) + " " + currentPrice);
            }
        } else {
            if (null != productDetailsResponseData.getSpecialprice() || !TextUtils.isEmpty(productDetailsResponseData.getSpecialprice())) {
                ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + getResources().getString(R.string.rupee_symbole) + " " + productDetailsResponseData.getSpecialprice());
            } else {
                ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + getResources().getString(R.string.rupee_symbole) + " " + productDetailsResponseData.getPrice());
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null != popupWindow)
                    popupWindow.dismiss();
            }
        }, 5000);

//        MainActivity.moveViewToScreenCenter(findViewById(R.id.btnBuyNow), addView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            Log.d("TIMER", "timer canceled");
            mTimer.cancel();
        }
    }


    public class ImageAdapter extends PagerAdapter {

        Context context;

        ImageAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return imageEntityList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            final ImageView imageView = new ImageView(context);
            Picasso.with(context)
                    .load(ImageUtils.getFullSizeImage(imageEntityList.get(position), metrics))
                    .placeholder(R.drawable.pdp_banner_placeholder)
                    .resize(metrics.widthPixels, 406)
                    .centerInside()
                    .into(imageView);

            container.addView(imageView, 0);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openBannerImages();
                }
            });
            imageView.setTag("specimen" + position);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ImageView) object);
        }
    }

    public void startRunningGauge() {
        new Thread() {
            public void run() {
                for (int i = 0; i < 250; i++) {
                    try {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                degree++;
                                sweepAngleControl++;
                                if (degree < 45) {
                                    mGaugeView.setRotateDegree(degree);
                                }

                                if (sweepAngleControl <= 90) {
                                    sweepAngleFirstChart++;
                                    mGaugeView.setSweepAngleFirstChart(sweepAngleFirstChart);
                                } else if (sweepAngleControl <= 180) {
                                    sweepAngleSecondChart++;
                                    mGaugeView.setSweepAngleSecondChart(sweepAngleSecondChart);
                                } else if (sweepAngleControl <= 270) {
                                    sweepAngleThirdChart++;
                                    mGaugeView.setSweepAngleThirdChart(sweepAngleThirdChart);
                                }

                            }
                        });
                        Thread.sleep(15);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }.start();
    }

    public void setCartCount(int cartCount) {

        if (0 != cartCount) {
            lblCartCount.setVisibility(View.VISIBLE);
            lblCartCount.setText("" + cartCount);
        } else {
            lblCartCount.setVisibility(View.GONE);
        }
    }
}
