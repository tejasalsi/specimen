package com.specimen.product.productdetails;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.specimen.R;
import com.specimen.widget.TouchImageView;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by root on 5/8/15.
 */
public class ProductDetailsPagerAdapter extends PagerAdapter {

    private Activity activity;
    private List<String> imagePaths;
    private LayoutInflater inflater;

    int metricsHeight, metricsWidth;

    public ProductDetailsPagerAdapter(Activity activity, List<String> imagePaths) {
        this.activity = activity;
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.imagePaths = imagePaths;
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        metricsHeight = metrics.heightPixels;
        metricsWidth = metrics.widthPixels;
    }

    @Override
    public int getCount() {
        return this.imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View viewLayout = inflater.inflate(R.layout.product_detail_full_screen_image_expand_zoom, container, false);
        TouchImageView imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
        Picasso.with(activity)
                .load(imagePaths.get(position))
                .placeholder(R.drawable.pdp_banner_placeholder)
                .resize(metricsWidth, metricsHeight)
                .centerInside()
                .into(imgDisplay);

        container.addView(viewLayout);
        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
