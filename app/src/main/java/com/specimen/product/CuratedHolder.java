package com.specimen.product;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.Curated;

/**
 * Created by Rohit on 11/17/2015.
 */
public class CuratedHolder extends RecyclerView.ViewHolder {

    private TextView header;
    private TextView description;

    public CuratedHolder(View itemView) {
        super(itemView);
        header = (TextView) itemView.findViewById(R.id.txt_lablename);
        description = (TextView) itemView.findViewById(R.id.txt_lableDES);
    }

    public void getView(Curated curated) {
        header.setText(curated.getName().toUpperCase());
        description.setText(curated.getDescription());
        description.setPaintFlags(description.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

}
