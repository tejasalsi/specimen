package com.specimen;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import com.specimen.core.IResponseSubscribe;
import com.specimen.core.response.APIResponse;
import com.specimen.listener.INetworkStateListener;
import com.specimen.trustcircle.MateAlertFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Tejas on 6/2/2015.
 * Base Activity containg all the fragments.
 */

public abstract class BaseActivity extends AppCompatActivity implements IResponseSubscribe, INetworkStateListener {


    private View mView;
    private WindowManager mWM;
    private String notificationFragment = null;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mWM = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        mView = inflater.inflate(R.layout.network_panel, null);

        notificationFragment = getIntent().getStringExtra("trustee_fragment");
        String product_id = getIntent().getStringExtra("product_id");

        if (notificationFragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            if (notificationFragment.equals("trustee_alert")) {
                MateAlertFragment mateAlertFragment = new MateAlertFragment();
                Bundle bundle = new Bundle();
                bundle.putString("product_id", product_id);
                mateAlertFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.askamate_container, mateAlertFragment, "MATE_ALERT");
                fragmentTransaction.addToBackStack("MATE_ALERT");
                fragmentTransaction.commit();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        NetworkStateWatcher.getInstance().register(this);
        if (NetworkStateWatcher.isConnectedToNetwork == 1) {
            hide();
        } else if (NetworkStateWatcher.isConnectedToNetwork == 0) {
            show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        NetworkStateWatcher.getInstance().unregister(this);
        hide();

    }

    @Override
    public void onSuccess(APIResponse response, String tag) {

    }

    @Override
    public void onFailure(Exception error) {
        //Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show();
    }

    public void switchFragment(Fragment fragment, boolean addtoBackStack, int layout_id) {
        switchFragment(fragment, addtoBackStack, layout_id, 0, 0);
    }

    public void switchFragment(Fragment fragment, boolean addtoBackStack, int layout_id, int enterAnimation, int exitAnimation) {
        switchFragment(fragment, addtoBackStack, layout_id, enterAnimation, exitAnimation, enterAnimation, exitAnimation);
    }

    public void switchFragment(Fragment fragment, boolean addtoBackStack,
                               int layout_id, int enterAnimation, int exitAnimation, int popEnterAnimation, int popExitAnimation) {
        setTitle(getString(R.string.app_name));
        if (!isFinishing()) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                    .beginTransaction();
            if (enterAnimation != 0 && exitAnimation != 0) {
                fragmentTransaction.setCustomAnimations(enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation);
            }
            fragmentTransaction.replace(layout_id, fragment);
            if (addtoBackStack) {
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction.commit();
        }

    }

    @Override
    public void onNetworkConnect() {

        hide();
    }

    @Override
    public void onNetworkDisconnect() {

        show();
    }

    public void show() {

        if (mView != null) {

//            WindowManager.LayoutParams mParams = new WindowManager.LayoutParams();
//            final Resources resources = getResources();
////            mParams.height = 200;
//            final Configuration config = mView.getContext().getResources().getConfiguration();
//            int gravity = 0;
//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
//                gravity = Gravity.getAbsoluteGravity(Gravity.BOTTOM, config.getLayoutDirection());
//            }
//
//            resources.updateConfiguration(config, resources.getDisplayMetrics());
//
//            if ((gravity & Gravity.HORIZONTAL_GRAVITY_MASK) == Gravity.FILL_HORIZONTAL) {
//                mParams.horizontalWeight = 1.0f;
//            }
//            if ((gravity & Gravity.VERTICAL_GRAVITY_MASK) == Gravity.FILL_VERTICAL) {
//                mParams.verticalWeight = 1.0f;
//            }
//            mParams.x = 0;
//            mParams.y = 0;
//            mParams.verticalMargin = 0;
//            mParams.horizontalMargin = 0;
//            mParams.flags = mParams.FLAG_NOT_TOUCH_MODAL | mParams.FLAG_NOT_FOCUSABLE;
//            mParams.packageName = getPackageName();

            WindowManager.LayoutParams mParams = new WindowManager.LayoutParams();
            mParams.gravity = Gravity.BOTTOM;
            mParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            mParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            mParams.type = WindowManager.LayoutParams.TYPE_TOAST;
            mParams.flags = mParams.FLAG_NOT_TOUCH_MODAL | mParams.FLAG_NOT_FOCUSABLE;
            mParams.packageName = getPackageName();

            if (mView.getParent() != null) {
                mWM.removeView(mView);
            }
            mWM.addView(mView, mParams);
        }
    }


    public void hide() {

        if (mView != null) {
            // note: checking parent() just to make sure the view has
            // been added...  i have seen cases where we get here when
            // the view isn't yet added, so let's try not to crash.
            if (mView.getParent() != null) {

                mWM.removeView(mView);
            }
        }
    }
}
