package com.specimen;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.specimen.core.IResponseSubscribe;
import com.specimen.core.response.APIResponse;

/**
 * Created by Intelliswift on 7/30/2015.
 */
public class BaseFragment extends Fragment implements IResponseSubscribe {

    protected BaseActivity mContext;

    public BaseFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = (BaseActivity) activity;
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {

    }

    @Override
    public void onFailure(Exception error) {

    }


}
