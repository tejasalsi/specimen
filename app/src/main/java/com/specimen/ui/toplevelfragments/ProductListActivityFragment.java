package com.specimen.ui.toplevelfragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.Subcategory;
import com.specimen.util.BusProvider;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class ProductListActivityFragment extends Fragment //implements View.OnClickListener {
{


    public ProductListActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        return inflater.inflate(R.layout.fragment_product_list, container, false);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }


}
