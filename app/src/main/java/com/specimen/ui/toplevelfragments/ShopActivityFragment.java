package com.specimen.ui.toplevelfragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.category.ICategoryFacade;
import com.specimen.core.model.TopCategory;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.CategoryListResponse;
import com.specimen.shop.SpecimenCategoryPagerAdapter;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class ShopActivityFragment extends Fragment implements IResponseSubscribe {

    private ViewPager viewPager;
    public final String TAG = "SHOP";
    ArrayList<TopCategory> tabTitles;
    PagerSlidingTabStrip tabs;
    APIResponse response = null;

    public ShopActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.categoryViewPager);
        tabTitles = new ArrayList<>();


        // tabTitles = {"TOPS","BOTTOMS","SUITS","ACCESSORIES","LOOKS"};


        // Bind the tabs to the ViewPager
        tabs = (PagerSlidingTabStrip) view.findViewById(R.id.categoryTabs);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        if (response == null) {
            ((ICategoryFacade) IOCContainer.getInstance().getObject(ServiceName.CATEGORY_SERVICE, TAG)).getCategoryList();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        this.response = response;
        if (!tag.equals(TAG)) {
            return;
        }
        if (response instanceof CategoryListResponse) {
            Log.d(TAG, ((CategoryListResponse) response).getData().toString());
            for (int i = 0; i < ((CategoryListResponse) response).getData().size(); i++) {
                tabTitles.add(((CategoryListResponse) response).getData().get(i));
            }
            Log.d(TAG, tabTitles.toString());
            //tabTitles = ((CategoryListResponse) response).getData().toString();
            SpecimenCategoryPagerAdapter specimenCategoryPagerAdapter = new SpecimenCategoryPagerAdapter(getActivity(), getChildFragmentManager(), tabTitles);
            viewPager.setAdapter(specimenCategoryPagerAdapter);

            tabs.setViewPager(viewPager);

            specimenCategoryPagerAdapter.notifyDataSetChanged();

        }

    }

    @Override
    public void onFailure(Exception error) {
        Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
        Log.e(TAG, error.toString());
    }
}
