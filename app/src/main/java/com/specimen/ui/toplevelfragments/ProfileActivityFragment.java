package com.specimen.ui.toplevelfragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.specimen.R;
import com.specimen.trustcircle.TrustCircleActivity;
import com.specimen.util.CircleTransform;
import com.squareup.picasso.Picasso;

/**
 * A placeholder fragment containing a simple view.
 */
public class ProfileActivityFragment extends Fragment implements View.OnClickListener {

    ImageView profileImage;
    LinearLayout trustCircleLayout;

    public ProfileActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_profile, container, false);
        profileImage = (ImageView) view.findViewById(R.id.profile_image);
        trustCircleLayout = (LinearLayout) view.findViewById(R.id.trustCircleProfileLayout);
        Picasso.with(getActivity())
                .load(R.drawable.profile_image)
                .resize(76, 76)
                .transform(new CircleTransform()).into(profileImage);

        trustCircleLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), TrustCircleActivity.class));

            }
        });


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.trustCircleProfileLayout:
                startActivity(new Intent(getActivity(), TrustCircleActivity.class));
                break;
        }
    }
}
