package com.specimen.util;

import android.app.Activity;

import com.facebook.login.LoginManager;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Arrays;

/**
 * Created by Tejas on 8/18/2015.
 */
public class PerformSocialLogin {

    Activity activity;
    GoogleApiClient googleApiClient;

    public PerformSocialLogin(Activity activity, GoogleApiClient googleApiClient) {
        this.activity = activity;
        this.googleApiClient = googleApiClient;
    }

    public PerformSocialLogin(Activity activity) {
        this.activity = activity;
    }

    public void initiateSocialLogIn(int logInType, LoginManager fbLoginManager) {
        //perform facebook login
        if (logInType == 1) {
            fbLoginManager.logInWithReadPermissions(activity, Arrays.asList("public_profile", "user_friends", "email"));
        }
        //perform google plus login
        else if (logInType == 2) {
            googleApiClient.connect();
        }
    }
}
