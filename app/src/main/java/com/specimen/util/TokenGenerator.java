package com.specimen.util;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by Tejas on 6/8/2015.
 * The unique token will be generated while login/registration
 */
public final class TokenGenerator {

    private SecureRandom randomToken;

    public TokenGenerator() {
        randomToken = new SecureRandom();
    }

    public String generateToken() {
        return new BigInteger(130, randomToken).toString(32);
    }

}
