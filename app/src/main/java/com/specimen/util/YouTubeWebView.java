package com.specimen.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.webkit.WebView;
import android.widget.Toast;

/**
 * Created by Tejas on 10/15/2015.
 */
public class YouTubeWebView extends WebView {

    Context mContext;

    public YouTubeWebView(Context context){
        super(context);
        mContext = context;
        getSettings().setJavaScriptEnabled(true);
    }

    public YouTubeWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        getSettings().setJavaScriptEnabled(true);
    }

    public YouTubeWebView(Context context, AttributeSet attrs, int defStyleAttr){
        super(context, attrs, defStyleAttr);
        mContext = context;
        getSettings().setJavaScriptEnabled(true);
    }

    public void loadVideo(String videoURL) {
        if (!TextUtils.isEmpty(videoURL)) {
            String playVideo = "<html><body><iframe class=\"youtube-player\" type=\"text/html\" width=\"" + 300 + "\" height=\"" + 200 + "\" src=\"http://www.youtube.com/embed/" + videoURL + "\" frameborder=\"0\"></body></html>";
            loadData(playVideo, "text/html", "utf-8");
        } else {
            Toast.makeText(mContext, "Invalid video URL.", Toast.LENGTH_SHORT).show();
        }
    }
}
