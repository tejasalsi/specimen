package com.specimen.util;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by root on 5/10/15.
 */
public class StoreAndShareProductBitmapUtil {

    final String TAG = "StoreAndShareProductBitmapUtil";
    Activity mActivity;
    String mImageName;

    public StoreAndShareProductBitmapUtil(Activity activity) {
        this.mActivity = activity;
    }

    private void shareProduct(String imagePath) {
        Uri uri = Uri.fromFile(new File(imagePath));
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "SPECIMEN PRODUCT DETAILS SHARE TEST TEXT");
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("text/plain");
        shareIntent.setType("image/*");
        mActivity.startActivity(Intent.createChooser(shareIntent, ""));
    }

    public void storeImage(Bitmap bitmapImage, boolean canShare) {
        File pictureFile = getOutputMediaFile();
        if (null == bitmapImage || pictureFile == null) {
            //Log.d(TAG, "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
            if (canShare) shareProduct(pictureFile.getPath());
        } catch (FileNotFoundException e) {
           // Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
           // Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + mActivity.getApplicationContext().getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        mImageName = "Specimen_" + timeStamp + ".jpg";
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public String getStoreImageName() {
        return mImageName;
    }

}
