package com.specimen.citruspayment;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;

/**
 * Created by Rohan_2 on 3/1/2016.
 */
public class BankIconGridAdapter extends BaseAdapter {

    private Context context;
    int[] bankIcon = {R.drawable.axisbank,R.drawable.citibank,R.drawable.hdfcbank,R.drawable.icicibank,R.drawable.kotakbank,R.drawable.statebank};
   String[] bankName = {"AXIS","Citi","HDFC","ICICI","KOTAK","SBI"};
    private int selectedIndex = -1;
    public BankIconGridAdapter(Context context)
    {
        this.context = context;
    }
    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(view == null)
        {
            view = inflater.inflate(R.layout.bank_icon_row,null,false);
        }

        ImageView bankImage = (ImageView)view.findViewById(R.id.img_bank_icon);
        TextView bankNameTxt = (TextView)view.findViewById(R.id.txtview_bank_name);

        bankImage.setImageResource(bankIcon[position]);
        bankNameTxt.setText(""+bankName[position]);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedIndex = position;
                notifyDataSetChanged();
            }
        });
        if(position == selectedIndex)
        {
          bankImage.setBackgroundResource(R.drawable.bg_selected_bank);
            bankNameTxt.setTypeface(null, Typeface.BOLD);
        }
        else
        {
          bankImage.setBackgroundResource(R.drawable.bank_bg);
            bankNameTxt.setTypeface(null, Typeface.NORMAL);
        }

        return view;
    }
    public void deselectAll()
    {
        selectedIndex = -1;
        notifyDataSetChanged();
    }
}
