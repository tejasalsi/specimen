package com.specimen.citruspayment;

import java.util.ArrayList;

/**
 * Created by Swapnil on 12/4/2015.
 */
public class NetBankingIssuer {

    public String bankName;
    public String issuerCode;

    public ArrayList<NetBankingIssuer> netBankingIssuers;

    public NetBankingIssuer(){

    }

    public NetBankingIssuer(String bankName, String issuerCode) {
        this.bankName = bankName;
        this.issuerCode = issuerCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getIssuerCode() {
        return issuerCode;
    }

    public void setIssuerCode(String issuerCode) {
        this.issuerCode = issuerCode;
    }

    public ArrayList<NetBankingIssuer> getBanks(){
        netBankingIssuers = new ArrayList<NetBankingIssuer>();

        netBankingIssuers.add(new NetBankingIssuer("AXIS Bank","CID002"));
        netBankingIssuers.add(new NetBankingIssuer("SBI Bank","CID005"));
        netBankingIssuers.add(new NetBankingIssuer("ICICI Bank","CID001"));
        netBankingIssuers.add(new NetBankingIssuer("CITI Bank","CID003"));
        netBankingIssuers.add(new NetBankingIssuer("HDFC Bank","CID010"));
        netBankingIssuers.add(new NetBankingIssuer("Kotak Mahindra Bank","CID033"));

        return netBankingIssuers;

    }
}
