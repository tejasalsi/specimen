package com.specimen.citruspayment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.Address;

/**
 * Created by DhirajK on 12/7/2015.
 */
public class SetAmountAddressData {

    public void setAmountAndAdreesDataToView(View view, Bundle bundle) {
        TextView itemCountSummary, orderTotalPriceSummary, totalPriceSummary;
        TextView shippingTo, shippingAddress1, shippingAddress2, shippingAddress3, shippingAddressCity, shippingAddressState, shippingContact;


        itemCountSummary = (TextView) view.findViewById(R.id.itemCountSummary);
        orderTotalPriceSummary = (TextView) view.findViewById(R.id.orderTotalPriceSummary);
        totalPriceSummary = (TextView) view.findViewById(R.id.totalPriceSummary);

        shippingTo = (TextView) view.findViewById(R.id.shippingTo);
        shippingAddress1 = (TextView) view.findViewById(R.id.shippingAddress1);
        shippingAddress2 = (TextView) view.findViewById(R.id.shippingAddress2);
        shippingAddress3 = (TextView) view.findViewById(R.id.shippingAddress3);
        shippingAddressCity = (TextView) view.findViewById(R.id.shippingAddressCity);
        shippingAddressState = (TextView) view.findViewById(R.id.shippingAddressState);
        shippingContact = (TextView) view.findViewById(R.id.shippingContact);

        String itemCount = bundle.getString("CART_PRODUCT_COUNT");
        if (1 < Integer.parseInt(itemCount)) {
            itemCount = itemCount + " Item";
        } else {
            itemCount = itemCount + " Items";
        }
        itemCountSummary.setText(itemCount);
        orderTotalPriceSummary.setText(view.getContext().getResources().getString(R.string.rupee_symbole) + " " + bundle.getString("TOTAL"));
        totalPriceSummary.setText(view.getContext().getResources().getString(R.string.rupee_symbole) + " " + bundle.getString("TOTAL"));
        Address address = bundle.getParcelable("address");
        if (address.getIsPrimary()) {

            shippingTo.setText("" + address.getName());

            if (!TextUtils.isEmpty(address.getAddress()))
                shippingAddress1.setText("" + address.getAddress());
            else
                shippingAddress1.setVisibility(View.GONE);

//            if (!TextUtils.isEmpty(address.getAddress1()))
//                shippingAddress2.setText("" + address.getAddress1());
//            else
                shippingAddress2.setVisibility(View.GONE);

//            shippingAddress3.setText("" + address.getAddress2() + "\n" + address.getLocality() + " " + address.getLandmark());
            shippingAddress3.setText("\n" + address.getLocality());
            shippingAddressCity.setText("" + address.getCity() + " - " + address.getPincode());
            shippingAddressState.setText("" + address.getState());
            shippingContact.setText("Mobile: " + address.getMobile_no());

        }
    }
}
