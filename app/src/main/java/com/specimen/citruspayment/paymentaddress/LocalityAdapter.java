package com.specimen.citruspayment.paymentaddress;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.response.LocalityResponse.DataEntity.LocalityEntity;

import java.util.List;

/**
 * Created by root on 3/1/16.
 */
public class LocalityAdapter extends ArrayAdapter<LocalityEntity> {

    private List<LocalityEntity> localityResponseList;
    private String city, state, country;
    private int resource;

    // View lookup cache
    private static class ViewHolder {
        TextView locality;
    }

    /**
     * Constructor
     *
     * @param context  The current context.
     * @param resource The resource ID for a layout file containing a TextView to use when
     *                 instantiating views.
     * @param objects  The objects to represent in the ListView.
     */
    public LocalityAdapter(Context context, int resource, List<LocalityEntity> objects, String city, String state, String country) {
        super(context, resource, objects);
        this.localityResponseList = objects;
        this.resource = resource;
        this.city = city;
        this.state = state;
        this.country = country;
    }

    @Override
    public int getCount() {
        return localityResponseList.size();
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(resource, parent, false);
            viewHolder.locality = (TextView) convertView.findViewById(R.id.lblLocalityText);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.locality.setText(localityResponseList.get(position).getName());
//        + ", " + city + ", " + state + ", " + country);

        return convertView;
    }
}
