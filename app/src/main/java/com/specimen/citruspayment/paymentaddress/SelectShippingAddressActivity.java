package com.specimen.citruspayment.paymentaddress;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.address.IAddressFacade;
import com.specimen.core.model.Address;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddressResponse;
import com.specimen.me.address.AddAddressFragment;
import com.specimen.me.address.IAddressEditor;
import com.specimen.widget.ProgressView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by DhirajK on 12/1/2015.
 */
public class SelectShippingAddressActivity extends BaseActivity implements IAddressEditor, IResponseSubscribe {

    RecyclerView storedAddressList;
    AddressResponse addressResponse;
    List<Address> addressList;
    List<Address> sortedArrayList;
    private Button btnConfirm;
    private int selectedAddressId;
    private ShippingAddressAdapter shippingAddressAdapter;
    private String TAG = "SelectShippingAddressActivity";
    private ProgressView progressView;

    void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.lblStepsHeader)).setText("DELIVERY");
        ((TextView) toolbar.findViewById(R.id.lblStepsCount)).setText("STEP 2/3");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent trunkIntent = new Intent();
        trunkIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        trunkIntent.setClass(SelectShippingAddressActivity.this, MainActivity.class);
        trunkIntent.putExtra("tag", MainActivity.TRUNK);
        startActivity(trunkIntent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_shipping_address);
        setToolbar();

        addressResponse = getIntent().getParcelableExtra("address");
        if (null != addressResponse) {
            addressList = addressResponse.getData();
            storedAddressList = (RecyclerView) findViewById(R.id.list);
            btnConfirm = (Button) findViewById(R.id.btnConfirm);
            progressView = (ProgressView) findViewById(R.id.progressBar);

//            Collections.sort(addressList, new Comparator<Address>() {
//                @Override
//                public int compare(Address address, Address t1) {
//                    if (address.getIsPrimary() || t1.getIsPrimary()) {
//                        return -1;
//                    } else {
//                        return 0;
//                    }
//                }
//            });

            selectedAddressId = 0;
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            storedAddressList.setLayoutManager(linearLayoutManager);
            shippingAddressAdapter
                    = new ShippingAddressAdapter(R.layout.address_store_view, addressList, this);
            storedAddressList.setAdapter(shippingAddressAdapter);

            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // TODO : Set primary
                    if (null != addressList && 0 < addressList.size()) {
                        Address address = addressList.get(selectedAddressId);
                        address.setIsPrimary(true);
                        ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, "setPrimary")).changeStoreAddress(address);
                    }
                }
            });
        }
    }

    @Override
    public void editAddress(int id) {
//        for (int i = 0; i < addressList.size(); i++) {
//            if (addressList.get(i).getId().equals("" + id)) {
//                selectedAddress = addressList.get(i);
//            }
//        }
        Intent intent = new Intent();
        intent.putExtra("isFirstAddressInserting", false);
        intent.putExtra("addressToBe", "EditAddress");
        intent.putExtra("selectedAddress", addressList.get(id));
        intent.setClass(this, ShippingAddressActivity.class);
        startActivityForResult(intent, 2);// Activity is started with requestCode 2

    }

    @Override
    public void deleteAddress(int id) {
        progressView.setVisibility(View.VISIBLE);
        ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, TAG))
                .deleteStoreAddress(addressResponse.getData().get(id));
    }

    @Override
    public void selectedAddressId(int id) {
        selectedAddressId = id;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 2) {
            // Make sure the request was successful
            if (null != data) {
                Bundle bundle = data.getExtras();
                addressResponse = bundle.getParcelable("new_address");
                updateAddressList(addressResponse);
            }
        }
    }

    public void updateAddressList(AddressResponse addressResponse) {
        if (addressResponse != null) {
//            addressList = addressResponse.getData();
//            Collections.sort(addressList, new Comparator<Address>() {
//                @Override
//                public int compare(Address address, Address t1) {
//                    if (address.getIsPrimary() || t1.getIsPrimary()) {
//                        return -1;
//                    } else {
//                        return 0;
//                    }
//                }
//            });
            addressList = addressResponse.getData();
            if (null != addressList && 0 == addressList.size()) {
                SelectShippingAddressActivity.this.finish();
            } else {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                storedAddressList.setLayoutManager(linearLayoutManager);
                shippingAddressAdapter = new ShippingAddressAdapter(R.layout.address_store_view, addressList, this);
                storedAddressList.setAdapter(shippingAddressAdapter);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        progressView.setVisibility(View.GONE);
        if (tag.equals(TAG)) {
            if (response instanceof AddressResponse) {
                AddressResponse mAddressResponse = (AddressResponse) response;
                if (mAddressResponse.getData() != null)
                    updateAddressList(mAddressResponse);
            }
        }

        if (tag.equals("setPrimary")) {
            if (response instanceof AddressResponse) {
                SelectShippingAddressActivity.this.finish();
            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        progressView.setVisibility(View.GONE);
    }

}
