package com.specimen.citruspayment.paymentaddress;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.address.IAddressFacade;
import com.specimen.core.model.Address;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddressResponse;
import com.specimen.widget.ProgressView;

/**
 * Created by root on 19/11/15.
 */
public class ShippingAddressActivity extends BaseActivity {

    final String TAG = "ShippingAddressActivity";
    private AddressResponse addressResponseParceable;
    private ProgressView progressView;
    private TextInputLayout fPincodeLayout, fLocalityLayout, fNameLayout, fAddressLayout, fMobileLayout;

    private EditText txtName, txtAddress, txtAddress1, txtAddress2, txtPincode, txtLandmark, txtLocality, txtMobileno;
    private TextView txtCity, txtState;
    private CheckBox chkDefaultAddress;
    private String name, address, address1, address2, pincode, landmark, locality, city, mobileno, state;
    private String addressToBe;
    private Address mAddress;
    private boolean isFirstAddressInserting, shouldDefaultAddressCheckBoxShow;
    private Address addressObj;

    private boolean isLocalityValid;
    private Button btnChoose;

    void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.lblStepsHeader)).setText("DELIVERY");
        ((TextView) toolbar.findViewById(R.id.lblStepsCount)).setText("STEP 2/3");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_address);
        setToolbar();

        //Displaying TextInputLayout Error
        fPincodeLayout = (TextInputLayout) findViewById(R.id.fPincodeLayout);
        fLocalityLayout = (TextInputLayout) findViewById(R.id.fLocalityLayout);
        fNameLayout = (TextInputLayout) findViewById(R.id.fNameLayout);
        fAddressLayout = (TextInputLayout) findViewById(R.id.fAddressLayout);
        fMobileLayout = (TextInputLayout) findViewById(R.id.fMobileLayout);

        fPincodeLayout.setErrorEnabled(true);
        fLocalityLayout.setErrorEnabled(true);
        fNameLayout.setErrorEnabled(true);
        fAddressLayout.setErrorEnabled(true);
        fMobileLayout.setErrorEnabled(true);

//        setToolbar();
//        mProgressView = (ProgressView) findViewById(R.id.progressView);

        txtName = (EditText) findViewById(R.id.txtName);
        txtAddress = (EditText) findViewById(R.id.txtAddress);
        txtAddress1 = (EditText) findViewById(R.id.txtAddress1);
        txtAddress2 = (EditText) findViewById(R.id.txtAddress2);
        txtPincode = (EditText) findViewById(R.id.txtPincode);
        txtLandmark = (EditText) findViewById(R.id.txtLandmark);
        txtLocality = (EditText) findViewById(R.id.txtLocality);
//        txtCity = (EditText) findViewById(R.id.txtCity);
        txtCity = (TextView) findViewById(R.id.txtCity);
        txtState = (TextView) findViewById(R.id.txtState);
        txtMobileno = (EditText) findViewById(R.id.txtMobile);
        chkDefaultAddress = (CheckBox) findViewById(R.id.chkDefaultAddress);
        progressView = (ProgressView) findViewById(R.id.progressBar);
        btnChoose = (Button)findViewById(R.id.btnChoose);

        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            isFirstAddressInserting = bundle.getBoolean("isFirstAddressInserting", false);
            addressToBe = bundle.getString("addressToBe");
            shouldDefaultAddressCheckBoxShow = bundle.getBoolean("shouldDefaultAddressCheckBoxShow", false);
//            chkDefaultAddress.setChecked(isFirstAddressInserting);
            if (!isFirstAddressInserting || shouldDefaultAddressCheckBoxShow) {
                chkDefaultAddress.setVisibility(View.VISIBLE);
            }
        }

        btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(ShippingAddressActivity.this, SelectLocalityActivity.class);
                intent.putExtra("PINCODE", txtPincode.getText().toString());
                intent.putExtra("LOCALITY", locality = txtLocality.getText().toString());
                startActivityForResult(intent, 500);
            }
        });

        findViewById(R.id.btnAddAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });

        findViewById(R.id.btnCancelAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (isValid(addressToBe) && addressToBe.equals("EditAddress")) {
            mAddress = getIntent().getParcelableExtra("selectedAddress");
            chkDefaultAddress.setVisibility(View.VISIBLE);
            updatetUI();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    void updatetUI() {
        txtName.setText(mAddress.getName());
        txtAddress.setText(mAddress.getAddress());
//        txtAddress1.setText(mAddress.getAddress1());
//        txtAddress2.setText(mAddress.getAddress2());
        txtPincode.setText(mAddress.getPincode());
//        txtLandmark.setText(mAddress.getLandmark());
        txtLocality.setText(mAddress.getLocality());
        txtCity.setText(city = mAddress.getCity());
        txtState.setText(state = mAddress.getState());
        txtMobileno.setText(mAddress.getMobile_no());
        chkDefaultAddress.setChecked(mAddress.getIsPrimary());

//        ((Button) findViewById(R.id.btnAddAddress)).setText("UPDATE ADDRESS");

    }

    void validation() {

        if (!isValid(pincode = getText(txtPincode))) {
//            Toast.makeText(this, "Enter pincode", Toast.LENGTH_LONG).show();
            fPincodeLayout.setError("PLEASE ENTER PIN CODE");
            return;
        }

        if (!isValid(locality = getText(txtLocality))) {
//            Toast.makeText(this, "Enter locality", Toast.LENGTH_LONG).show();
            fLocalityLayout.setError("PLEASE ENTER LOCALITY");
            return;
        }

        if (!isValid(name = getText(txtName))) {
//            Toast.makeText(this, "Enter name", Toast.LENGTH_LONG).show();
            fNameLayout.setError("PLEASE ENTER NAME");
            return;
        }

        if (!isValid(address = getText(txtAddress))) {
//            Toast.makeText(this, "Enter address", Toast.LENGTH_LONG).show();
            fAddressLayout.setError("PLEASE ENTER ADDRESS");
            return;
        }

//        address1 = getText(txtAddress1);
//        address2 = getText(txtAddress2);

//        if (!isValid(landmark = getText(txtLandmark))) {
//            Toast.makeText(this, "Enter landmark", Toast.LENGTH_LONG).show();
//            return;
//        }


//        if (!isValid(city = getText(txtCity))) {
//            Toast.makeText(this, "Enter city", Toast.LENGTH_LONG).show();
//            return;
//        }

        if (!isValid(mobileno = getText(txtMobileno))) {
//            Toast.makeText(this, "Enter mobile no", Toast.LENGTH_LONG).show();
            fMobileLayout.setError("PLEASE ENTER MOBILE NO");
            return;
        }

        if (mobileno.length() < 10 || mobileno.length() > 17) {
//            Toast.makeText(this, "Enter valid mobile no", Toast.LENGTH_LONG).show();
            fMobileLayout.setError("MINIMUM LENGTH IS 10");
            return;
        }

        addressObj = new Address();
        addressObj.setName(name);
        addressObj.setAddress(address);
//        addressObj.setAddress1(TextUtils.isEmpty(address1) ? "" : address1);
//        addressObj.setAddress2(TextUtils.isEmpty(address2) ? "" : address2);
        addressObj.setPincode(pincode);
//        addressObj.setLandmark(landmark);
        addressObj.setLocality(locality);
//        addressObj.setDistrict(city);
        addressObj.setMobile_no(mobileno);
        addressObj.setCity(city);
        addressObj.setState(state);

        if (isFirstAddressInserting) {
            addressObj.setIsPrimary(true);
        } else {
            addressObj.setIsPrimary(chkDefaultAddress.isChecked());
        }

        if (isValid(addressToBe) && addressToBe.equals("EditAddress")) {

            if (null != city && null != state) {
                if (city.length() <= 0 || state.length() <= 0) {
                    fPincodeLayout.setError("Sorry! This order cannot be delivered to this pin code.");
                    isLocalityValid = false;
                } else {
                    isLocalityValid = true;
                }
            } else {
                fPincodeLayout.setError("Sorry! This order cannot be delivered to this pin code.");
                isLocalityValid = false;
            }
        }

        if (isLocalityValid) {
            if (isValid(addressToBe) && addressToBe.equals("EditAddress")) {
                addressObj.setId(mAddress.getId());
                editAddressAPICAll(addressObj);
            } else {
                addNewAddressAPICAll(addressObj);
            }
        }

//        // check whether picode available in fedex list
//        codAvailable(pincode);

    }

    boolean isValid(String text) {
        return null != text;
    }

    String getText(EditText editText) {
        if (editText.getText().length() != 0) {
            return editText.getText().toString().trim();
        }
        return null;
    }

    private void codAvailable(String pincode) {
        progressView.setVisibility(View.VISIBLE);
        ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, "checkZipcode")).checkavailableZipcodes(pincode);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        progressView.setVisibility(View.GONE);

        if (!tag.equals(TAG)) {
            return;
        }
        if (response instanceof AddressResponse) {
            int size = (((AddressResponse) response).getData() != null) ? ((AddressResponse) response).getData().size() : 0;
            if (0 != size) {
                addressResponseParceable = (AddressResponse) response;
                Intent intent = new Intent();
                intent.putExtra("new_address", addressResponseParceable);
                setResult(2, intent);
                finish();
            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        progressView.setVisibility(View.VISIBLE);
    }

    void addNewAddressAPICAll(Address addressObj) {
        progressView.setVisibility(View.VISIBLE);
        ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, TAG)).addNewAddress(addressObj);
    }

    void editAddressAPICAll(Address addressObj) {
        progressView.setVisibility(View.VISIBLE);
        ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, TAG)).changeStoreAddress(addressObj);
    }

    @Override
    public void onBackPressed() {
        Intent trunkIntent = new Intent();
        trunkIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        trunkIntent.setClass(ShippingAddressActivity.this, MainActivity.class);
        trunkIntent.putExtra("tag", MainActivity.TRUNK);
        startActivity(trunkIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 500) {
            if (resultCode == RESULT_OK) {
                if (null != data) {
                    city = data.getStringExtra("CITY");
                    state = data.getStringExtra("STATE");

                    txtLocality.setText("" + data.getStringExtra("LOCALITY"));
                    txtCity.setText(city != null ? city : "");
                    txtState.setText(state != null ? state : "");

                    if(null != city && null != state) {
                        if (city.length() <= 0 || state.length() <= 0) {
                            fPincodeLayout.setError("Sorry! This order cannot be delivered to this pin code.");
                            isLocalityValid = false;
                        } else {
                            isLocalityValid = true;
                        }
                    } else {
                        fPincodeLayout.setError("Sorry! This order cannot be delivered to this pin code.");
                        isLocalityValid = false;
                    }
                }
            }
        }
    }
}
