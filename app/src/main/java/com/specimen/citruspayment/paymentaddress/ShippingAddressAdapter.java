package com.specimen.citruspayment.paymentaddress;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.Address;
import com.specimen.me.address.IAddressEditor;

import java.util.List;

/**
 * Created by root on 19/11/15.
 */
public class ShippingAddressAdapter extends RecyclerView.Adapter<ShippingAddressAdapter.AddressViewHolder> {

    final String TAG = "ShippingAddressAdapter";
    IAddressEditor mIAddressEditor;
    List<Address> addressList;
    int resource;
    int mSelectedItem = 0;
    View view;

    /**
     * Constructor
     *
     * @param resource The resource ID for a layout file containing a TextView to use when
     */
    public ShippingAddressAdapter(int resource, List<Address> addressList, IAddressEditor iAddressEditor) {
        this.resource = resource;
        this.addressList = addressList;
        this.mIAddressEditor = iAddressEditor;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    /**
     * Called when RecyclerView needs a new {@link RecyclerView.ViewHolder} of the given type to represent
     * an item.
     * <p/>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p/>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(RecyclerView.ViewHolder, int)}. Since it will be re-used to display different
     * items in the data set, it is a good idea to cache references to sub views of the View to
     * avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(RecyclerView.ViewHolder, int)
     */
    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        AddressViewHolder addressViewHolder = new AddressViewHolder(view);
        addressViewHolder.setClickListener();
        return addressViewHolder;
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method
     * should update the contents of the {@link RecyclerView.ViewHolder#itemView} to reflect the item at
     * the given position.
     * <p/>
     * Note that unlike {@link ListView}, RecyclerView will not call this
     * method again if the position of the item changes in the data set unless the item itself
     * is invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside this
     * method and should not keep a copy of it. If you need the position of an item later on
     * (e.g. in a click listener), use {@link RecyclerView.ViewHolder#getAdapterPosition()} which will have
     * the updated adapter position.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(AddressViewHolder holder, int position) {
        Address address = addressList.get(position);
        String addressString = address.getAddress()
//                + (TextUtils.isEmpty(address.getAddress1()) ? "" : "\n" + address.getAddress1())
//                + (TextUtils.isEmpty(address.getAddress2()) ? "" : "\n" + address.getAddress2())
//                + "\n" + address.getDistrict()
                + "\n" + address.getLocality()
//                + "\n" + address.getLandmark()
                + "\n" + address.getCity() + ": " + address.getPincode()
                + "\n" + address.getState()
                + "\n\nMobile: " + address.getMobile_no();

        Log.d(TAG, "onBindViewHolder() : " + addressString);
        holder.lblName.setText(address.getName());
        holder.lblAddress.setText(addressString);

        holder.rbAddress.setChecked(mSelectedItem == position);

        if (address.getIsPrimary() && (mSelectedItem == position)) {
            holder.linear_add_address.setBackgroundResource(R.drawable.green_border);
        } else {
            holder.linear_add_address.setBackgroundResource(0);
        }

        if (address.getIsPrimary() && (mSelectedItem == (-1))) {
            holder.linear_add_address.setBackgroundResource(R.drawable.green_border);
            holder.rbAddress.setChecked(true);
        }
    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return addressList.size();
    }

    public class AddressViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout linear_add_address;
        TextView lblName;
        TextView lblAddress;
        ImageView btnEditAddress, btnDeleteAddress;
        RadioButton rbAddress;

        AddressViewHolder(View itemView) {
            super(itemView);
            linear_add_address = (LinearLayout) itemView.findViewById(R.id.linear_add_address);
            lblName = (TextView) itemView.findViewById(R.id.lblName);
            lblAddress = (TextView) itemView.findViewById(R.id.lblAddress);
            rbAddress = (RadioButton) itemView.findViewById(R.id.rbAddress);
            btnEditAddress = (ImageView) itemView.findViewById(R.id.btnEditAddress);
            btnDeleteAddress = (ImageView) itemView.findViewById(R.id.btnDeleteAddress);
        }

        public void setClickListener() {
            btnEditAddress.setOnClickListener(this);
            btnDeleteAddress.setOnClickListener(this);
            rbAddress.setOnClickListener(this);
            linear_add_address.setOnClickListener(this);
            super.itemView.setOnClickListener(this);
        }

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            addressList.get(mSelectedItem = getAdapterPosition()).setIsPrimary(true);

            switch (v.getId()) {

                case R.id.linear_add_address:
                    mIAddressEditor.selectedAddressId(mSelectedItem);
                    break;

                case R.id.btnEditAddress:
                    mIAddressEditor.editAddress(mSelectedItem);
                    break;

                case R.id.btnDeleteAddress:
                    mIAddressEditor.deleteAddress(mSelectedItem);
                    break;
            }

            notifyDataSetChanged();
        }
    }
}

