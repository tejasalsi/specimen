package com.specimen.citruspayment.paymentaddress;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.citruspayment.SelectPaymentMethodActivity;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.address.IAddressFacade;
import com.specimen.core.model.Address;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddressResponse;

/**
 * Created by DhirajK on 11/18/2015.
 */
public class PaymentConfirmDeliveryAddressActivity extends BaseActivity {

    private static final String TAG = "PaymentConfirmDeliveryAddressActivity";
    AddressResponse addressResponseParceable;
    TextView txtName, txtAddress, txtAddressCount, totalPriceSummary, orderTotalPriceSummary, itemCountSummary, tvIsCashOnDelivery;
    RelativeLayout linearChangeShoppingAddress;
    private String total, cart_product_count;
    private RelativeLayout llAddNewAddress;
    private Button btnContinueToPay;
    private Address tempAddress = null;
    private boolean isDeleveryPossible = true;
    private boolean isAddressListSizeGreaterThanOne;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_confirm_delivery_address);
        setToolbar();

        txtName = (TextView) findViewById(R.id.txtName);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtAddressCount = (TextView) findViewById(R.id.txtAddressCount);
        totalPriceSummary = (TextView) findViewById(R.id.totalPriceSummary);
        itemCountSummary = (TextView) findViewById(R.id.itemCountSummary);
        orderTotalPriceSummary = (TextView) findViewById(R.id.orderTotalPriceSummary);
        linearChangeShoppingAddress = (RelativeLayout) findViewById(R.id.linearChangeShoppingAddress);
        llAddNewAddress = (RelativeLayout) findViewById(R.id.llAddNewAddress);
        btnContinueToPay = (Button) findViewById(R.id.btnContinueToPay);
        tvIsCashOnDelivery = (TextView) findViewById(R.id.tvIsCashOnDelivery);

        linearChangeShoppingAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setClass(PaymentConfirmDeliveryAddressActivity.this,
                        SelectShippingAddressActivity.class);
                intent.putExtra("address", addressResponseParceable);
                startActivity(intent);
            }
        });

        llAddNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFirstAddress(false);
            }
        });


        final Bundle bundle = getIntent().getExtras();
        if (null == bundle) {
            return;
        }
        setOrderSummaryLayout(bundle);
        btnContinueToPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDeleveryPossible) {
                    Intent intent = new Intent(PaymentConfirmDeliveryAddressActivity.this, SelectPaymentMethodActivity.class);
                    Bundle bundle1 = new Bundle();

                    if (tempAddress != null) {
                        bundle1.putParcelable("address", tempAddress);
                        bundle1.putString("TOTAL", bundle.getString("TOTAL"));
                        bundle1.putString("cod", tempAddress.getCod());
                        bundle1.putString("CART_PRODUCT_COUNT", bundle.getString("CART_PRODUCT_COUNT"));
                        intent.putExtras(bundle1);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Please select shipping address.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "We are currently not shipping in your area.\n Please try different shipping address.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.lblStepsHeader)).setText("DELIVERY");
        ((TextView) toolbar.findViewById(R.id.lblStepsHeader)).setTextSize(TypedValue.COMPLEX_UNIT_SP,16);

        Toolbar.LayoutParams layoutParams = new Toolbar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;
         toolbar.findViewById(R.id.lblStepsHeader).setLayoutParams(layoutParams);

        ((TextView) toolbar.findViewById(R.id.lblStepsCount)).setText("STEP 2/3");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, TAG)).getStoredAddress();
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    // Call Back method  to get the Message form other Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            // check if intent is null
            if (null == data) {
                //   finish();
            } else {
                Bundle bundle = data.getExtras();
                addressResponseParceable = bundle.getParcelable("new_address");
                int size = (addressResponseParceable.getData() != null) ? addressResponseParceable.getData().size() : 0;
                if (0 != size) {
                    findViewById(R.id.linearConfirmAddress).setVisibility(View.VISIBLE);
                    setDefaultAddressLayout();
                }
            }
        }
        if (requestCode == 1) {
            onResume();
        }
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        super.onSuccess(response, tag);

        if (tag.equals(TAG)) {

            if (response instanceof AddressResponse) {
                int size = (((AddressResponse) response).getData() != null) ? ((AddressResponse) response).getData().size() : 0;
                if (0 != size) {
                    isAddressListSizeGreaterThanOne = true;
                    addressResponseParceable = new AddressResponse(((AddressResponse) response).getData());
                    setDefaultAddressLayout();
                    findViewById(R.id.linearConfirmAddress).setVisibility(View.VISIBLE);
                } else {
                    addFirstAddress(true);
                }
            }
        }

        if (tag.equals("checkZipcode")) {
            if (response.getStatus().equalsIgnoreCase("failure")) {
                isDeleveryPossible = false;
            } else {
                isDeleveryPossible = true;
            }
        }
    }

    public void addFirstAddress(boolean isFirstAddressInserting) {
        findViewById(R.id.linearConfirmAddress).setVisibility(View.GONE);
        addressResponseParceable = null;
        Intent intent = new Intent();
        intent.putExtra("isFirstAddressInserting", isFirstAddressInserting);
        if (!isFirstAddressInserting && isAddressListSizeGreaterThanOne) {
            intent.putExtra("shouldDefaultAddressCheckBoxShow", true);
        }
        intent.setClass(this, ShippingAddressActivity.class);
        startActivityForResult(intent, 2);// Activity is started with requestCode 2
    }

    @Override
    public void onFailure(Exception error) {
        super.onFailure(error);
    }

    private void setDefaultAddressLayout() {
        txtAddressCount.setText("" + addressResponseParceable.getData().size() + " saved");
        for (Address address : addressResponseParceable.getData()) {
            if (address.getIsPrimary()) {
                tempAddress = address;
                txtName.setText("" + address.getName());
                String addressString = address.getAddress()
//                + (TextUtils.isEmpty(address.getAddress1()) ? "" : "\n" + address.getAddress1())
//                + (TextUtils.isEmpty(address.getAddress2()) ? "" : "\n" + address.getAddress2())
//                + "\n" + address.getDistrict()
                        + "\n" + address.getLocality()
//                + "\n" + address.getLandmark()
                        + "\n" + address.getCity() + ": " + address.getPincode()
                        + "\n" + address.getState()
                        + "\n\nMobile: " + address.getMobile_no();
                txtAddress.setText(addressString);

                ((IAddressFacade) IOCContainer.getInstance().getObject(ServiceName.ADDRESS_SERVICE, "checkZipcode")).checkavailableZipcodes(address.getPincode());

                if (!TextUtils.isEmpty(address.getCod())) {
                    if (address.getCod().equalsIgnoreCase("true")) {
                        tvIsCashOnDelivery.setText("• CASH ON DELIVERY IS AVAILABLE");
                    } else {
                        tvIsCashOnDelivery.setText("• CASH ON DELIVERY IS NOT AVAILABLE");
                    }
                } else {
                    tvIsCashOnDelivery.setText("• CASH ON DELIVERY IS NOT AVAILABLE");
                }
                break;
            }
        }
    }

    private void setOrderSummaryLayout(Bundle bundle) {
        total = bundle.getString("TOTAL");
        cart_product_count = bundle.getString("CART_PRODUCT_COUNT");

        totalPriceSummary.setText("" + getResources().getString(R.string.rupee_symbole) + " " + total);
        orderTotalPriceSummary.setText("" + getResources().getString(R.string.rupee_symbole) + " " + total);
        itemCountSummary.setText("" + cart_product_count + " items");
    }
}
