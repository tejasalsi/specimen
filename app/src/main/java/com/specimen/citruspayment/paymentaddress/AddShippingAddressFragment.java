//package com.specimen.citruspayment.paymentaddress;
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import com.specimen.BaseFragment;
//import com.specimen.R;
//import com.specimen.core.IOCContainer;
//import com.specimen.core.IResponseSubscribe;
//import com.specimen.core.model.Address;
//import com.specimen.core.response.APIResponse;
//import com.specimen.core.response.AddressResponse;
//
///**
// * Created by root on 19/11/15.
// */
//public class AddShippingAddressFragment extends BaseFragment implements IResponseSubscribe {
//
//    final String TAG = "AddShippingAddressFragment";
//    IAddAddress iAddAddress;
//    EditText txtName, txtAddress, txtAddress1, txtAddress2, txtPincode, txtLandmark, txtLocality, txtCity, txtMobileno;
//    CheckBox chkDefaultAddress;
//    String name, address, address1, address2, pincode, landmark, locality, city, mobileno;
//    String addressToBe;
//    Address mAddress;
//
//    @Override
//    public void onSuccess(APIResponse response, String tag) {
//        if (null != tag) {
//            return;
//        }
//
//        if (response instanceof AddressResponse) {
//            iAddAddress.addNewAddressCallback();
//        }
//    }
//
//    @Override
//    public void onFailure(Exception error) {
//        iAddAddress.addNewAddressCallback();
//    }
//
//    public interface IAddAddress {
//        void addNewAddressCallback();
//    }
//
//    public AddShippingAddressFragment(IAddAddress iAddAddress, Address address) {
//        this.iAddAddress = iAddAddress;
//        this.mAddress = address;
//    }
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        ((ShippingAddressActivity) getActivity()).setUpadatedFragmentVisiblility(true);
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        ((ShippingAddressActivity) getActivity()).setUpadatedFragmentVisiblility(false);
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Bundle bundle = this.getArguments();
//        if (null != bundle) {
//            addressToBe = bundle.getString("addressToBe");
//        }
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        final View newAddressView = inflater.inflate(R.layout.fragment_add_address, container, false);
//
//        ((ShippingAddressActivity) getActivity()).setToolbarTitle(R.string.address_new);
//
//        txtName = (EditText) newAddressView.findViewById(R.id.txtName);
//        txtAddress = (EditText) newAddressView.findViewById(R.id.txtAddress);
//        txtAddress1 = (EditText) newAddressView.findViewById(R.id.txtAddress1);
//        txtAddress2 = (EditText) newAddressView.findViewById(R.id.txtAddress2);
//        txtPincode = (EditText) newAddressView.findViewById(R.id.txtPincode);
//        txtLandmark = (EditText) newAddressView.findViewById(R.id.txtLandmark);
//        txtLocality = (EditText) newAddressView.findViewById(R.id.txtLocality);
//        txtCity = (EditText) newAddressView.findViewById(R.id.txtCity);
//        txtMobileno = (EditText) newAddressView.findViewById(R.id.txtMobile);
//        chkDefaultAddress = (CheckBox) newAddressView.findViewById(R.id.chkDefaultAddress);
//
//        newAddressView.findViewById(R.id.btnAddAddress).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                validation();
//            }
//        });
//
//        if (isValid(addressToBe) && addressToBe.equals("EditAddress")) {
//            updatetUI(newAddressView);
//        }
//
//        return newAddressView;
//    }
//
//    void updatetUI(View editAddressView) {
//        txtName.setText(mAddress.getName());
//        txtAddress.setText(mAddress.getAddress());
//        txtAddress1.setText(mAddress.getAddress1());
//        txtAddress2.setText(mAddress.getAddress2());
//        txtPincode.setText(mAddress.getPincode());
//        txtLandmark.setText(mAddress.getLandmark());
//        txtLocality.setText(mAddress.getLocality());
//        txtCity.setText(mAddress.getDistrict());
//        txtMobileno.setText(mAddress.getMobile_no());
//        chkDefaultAddress.setChecked(mAddress.getIsPrimary());
//
//        ((Button) editAddressView.findViewById(R.id.btnAddAddress)).setText("UPDATE ADDRESS");
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
//    }
//
//    void validation() {
//        if (!isValid(name = getText(txtName))) {
//            Toast.makeText(getActivity(), "Enter name", Toast.LENGTH_LONG).show();
//            return;
//        }
//
//        if (!isValid(address = getText(txtAddress))) {
//            Toast.makeText(getActivity(), "Enter address", Toast.LENGTH_LONG).show();
//            return;
//        }
//
//        address1 = getText(txtAddress1);
//        address2 = getText(txtAddress2);
//
//        if (!isValid(pincode = getText(txtPincode))) {
//            Toast.makeText(getActivity(), "Enter pincode", Toast.LENGTH_LONG).show();
//            return;
//        }
//
//        if (!isValid(landmark = getText(txtLandmark))) {
//            Toast.makeText(getActivity(), "Enter landmark", Toast.LENGTH_LONG).show();
//            return;
//        }
//
//        if (!isValid(locality = getText(txtLocality))) {
//            Toast.makeText(getActivity(), "Enter locality", Toast.LENGTH_LONG).show();
//            return;
//        }
//
//        if (!isValid(city = getText(txtCity))) {
//            Toast.makeText(getActivity(), "Enter city", Toast.LENGTH_LONG).show();
//            return;
//        }
//
//        if (!isValid(mobileno = getText(txtMobileno))) {
//            Toast.makeText(getActivity(), "Enter mobile no", Toast.LENGTH_LONG).show();
//            return;
//        }
//
//        Address addressObj = new Address();
//        addressObj.setName(name);
//        addressObj.setAddress(address);
//        addressObj.setAddress1(TextUtils.isEmpty(address1) ? "" : address1);
//        addressObj.setAddress2(TextUtils.isEmpty(address2) ? "" : address2);
//        addressObj.setPincode(pincode);
//        addressObj.setLandmark(landmark);
//        addressObj.setLocality(locality);
//        addressObj.setDistrict(city);
//        addressObj.setMobile_no(mobileno);
//        addressObj.setIsPrimary(chkDefaultAddress.isChecked());
//
//        if (isValid(addressToBe) && addressToBe.equals("EditAddress")) {
//            addressObj.setId(mAddress.getId());
//            ((ShippingAddressActivity) getActivity()).editAddressAPICAll(addressObj);
//        } else {
//            ((ShippingAddressActivity) getActivity()).addNewAddressAPICAll(addressObj);
//        }
//    }
//
//    boolean isValid(String text) {
//        return null != text;
//
//    }
//
//    String getText(EditText editText) {
//        if (editText.getText().length() != 0) {
//            return editText.getText().toString().trim();
//        }
//
//        return null;
//    }
//}