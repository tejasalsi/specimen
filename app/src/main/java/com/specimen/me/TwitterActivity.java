package com.specimen.me;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseActivity;
import com.specimen.R;

import java.io.InputStream;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by Tejas on 9/23/2015.
 */
public class TwitterActivity extends BaseActivity {

    private WebView webView;
    private RelativeLayout twitterSharelayout;
    private static SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;
    Toolbar toolbar;
    TextView toolbarTitle;

    private String consumerKey = null;
    private String consumerSecret = null;
    private String oAuthVerifier = null;

    /* Shared preference keys */
    private static final String PREF_NAME = "sample_twitter_pref";
    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    private static final String PREF_USER_NAME = "twitter_user_name";

    private static Twitter twitter;
    private static RequestToken requestToken;

    private ProgressDialog pDialog;

    private EditText sharePostText, twitterPin;
    private Button postButton, pinConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_twitter);
        setToolbar();

        webView = (WebView) findViewById(R.id.twitterWebView);
        twitterSharelayout = (RelativeLayout) findViewById(R.id.twitterShareLayout);
        postButton = (Button) findViewById(R.id.twitterPostBtn);
        pinConfirm = (Button) findViewById(R.id.confirmPINBtn);

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new UpdateTwitterStatus().execute(sharePostText.getText().toString().trim());
            }
        });

        pinConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (twitterPin.getText().equals("") || twitterPin.getText() == null)
                    Toast.makeText(TwitterActivity.this, "PIN required for confirmation", Toast.LENGTH_SHORT).show();
                else
                    onComplete(twitterPin.getText().toString());

            }
        });

        sharePostText = (EditText) findViewById(R.id.twitterStatus);
        twitterPin = (EditText) findViewById(R.id.editPin);

        initTwitterConfigs();

        /* Initialize application preferences */
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = mSharedPreferences.edit();

        boolean isLoggedIn = mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);

        loginToTwitter();

		/*  if already logged in, then hide login layout and show share layout */
        if (isLoggedIn) {
            String username = mSharedPreferences.getString(PREF_USER_NAME, "");
        } /*else {

            if (oAuthVerifier != null) {
                try {

					*//* Getting oAuth authentication token *//*
                    AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, oAuthVerifier);

					*//* Getting user id form access token *//*
                    long userID = accessToken.getUserId();
                    final User user = twitter.showUser(userID);
                    final String username = user.getName();
                    Log.d("twitter username:", username);

					*//* save updated token *//*
                    saveTwitterInfo(accessToken);

                } catch (Exception e) {
                    Log.e("Failed Twitter login", e.getMessage());
                }
            }

        }*/


    }

    void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbarTwitter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setToolbarTitle(R.string.twitter_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    public void onComplete(String pin) {
        oAuthVerifier = pin;
        new AsyncTask<String, Integer, String>() {
            @Override
            protected String doInBackground(String... strings) {
                try {

					/* Getting oAuth authentication token */
                    AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, oAuthVerifier);

					/* Getting user id form access token */
                    long userID = accessToken.getUserId();
                    final User user = twitter.showUser(userID);
                    final String username = user.getName();
                    Log.d("twitter username:", username);

					/* save updated token */
                    saveTwitterInfo(accessToken);

                } catch (Exception e) {
                    Log.e("Failed Twitter login", e.getMessage());
                    Toast.makeText(TwitterActivity.this, "Your Twitter Authorization failed, please try again.", Toast.LENGTH_SHORT).show();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                webView.setVisibility(View.GONE);
                pinConfirm.setVisibility(View.GONE);
                twitterPin.setVisibility(View.GONE);
                twitterSharelayout.setVisibility(View.VISIBLE);
                editor.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
                editor.commit();
            }
        }.execute();

    }


    class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.d("twitter url1:", url);

            if (url.equals("https://api.twitter.com/oauth/authenticate")) {
                twitterPin.setVisibility(View.VISIBLE);
                pinConfirm.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Saving user information, after user is authenticated for the first time.
     * You don't need to show user to login, until user has a valid access token
     */
    private void saveTwitterInfo(AccessToken accessToken) {

        long userID = accessToken.getUserId();

        User user;
        try {
            user = twitter.showUser(userID);

            String username = user.getName();

			/* Storing oAuth tokens to shared preferences */
            editor.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
            editor.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
            editor.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
            editor.putString(PREF_USER_NAME, username);
            editor.commit();

        } catch (TwitterException e1) {
            e1.printStackTrace();
        }
    }

    private void initTwitterConfigs() {
        consumerKey = getString(R.string.twitter_consumer_key);
        consumerSecret = getString(R.string.twitter_consumer_secret);
        oAuthVerifier = getString(R.string.twitter_oauth_verifier);
    }

    private void loginToTwitter() {
        boolean isLoggedIn = mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);

        if (!isLoggedIn) {
            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(consumerKey);
            builder.setOAuthConsumerSecret(consumerSecret);

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

            new AsyncTask<String, Integer, String>() {
                @Override
                protected String doInBackground(String... strings) {
                    try {
                        requestToken = twitter.getOAuthRequestToken();

                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    if (requestToken == null) {
                        Log.e("Twitter", "requestToken cannot be null");
                        finish();

                    } else {
                        final String url = requestToken.getAuthenticationURL();
                        if (null == url) {
                            Log.e("Twitter", "URL cannot be null");
                            finish();
                        }


                        webView.setWebViewClient(new MyWebViewClient());
                        webView.loadUrl(url);
                    }
                }
            }.execute();

        } else {
            //make twitter share layout visible
            twitterSharelayout.setVisibility(View.VISIBLE);
            webView.setVisibility(View.GONE);
        }
    }


    class UpdateTwitterStatus extends AsyncTask<String, String, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(TwitterActivity.this);
            pDialog.setMessage("Posting to twitter...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected Void doInBackground(String... args) {

            String status = args[0];
            try {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey(consumerKey);
                builder.setOAuthConsumerSecret(consumerSecret);

                // Access Token
                String access_token = mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");
                // Access Token Secret
                String access_token_secret = mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "");

                AccessToken accessToken = new AccessToken(access_token, access_token_secret);
                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

                // Update status
                StatusUpdate statusUpdate = new StatusUpdate(status);
                InputStream is = getResources().openRawResource(R.drawable.logo);
                statusUpdate.setMedia("test.jpg", is);

                twitter4j.Status response = twitter.updateStatus(statusUpdate);

                Log.d("Status", response.getText());

            } catch (TwitterException e) {
                Log.d("Failed to post!", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

			/* Dismiss the progress dialog after sharing */
            pDialog.dismiss();

            Toast.makeText(TwitterActivity.this, "Posted to Twitter!", Toast.LENGTH_SHORT).show();

            // Clearing EditText field
            sharePostText.setText("");
        }

    }

    public void setToolbarTitle(int toolbarTitleStringResourceId) {
        toolbarTitle.setText(toolbarTitleStringResourceId);
    }
}
