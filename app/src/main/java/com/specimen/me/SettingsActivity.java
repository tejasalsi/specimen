package com.specimen.me;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.authentication.SelectAuthenticationProcessActivity;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.authentication.IAuthenticationFacade;
import com.specimen.core.util.SharedPreferenceHelper;
import com.specimen.widget.ProgressView;

import java.io.IOException;

/**
 * Created by root on 21/9/15.
 */
public class SettingsActivity extends BaseActivity {

    final String IS_PUSH_NOTIFICATION_ON = "IS_PUSH_NOTIFICATION_ON";
    final String TAG = "SettingsActivity";
    final String TAG_FRAGMENT = "CHANGE_PASSOWRD";
    ImageView mSwitch;

    private boolean isSwitchImageClicked;
    private ProgressView progressView;

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof ChangePasswordDialogFragment) {
            removeChangePasswordFragment();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        progressView = (ProgressView) findViewById(R.id.progressBar);
        mSwitch = (ImageView) findViewById(R.id.switchNotification);
        //attach a listener to check for changes in state

        mSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSwitch.setEnabled(false);
                if (!isSwitchImageClicked) {
                    isSwitchImageClicked = true;
                    SharedPreferenceHelper.put(SharedPreferenceHelper.APP_PROPERTIES,
                            IS_PUSH_NOTIFICATION_ON, isSwitchImageClicked);

                    setNotificationView(isSwitchImageClicked);
                } else {
                    isSwitchImageClicked = false;
                    SharedPreferenceHelper.put(SharedPreferenceHelper.APP_PROPERTIES,
                            IS_PUSH_NOTIFICATION_ON, isSwitchImageClicked);

                    setNotificationView(isSwitchImageClicked);
                }
            }
        });

        isSwitchImageClicked = SharedPreferenceHelper.get(SharedPreferenceHelper.APP_PROPERTIES, IS_PUSH_NOTIFICATION_ON, false);
        mSwitch.setPressed(isSwitchImageClicked);

        findViewById(R.id.imbChangePwd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.linearMain).setVisibility(View.GONE);
                switchFragment(new ChangePasswordDialogFragment(), false, R.id.fragment_container);
            }
        });

        findViewById(R.id.imbLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((IAuthenticationFacade) IOCContainer.getInstance().getObject(ServiceName.AUTHENTICATION_SERVICE, "")).logout();
                final IApplicationFacade applicationFacade = (IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, TAG);
                applicationFacade.setUserResponse(null);

                if (!FacebookSdk.isInitialized()) {
                    FacebookSdk.sdkInitialize(SettingsActivity.this);
                }
                if (LoginManager.getInstance() != null) {
                    LoginManager.getInstance().logOut();
                }

                Intent logoutIntent = new Intent();
                logoutIntent.setClass(SettingsActivity.this, SelectAuthenticationProcessActivity.class);
                logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                finish();
                startActivity(logoutIntent);
            }
        });

        setToolbar();
    }

    public void removeChangePasswordFragment() {
        getSupportFragmentManager().beginTransaction().
                remove(getSupportFragmentManager().findFragmentById(R.id.fragment_container)).commit();
        findViewById(R.id.linearMain).setVisibility(View.VISIBLE);

    }

    void setNotificationView(boolean flag) {

        new AsyncTask<Boolean, Void, Boolean>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressView.setAnimation();
               // progressView.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
               // progressView.setVisibility(View.GONE);
                mSwitch.setEnabled(true);
                mSwitch.setPressed(isSwitchImageClicked);
            }

            @Override
            protected Boolean doInBackground(Boolean... params) {
                Boolean flag = params[0];
              /*  if (flag) {
                    try {
                        GoogleCloudMessaging.getInstance(SettingsActivity.this).register(getString(R.string.gcm_defaultSenderId));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        GoogleCloudMessaging.getInstance(SettingsActivity.this).unregister();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }*/

                return flag;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, isSwitchImageClicked);
    }

    void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("SETTING");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setVisibility() {
        findViewById(R.id.linearMain).setVisibility(View.VISIBLE);
    }
}
