package com.specimen.me;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.authentication.IAuthenticationFacade;
import com.specimen.core.response.APIResponse;

/**
 * Created by root on 22/9/15.
 */
public class ChangePasswordDialogFragment extends BaseFragment implements DialogPasswordChnageSuccess.IPasswordChangedCallback {

    final String TAG = "ChangePasswordDialogFragment";

    EditText txtOldPwd, txtNewPwd, txtConfirmNewPwd;
    TextView lblErrorMsg;
    Button btnChangePwd;
    DialogPasswordChnageSuccess dialogFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        txtOldPwd = (EditText) view.findViewById(R.id.txtOldPwd);
        txtNewPwd = (EditText) view.findViewById(R.id.txtNewPwd);
        txtConfirmNewPwd = (EditText) view.findViewById(R.id.txtConfirmNewPwd);
        lblErrorMsg = (TextView) view.findViewById(R.id.lblErrorMsg);
        btnChangePwd = (Button) view.findViewById(R.id.btnChangePwd);

        btnChangePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                validation();
            }
        });

        view.findViewById(R.id.userAuthBackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
//                getActivity().onBackPressed();
                ((SettingsActivity)getActivity()).removeChangePasswordFragment();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Register the service to get API request and response
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // unregister the service to get disable API request and response
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    void validation() {
        if (TextUtils.isEmpty(txtOldPwd.getText().toString())) {
            lblErrorMsg.setVisibility(View.VISIBLE);
            lblErrorMsg.setText("PLEASE ENTER OLD PASSWORD.");
            return;
        }

        if (TextUtils.isEmpty(txtNewPwd.getText().toString())) {
            lblErrorMsg.setVisibility(View.VISIBLE);
            lblErrorMsg.setText("PLEASE ENTER NEW PASSWORD.");
            return;
        }

        if (txtNewPwd.getText().toString().length() < 6) {
            lblErrorMsg.setVisibility(View.VISIBLE);
            lblErrorMsg.setText("NEW PASSWORD SHOULD NOT BE LESS THAN 6 CHARACTERS.");
            return;
        }

        if (TextUtils.isEmpty(txtConfirmNewPwd.getText().toString())) {
            lblErrorMsg.setVisibility(View.VISIBLE);
            lblErrorMsg.setText("PLEASE ENTER CONFIRM PASSWORD.");
            return;
        }

//        if (txtConfirmNewPwd.getText().toString().length() < 7) {
//            lblErrorMsg.setVisibility(View.VISIBLE);
//            lblErrorMsg.setText("NEW PASSWORD SHOULD NOT BE LESS THAN 6 CHARACTERS.");
//            return;
//        }

        if (!txtNewPwd.getText().toString().equals(txtConfirmNewPwd.getText().toString())) {
            lblErrorMsg.setVisibility(View.VISIBLE);
            lblErrorMsg.setText("NEW PASSWORD DOES NOT MATCH WITH THE CONFIRM PASSWORD.");
            return;
        }

        if (txtOldPwd.getText().toString().trim().equals(txtNewPwd.getText().toString().trim())) {
            lblErrorMsg.setVisibility(View.VISIBLE);
            lblErrorMsg.setText("NEW PASSWORD CANNOT BE SAME AS OLD PASSWORD.");
            return;
        }

        lblErrorMsg.setVisibility(View.INVISIBLE);
        ((IAuthenticationFacade) IOCContainer.getInstance().getObject(ServiceName.AUTHENTICATION_SERVICE, TAG))
                .changePassword(txtOldPwd.getText().toString(), txtNewPwd.getText().toString());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((SettingsActivity) mContext).setVisibility();
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        super.onSuccess(response, tag);
        if (!tag.equals(TAG)) {
            return;
        }

        if ("Success".equals(response.getStatus())) {
            lblErrorMsg.setVisibility(View.GONE);
            dialogFragment = new DialogPasswordChnageSuccess(this);
            dialogFragment.show(getChildFragmentManager(), "");
            dialogFragment.setCancelable(false);
        } else {
            lblErrorMsg.setVisibility(View.VISIBLE);
            lblErrorMsg.setText("" + response.getMessage().toUpperCase());
        }
    }

    @Override
    public void changedPasswordCallback() {
//        mContext.onBackPressed();

        ((SettingsActivity)getActivity()).removeChangePasswordFragment();
    }
}
