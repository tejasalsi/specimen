package com.specimen.me.address;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.citruspayment.paymentaddress.SelectLocalityActivity;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.model.Address;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddressResponse;

/**
 * Created by Tejas on 6/1/2015.
 * If user wants to add new address here is Add Address Fragment.
 */
public class AddAddressFragment extends BaseFragment implements IResponseSubscribe {

    final String TAG = "AddAddressFragment";
    private IAddAddress iAddAddress;
    private EditText txtName, txtAddress, txtAddress1, txtAddress2, txtPincode, txtLandmark, txtLocality, txtMobileno;
    private CheckBox chkDefaultAddress;
    private TextView lblErrorMsg, txtCity, txtState;
    private TextInputLayout fPincodeLayout, fLocalityLayout, fNameLayout, fAddressLayout, fMobileLayout;
    private String name, address, address1, address2, pincode, landmark, locality, city, mobileno, state;
    private String addressToBe;
    private Address mAddress;
    private View newAddressView;
    private Button btnChoose;

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (null != tag) {
            return;
        }

        if (response instanceof AddressResponse) {
            iAddAddress.addNewAddressCallback();
        }
    }

    @Override
    public void onFailure(Exception error) {
        iAddAddress.addNewAddressCallback();
    }

    public void setLocality(String locality, String city, String state) {
        txtLocality.setText(this.locality = locality);
        txtCity.setText(this.city = city);
        txtState.setText(this.state = state);
    }

    public interface IAddAddress {
        void addNewAddressCallback();
    }



    public AddAddressFragment(IAddAddress iAddAddress, Address address) {
        this.iAddAddress = iAddAddress;
        this.mAddress = address;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((AddressActivity) getActivity()).setUpadatedFragmentVisiblility(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((AddressActivity) getActivity()).setUpadatedFragmentVisiblility(false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (null != bundle) {
            addressToBe = bundle.getString("addressToBe");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        newAddressView = inflater.inflate(R.layout.fragment_add_address, container, false);
        newAddressView.findViewById(R.id.toolbar).setVisibility(View.GONE);
        ((AddressActivity) getActivity()).setToolbarTitle(R.string.address_new);

        //Displaying TextInputLayout Error
        fPincodeLayout = (TextInputLayout) newAddressView.findViewById(R.id.fPincodeLayout);
        fLocalityLayout = (TextInputLayout) newAddressView.findViewById(R.id.fLocalityLayout);
        fNameLayout = (TextInputLayout) newAddressView.findViewById(R.id.fNameLayout);
        fAddressLayout = (TextInputLayout) newAddressView.findViewById(R.id.fAddressLayout);
        fMobileLayout = (TextInputLayout) newAddressView.findViewById(R.id.fMobileLayout);

        fPincodeLayout.setErrorEnabled(true);
        fLocalityLayout.setErrorEnabled(true);
        fNameLayout.setErrorEnabled(true);
        fAddressLayout.setErrorEnabled(true);
        fMobileLayout.setErrorEnabled(true);

        txtName = (EditText) newAddressView.findViewById(R.id.txtName);
        txtAddress = (EditText) newAddressView.findViewById(R.id.txtAddress);
        txtAddress1 = (EditText) newAddressView.findViewById(R.id.txtAddress1);
        txtAddress2 = (EditText) newAddressView.findViewById(R.id.txtAddress2);
        txtPincode = (EditText) newAddressView.findViewById(R.id.txtPincode);
        txtLandmark = (EditText) newAddressView.findViewById(R.id.txtLandmark);
        txtLocality = (EditText) newAddressView.findViewById(R.id.txtLocality);
//        txtCity = (EditText) newAddressView.findViewById(R.id.txtCity);
        txtCity = (TextView) newAddressView.findViewById(R.id.txtCity);
        txtState = (TextView) newAddressView.findViewById(R.id.txtState);
        txtMobileno = (EditText) newAddressView.findViewById(R.id.txtMobile);
        chkDefaultAddress = (CheckBox) newAddressView.findViewById(R.id.chkDefaultAddress);
        lblErrorMsg = (TextView) newAddressView.findViewById(R.id.lblErrorMsg);
        btnChoose = (Button)newAddressView.findViewById(R.id.btnChoose);

        newAddressView.findViewById(R.id.btnAddAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });

        newAddressView.findViewById(R.id.btnCancelAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, SelectLocalityActivity.class);
                intent.putExtra("PINCODE", txtPincode.getText().toString());
                intent.putExtra("LOCALITY", locality = txtLocality.getText().toString());
                startActivityForResult(intent, 500);
            }
        });

        if (isValid(addressToBe) && addressToBe.equals("EditAddress")) {
            updatetUI(newAddressView);
        }

        txtPincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                txtLocality.setText("");
            }
        });
        return newAddressView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        if (requestCode == 500) {
            if (resultCode == mContext.RESULT_OK) {
                if (null != data) {
                    setLocality(data.getStringExtra("LOCALITY"), data.getStringExtra("CITY"), data.getStringExtra("STATE"));
                }
            }
        }
    }

    void updatetUI(View editAddressView) {
        txtName.setText(mAddress.getName());
        txtAddress.setText(mAddress.getAddress());
//        txtAddress1.setText(mAddress.getAddress1());
//        txtAddress2.setText(mAddress.getAddress2());
        txtPincode.setText(mAddress.getPincode());
//        txtLandmark.setText(mAddress.getLandmark());
        txtLocality.setText(mAddress.getLocality());
//        txtCity.setText(mAddress.getDistrict());
        txtCity.setText(mAddress.getCity());
        txtState.setText(mAddress.getState());
        txtMobileno.setText(mAddress.getMobile_no());
        chkDefaultAddress.setChecked(mAddress.getIsPrimary());

        ((Button) editAddressView.findViewById(R.id.btnAddAddress)).setText("UPDATE ADDRESS");
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    void validation() {

        if (!isValid(locality = getText(txtLocality))) {
            lblErrorMsg.setText("PLEASE ENTER LOCALITY");
            fLocalityLayout.setError("PLEASE ENTER LOCALITY");
//            Toast.makeText(getActivity(), "Enter locality", Toast.LENGTH_LONG).show();
            return;
        }

        if (!isValid(pincode = getText(txtPincode))) {
            lblErrorMsg.setText("PLEASE ENTER PINCODE");
            fPincodeLayout.setError("PLEASE ENTER PINCODE");
//            Toast.makeText(getActivity(), "Enter pincode", Toast.LENGTH_LONG).show();
            return;
        }

        if (!isValid(name = getText(txtName))) {
            lblErrorMsg.setText("PLEASE ENTER NAME");
//            Toast.makeText(getActivity(), "Enter name", Toast.LENGTH_LONG).show();
            fNameLayout.setError("PLEASE ENTER NAME");
            return;
        }

        if (!isValid(address = getText(txtAddress))) {
//                && !isValid(address1 = getText(txtAddress1))
//                && !isValid(address2 = getText(txtAddress2))) {
            lblErrorMsg.setText("PLEASE ENTER ADDRESS");
            fAddressLayout.setError("PLEASE ENTER ADDRESS");
//            Toast.makeText(getActivity(), "Enter address", Toast.LENGTH_LONG).show();
            return;
        }


//        address1 = getText(txtAddress1);
//        address2 = getText(txtAddress2);


//        if (!isValid(landmark = getText(txtLandmark))) {
//            lblErrorMsg.setText("PLEASE ENTER LANDMARK.");
////            Toast.makeText(getActivity(), "Enter landmark", Toast.LENGTH_LONG).show();
//            return;
//        }


         if (txtCity.getText().toString().trim().length() == 0 || txtState.getText().toString().trim().length() == 0) {

            Toast.makeText(getActivity(), "NO CITY/DISTRICT OR STATE FOUND.", Toast.LENGTH_LONG).show();
            return;
        }
  /*      if () {
            lblErrorMsg.setText("NO STATE FOUND.");
            fAddressLayout.setError("NO CITY/DISTRICT FOUND.");
//            Toast.makeText(getActivity(), "Enter city", Toast.LENGTH_LONG).show();
            return;
        }*/

        if (!isValid(mobileno = getText(txtMobileno))) {
            lblErrorMsg.setText("PLEASE ENTER MOBILE NO");
            fMobileLayout.setError("PLEASE ENTER MOBILE NO");
//            Toast.makeText(getActivity(), "Enter mobile no", Toast.LENGTH_LONG).show();
            return;
        }

        lblErrorMsg.setVisibility(View.INVISIBLE);

        Address addressObj = new Address();
        addressObj.setName(name);
        addressObj.setAddress(address);
        addressObj.setAddress1(TextUtils.isEmpty(address1) ? "" : address1);
        addressObj.setAddress2(TextUtils.isEmpty(address2) ? "" : address2);
        addressObj.setPincode(pincode);
//        addressObj.setLandmark(landmark);
        addressObj.setLocality(locality);
//        addressObj.setDistrict(city);
        addressObj.setCity(city);
        addressObj.setState(state);
        addressObj.setMobile_no(mobileno);
        addressObj.setIsPrimary(chkDefaultAddress.isChecked());

        if (isValid(addressToBe) && addressToBe.equals("EditAddress")) {
            newAddressView.findViewById(R.id.btnAddAddress).setEnabled(false);
            addressObj.setId(mAddress.getId());
            ((AddressActivity) getActivity()).editAddressAPICAll(addressObj);
        } else {
            newAddressView.findViewById(R.id.btnAddAddress).setEnabled(false);
            ((AddressActivity) getActivity()).addNewAddressAPICAll(addressObj);
        }
    }

    boolean isValid(String text) {
        return null != text;

    }

    String getText(EditText editText) {
        if (editText.getText().length() != 0) {
            return editText.getText().toString().trim();
        }


        return null;
    }
}
