package com.specimen.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.product.IProductFacade;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.SpyAndCartItemCountResponse;
import com.specimen.core.spyandcartitemcount.ISpyAndCartItemCountFacade;
import com.specimen.search.SearchFragment;

/**
 * Created by Tejas on 6/1/2015.
 * Seperate activity is provided to differentiate in home screen fragment and base activity fragments.
 * <p/>
 * Edited by Swapnil on 6/24/2015
 */
public class HomeActivity extends BaseFragment {

    private final String TAG = "HomeActivity";
    private ViewPager homePager;
    private PagerAdapter homePagerAdapter;
    private TextView lblTicker, lblBulletin, lblHandpicked;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(getContentView(), container, false);
        callAfterInitialization(view);

        ((ISpyAndCartItemCountFacade) IOCContainer.getInstance().getObject(ServiceName.SPYANDCART_ITEM_COUNT_SERVICE, TAG)).getTotalSpyCartCount();

        return view;
    }

    protected void callAfterInitialization(View view) {
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);

        homePager = (ViewPager) view.findViewById(R.id.userauthenticate_pager);
        homePagerAdapter = new HomeActivityDealPagerAdapter(getChildFragmentManager());
        homePager.setAdapter(homePagerAdapter);
//        homePager.setIsSwipeable(true);

        lblTicker = (TextView) view.findViewById(R.id.lblTickerTab);
        lblBulletin = (TextView) view.findViewById(R.id.lblBulletinTab);
        lblHandpicked = (TextView) view.findViewById(R.id.lblHandpickedTab);

        lblTicker.setBackgroundColor(getResources().getColor(R.color.specimen_grey));

        lblTicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePager.setCurrentItem(0, true);
                setTabBackgeound(lblTicker);
//                showViewPagerButtons();
//                mainMenu.setVisibility(View.VISIBLE);
            }
        });

        lblBulletin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePager.setCurrentItem(1, true);
                setTabBackgeound(lblBulletin);
//                showViewPagerButtons();
//                mainMenu.setVisibility(View.VISIBLE);
            }
        });

        lblHandpicked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homePager.setCurrentItem(2, true);
                setTabBackgeound(lblHandpicked);
//                hideViewPagerButtons();
//                mainMenu.setVisibility(View.GONE);
            }
        });

        view.findViewById(R.id.imbSearch).setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
        view.findViewById(R.id.imbSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.switchFragment(new SearchFragment(),true,R.id.fragmentContainer);
            }
        });

        homePager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        lblTicker.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        lblBulletin.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        lblHandpicked.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        break;
                    case 1:
                        lblTicker.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        lblBulletin.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        lblHandpicked.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        break;
                    case 2:
                        lblTicker.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        lblBulletin.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
                        lblHandpicked.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    protected int getContentView() {
        return R.layout.activity_home;
    }

    void setTabBackgeound(View selectedTab) {
        lblTicker.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
        lblBulletin.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));
        lblHandpicked.setBackgroundColor(getResources().getColor(R.color.specimen_grey_light));

        selectedTab.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        super.onSuccess(response, tag);

        if(!tag.equals(TAG)) {
            return;
        }

        if(response instanceof SpyAndCartItemCountResponse) {
            SpyAndCartItemCountResponse spyAndCartItemCountResponse = (SpyAndCartItemCountResponse) response;
            Log.i(TAG, "spy count : " + spyAndCartItemCountResponse.getData().getSpy_count());
            Log.i(TAG, "cart count : " + spyAndCartItemCountResponse.getData().getShoppingcart_count());

            if(!TextUtils.isEmpty(spyAndCartItemCountResponse.getData().getShoppingcart_count())) {
                ((MainActivity) getActivity()).setCartCount(Integer.parseInt(spyAndCartItemCountResponse.getData().getShoppingcart_count()));
//                MainActivity.addToCartAnimatedView();
            }

            MainActivity.setSpyCount(spyAndCartItemCountResponse.getData().getSpy_count());
        }
    }

    @Override
    public void onFailure(Exception error) {
        super.onFailure(error);
    }
}
