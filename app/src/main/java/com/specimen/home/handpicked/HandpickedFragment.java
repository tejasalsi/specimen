package com.specimen.home.handpicked;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.askpersonalstylist.AskStylistActivity;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.model.IsSurveyCompleteResponse;
import com.specimen.core.response.APIResponse;
import com.specimen.core.survey.ISurveyFacade;
import com.specimen.home.handpicked.HandpickedActivity;
import com.specimen.survey.SurveyActivity;
import com.specimen.widget.ProgressView;

/**
 * Created by Swapnil on 21/7/15.
 */
public class HandpickedFragment extends BaseFragment implements IResponseSubscribe, View.OnClickListener {

    private static final String TAG = "HANDPICKED_FRAGMENT";
    private Button btnShowHandpicked, btnStartSurvey;
    ImageButton imbAskMe;
    private LinearLayout llStartSurvay;
    private RelativeLayout rlShowHandpicked;
    private ProgressView progressView;
    private TextView handpickStatusTxtView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_handpicked, container, false);

        btnStartSurvey = (Button) rootView.findViewById(R.id.btnStartSurvey);
        btnShowHandpicked = (Button) rootView.findViewById(R.id.btnShowHandpicked);
        imbAskMe = (ImageButton) rootView.findViewById(R.id.imbAskStylist);

        llStartSurvay = (LinearLayout) rootView.findViewById(R.id.llStartSurvey);
        rlShowHandpicked = (RelativeLayout) rootView.findViewById(R.id.rlShowHandpicked);
        progressView = (ProgressView) rootView.findViewById(R.id.progressBar);
        handpickStatusTxtView = (TextView)rootView.findViewById(R.id.textView20);

        btnShowHandpicked.setOnClickListener(this);
        btnStartSurvey.setOnClickListener(this);
        imbAskMe.setOnClickListener(this);


        llStartSurvay.setVisibility(View.GONE);
        rlShowHandpicked.setVisibility(View.GONE);

        getSurveyStatus();

        return rootView;
    }

    // Check whether user has completed survey or not
    private void getSurveyStatus() {
        progressView.setVisibility(View.VISIBLE);
        ((ISurveyFacade) IOCContainer.getInstance().getObject(ServiceName.SURVEY_SERVICE, TAG)).isSurveyCompleted();
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        progressView.setVisibility(View.GONE);
        Log.d(TAG, tag + " " + response.toString());
        if (TAG.equals(tag)) {
            rlShowHandpicked.setVisibility(View.VISIBLE);
            if (response instanceof IsSurveyCompleteResponse) {
                IsSurveyCompleteResponse surveyResponse = (IsSurveyCompleteResponse) response;
                if (surveyResponse.getIs_survey_completed().equals("true")) {
                    rlShowHandpicked.setVisibility(View.VISIBLE);
                   if(surveyResponse.getIsHandpickedAvailable().equalsIgnoreCase("true")) {

                       handpickStatusTxtView.setText(R.string.handpicked_wehave);
                   }
                    else
                   {
                       handpickStatusTxtView.setText(R.string.handpicked_no_more);
                       btnShowHandpicked.setVisibility(View.INVISIBLE);
                   }
                   } else {
                    llStartSurvay.setVisibility(View.VISIBLE);

                }
            }
        }
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Register the service to get API request and response
        getSurveyStatus();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // unregister the service to get disable API request and response
        progressView.setVisibility(View.GONE);
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onFailure(Exception error) {
        Log.e(TAG, error.toString());
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btnShowHandpicked:
                intent = new Intent(mContext, HandpickedActivity.class);
                startActivity(intent);
                break;

            case R.id.btnStartSurvey:
                intent = new Intent(mContext, SurveyActivity.class);
                startActivity(intent);
                break;

            case R.id.imbAskStylist:
                intent = new Intent(mContext, AskStylistActivity.class);
                startActivity(intent);
                break;
        }
    }
}
