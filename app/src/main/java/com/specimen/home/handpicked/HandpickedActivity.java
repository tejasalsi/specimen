package com.specimen.home.handpicked;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ActionViewTarget;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.cart.ICartFacade;
import com.specimen.core.home.handpicked.IHandpickedFacade;
import com.specimen.core.model.HandpickedProduct;
import com.specimen.core.model.HandpickedProductsResponse;
import com.specimen.core.model.HandpickedRemovedResponse;
import com.specimen.core.model.OnAddToSpiedResponse;
import com.specimen.core.model.ProductSizesListParable;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddOrRemoveFromWishListResponse;
import com.specimen.core.response.CartDetailsResponse;
import com.specimen.home.OnSizeSelectedListener;
import com.specimen.product.productdetails.ProductDetailsActivity;
import com.specimen.spy.SpyActivity;
import com.specimen.ui.dialog.SizeDialog;
import com.specimen.widget.ProgressView;
import com.specimen.widget.swipeablecards.model.CardModel;
import com.specimen.widget.swipeablecards.view.CardContainer;
import com.specimen.widget.swipeablecards.view.SimpleCardStackAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swapnil on 8/3/2015.
 */
public class HandpickedActivity extends BaseActivity implements CardModel.OnCardDismissedListener, View.OnClickListener, OnSizeSelectedListener {

    private static final String TAG = "HANDPICKED_ACTIVITY";
    private CardContainer mCardContainer;
    private Button btnInfo, btnBuyNow, btnDelete, btnSpy;
    private ImageView ivClose;
    private Button btnShowSpied;
    private int productCounter = 1;
    private List<HandpickedProduct> mListHandpickedProducts;
    private HandpickedProductsResponse handpickedProductsResponse;
    private SimpleCardStackAdapter adapter;
    private LinearLayout llButtons, llSpied;
    private RelativeLayout rlNoMoreHandpicked;
    private Intent intent;
    private ProgressView progressView;
    private Animation animFadeOut, animFadeIn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        callAfterInitialization();
    }

    protected void callAfterInitialization() {
        llButtons = (LinearLayout) findViewById(R.id.llButtons);
        rlNoMoreHandpicked = (RelativeLayout) findViewById(R.id.rlNoHandpicked);
        btnInfo = (Button) findViewById(R.id.btnInfo);
        btnBuyNow = (Button) findViewById(R.id.btnBuyNow);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnSpy = (Button) findViewById(R.id.btnAddToSpy);
        btnShowSpied = (Button) findViewById(R.id.btnShowSpied);
        progressView = (ProgressView) findViewById(R.id.progressBar);
        llSpied = (LinearLayout) findViewById(R.id.llSpied);
        ivClose = (ImageView) findViewById(R.id.ivClose);

        animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        llSpied.setOnClickListener(this);
        //llSpied.setAnimation(animFadeIn);
        //  llSpied.setLayoutAnimationListener(new a);
        llSpied.setVisibility(View.GONE);

        btnInfo.setOnClickListener(this);
        btnBuyNow.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnSpy.setOnClickListener(this);
        btnShowSpied.setOnClickListener(this);
        ivClose.setOnClickListener(this);

//        View mainView = (View) findViewById(R.id.mainMenu);
//        mainView.setVisibility(View.GONE);

        mCardContainer = (CardContainer) findViewById(R.id.layoutview);

        loadCardsData();

    }

    public void setSpiedViewAnimation(int imageResource,String message) {
        ((ImageView)llSpied.findViewById(R.id.imageviewIcon)).setImageResource(imageResource);
        ((TextView)llSpied.findViewById(R.id.txtview)).setText(message);
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                llSpied.startAnimation(animFadeOut);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                llSpied.setVisibility(View.GONE);
                int count = adapter.getCount();
                if (count > 0) {
                    adapter.pop();
                    mCardContainer.setmTopCard();
                    if (adapter.getCount() < 1) {
                        hideCardLayout();
                    }
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        llSpied.startAnimation(animFadeIn);
        llSpied.setVisibility(View.VISIBLE);
    }

    public void hideCardLayout() {
        llButtons.setVisibility(View.GONE);
        mCardContainer.setVisibility(View.GONE);
        rlNoMoreHandpicked.setVisibility(View.VISIBLE);
    }

    // Load handpicked data
    private void loadCardsData() {
        progressView.setVisibility(View.VISIBLE);
        ((IHandpickedFacade) IOCContainer.getInstance().getObject(ServiceName.HANDPICKED_SERVICE, TAG)).getHandpickedProducts();
    }

    protected int getContentView() {
        return R.layout.activity_handpicked;
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        Log.d(TAG, tag + " " + response.toString());
        progressView.setVisibility(View.GONE);
        if (TAG.equals(tag)) {
            // If response comes from get Handpicked Product request
            if (response instanceof HandpickedProductsResponse) {
                handpickedProductsResponse = (HandpickedProductsResponse) response;
                int size = handpickedProductsResponse.getData().getProducts().size();

                CardModel card;
                adapter = new SimpleCardStackAdapter(getApplicationContext());
                mListHandpickedProducts = handpickedProductsResponse.getData().getProducts();
                HandpickedProduct mProduct;

                // Add items to swipeable cards and set on Dismiss listeners to the elements
                for (int i = 0; i < size; i++) {
                    mProduct = mListHandpickedProducts.get(i);
                    if (mProduct.getImage().size() > 0) {
                        card = new CardModel(mProduct.getName(), mProduct.getPrice(), mProduct.getImage().get(0));

                    } else {
                        card = new CardModel(mProduct.getName(), mProduct.getPrice(), null);
                    }
                    card.setOnCardDismissedListener(this);
                    adapter.add(card);
                }

                mCardContainer.setAdapter(adapter);

                if (mListHandpickedProducts.size() < 1) {
                    hideCardLayout();
                    Toast.makeText(this, "Apology. No product found for you.", Toast.LENGTH_LONG).show();
                    return;
                }
//                else{
//                    showcaseView();
//                }
            }

            // If product added to cart
            if (response instanceof CartDetailsResponse) {
                setSpiedViewAnimation(R.drawable.trunk_white,"added to trunk");
                Toast.makeText(getApplicationContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                MainActivity.setCartCount(((CartDetailsResponse) response).getData().getshoppingcart_count());
//                progressView.setVisibility(View.VISIBLE);
//                ((IHandpickedFacade) IOCContainer.getInstance().getObject(ServiceName.HANDPICKED_SERVICE, TAG)).removeHandpickedProduct(mListHandpickedProducts.get(mListHandpickedProducts.size() - productCounter).getId());
            }

//            // If response from add to spy
//            if (response instanceof OnAddToSpiedResponse) {
//                OnAddToSpiedResponse mFavListResp = (OnAddToSpiedResponse) response;
//                setSpiedViewAnimation();
//                Toast.makeText(getApplicationContext(), mFavListResp.getMessage(), Toast.LENGTH_SHORT).show();
//                MainActivity.setSpyCount(((OnAddToSpiedResponse) response).getshoppingcart_count());
////                ((IHandpickedFacade) IOCContainer.getInstance().getObject(ServiceName.HANDPICKED_SERVICE, TAG)).removeHandpickedProduct(mListHandpickedProducts.get(mListHandpickedProducts.size()-productCounter).getId());
//            }

            // If response from add to spy
            if (response instanceof AddOrRemoveFromWishListResponse) {
                AddOrRemoveFromWishListResponse mFavListResp = (AddOrRemoveFromWishListResponse) response;
                setSpiedViewAnimation(R.drawable.spi_white,"spied");
                Toast.makeText(getApplicationContext(), mFavListResp.getMessage(), Toast.LENGTH_SHORT).show();
                MainActivity.setSpyCount(mFavListResp.getData().getSpy_count());
//                ((IHandpickedFacade) IOCContainer.getInstance().getObject(ServiceName.HANDPICKED_SERVICE, TAG)).removeHandpickedProduct(mListHandpickedProducts.get(mListHandpickedProducts.size()-productCounter).getId());

                int count = adapter.getCount();
               /* if (count > 0) {
                    adapter.pop();
                    mCardContainer.setmTopCard();
                    if (adapter.getCount() < 1) {
                        hideCardLayout();
                    }
                }*/

            }

            // Product removed from handpick
            if (response instanceof HandpickedRemovedResponse) {
                HandpickedRemovedResponse mHandpickedDeleteResponse = (HandpickedRemovedResponse) response;
                int count = adapter.getCount();
           /*     if (count > 0) {
                    adapter.pop();
                    mCardContainer.setmTopCard();
                    if (adapter.getCount() < 1) {
                        hideCardLayout();
                    }
                }*/
                productCounter++;
                setSpiedViewAnimation(R.drawable.delete_white,"removed");
                Toast.makeText(getApplicationContext(), mHandpickedDeleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    void showcaseView() {
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
        lps.setMargins(margin, margin, margin, margin);
        ViewTarget target = new ViewTarget(R.id.btnInfo, this);
        ShowcaseView sv = new ShowcaseView.Builder(this)
                .setTarget(target)
                .setContentTitle("ShowcaseView")
                .setContentText("This is highlighting the Home button")
                .hideOnTouchOutside()
                .build();
        sv.setButtonPosition(lps);

    }

    @Override
    public void onResume() {
        super.onResume();
        // Register the service to get API request and response
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // unregister the service to get disable API request and response
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onFailure(Exception error) {
        Log.e(TAG, error.toString());
    }

    @Override
    public void onSpyProduct() {
        ((IHandpickedFacade) IOCContainer.getInstance().getObject(ServiceName.HANDPICKED_SERVICE, TAG)).setSpied(mListHandpickedProducts.get(mListHandpickedProducts.size() - productCounter).getId());
        Log.d("Swipeable Card", "Added to spy");
    }

    // callback method to remove item from
    @Override
    public void onDeleteProduct() {
        Log.d("Swipeable Card", "Delete from list");
        ((IHandpickedFacade) IOCContainer.getInstance().getObject(ServiceName.HANDPICKED_SERVICE, TAG)).removeHandpickedProduct(mListHandpickedProducts.get(mListHandpickedProducts.size() - productCounter).getId());
    }

    @Override
    public void onClick(View v) {
        HandpickedProduct mHandpickedProduct = null;
        try {
            if (mListHandpickedProducts != null && 0 < mListHandpickedProducts.size()) {
                if (mListHandpickedProducts.size() > (mListHandpickedProducts.size() - productCounter) && mListHandpickedProducts.size() > 0) {
                    mHandpickedProduct = mListHandpickedProducts.get(mListHandpickedProducts.size() - productCounter);
                }
            }
        } catch (Exception e) {
            // TODO : Handle Exception
        }
        switch (v.getId()) {
            case R.id.btnBuyNow:

                progressView.setVisibility(View.GONE);

                if (mHandpickedProduct.getSizes() != null) {
                    int size = mHandpickedProduct.getSizes().size();
                    List<Boolean> checkProductSizesIfBlank = new ArrayList<>();
                    for (int i = 0; i < size; i++) {
                        if (mHandpickedProduct.getSizes().get(i).getStock_count() == 0) {
                            checkProductSizesIfBlank.add(true);
                        }
                    }

                    if (0 < checkProductSizesIfBlank.size() && size == checkProductSizesIfBlank.size()) {
                        Toast.makeText(this, "Apologies, this product is out of stock.", Toast.LENGTH_SHORT).show();
                    } else {
                        SizeDialog sizeDialog = new SizeDialog();
                        sizeDialog.setOnSizeSelectedListner(HandpickedActivity.this);
                        Bundle bundle = new Bundle();
                        ProductSizesListParable productSizesListParable = new ProductSizesListParable(mHandpickedProduct != null ? mHandpickedProduct.getSizes() : null);
                        bundle.putParcelable("product", productSizesListParable);
                        bundle.putString("productId", mHandpickedProduct.getId());
                        bundle.putString("attributeId", mHandpickedProduct.getSizes().get(0).getAttribute_id());
                        bundle.putString("tag", TAG);
                        sizeDialog.setArguments(bundle);
                        sizeDialog.show(getSupportFragmentManager(), TAG);
                    }
                } else {
                    Toast.makeText(this, "Apologies, this product is out of stock.", Toast.LENGTH_SHORT).show();
                }


                break;

            case R.id.btnDelete:
                ((IHandpickedFacade) IOCContainer.getInstance().getObject(ServiceName.HANDPICKED_SERVICE, TAG)).removeHandpickedProduct(mListHandpickedProducts.get(mListHandpickedProducts.size() - productCounter).getId());

                break;

            case R.id.btnAddToSpy:
                ((IHandpickedFacade) IOCContainer.getInstance().getObject(ServiceName.HANDPICKED_SERVICE, TAG)).setSpied(mListHandpickedProducts.get(mListHandpickedProducts.size() - productCounter).getId());
                break;


            case R.id.btnInfo:
                // Redirect to the product details page
                intent = new Intent(this, ProductDetailsActivity.class);
                intent.putExtra("PRODUCT_ID", mListHandpickedProducts.get(mListHandpickedProducts.size() - productCounter).getId());
                startActivity(intent);

                break;

            case R.id.btnShowSpied:
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("tag", MainActivity.SPY);
                startActivity(intent);
                finish();
                break;

            case R.id.ivClose:
                this.finish();
                break;
        }
    }


    // After selecting a size of product add product to the cart
    @Override
    public void onSizeSelected(String productId) {
      /*  ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, TAG))
                .addToCart(mListHandpickedProducts.get(mListHandpickedProducts.size() - productCounter).getId(), "1", productId,
                        mListHandpickedProducts.get(mListHandpickedProducts.size() - productCounter).getSizes().get(0).getAttribute_id());*/
    }


}
