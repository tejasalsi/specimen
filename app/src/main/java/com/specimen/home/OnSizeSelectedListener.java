package com.specimen.home;

/**
 * Created by Swapnil on 8/4/2015.
 */
public interface OnSizeSelectedListener {

    void onSizeSelected(String productId);
}
