package com.specimen.home.deal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.deal.IDealFacade;
import com.specimen.core.model.DealsEntity;
import com.specimen.core.model.ProductSizesListParable;
import com.specimen.ui.dialog.SizeDialog;
import com.specimen.util.ImageUtils;
import com.specimen.util.ViewHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by root on 6/10/15.
 */
public class DealAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IDealCloseTimerUpdate {

    final int OPEN = 1;
    final int CLOSE = 2;
    final int UPCOMING = 3;

    Activity mActivity;
    DisplayMetrics mDisplayMetrics;
    IDealOnclickListeners mIDealOnclickListeners;

    List<DealsEntity> dealsEntityList;
    int viewPosition;
    ArrayList<IUpdateTimer> iUpdateTimer;
    List<Integer> mSelectedProductSpiedCount;
    private static Timer mTimer = null;

    /**
     * @param activity
     * @param dealsEntityList
     */
    public DealAdapter(Activity activity, List<DealsEntity> dealsEntityList) {
        this.mActivity = activity;
        this.dealsEntityList = dealsEntityList;
        this.mSelectedProductSpiedCount = new ArrayList<>();
        this.mDisplayMetrics = mActivity.getResources().getDisplayMetrics();
        iUpdateTimer = new ArrayList<>();
        useTimer();
    }

    /**
     * @param iDealOnclickListeners
     */
    public void setDealOnclickListeners(IDealOnclickListeners iDealOnclickListeners) {
        mIDealOnclickListeners = iDealOnclickListeners;
    }

    /**
     * @param recyclerView
     */
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void updateDeals() {
        notifyDataSetChanged();
    }

    /**
     * Called when RecyclerView needs a new {@link ViewHolder} of the given type to represent
     * an item.
     * <p/>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p/>
     * The new ViewHolder will be used to display items of the adapter using
     * . Since it will be re-used to display different
     * items in the data set, it is a good idea to cache references to sub views of the View to
     * avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (OPEN == viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.deal_open_layout, parent, false);
            OpenDealHolder openDealViewHolder = new OpenDealHolder(view);
            iUpdateTimer.add(openDealViewHolder);
//            openDealViewHolder.startRunningGauge(mActivity);
            return openDealViewHolder;
        } else if (CLOSE == viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.deal_closed_layout, parent, false);
            CloseDealHolder closeDealViewHolder = new CloseDealHolder(view);
//            closeDealViewHolder.startRunningGauge(mActivity, );
            return closeDealViewHolder;
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.deal_upcoming_layout, parent, false);
            UpcomingDealHolder upcomingDealHolder = new UpcomingDealHolder(view, mActivity, this);
            upcomingDealHolder.setClickListener();
            iUpdateTimer.add(upcomingDealHolder);
            return upcomingDealHolder;
        }
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method
     * should update the contents of the  to reflect the item at
     * the given position.
     * <p/>
     * Note that unlike {@link RecyclerView}, RecyclerView will not call this
     * method again if the position of the item changes in the data set unless the item itself
     * is invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside this
     * method and should not keep a copy of it. If you need the position of an item later on
     * (e.g. in a click listener), use  which will have
     * the updated adapter position.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        viewPosition = position;

        if (OPEN == holder.getItemViewType()) {
            DealsEntity dealsEntity = dealsEntityList.get(position);
            ((OpenDealHolder) holder).setDealsEntity(dealsEntity);
            ((OpenDealHolder) holder).setDealCloseTimerInterface(this);
            setCurrentDealLayout(dealsEntity, (OpenDealHolder) holder);
            ((OpenDealHolder) holder).startRunningGauge(mActivity, dealsEntityList.get(position).getTotal_stock());
        } else if (CLOSE == holder.getItemViewType()) {
            DealsEntity dealsEntity = dealsEntityList.get(position);
            setClosedDealLayout(dealsEntity, (CloseDealHolder) holder);
            ((CloseDealHolder) holder).startRunningGauge(mActivity, dealsEntityList.get(position).getTotal_stock());
        } else {
            DealsEntity dealsEntity = dealsEntityList.get(position);
            ((UpcomingDealHolder) holder).setDealsEntity(dealsEntity);
            ((UpcomingDealHolder) holder).setDealCloseTimerInterface(this);
        }
    }

    /**
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        if (dealsEntityList.get(position).getStatus().equals("open")) {
            return OPEN;
        } else if (dealsEntityList.get(position).getStatus().equals("close")) {
            return CLOSE;
        } else {
            return UPCOMING;
        }
    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return dealsEntityList.size();
    }

    /**
     * open deal layout
     *
     * @param dealsEntity
     * @param openDealViewHolder
     */
    private void setCurrentDealLayout(final DealsEntity dealsEntity, final OpenDealHolder openDealViewHolder) {

        if (null != dealsEntity.getImage() && 0 < dealsEntity.getImage().size()) {
            Picasso.with(mActivity)
                    .load(ImageUtils.getFullSizeImage(dealsEntity.getImage().get(0), mDisplayMetrics))
//                .placeholder(R.drawable.deal_page_product_banner_placeholder)
                    .placeholder(R.drawable.banner_explore_placeholder)
                    .resize(mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels)
                    .centerInside()
                    .into(openDealViewHolder.tickerBannerImg_open_deal);
        }

        /******************************************************************************************/

        // set views / spy count / stock count
        openDealViewHolder.tickerProductTitle_open_deal.setText(dealsEntity.getName().toUpperCase());
        openDealViewHolder.text_open_deal_Spying.setText(dealsEntity.getSpied_count() + " SPYING");
        openDealViewHolder.lbl_open_deal_TotalViews.setText(dealsEntity.getView_count() + " VIEWS");
//        openDealViewHolder.lbl_open_deal_RemainingStock.setText(dealsEntity.getRemaining_stock() + " LEFT");
        openDealViewHolder.lbl_open_deal_RemainingStock.setText(dealsEntity.getTotal_stock() + " LEFT");

        /******************************************************************************************/

        // Deal price
        if (null == dealsEntity.getCurrent_price()) {
            openDealViewHolder.tickerProductPrice_open_deal
                    .setText(mActivity.getResources().getString(R.string.rupee_symbole) + " " + dealsEntity.getOriginal_price());

            // Add to cartRemaining_stock()
            openDealViewHolder.btn_open_deal_buy.setText("BUY "
                    + mActivity.getResources().getString(R.string.rupee_symbole)
                    + " " + dealsEntity.getOriginal_price());
        } else {
            openDealViewHolder.tickerProductPrice_open_deal
                    .setText(mActivity.getResources().getString(R.string.rupee_symbole) + " " + dealsEntity.getCurrent_price());

            String currentPrice = dealsEntity.getCurrent_price().replaceAll(",","");
            String originalPrice = dealsEntity.getOriginal_price().replaceAll(",","");

            if (Double.valueOf(currentPrice) > Double.valueOf(originalPrice)) {
                openDealViewHolder.imgUpDownPrice_open_deal.setImageResource(R.drawable.arrow_red_down);
            } else {
                openDealViewHolder.imgUpDownPrice_open_deal.setImageResource(R.drawable.arrow_green_up);
            }

            // Add to cartRemaining_stock()
            openDealViewHolder.btn_open_deal_buy.setText("BUY "
                    + mActivity.getResources().getString(R.string.rupee_symbole)
                    + " " + dealsEntity.getCurrent_price());
        }
        openDealViewHolder.text_open_deal_OriginalPrice
                .setText(mActivity.getResources().getString(R.string.rupee_symbole) + " " + dealsEntity.getOriginal_price());
        openDealViewHolder.text_open_deal_TodaysLow
                .setText(mActivity.getResources().getString(R.string.rupee_symbole) + " " + dealsEntity.getLow_price());
        openDealViewHolder.text_open_deal_TodayHigh
                .setText(mActivity.getResources().getString(R.string.rupee_symbole) + " " + dealsEntity.getHigh_price());

        /******************************************************************************************/

        String friendsCount = ((IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, "")).getOfflineResponse("fbFriendsCount");
        openDealViewHolder.text_open_deal_Friends.setText((TextUtils.isEmpty(friendsCount) ? "0" : friendsCount) + " ARE FRIENDS");

        /******************************************************************************************/

        if (dealsEntity.getTotal_stock() == 0) {
            openDealViewHolder.btn_open_deal_buy.setBackgroundColor(mActivity.getResources().getColor(R.color.specimen_grey_light));
            openDealViewHolder.btn_open_deal_buy.setTextColor(mActivity.getResources().getColor(R.color.white));
            openDealViewHolder.btn_open_deal_buy.setEnabled(false);
        }

        /******************************************************************************************/

        openDealViewHolder.btn_open_deal_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIDealOnclickListeners.dealBuynow(dealsEntity);
            }
        });

        if (dealsEntity.getIsSpied()) {
            openDealViewHolder.btn_open_deal_spy.setBackgroundResource(R.drawable.spy_btn_green);
            openDealViewHolder.btn_open_deal_spy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mIDealOnclickListeners.removeDealSpy(dealsEntity.getId());
                    openDealViewHolder.btn_open_deal_spy.setBackgroundResource(R.drawable.spy_btn_hover);
                }
            });
        } else {
            openDealViewHolder.btn_open_deal_spy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isProductSpiedInList(viewPosition)) {
                        mIDealOnclickListeners.removeDealSpy(dealsEntity.getId());
                        openDealViewHolder.btn_open_deal_spy.setBackgroundResource(R.drawable.spy_btn_hover);
                        mSelectedProductSpiedCount.remove(viewPosition);
                    } else {
                        mSelectedProductSpiedCount.add(viewPosition);
                        openDealViewHolder.btn_open_deal_spy.setBackgroundResource(R.drawable.spy_btn_green);
                        mIDealOnclickListeners.dealSpy(dealsEntity.getId());
                    }
                }
            });
        }

        openDealViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIDealOnclickListeners.dealProductClick(dealsEntity, openDealViewHolder.hrtxt_open_deal.getText().toString(),
                        openDealViewHolder.mintxt_open_deal.getText().toString(), openDealViewHolder.sectxt_open_deal.getText().toString());
            }
        });
    }

    /**
     * @param dealsEntity
     * @param closeDealHolder
     */
    private void setClosedDealLayout(final DealsEntity dealsEntity, final CloseDealHolder closeDealHolder) {
        if (null != dealsEntity.getImage() && 0 < dealsEntity.getImage().size()) {
            Picasso.with(mActivity)
                    .load(ImageUtils.getFullSizeImage(dealsEntity.getImage().get(0), mDisplayMetrics))
//                .placeholder(R.drawable.deal_page_product_banner_placeholder)
                    .placeholder(R.drawable.banner_explore_placeholder)
                    .resize(300, 480)
                    .centerInside()
                    .into(closeDealHolder.tickerBannerImg_closed_deal);
        }

        /******************************************************************************************/

        // set views / spy count / stock count
        closeDealHolder.tickerProductTitle_closed_deal.setText(dealsEntity.getName().toUpperCase());
        closeDealHolder.text_closed_deal_Spying.setText(dealsEntity.getSpied_count() + " SPYING");
        closeDealHolder.lbl_closed_deal_TotalViews.setText(dealsEntity.getView_count() + " VIEWS");
//        closeDealHolder.lblRemainingStock_closed_deal.setText(dealsEntity.getRemaining_stock() + " LEFT");
        closeDealHolder.lblRemainingStock_closed_deal.setText(dealsEntity.getTotal_stock() + " LEFT");

        /******************************************************************************************/

        // Deal price
        closeDealHolder.tickerProductPrice_closed_deal
                .setText(mActivity.getResources().getString(R.string.rupee_symbole) + " " + dealsEntity.getOriginal_price());
        closeDealHolder.text_closed_deal_OriginalPrice
                .setText(mActivity.getResources().getString(R.string.rupee_symbole) + " " + dealsEntity.getOriginal_price());
        closeDealHolder.text_closed_deal_TodaysLow
                .setText(mActivity.getResources().getString(R.string.rupee_symbole) + " " + dealsEntity.getLow_price());
        closeDealHolder.text_closed_deal_TodayHigh
                .setText(mActivity.getResources().getString(R.string.rupee_symbole) + " " + dealsEntity.getHigh_price());

        /******************************************************************************************/

        String friendsCount = ((IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, "")).getOfflineResponse("fbFriendsCount");
        closeDealHolder.text_closed_deal_Friends.setText((TextUtils.isEmpty(friendsCount) ? "0" : friendsCount) + " ARE FRIENDS");

        /******************************************************************************************/

        // Add to cart
        closeDealHolder.btn_closed_deal_buy.setText("BUY " + mActivity.getResources().getString(R.string.rupee_symbole) + " " + dealsEntity.getOriginal_price());

        if (0 == dealsEntity.getTotal_stock()) {
            closeDealHolder.btn_closed_deal_buy.setBackgroundColor(mActivity.getResources().getColor(R.color.specimen_grey));
            closeDealHolder.btn_closed_deal_buy.setTextColor(Color.BLACK);
            closeDealHolder.btn_closed_deal_buy.setEnabled(false);
        } else {
            closeDealHolder.btn_closed_deal_buy.setBackgroundColor(mActivity.getResources().getColor(R.color.specimen_green));
            closeDealHolder.btn_closed_deal_buy.setTextColor(Color.WHITE);
            closeDealHolder.btn_closed_deal_buy.setEnabled(true);
        }

        closeDealHolder.btn_closed_deal_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIDealOnclickListeners.dealBuynow(dealsEntity);
            }
        });

        if (dealsEntity.getIsSpied()) {
            closeDealHolder.btn_closed_deal_spy.setBackgroundResource(R.drawable.spy_btn_green);
            closeDealHolder.btn_closed_deal_spy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mIDealOnclickListeners.removeDealSpy(dealsEntity.getId());
                    closeDealHolder.btn_closed_deal_spy.setBackgroundResource(R.drawable.spy_btn_hover);
                }
            });
        } else {
            closeDealHolder.btn_closed_deal_spy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isProductSpiedInList(viewPosition)) {
                        closeDealHolder.btn_closed_deal_spy.setBackgroundResource(R.drawable.spy_btn_green);
                        mIDealOnclickListeners.removeDealSpy(dealsEntity.getId());
                        closeDealHolder.btn_closed_deal_spy.setBackgroundResource(R.drawable.spy_btn_hover);
                        mSelectedProductSpiedCount.remove(viewPosition);
                    } else {
                        mSelectedProductSpiedCount.add(viewPosition);
                        closeDealHolder.btn_closed_deal_spy.setBackgroundResource(R.drawable.spy_btn_green);
                        mIDealOnclickListeners.dealSpy(dealsEntity.getId());
                    }
                }
            });
        }

        closeDealHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIDealOnclickListeners.dealProductClick(dealsEntity, "", "", "");
            }
        });
    }

    /**
     *
     */
    private Handler messageHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            DealFragment.currentTimeInMillis++;
            for (IUpdateTimer iUpdate : iUpdateTimer) {
                iUpdate.updateTimer(DealFragment.currentTimeInMillis);
            }
        }
    };

    /**
     *
     */
    private void useTimer() {
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                messageHandler.sendEmptyMessage(0);
            }
        }, 0, 1 * 1000);
    }

    public void cancleTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            DealFragment.currentTimeInMillis = 0;
        }
    }

    /**
     * @param status
     */
    @Override
    public void dealCloseTimerUpdate(String status) {
        DealFragment.newInstance().recursiveDealAPICall();
    }

    private boolean isProductSpiedInList(int viewPosition) {
        for (int position : mSelectedProductSpiedCount) {
            if (position == viewPosition) {
                return true;
            }
        }

        return false;
    }
}
