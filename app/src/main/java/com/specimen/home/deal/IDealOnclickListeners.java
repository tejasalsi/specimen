package com.specimen.home.deal;

import com.specimen.core.model.DealsEntity;

/**
 * Created by root on 10/10/15.
 */
public interface IDealOnclickListeners {

    void dealBuynow(DealsEntity dealsEntity);

    void dealSpy(String productId);

    void dealProductClick (DealsEntity dealsEntity, String hrs, String mins, String secs);

    void removeDealSpy(String productId);
}
