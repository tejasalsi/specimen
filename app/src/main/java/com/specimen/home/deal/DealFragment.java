package com.specimen.home.deal;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.specimen.BaseFragment;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.cart.ICartFacade;
import com.specimen.core.deal.IDealFacade;
import com.specimen.core.model.BulletinEntity;
import com.specimen.core.model.DealResponseData;
import com.specimen.core.model.DealsEntity;
import com.specimen.core.model.ProductSizesListParable;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.AddOrRemoveFromWishListResponse;
import com.specimen.core.response.CartDetailsResponse;
import com.specimen.core.response.DealResponse;
import com.specimen.core.spy.ISpyFacade;
import com.specimen.home.OnSizeSelectedListener;
import com.specimen.product.productdetails.ProductDetailsActivity;
import com.specimen.ui.dialog.SizeDialog;
import com.specimen.util.ImageUtils;
import com.specimen.widget.ProgressView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by root on 21/7/15.
 */

public class DealFragment extends BaseFragment implements IResponseSubscribe, OnSizeSelectedListener, IDealOnclickListeners {

    public static long currentTimeInMillis;

    private final String TAG = "DEAL";

    private DealAdapter mDealAdapter;
    private RecyclerView dealRecyclerView;
    private View tickerView;
    private ProgressView mProgressView;
    private List<DealsEntity> dealsEntityList;
    private DealsEntity tempDealsEntity;


    public static DealFragment newInstance() {
        return new DealFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        tickerView = inflater.inflate(R.layout.fragment_ticker, container, false);
        mProgressView = (ProgressView) tickerView.findViewById(R.id.progressView);

        mProgressView.post(new Runnable() {
            @Override
            public void run() {
                mProgressView.setVisibility(View.VISIBLE);
                mProgressView.setAnimation();
            }
        });
//        ((IDealFacade) IOCContainer.getInstance().getObject(ServiceName.DEAL_SERVICE, TAG)).deal();
        getFBFriends();

//        /* make the API call */
//        new GraphRequest(
//                AccessToken.getCurrentAccessToken(),
//                "/me/friends",
//                null,
//                HttpMethod.GET,
//                new GraphRequest.Callback() {
//                    public void onCompleted(GraphResponse response) {
//            /* handle the result */
//                        Toast.makeText(getActivity(), ""+response.toString(), Toast.LENGTH_SHORT).show();
//                    }
//                }
//        ).executeAsync();

        return tickerView;
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//            ((IDealFacade) IOCContainer.getInstance().getObject(ServiceName.DEAL_SERVICE, TAG)).deal();
//        }
//    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        mProgressView.post(new Runnable() {
            @Override
            public void run() {
                mProgressView.setVisibility(View.VISIBLE);
                mProgressView.setAnimation();
            }
        });
        ((IDealFacade) IOCContainer.getInstance().getObject(ServiceName.DEAL_SERVICE, TAG)).deal();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    void currentDeal() {
        cancelTimer();

        mDealAdapter = new DealAdapter(mContext, dealsEntityList);
        mDealAdapter.setDealOnclickListeners(this);
        dealRecyclerView = (RecyclerView) tickerView.findViewById(R.id.list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        dealRecyclerView.setLayoutManager(layoutManager);
        dealRecyclerView.setAdapter(mDealAdapter);
        int lastFirstVisiblePosition = ((LinearLayoutManager) dealRecyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        layoutManager.scrollToPositionWithOffset(lastFirstVisiblePosition, 20);
    }

    void setDealLayout(DealResponse response) {
        DealResponseData dealResponseData = response.getData();
        if (null != response.getData() && null != response.getData().getDeals() && 0 < response.getData().getDeals().size()) {
            dealsEntityList = new ArrayList<>();
            dealsEntityList.clear();
            dealsEntityList.addAll(dealResponseData.getDeals());
            Collections.sort(dealsEntityList, new Comparator<DealsEntity>() {
                @Override
                public int compare(DealsEntity lhs, DealsEntity rhs) {
                    // compareTo should return < 0 if this is supposed to be
                    // less than other, > 0 if this is supposed to be greater than
                    // other and 0 if they are supposed to be equal

                    String lhsStatus = lhs.getStatus();
                    String rhsStatus = rhs.getStatus();

                    if (lhsStatus.equalsIgnoreCase("open")) {
                        return -1;
                    } else if (rhsStatus.equalsIgnoreCase("open")) {
                        return 1;
                    } else if (lhsStatus.equalsIgnoreCase("upcoming")) {
                        return -1;
                    } else if (rhsStatus.equalsIgnoreCase("upcoming")) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            });
            currentDeal();
        } else {
            Toast.makeText(getActivity(), "Ahhh... No deal is running. Please come back next time.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        mProgressView.setVisibility(View.GONE);
        if (!tag.equals("DEAL")) {
            return;
        }

        if (response instanceof DealResponse) {
            setDealLayout((DealResponse) response);
            recursiveDealAPICall();
        }

        if (response instanceof CartDetailsResponse) {
//            Toast.makeText(getActivity(), "" + response.getMessage(), Toast.LENGTH_SHORT).show();
//            currentTimeInMillis = 0;
            MainActivity.setCartCount(((CartDetailsResponse) response).getData().getshoppingcart_count());
//            ((IDealFacade) IOCContainer.getInstance().getObject(ServiceName.DEAL_SERVICE, TAG)).deal();

            View addView = LayoutInflater.from(mContext).inflate(R.layout.animated_add_cart_alert_view, null);
            DisplayMetrics matrix = mContext.getResources().getDisplayMetrics();

            final PopupWindow popupWindow = new PopupWindow(
                    addView,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            Button btnDismiss = (Button) addView.findViewById(R.id.imgCancelPopup);
            btnDismiss.setOnClickListener(new Button.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (null != popupWindow)
                        popupWindow.dismiss();
                }
            });
            popupWindow.showAsDropDown(MainActivity.trunk_tab, 50, (int) MainActivity.trunk_tab.getY());
            // Closes the popup window when touch outside.
            popupWindow.setOutsideTouchable(true);
            popupWindow.setFocusable(true);
            // Removes default background.
            popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            ((TextView) addView.findViewById(R.id.lblAnimatedCartProductName)).setText("" + tempDealsEntity.getName());

           String remainingTime = tempDealsEntity.getRemaining_time();

           if(!remainingTime.isEmpty() && !remainingTime.equals("00:00:00"))
           {
               String [] nextPriceTime = remainingTime.split(":");

               //((TextView)addView.findViewById(R.id.lblAnimatedRemainingTime)).setText("NEXT PRICE IN "+nextPriceTime[1]+":"+nextPriceTime[2]);
               ((TextView)addView.findViewById(R.id.lblAnimatedRemainingTime)).setText("NEXT PRICE IN "+ remainingTime);

           }
            else
           {
               ((TextView)addView.findViewById(R.id.lblAnimatedRemainingTime)).setVisibility(View.INVISIBLE);
           }



            if (null != tempDealsEntity.getImage() && 0 < tempDealsEntity.getImage().size()) {
                Picasso.with(mContext)
                        .load(ImageUtils.getFullSizeImage(tempDealsEntity.getImage().get(0), matrix))
                        .placeholder(R.drawable.pdp_banner_placeholder)
                        .resize(matrix.widthPixels, matrix.heightPixels)
                        .centerInside()
                        .into((ImageView) addView.findViewById(R.id.imgAnimatedCart));
            }
            if (null == tempDealsEntity.getCurrent_price()) {
                ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + getResources().getString(R.string.rupee_symbole) + " " + tempDealsEntity.getOriginal_price());
            } else {
                ((TextView) addView.findViewById(R.id.lblAnimatedCartProductPrice)).setText("Price: " + getResources().getString(R.string.rupee_symbole) + " " + tempDealsEntity.getCurrent_price());
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (null != popupWindow)
                        popupWindow.dismiss();
                }
            }, 5000);

            MainActivity.recursiveApiCall();

        }

        if (response instanceof AddOrRemoveFromWishListResponse) {
            Toast.makeText(getActivity(), "" + response.getMessage(), Toast.LENGTH_SHORT).show();
//            currentTimeInMillis = 0;
            MainActivity.setSpyCount(((AddOrRemoveFromWishListResponse) response).getData().getSpy_count());
            // Total SPY
            ((IDealFacade) IOCContainer.getInstance().getObject(ServiceName.DEAL_SERVICE, TAG)).deal();
//            ((TextView) findViewById(R.id.lblTotalSpy))
//                    .setText((Integer.parseInt(productDetailsResponseData.getSpied_count())+1) + " SPYING");

        }
    }

    @Override
    public void onFailure(Exception error) {
        mProgressView.setVisibility(View.GONE);
//        Toast.makeText(getActivity(), "Ahhahh.. server encountered an error.", Toast.LENGTH_SHORT).show();
    }

    public void recursiveDealAPICall() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                ((IDealFacade) IOCContainer.getInstance().getObject(ServiceName.DEAL_SERVICE, TAG)).deal();
            }
        }, (1000 * 180)); // 3 minutes delay (takes millis)
    }

    // After selecting a size of product add product to the cart
    @Override
    public void onSizeSelected(String productId) {
//        ((ICartFacade) IOCContainer.getInstance().getObject(ServiceName.CART_SERVICE, TAG))
//                .addToCart(id, "1", sizeId, attributeId);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelTimer();
    }

    private void cancelTimer() {
        currentTimeInMillis = 0;
        if (mDealAdapter != null)
            mDealAdapter.cancleTimer();
    }

    @Override
    public void dealBuynow(DealsEntity dealsEntity) {
        tempDealsEntity = dealsEntity;

        if (dealsEntity.getSizes() != null) {
            int size = dealsEntity.getSizes().size();
            List<Boolean> checkProductSizesIfBlank = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                if (dealsEntity.getSizes().get(i).getStock_count() == 0) {
                    checkProductSizesIfBlank.add(true);
                }
            }

            if (0 < checkProductSizesIfBlank.size() && size == checkProductSizesIfBlank.size()) {
                Toast.makeText(mContext, "Apologies, this product is out of stock.", Toast.LENGTH_SHORT).show();
            } else {
                SizeDialog sizeDialog = new SizeDialog();
                sizeDialog.setOnSizeSelectedListner(this);
                Bundle bundle = new Bundle();
                ProductSizesListParable productSizesListParable = new ProductSizesListParable(dealsEntity.getSizes());
                bundle.putParcelable("product", productSizesListParable);
                bundle.putString("productId", dealsEntity.getId());
                bundle.putString("attributeId", dealsEntity.getSizes().get(0).getAttribute_id());
                bundle.putString("tag", TAG);
                sizeDialog.setArguments(bundle);
                sizeDialog.show(getChildFragmentManager(), TAG);
            }
        } else {
            Toast.makeText(mContext, "Apologies, this product is out of stock.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void dealSpy(String productId) {
        ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).setSpied(productId);
    }

    @Override
    public void removeDealSpy(String productId) {
        ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).removeSpied(productId);
    }

    @Override
    public void dealProductClick(DealsEntity dealsEntity, String hrs, String mins, String secs) {
        Intent intent = new Intent();
        intent.setClass(getActivity(), ProductDetailsActivity.class);
        if ("open".equals(dealsEntity.getStatus())) {
            intent.putExtra("IS_DEAL_PRODUCT", true);
            intent.putExtra("CURRENT_PRICE", dealsEntity.getCurrent_price());
            intent.putExtra("ORIGINAL_PRICE", dealsEntity.getOriginal_price());
            intent.putExtra("HIGH_PRICE", dealsEntity.getHigh_price());
            intent.putExtra("LOW_PRICE", dealsEntity.getLow_price());
            intent.putExtra("HOURS", hrs);
            intent.putExtra("MINUTES", mins);
            intent.putExtra("SECONDS", secs);
        } else {

        }

        intent.putExtra("PRODUCT_ID", dealsEntity.getId());
//        intent.putExtra("REMAINING_STOCK", dealsEntity.getRemaining_stock());
        intent.putExtra("REMAINING_STOCK", dealsEntity.getTotal_stock());
        startActivity(intent);
    }

    private void getFBFriends() {
        Set<String> fields = new HashSet<String>();
        String[] requiredFields = new String[]{"id", "name", "installed"};
        fields.addAll(Arrays.asList(requiredFields));
        Bundle parameters = new Bundle();
        parameters.putString("fields", TextUtils.join(",", fields));
        AccessToken accessToken = null;
        if (FacebookSdk.isInitialized())
            accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            GraphRequest friendsRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "me/friends", parameters, HttpMethod.GET, new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse response) {
                    Log.d("Friends are here", "" + response.getRawResponse());
                    JSONObject friendsObject = response.getJSONObject();
                    JSONArray friendsList = null;
                    try {
                        friendsList = friendsObject.getJSONArray("data");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (friendsList != null) {
                        String friendsCount = String.valueOf(friendsList.length());
                        ((IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, "")).setOfflineResponse("fbFriendsCount", friendsCount);
                    }
                }
            });
            friendsRequest.executeAsync();
        }
    }
}
