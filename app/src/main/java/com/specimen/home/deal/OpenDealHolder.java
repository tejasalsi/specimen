package com.specimen.home.deal;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.DealsEntity;
import com.specimen.util.GaugeView;
import com.specimen.util.NumberFormatsUtil;

/**
 * Created by root on 6/10/15.
 */
public class OpenDealHolder extends RecyclerView.ViewHolder implements IUpdateTimer {

    TextView tickerProductTitle_open_deal, text_open_deal_Spying,
            lbl_open_deal_TotalViews, lbl_open_deal_RemainingStock,
            tickerProductPrice_open_deal, text_open_deal_OriginalPrice,
            text_open_deal_TodaysLow, text_open_deal_TodayHigh,
            hrtxt_open_deal, mintxt_open_deal, sectxt_open_deal, text_open_deal_Friends;
    Button btn_open_deal_buy, btn_open_deal_spy;
    ImageView tickerBannerImg_open_deal, imgUpDownPrice_open_deal;
    GaugeView mGaugeView;
    private DealsEntity dealsEntity;
    private IDealCloseTimerUpdate mIDealCloseTimerUpdate;

    //Just for testing
    private float degree = -225;
    private float sweepAngleControl = 0;
    private float sweepAngleFirstChart = 1;
    private float sweepAngleSecondChart = 1;
    private float sweepAngleThirdChart = 1;

    public OpenDealHolder(View itemView) {
        super(itemView);
        tickerProductTitle_open_deal = (TextView) itemView.findViewById(R.id.tickerProductTitle_open_deal);
        text_open_deal_Spying = (TextView) itemView.findViewById(R.id.text_open_deal_Spying);
        lbl_open_deal_TotalViews = (TextView) itemView.findViewById(R.id.lbl_open_deal_TotalViews);
        lbl_open_deal_RemainingStock = (TextView) itemView.findViewById(R.id.lbl_open_deal_RemainingStock);
        tickerProductPrice_open_deal = (TextView) itemView.findViewById(R.id.tickerProductPrice_open_deal);
        text_open_deal_OriginalPrice = (TextView) itemView.findViewById(R.id.text_open_deal_OriginalPrice);
        text_open_deal_TodaysLow = (TextView) itemView.findViewById(R.id.text_open_deal_TodaysLow);
        text_open_deal_TodayHigh = (TextView) itemView.findViewById(R.id.text_open_deal_TodayHigh);
        hrtxt_open_deal = (TextView) itemView.findViewById(R.id.hrtxt_open_deal);
        mintxt_open_deal = (TextView) itemView.findViewById(R.id.mintxt_open_deal);
        sectxt_open_deal = (TextView) itemView.findViewById(R.id.sectxt_open_deal);
        btn_open_deal_buy = (Button) itemView.findViewById(R.id.btn_open_deal_buy);
        btn_open_deal_spy = (Button) itemView.findViewById(R.id.btn_open_deal_spy);
        tickerBannerImg_open_deal = (ImageView) itemView.findViewById(R.id.tickerBannerImg_open_deal);
        imgUpDownPrice_open_deal = (ImageView) itemView.findViewById(R.id.imgUpDownPrice_open_deal);
        text_open_deal_Friends = (TextView) itemView.findViewById(R.id.text_open_deal_Friends);
        mGaugeView = (GaugeView) itemView.findViewById(R.id.tickerGaugeNiddle);
        mGaugeView.setRotateDegree(degree);

    }

    public void setDealsEntity(DealsEntity dealsEntity) {
        this.dealsEntity = dealsEntity;
    }

    public void setDealCloseTimerInterface(IDealCloseTimerUpdate iDealCloseTimerUpdate) {
        this.mIDealCloseTimerUpdate = iDealCloseTimerUpdate;
    }

    @Override
    public void updateTimer(long updatedTimerValue) {
        if (dealsEntity == null) return;

        final String constantTimeString = "00:00:00";
        final String[] remainingTime = (!TextUtils.isEmpty(dealsEntity.getRemaining_time()) ?
                dealsEntity.getRemaining_time().split(":") : constantTimeString.split(":"));
        long hoursInmillis = Long.parseLong(remainingTime[0]) * (60 * 60 * 1000);
        long minsInMillis = Long.parseLong(remainingTime[1]) * (60 * 1000);
        long secsInMillis = Long.parseLong(remainingTime[2]) * 1000;

        final long dealTimeInMillis = hoursInmillis + minsInMillis + secsInMillis;
        final long updatedTimeInMillis = dealTimeInMillis - (updatedTimerValue * 1000);

        if (NumberFormatsUtil.isNegative(updatedTimeInMillis)) {
            hrtxt_open_deal.setText("00");
            mintxt_open_deal.setText("00");
            sectxt_open_deal.setText("00");
            mIDealCloseTimerUpdate.dealCloseTimerUpdate("open");
        } else {
            setTimerAnimation(updatedTimeInMillis);

        }
    }


    void setTimerAnimation(long updatedTimeInMillis) {

        long sec = (updatedTimeInMillis / 1000) % 60;
        long hr = (updatedTimeInMillis / (1000 * 60 * 60));
        long min = (updatedTimeInMillis / (1000 * 60) % 60);
        sectxt_open_deal.setText("" + String.format("%02d", sec));
        mintxt_open_deal.setText("" + String.format("%02d", min));
        hrtxt_open_deal.setText("" + String.format("%02d", hr));

        ObjectAnimator secObjectAnimator = ObjectAnimator.ofFloat(sectxt_open_deal, "rotationX", 90f, -90f);
        secObjectAnimator.setDuration(1000);
        secObjectAnimator.setInterpolator(new LinearInterpolator());
        secObjectAnimator.start();

        if (sec == 59) {
            ObjectAnimator minObjectAnimator = ObjectAnimator.ofFloat(mintxt_open_deal, "rotationX", 90f, 0.0f);
            minObjectAnimator.setDuration(800);
            minObjectAnimator.setInterpolator(new LinearInterpolator());
            minObjectAnimator.start();
        }
        if (min == 59) {
            ObjectAnimator hrObjectAnimator = ObjectAnimator.ofFloat(hrtxt_open_deal, "rotationX", 90f, 0.0f);
            hrObjectAnimator.setDuration(800);
            hrObjectAnimator.setInterpolator(new LinearInterpolator());
            hrObjectAnimator.start();
        }
    }

    public void startRunningGauge(final Activity activity, final int totalStock) {
        new Thread() {
            public void run() {
                for (int i = 0; i < totalStock; i++) {
                    try {
                        activity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                degree++;
                                sweepAngleControl++;

//                                mGaugeView.setRotateDegree(degree);
//
//                                if (totalStock == 0) {
//                                    sweepAngleFirstChart++;
//                                    mGaugeView.setSweepAngleFirstChart(sweepAngleFirstChart);
//                                } else if (totalStock >= 100){
//                                    sweepAngleThirdChart++;
//                                    mGaugeView.setSweepAngleSecondChart(sweepAngleSecondChart);
//                                } else {
//                                    sweepAngleSecondChart++;
//                                    mGaugeView.setSweepAngleThirdChart(sweepAngleThirdChart);
//                                }

                                if (degree < 45) {
                                    mGaugeView.setRotateDegree(degree);
                                }

                                if (sweepAngleControl <= 90) {
                                    sweepAngleFirstChart++;
                                    mGaugeView.setSweepAngleFirstChart(sweepAngleFirstChart);
                                } else if (sweepAngleControl <= 180) {
                                    sweepAngleSecondChart++;
                                    mGaugeView.setSweepAngleSecondChart(sweepAngleSecondChart);
                                } else if (sweepAngleControl <= 270) {
                                    sweepAngleThirdChart++;
                                    mGaugeView.setSweepAngleThirdChart(sweepAngleThirdChart);
                                }

                            }
                        });
                        Thread.sleep(15);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}