package com.specimen.home.bulletin;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.R;
import com.specimen.core.model.BulletinData;
import com.specimen.core.model.BulletinEntity;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 20/10/15.
 */
public class ExploreBulletinHolder extends BulletinHolder {

    RecyclerView mRecyclerView;

    public ExploreBulletinHolder(View itemView) {
        super(itemView);
        this.mRecyclerView = (RecyclerView) itemView.findViewById(R.id.rvBestSellers);
    }

    @Override
    public void setView(BulletinEntity bulletinEntity) {

        MyLinearLayoutManager myLinearLayoutManager = new MyLinearLayoutManager(mRecyclerView.getContext(), LinearLayoutManager.HORIZONTAL, false);
//        GridLayoutManager horizontalLinearLayoutManager = new GridLayoutManager(mRecyclerView.getContext(), 2, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(myLinearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        ExploreAdapter collectionAdapter = new ExploreAdapter(mRecyclerView.getContext(), R.layout.bulletin_explore_item, (ArrayList<BulletinData>) bulletinEntity.getData());
        mRecyclerView.setAdapter(collectionAdapter);
    }


    private class ExploreAdapter extends RecyclerView.Adapter<ExploreAdapter.ViewHolder> {

        private Context context;
        List<BulletinData> explores;
        int resourceLayout;

        public ExploreAdapter(Context context, int layout, ArrayList<BulletinData> objects) {
            this.context = context;
            this.explores = objects;
            this.resourceLayout = layout;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(resourceLayout, null);

            return new ViewHolder(view);

        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.setView(explores.get(position));
        }

        @Override
        public int getItemCount() {
            return explores.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private final DisplayMetrics matrix;
            public ImageView collectionImage;
//            public TextView collectionTitle;

            public ViewHolder(View itemView) {
                super(itemView);
                collectionImage = (ImageView) itemView.findViewById(R.id.ivExploreImage);
//                collectionTitle = (TextView) itemView.findViewById(R.id.gridTitle);
                matrix = context.getResources().getDisplayMetrics();
            }

            public void setView(BulletinData collection) {
                if (null != collection.images && ImageUtils.getFullSizeImage(collection.images, matrix) != null) {
                    try {
                        Picasso.with(context)
                                .load(ImageUtils.getFullSizeImage(collection.images, matrix))
                                .placeholder(R.drawable.banner_explore_placeholder)
                                .into(collectionImage);
//                collectionTitle.setText(collection.getName().toUpperCase());
                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        Picasso.with(context)
                                .load(R.drawable.banner_explore_placeholder)
                                .into(collectionImage);
                    }
                } else {
                    Picasso.with(context)
                            .load(R.drawable.banner_explore_placeholder)
                            .into(collectionImage);
                }
            }
        }
    }
}
