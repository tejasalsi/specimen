package com.specimen.home.bulletin;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.specimen.R;
import com.specimen.widget.ProgressView;

public class BulletinWebActivity extends AppCompatActivity {

    private WebView wvBulletinBlog;
    private String url;
    private ProgressView progressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulletin_web);
        progressView = (ProgressView) findViewById(R.id.progressBar);
        Bundle bundle = getIntent().getExtras();
        url = bundle.getString("url");
        wvBulletinBlog = (WebView) findViewById(R.id.wvBulletinWeb);
        wvBulletinBlog.getSettings().setJavaScriptEnabled(true);

        if(!TextUtils.isEmpty(url)){
            progressView.setVisibility(View.VISIBLE);
            wvBulletinBlog.loadUrl(url);
        }else{
            Toast.makeText(BulletinWebActivity.this, "Url is missing.", Toast.LENGTH_SHORT).show();
        }

        wvBulletinBlog.setWebViewClient(new WebViewClient(){
            public void onPageFinished(WebView view, String url) {
                progressView.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                progressView.setVisibility(View.GONE);
                Toast.makeText(view.getContext(),description, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
