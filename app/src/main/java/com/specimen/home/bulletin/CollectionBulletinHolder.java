package com.specimen.home.bulletin;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.BulletinData;
import com.specimen.core.model.BulletinEntity;
import com.specimen.core.model.Collection;
import com.specimen.look.LooksHeaderAdapter;
import com.specimen.shop.CollectionAdapter;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 20/10/15.
 */
public class CollectionBulletinHolder extends BulletinHolder {

    RecyclerView mRecyclerView;
    TextView mTvTitle;
    Context mContext;


    public CollectionBulletinHolder(View itemView) {
        super(itemView);
        this.mRecyclerView = (RecyclerView) itemView.findViewById(R.id.rvBestSellers);
        this.mTvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
        this.mContext = itemView.getContext();
    }

    @Override
    public void setView(BulletinEntity bulletinEntity) {
        mRecyclerView.setHasFixedSize(true);
        mTvTitle.setText(mContext.getResources().getString(R.string.collection_text));
        MyLinearLayoutManager myLinearLayoutManager = new MyLinearLayoutManager(mRecyclerView.getContext(), LinearLayoutManager.HORIZONTAL, false);
//        GridLayoutManager horizontalLinearLayoutManager = new GridLayoutManager(mRecyclerView.getContext(), 3, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(myLinearLayoutManager);


        CollectionAdapter collectionAdapter = new CollectionAdapter(mRecyclerView.getContext(), R.layout.top_level_grid_item, (ArrayList<BulletinData>) bulletinEntity.getData());
        mRecyclerView.setAdapter(collectionAdapter);


    }

    private class CollectionAdapter extends RecyclerView.Adapter<CollectionAdapter.ViewHolder> {

        private Context context;
        List<BulletinData> collections;
        int resourceLayout;

        public CollectionAdapter(Context context, int layout, ArrayList<BulletinData> objects) {
            this.context = context;
            this.collections = objects;
            this.resourceLayout = layout;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(resourceLayout, null);

            return new ViewHolder(view);

        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.setView(collections.get(position));
        }

        @Override
        public int getItemCount() {
            return collections.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private final DisplayMetrics matrix;
            public ImageView collectionImage;
            public TextView collectionTitle;

            public ViewHolder(View itemView) {
                super(itemView);
                collectionImage = (ImageView) itemView.findViewById(R.id.gridImage);
                collectionTitle = (TextView) itemView.findViewById(R.id.gridTitle);
                matrix = context.getResources().getDisplayMetrics();
            }

            public void setView(BulletinData collection) {
                try {
                    Picasso.with(context)
                            .load(ImageUtils.getFullSizeImage(collection.images, matrix))
                            .placeholder(R.drawable.banner_explore_placeholder)
                            .into(collectionImage);
                } catch (Exception e) {
                    Picasso.with(context)
                            .load(R.drawable.banner_explore_placeholder)
                            .into(collectionImage);
                }
                collectionTitle.setText(collection.name);
//                collectionTitle.setText(collection.getName().toUpperCase());
//                Picasso.with(context)
//                        .load(collection.image.toString())
//                        .placeholder(R.drawable.banner_explore_placeholder)
//                        .into(collectionImage);
//                collectionTitle.setText(collection.name.toUpperCase());
            }

        }
    }

}
