package com.specimen.home.bulletin;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.R;
import com.specimen.core.model.BulletinData;
import com.specimen.core.model.BulletinEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 20/10/15.
 */
public class BulletinAdapter extends RecyclerView.Adapter<BulletinHolder> {


    private final int BANNER = 0, BLOG = 6, VIDEO = 3, BEST_SELLER = 7, RECENTLYVIEWED = 1,COLLECTION = 4, EXPLORE = 5;

    private LayoutInflater mInflater;

    private List<BulletinEntity> mBulletinEntity;

    public BulletinAdapter(Activity activity, List<BulletinEntity> bulletinEntity) {
        this.mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //TODO: need to pass &initialize list here
        this.mBulletinEntity = bulletinEntity;
    }

    @Override
    public BulletinHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.from(parent.getContext()).inflate(R.layout.bulletin_adapter, parent, false);
        BulletinHolder holder = null;

        switch (viewType) {
            case BANNER:
                view = mInflater.from(parent.getContext()).inflate(R.layout.view_pager_layout, parent, false);
                //TODO: Return BannerViewHolder;
                holder = new BannerBulletinHolder(view);
                break;
            case VIDEO:
                view = mInflater.from(parent.getContext()).inflate(R.layout.bullettin_video_layout, parent, false);
                holder = new VideoBulletinHolder(view);
                break;
            case BEST_SELLER:
                view = mInflater.from(parent.getContext()).inflate(R.layout.bulletin_best_seller, parent, false);
                holder = new BestSellerBulletinHolder(view, view.getContext().getResources().getString(R.string.best_selling));
                break;
            case BLOG:
                view = mInflater.from(parent.getContext()).inflate(R.layout.bulletin_blog, parent, false);
                holder = new BlogBulletinHolder(view);
                break;
            case RECENTLYVIEWED:
//                view = mInflater.from(parent.getContext()).inflate(R.layout.bulletin_best_seller, parent, false);
//                holder = new BestSellerBulletinHolder(view, view.getContext().getResources().getString(R.string.recently_viewd));
                break;
            case COLLECTION:
                //TODO: Return CollectionViewHolder;
                view = mInflater.from(parent.getContext()).inflate(R.layout.bulletin_best_seller, parent, false);
                holder = new CollectionBulletinHolder(view);
                break;
            case EXPLORE:
//                 //TODO: Return CollectionViewHolder;
                view = mInflater.from(parent.getContext()).inflate(R.layout.bulletin_explore, parent, false);
                holder = new ExploreBulletinHolder(view);
                break;
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(BulletinHolder holder, int position) {
        holder.setView(mBulletinEntity.get(position));
    }

    @Override
    public int getItemCount() {
        return mBulletinEntity.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mBulletinEntity.get(position).getType();

    }
}
