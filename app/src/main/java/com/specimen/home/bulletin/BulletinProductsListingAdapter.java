package com.specimen.home.bulletin;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.model.BulletinData;
import com.specimen.core.spy.ISpyFacade;
import com.specimen.product.productdetails.ProductDetailsActivity;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Swapnil on 11/25/2015.
 */
public class BulletinProductsListingAdapter extends RecyclerView.Adapter<BulletinProductsListingAdapter.BulletinProductsListingHolder> {
    private final DisplayMetrics matrix;
    Context context;
    LayoutInflater inflater;
    List<BulletinData> products;
    int resourceLayout;

    public BulletinProductsListingAdapter(Context context, int resource, List<BulletinData> objects) {
        this.context = context;
        this.resourceLayout = resource;
        this.products = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        matrix = context.getResources().getDisplayMetrics();
    }

    public void setResourceLayout(int resourceLayout) {
        this.resourceLayout = resourceLayout;
    }

    @Override
    public BulletinProductsListingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resourceLayout, null);
        return new BulletinProductsListingHolder(view, matrix);
    }

    @Override
    public void onBindViewHolder(BulletinProductsListingHolder holder, int position) {
        holder.getView(products.get(position));
    }

    @Override
    public int getItemCount() {
        return (null != products ? products.size() : 0);
    }

    class BulletinProductsListingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private static final String TAG = "BestSEllerAdpaterHolder";
        //            private ShareToPinterest mShareToPinterest;
        public ImageView productImage;
        public TextView productName;
        public TextView productPrice;
        public TextView specialPrice;
        public ImageView spiedImage;


        //            public View scrachView;
        BulletinData product;
        Context context;
        DisplayMetrics matrix;

        public BulletinProductsListingHolder(View itemView, DisplayMetrics matrix) {
            super(itemView);

            this.productImage = (ImageView) itemView.findViewById(R.id.product_image);
            this.productName = (TextView) itemView.findViewById(R.id.tvProductName);
            this.productPrice = (TextView) itemView.findViewById(R.id.tvProductPrice);
            this.specialPrice = (TextView) itemView.findViewById(R.id.tvProductSpecialPrice);
//                this.scrachView = itemView.findViewById(R.id.scratched_layout);

            this.spiedImage = (ImageView) itemView.findViewById(R.id.spied_icon_image);

            this.matrix = matrix;
            this.context = itemView.getContext();
        }

        public void getView(final BulletinData product) {

            this.product = product;
            if (product.images != null) {
                Picasso.with(context)
                        .load(ImageUtils.getProductImageUrl(product.images, matrix))
                        .placeholder(R.drawable.product_list_placeholder)
                        .resize(300, 480)
                        .centerInside()
                        .into(productImage);
            } else {
                Picasso.with(context).load(R.drawable.product_list_placeholder).into(productImage);
            }


            productName.setText(""+product.name.toUpperCase());

            spiedImage.setOnClickListener(this);

            if (!product.isSpied) {
                spiedImage.setImageResource(R.drawable.spi_circle_black);

            } else {
                spiedImage.setImageResource(R.drawable.prodlist_spi_enabled);
            }
            specialPrice.setVisibility(View.GONE);

            String rsSymbol = context.getResources().getString(R.string.Rs);
            String specialPriceText = product.specialprice;
            if (specialPriceText != null ) {
                if ("0".equals(specialPriceText)) {
                    productPrice.setText(rsSymbol + product.price);
//                        scrachView.setVisibility(View.GONE);
//                        scratched_priceText.setText(null);
                } else {
//                        scrachView.setVisibility(View.VISIBLE);
//                        scratched_priceText.setText(rsSymbol + product.price);
                    productPrice.setText(rsSymbol + product.price);
                    specialPrice.setVisibility(View.VISIBLE);
                    productPrice.setPaintFlags(productPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    specialPrice.setText(rsSymbol + specialPriceText);
//                        productPrice.setTextColor(context.getResources().getColor(R.color.specimen_green));
                }
            } else {
//                    productPrice.setTextColor(context.getResources().getColor(android.R.color.black));
                productPrice.setText(rsSymbol + product.price);
                specialPrice.setVisibility(View.GONE);
//                    scratched_priceText.setText(null);
            }
            productImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), ProductDetailsActivity.class);
                    intent.putExtra("PRODUCT_ID", String.valueOf(product.id));
                    v.getContext().startActivity(intent);
                }
            });

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.spied_icon_image:
                    toggleSpiedState();
                    break;

            }
        }

        private void toggleSpiedState() {
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(spiedImage, "rotationX", 90f, 0.0f);
            objectAnimator.setDuration(600);
            objectAnimator.setStartDelay(0);
            objectAnimator.start();
            if (!product.isSpied) {
                spiedImage.setImageResource(R.drawable.prodlist_spi_enabled);
                product.isSpied = true;
                addToSpied();

            } else {
                spiedImage.setImageResource(R.drawable.spi_circle_black);
                product.isSpied = false;

                removeFromSpied();
            }
            spiedImage.invalidate();
        }

        private void removeFromSpied() {
            ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).removeSpied("" + product.id);
        }

        private void addToSpied() {
            ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).setSpied("" + product.id);
        }


    }
}