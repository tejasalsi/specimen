package com.specimen.home.bulletin;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.cart.ICartFacade;
import com.specimen.core.model.BulletinData;
import com.specimen.core.model.BulletinEntity;
import com.specimen.core.model.Product;
import com.specimen.core.model.ProductSizesListParable;
import com.specimen.core.spy.ISpyFacade;
import com.specimen.home.OnSizeSelectedListener;
import com.specimen.product.ProductHolder;
import com.specimen.product.productdetails.ProductDetailsActivity;
import com.specimen.ui.dialog.SizeDialog;
import com.specimen.util.ImageUtils;
import com.specimen.util.ShareToPinterest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 20/10/15.
 */
public class BestSellerBulletinHolder extends BulletinHolder {

    RecyclerView mRecyclerView;
    TextView mTvTitle;
    Context mContext;
    String mCategory;


    public BestSellerBulletinHolder(View itemView, String category) {
        super(itemView);
        this.mRecyclerView = (RecyclerView) itemView.findViewById(R.id.rvBestSellers);
        this.mTvTitle = (TextView) itemView.findViewById(R.id.tvTitle);

        this.mCategory = category;
        this.mContext = itemView.getContext();
    }

    @Override
    public void setView(BulletinEntity bulletinEntity) {
//        mContext = mRecyclerView.getContext();

        mRecyclerView.setHasFixedSize(true);
        MyLinearLayoutManager myLinearLayoutManager = new MyLinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
//        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext,2,LinearLayoutManager.HORIZONTAL,false));
        mRecyclerView.setLayoutManager(myLinearLayoutManager);
        if (mCategory.equals(mContext.getResources().getString(R.string.recently_viewd))) {
            mTvTitle.setText(mContext.getResources().getString(R.string.recently_viewd));
            mRecyclerView.setBackgroundColor(Color.WHITE);
        } else {
            mTvTitle.setText(mContext.getResources().getString(R.string.best_selling));
            mRecyclerView.setBackgroundColor(mContext.getResources().getColor(R.color.specimen_green));
        }

        BestSEllerAdpater bestSEllerAdpater = new BestSEllerAdpater(mContext, R.layout.bulletin_productgrid_item, bulletinEntity.getData());
        mRecyclerView.setAdapter(bestSEllerAdpater);
    }


    private class BestSEllerAdpater extends RecyclerView.Adapter<BestSEllerAdpater.BestSEllerAdpaterHolder> {
        private final DisplayMetrics matrix;
        Context context;
        LayoutInflater inflater;
        List<BulletinData> products;
        int resourceLayout;

        public BestSEllerAdpater(Context context, int resource, List<BulletinData> objects) {
            this.context = context;
            this.resourceLayout = resource;
            this.products = objects;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            matrix = context.getResources().getDisplayMetrics();
        }

        public void setResourceLayout(int resourceLayout) {
            this.resourceLayout = resourceLayout;
        }

        @Override
        public BestSEllerAdpaterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(resourceLayout, null);
            return new BestSEllerAdpaterHolder(view, matrix);
        }

        @Override
        public void onBindViewHolder(BestSEllerAdpaterHolder holder, int position) {
            holder.getView(products.get(position));
        }

        @Override
        public int getItemCount() {
            return (null != products ? products.size() : 0);
        }

        class BestSEllerAdpaterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private static final String TAG = "BestSEllerAdpaterHolder";
            //            private ShareToPinterest mShareToPinterest;
            public ImageView productImage;
            public TextView productName;
            public TextView productPrice;
            public TextView specialPrice;
            public ImageView spiedImage;


            //            public View scrachView;
            BulletinData product;
            Context context;
            DisplayMetrics matrix;

            public BestSEllerAdpaterHolder(View itemView, DisplayMetrics matrix) {
                super(itemView);

                this.productImage = (ImageView) itemView.findViewById(R.id.product_image);
                this.productName = (TextView) itemView.findViewById(R.id.tvProductName);
                this.productPrice = (TextView) itemView.findViewById(R.id.tvProductPrice);
                this.specialPrice = (TextView) itemView.findViewById(R.id.tvProductSpecialPrice);
//                this.scrachView = itemView.findViewById(R.id.scratched_layout);

                this.spiedImage = (ImageView) itemView.findViewById(R.id.spied_icon_image);

                this.matrix = matrix;
                this.context = itemView.getContext();
            }

            public void getView(final BulletinData product) {

                this.product = product;
                if (product.images != null) {
                    Picasso.with(context)
                            .load(ImageUtils.getProductImageUrl(product.images, matrix))
                            .placeholder(R.drawable.product_list_placeholder)
                            .resize(300, 480)
                            .centerInside()
                            .into(productImage);
                } else {
                    Picasso.with(context).load(R.drawable.product_list_placeholder).into(productImage);
                }


                productName.setText(""+product.name.toUpperCase());

                spiedImage.setOnClickListener(this);

                if (!product.isSpied) {
                    spiedImage.setImageResource(R.drawable.spi_circle_black);

                } else {
                    spiedImage.setImageResource(R.drawable.prodlist_spi_enabled);
                }
                specialPrice.setVisibility(View.GONE);

                String rsSymbol = context.getResources().getString(R.string.Rs);
                String specialPriceText = product.specialprice;
                if (specialPriceText != null ) {
                    if ("0".equals(specialPriceText)) {
                        productPrice.setText(rsSymbol + product.price);
//                        scrachView.setVisibility(View.GONE);
//                        scratched_priceText.setText(null);
                    } else {
//                        scrachView.setVisibility(View.VISIBLE);
//                        scratched_priceText.setText(rsSymbol + product.price);
                        productPrice.setText(rsSymbol + product.price);
                        specialPrice.setVisibility(View.VISIBLE);
                        productPrice.setPaintFlags(productPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        specialPrice.setText(rsSymbol + specialPriceText);
//                        productPrice.setTextColor(context.getResources().getColor(R.color.specimen_green));
                    }
                } else {
//                    productPrice.setTextColor(context.getResources().getColor(android.R.color.black));
                    productPrice.setText(rsSymbol + product.price);
                    specialPrice.setVisibility(View.GONE);
//                    scratched_priceText.setText(null);
                }
                productImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(v.getContext(), ProductDetailsActivity.class);
                        intent.putExtra("PRODUCT_ID", String.valueOf(product.id));
                        v.getContext().startActivity(intent);
                    }
                });

            }

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.spied_icon_image:
                        toggleSpiedState();
                        break;

                }
            }

            private void toggleSpiedState() {
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(spiedImage, "rotationX", 90f, 0.0f);
                objectAnimator.setDuration(600);
                objectAnimator.setStartDelay(0);
                objectAnimator.start();
                if (!product.isSpied) {
                    spiedImage.setImageResource(R.drawable.prodlist_spi_enabled);
                    product.isSpied = true;
                    addToSpied();

                } else {
                    spiedImage.setImageResource(R.drawable.spi_circle_black);
                    product.isSpied = false;

                    removeFromSpied();
                }
                spiedImage.invalidate();
            }

            private void removeFromSpied() {
                ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).removeSpied("" + product.id);
            }

            private void addToSpied() {
                ((ISpyFacade) IOCContainer.getInstance().getObject(ServiceName.SPY_SERVICE, TAG)).setSpied("" + product.id);
            }


        }
    }
}
