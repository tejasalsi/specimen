package com.specimen.home.bulletin;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.BulletinEntity;
import com.specimen.util.ImageUtils;
import com.specimen.util.YouTubeWebView;
import com.squareup.picasso.Picasso;

/**
 * Created by root on 20/10/15.
 */
public class VideoBulletinHolder extends BulletinHolder {

    private final DisplayMetrics matrix;
    YouTubeWebView mYouTubeWebView;
    private TextView tvTitle, tvText;
    private ImageView ivThumbnail, ivPlayBtn;
    private Context mContext;
    private RelativeLayout rlImage;

    public VideoBulletinHolder(View itemView) {
        super(itemView);

        this.mYouTubeWebView = (YouTubeWebView) itemView.findViewById(R.id.buletinVideo);
        this.ivThumbnail = (ImageView) itemView.findViewById(R.id.ivYoutubeThumb);
        this.tvTitle = (TextView) itemView.findViewById(R.id.tvVideoTitle);
        this.tvText = (TextView) itemView.findViewById(R.id.tvVideoText);
        this.ivPlayBtn = (ImageView) itemView.findViewById(R.id.ivPlayBtn);
        this.rlImage = (RelativeLayout) itemView.findViewById(R.id.rlImage);
        this.mContext = itemView.getContext();
        this.matrix = mContext.getResources().getDisplayMetrics();
    }

    @Override
    public void setView(final BulletinEntity bulletinEntity) {
        //set the url to the youtube webview from bulletinEntity
        if (bulletinEntity != null && bulletinEntity.getData() != null && bulletinEntity.getData().size() > 0) {

            mYouTubeWebView.loadVideo(bulletinEntity.getData().get(0).url);
            String token[] = bulletinEntity.getData().get(0).url.split("=");
            Picasso.with(mContext)
                    .load("http://img.youtube.com/vi/"+token[1]+"/hqdefault.jpg")
                    .placeholder(R.drawable.product_list_placeholder)
                    .resize(matrix.widthPixels, (matrix.widthPixels / 4) * 3)
                    .centerCrop()
                    .into(ivThumbnail);

            ivPlayBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(bulletinEntity.getData().get(0).url)));
//                    rlImage.setVisibility(View.GONE);
//                    mYouTubeWebView.setVisibility(View.VISIBLE);
                }
            });
        }
    }
}
