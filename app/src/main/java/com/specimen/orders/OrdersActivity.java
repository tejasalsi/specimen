package com.specimen.orders;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.order.IOrdersFacade;
import com.specimen.core.model.OrderHistoryResponse;
import com.specimen.core.response.APIResponse;
import com.specimen.widget.ProgressView;

/**
 * Created by Swapnil on 8/20/2015.
 */
public class OrdersActivity extends BaseActivity implements IResponseSubscribe {

    private ViewPager mOrderPager;
    private OrderActivityTabsAdapter mOrderPagerAdapter;
    private TextView tvOngoingCounter, tvClosedCounter, tvReturnCounter;
    private LinearLayout empty_view;
    private Button btnEmptyView;
    private LinearLayout llOngoing, llClosed, llReturn;
    private String TAG = "ORDER_ACTIVITY";
    private ImageView ivBack;
    private ProgressView progressView;
    private TextView txtOngoingTab;
    private TextView txtCloseTab;
    private TextView txtReturnTab;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        empty_view = (LinearLayout) findViewById(R.id.empty_view);
        btnEmptyView = (Button) findViewById(R.id.btnEmptyView);

        getComponents();
        orderInfoApiCall();
    }

    private void getComponents() {
        tvOngoingCounter = (TextView) findViewById(R.id.tvOngoingCount);
        tvClosedCounter = (TextView) findViewById(R.id.tvCloseCount);
        tvReturnCounter = (TextView) findViewById(R.id.tvReturnesCount);
        llOngoing = (LinearLayout) findViewById(R.id.llOngoing);
        llClosed = (LinearLayout) findViewById(R.id.llClosed);
        llReturn = (LinearLayout) findViewById(R.id.llReturned);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        progressView = (ProgressView) findViewById(R.id.progressBar);
        mOrderPager = (ViewPager) findViewById(R.id.pagerOrder);
        llOngoing.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        txtOngoingTab = (TextView)findViewById(R.id.txtOngoingTab);
        txtOngoingTab.setTypeface(null, Typeface.BOLD);
        txtCloseTab = (TextView)findViewById(R.id.txtCloseTab);
        txtReturnTab = (TextView)findViewById(R.id.txtReturnTab);
        
        
        llOngoing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOrderPager.setCurrentItem(0, true);
                setTabBackgeound(llOngoing);
//                showViewPagerButtons();
//                mainMenu.setVisibility(View.VISIBLE);
            }
        });

        llClosed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOrderPager.setCurrentItem(1, true);
                setTabBackgeound(llClosed);
            }
        });

        llReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOrderPager.setCurrentItem(2, true);
                setTabBackgeound(llReturn);
            }
        });

        llReturn.setVisibility(View.GONE);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        mOrderPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        llOngoing.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                        llClosed.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        llReturn.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        txtOngoingTab.setTypeface(null, Typeface.BOLD);
                        txtCloseTab.setTypeface(null, Typeface.NORMAL);
                        break;
                    case 1:
                        llOngoing.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        llClosed.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                        llReturn.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
                        txtCloseTab.setTypeface(null, Typeface.BOLD);
                        txtOngoingTab.setTypeface(null, Typeface.NORMAL);
                        break;
//                    case 2:
//                        llOngoing.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
//                        llClosed.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
//                        llReturn.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
//                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(OrdersActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(OrdersActivity.this);
    }

    /**
     * get orders info API call
     */
    private void orderInfoApiCall() {
        progressView.setVisibility(View.VISIBLE);
        ((IOrdersFacade) IOCContainer.getInstance().getObject(ServiceName.ORDER_SERVICE, TAG)).getOrderInfo();
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        Log.d(TAG, response.getMessage());
        progressView.setVisibility(View.GONE);
        if (response instanceof OrderHistoryResponse) {
            OrderHistoryResponse orderHistoryResponse = (OrderHistoryResponse) response;

            if (null != ((OrderHistoryResponse) response).getData()) {
                int onGoingOrdersSize = ((OrderHistoryResponse) response).getData().getOnGoingOrders().size();
                int closedOrdersSize = ((OrderHistoryResponse) response).getData().getClosedOrders().size();
                int returnOrdersSize = ((OrderHistoryResponse) response).getData().getReturnOrders().size();

                tvOngoingCounter.setText(onGoingOrdersSize + "");
                tvClosedCounter.setText(closedOrdersSize + "");
                tvReturnCounter.setText(returnOrdersSize + "");

                if (0 == onGoingOrdersSize && 0 == closedOrdersSize && 0 == returnOrdersSize) {
                    findViewById(R.id.fragment_container).setVisibility(View.GONE);
                    empty_view.setVisibility(View.VISIBLE);
                    btnEmptyView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(OrdersActivity.this, MainActivity.class));
                            finish();
                        }
                    });
                } else {
                    findViewById(R.id.fragment_container).setVisibility(View.VISIBLE);
                    empty_view.setVisibility(View.GONE);
                    mOrderPagerAdapter = new OrderActivityTabsAdapter(getSupportFragmentManager(), orderHistoryResponse.getData());
                    mOrderPager.setAdapter(mOrderPagerAdapter);
                }
            }
        }
    }

    @Override
    public void onFailure(Exception error) {
        Log.e(TAG, error.getMessage() + "");
//        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
        progressView.setVisibility(View.GONE);
    }

    void setTabBackgeound(View selectedTab) {

        llOngoing.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
        llClosed.setBackgroundColor(getResources().getColor(R.color.specimen_grey));
        llReturn.setBackgroundColor(getResources().getColor(R.color.specimen_grey));

        selectedTab.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
