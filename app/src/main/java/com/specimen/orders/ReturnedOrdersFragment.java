package com.specimen.orders;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.R;
import com.specimen.core.model.OrdersEntity;

import java.util.ArrayList;

public class ReturnedOrdersFragment extends Fragment {


    OrderAdapter mOrderAdapter;
    RecyclerView rvReturnedOrders;


    public ReturnedOrdersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_on_going_orders, container, false);
        Bundle bundle = getArguments();

        final ArrayList<OrdersEntity> returnedOrders = bundle.getParcelableArrayList("returned");
        if(returnedOrders!=null) {
            rvReturnedOrders = (RecyclerView) rootView.findViewById(R.id.rvOngoing);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            rvReturnedOrders.setLayoutManager(layoutManager);
            mOrderAdapter = new OrderAdapter(getActivity(), returnedOrders);
            rvReturnedOrders.setAdapter(mOrderAdapter);
            mOrderAdapter.SetOnOrderClickListener(new OnOrderClickListner() {
                @Override
                public void onItemClickListner(View v, int position) {
                    Intent intent = new Intent(getActivity(), OrderDetailsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("orderid", returnedOrders.get(position).getOrderId());
                    startActivity(intent);
                }
            });
        }
        return rootView;
    }



}
