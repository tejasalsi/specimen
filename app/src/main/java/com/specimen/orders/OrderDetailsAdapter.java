package com.specimen.orders;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.cart.HeaderRecyclerViewAdapterV2;
import com.specimen.core.model.OrderDetailsResponseData;
import com.specimen.core.model.Product;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by root on 13/10/15.
 */
public class OrderDetailsAdapter extends HeaderRecyclerViewAdapterV2 {

    List<Product> productList;
    int resource;
    Activity mActivity;
    OrderDetailsResponseData orderDetailsResponseData;

    /**
     * Constructor
     *
     * @param resource               The resource ID for a layout file containing a TextView to use when
     * @param orderedDetailsResponse
     */
    public OrderDetailsAdapter(Activity activity, int resource, OrderDetailsResponseData orderedDetailsResponse) {
        this.mActivity = activity;
        this.resource = resource;
        this.productList = orderedDetailsResponse.getOrderItems();
        this.orderDetailsResponseData = orderedDetailsResponse;
    }

    @Override
    public boolean useHeader() {
        return false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindHeaderView(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public boolean useFooter() {
        return true;
    }

    @Override
    public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_details_adapter_footer, null);

        return new CustomFooterView(view);
    }

    @Override
    public void onBindFooterView(RecyclerView.ViewHolder holder, int position) {
        CustomFooterView mHolder = (CustomFooterView) holder;
        mHolder.totalTaxAmount.setText("TOTAL TAX "+ orderDetailsResponseData.getTotalTax());
        mHolder.totalAmt.setText(orderDetailsResponseData.getOrderTotalValue());
//        mHolder.cardNumber.setText(orderDetailsResponseData.getPaymentDetails());
        mHolder.cardNumber.setText("PAID BY: " + orderDetailsResponseData.getPaidBy());
        mHolder.addresseName.setText(orderDetailsResponseData.getAddress());
//        mHolder.addressLine1.setText(orderDetailsResponseData.getAddress());

    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, null);
        return new OrderDetailsViewHolder(view);
    }

    @Override
    public void onBindBasicItemView(RecyclerView.ViewHolder holder, int position) {
        OrderDetailsViewHolder mHolder = (OrderDetailsViewHolder) holder;
        Product product = productList.get(position);
        mHolder.lblProductName.setText(product.getName());
        mHolder.lblProductPrice.setText(mActivity.getResources().getString(R.string.rupee_symbole)+" "+product.getPrice());
        mHolder.lblProductQuantity.setText("Quantity : " + product.getQuantity());
        DisplayMetrics metrics = mActivity.getResources().getDisplayMetrics();
//
//        Picasso.with(mActivity)
//                .load(ImageUtils.getMediumImage(productList.get(position).getImage().get(0), metrics))
//                .placeholder(R.drawable.cart_img_placeholder)
//                .resize(150, 120)
//                .centerInside()
//                .into(mHolder.product_image);
        Picasso.with(mActivity)
                .load(productList.get(position).getImage().get(0).getMedium().getUrl())
                .placeholder(R.drawable.cart_img_placeholder)
                .resize(200, 240)
                .into(mHolder.product_image);
    }


    @Override
    public int getBasicItemCount() {
        return productList.size();
    }

    @Override
    public int getBasicItemType(int position) {
        return 0;
    }

    public class CustomFooterView extends RecyclerView.ViewHolder {

        protected TextView totalTaxAmount, totalAmt, cardNumber, addresseName, addressLine1, addressLine2;
        protected ImageView imgCancelOrder;


        public CustomFooterView(View itemView) {
            super(itemView);
            totalTaxAmount = (TextView) itemView.findViewById(R.id.totalTaxAmt);
            totalAmt = (TextView) itemView.findViewById(R.id.totalAmt);
            cardNumber = (TextView) itemView.findViewById(R.id.textCardNumber);
            addresseName = (TextView) itemView.findViewById(R.id.addresseName);
            addressLine1 = (TextView) itemView.findViewById(R.id.addressLine1);
            addressLine2 = (TextView) itemView.findViewById(R.id.addressLine2);
            imgCancelOrder = (ImageView) itemView.findViewById(R.id.imgCancelOrder);
        }
    }


    //    @Override
//    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
//        super.onAttachedToRecyclerView(recyclerView);
//    }
//
//    /**
//     * Called when RecyclerView needs a new {@link RecyclerView.ViewHolder} of the given type to represent
//     * an item.
//     * <p/>
//     * This new ViewHolder should be constructed with a new View that can represent the items
//     * of the given type. You can either create a new View manually or inflate it from an XML
//     * layout file.
//     * <p/>
//     * The new ViewHolder will be used to display items of the adapter using
//     * {@link #onBindViewHolder(RecyclerView.ViewHolder, int)}. Since it will be re-used to display different
//     * items in the data set, it is a good idea to cache references to sub views of the View to
//     * avoid unnecessary {@link View#findViewById(int)} calls.
//     *
//     * @param parent   The ViewGroup into which the new View will be added after it is bound to
//     *                 an adapter position.
//     * @param viewType The view type of the new View.
//     * @return A new ViewHolder that holds a View of the given view type.
//     * @see #getItemViewType(int)
//     * @see #onBindViewHolder(RecyclerView.ViewHolder, int)
//     */
//    @Override
//    public OrderDetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
//        OrderDetailsViewHolder addressViewHolder = new OrderDetailsViewHolder(view);
//        return addressViewHolder;
//    }
//
//    /**
//     * Called by RecyclerView to display the data at the specified position. This method
//     * should update the contents of the {@link RecyclerView.ViewHolder#itemView} to reflect the item at
//     * the given position.
//     * <p/>
//     * Note that unlike {@link ListView}, RecyclerView will not call this
//     * method again if the position of the item changes in the data set unless the item itself
//     * is invalidated or the new position cannot be determined. For this reason, you should only
//     * use the <code>position</code> parameter while acquiring the related data item inside this
//     * method and should not keep a copy of it. If you need the position of an item later on
//     * (e.g. in a click listener), use {@link RecyclerView.ViewHolder#getAdapterPosition()} which will have
//     * the updated adapter position.
//     *
//     * @param holder   The ViewHolder which should be updated to represent the contents of the
//     *                 item at the given position in the data set.
//     * @param position The position of the item within the adapter's data set.
//     */
//    @TargetApi(Build.VERSION_CODES.KITKAT)
//    @Override
//    public void onBindViewHolder(OrderDetailsViewHolder holder, int position) {
//        Product product = productList.get(position);
//        holder.lblProductName.setText(product.getName());
//        holder.lblProductPrice.setText(product.getPrice());
//        holder.lblProductQuantity.setText(product.getQauntity());
//
//        Picasso.with(mActivity)
//                .load(R.drawable.product_list_placeholder)
//                .placeholder(R.drawable.product_list_placeholder)
//                .resize(300, 480)
//                .centerInside()
//                .into(holder.product_image);
//    }
//
//    /**
//     * Returns the total number of items in the data set hold by the adapter.
//     *
//     * @return The total number of items in this adapter.
//     */
//    @Override
//    public int getItemCount() {
//        return productList.size();
//    }
//
    public class OrderDetailsViewHolder extends RecyclerView.ViewHolder {

        TextView lblProductName;
        TextView lblProductPrice;
        TextView lblProductQuantity;
        ImageView product_image;

        OrderDetailsViewHolder(View itemView) {
            super(itemView);
            lblProductName = (TextView) itemView.findViewById(R.id.lblProductName);
            lblProductPrice = (TextView) itemView.findViewById(R.id.lblProductPrice);
            lblProductQuantity = (TextView) itemView.findViewById(R.id.lblProductQuantity);
            product_image = (ImageView) itemView.findViewById(R.id.product_image);
        }
    }
}
