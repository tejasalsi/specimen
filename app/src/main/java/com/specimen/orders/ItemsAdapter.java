package com.specimen.orders;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.specimen.R;
import com.specimen.core.model.OrderProduct;
import com.specimen.core.model.Product;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Swapnil on 8/24/2015.
 */
public class ItemsAdapter extends RecyclerView.Adapter<ItemsViewHolder> {

    private final DisplayMetrics matrix;
    private Activity mContext;
    private List<Product> mProduct;

    public ItemsAdapter(Activity mContext, List<Product> orderItems) {
        this.mContext = mContext;
        this.mProduct = orderItems;
        this.matrix = mContext.getResources().getDisplayMetrics();
    }

    @Override
    public ItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_row_items, null);

        return new ItemsViewHolder(view, mProduct);
    }

    @Override
    public void onBindViewHolder(ItemsViewHolder holder, final int position) {
        holder.tvProductName.setText(mProduct.get(position).getName());
        holder.tvProductPrice.setText(mContext.getResources().getString(R.string.rupee_symbole) + " " + mProduct.get(position).getPrice());
        holder.tvQuantity.setText("Quantity: " + mProduct.get(position).getQuantity());

        if (mProduct.get(position).getImage() != null && mProduct.get(position).getImage().size() > 0) {
            Picasso.with(mContext)
                    .load(ImageUtils.getProductImageUrl(mProduct.get(position).getImage().get(0), matrix))
                    .placeholder(R.drawable.cart_img_placeholder)
                    .resize(200, 240)
                    .into(holder.ivProductImage);
        } else {
            Picasso.with(mContext)
                    .load(R.drawable.cart_img_placeholder)
                    .resize(200, 240)
                    .into(holder.ivProductImage);
        }


       if (mProduct.get(position).getFeedback()) {
            holder.btnFeedback.setVisibility(View.GONE);
        } else {
            holder.btnFeedback.setVisibility(View.VISIBLE);
        }

//        holder.btnFeedback.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mContext.startActivity(new Intent(mContext, FeedbackActivity.class));
//            }
//        });
//
//        holder.ivProductImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent productDetailIntent = new Intent(mContext, ProductDetailsActivity.class);
//                productDetailIntent.putExtra("PRODUCT_ID", mProduct.get(position).getId());
//                mContext.startActivity(productDetailIntent);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mProduct.size();
    }
}
