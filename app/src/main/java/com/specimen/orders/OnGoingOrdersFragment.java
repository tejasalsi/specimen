package com.specimen.orders;

import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.model.OrdersEntity;

import java.util.ArrayList;


public class OnGoingOrdersFragment extends BaseFragment {

    OrderAdapter mOrderAdapter;
    RecyclerView rvOngoingOrders;
    ArrayList<OrdersEntity> onGoingOrders;

    public OnGoingOrdersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        onGoingOrders = bundle.getParcelableArrayList("ongoing");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_on_going_orders, container, false);
        rvOngoingOrders = (RecyclerView) rootView.findViewById(R.id.rvOngoing);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvOngoingOrders.setLayoutManager(layoutManager);

        if (0 >= onGoingOrders.size()) {

        } else {
            mOrderAdapter = new OrderAdapter(mContext, onGoingOrders);
            rvOngoingOrders.setAdapter(mOrderAdapter);
            Log.e("OnGoing", onGoingOrders.toString());

            mOrderAdapter.SetOnOrderClickListener(new OnOrderClickListner() {
                @Override
                public void onItemClickListner(View v, int position) {
                    Intent intent = new Intent(getActivity(), OrderDetailsActivity.class);
                    intent.putExtra("orderid", onGoingOrders.get(position).getOrderId());
                    intent.putExtra("orderNo",onGoingOrders != null ? onGoingOrders.get(position).getOrderNo() : "");
                    intent.putExtra("order", onGoingOrders.get(position));
                    startActivity(intent);
                }
            });
        }

        return rootView;
    }
}
