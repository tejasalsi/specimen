package com.specimen.filter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Rohit on 10/19/2015.
 */
public abstract class BaseFilterAdapter extends RecyclerView.Adapter<BaseFilterHolder> {
    protected Context context;

    public BaseFilterAdapter(Context context) {
        this.context = context;
    }

}
