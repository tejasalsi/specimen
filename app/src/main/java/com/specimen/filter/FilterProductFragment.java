package com.specimen.filter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.category.ICategoryFacade;
import com.specimen.core.filter.IFilterFacade;
import com.specimen.core.model.Query;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.CategoryListResponse;
import com.specimen.core.response.FilterResponse;
import com.specimen.search.footerEvent.FilterToggle;
import com.specimen.util.BusProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tejas on 6/15/2015.
 * Filter UI for Product Gallery.
 */
@SuppressWarnings("ALL")
public class FilterProductFragment extends BaseFragment implements View.OnClickListener {

    private RecyclerView filterRecyclerView;
    private Button resetButton, applyButton;
    private BaseFilterAdapter filterAdapter;
    //    private CateoryFilterAdapter cateoryFilterAdapter;
    private List filterType;

    private boolean callFilterApi;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View filterView = inflater.inflate(R.layout.fragment_productfilter, container, false);


        filterRecyclerView = (RecyclerView) filterView.findViewById(R.id.filerRecyclerView);

        boolean isFromSearch = getArguments().getBoolean("isFromSearch");

        if (isFromSearch) {

            String topCategory = getArguments().getString("topCategory");

            if (topCategory != null) {

                initFilterAdapter();

            } else {

                filterAdapter = new CateoryFilterAdapter(getActivity(), new CategoryListResponse());
            }

        } else {

            initFilterAdapter();

        }


        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        filterRecyclerView.setLayoutManager(layoutManager);

        filterRecyclerView.setAdapter(filterAdapter);

        resetButton = (Button) filterView.findViewById(R.id.resetButton);
        applyButton = (Button) filterView.findViewById(R.id.applyButton);

        resetButton.setOnClickListener(this);
        applyButton.setOnClickListener(this);

        return filterView;
    }

    private void initFilterAdapter() {
        filterType = new ArrayList();
        filterAdapter = new FilterAdapter(getActivity(), filterType);
        callFilterApi = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);

        if (filterType != null)
            filterType.clear();

        filterAdapter.notifyDataSetChanged();
        if (callFilterApi) {
            ((IFilterFacade) IOCContainer.getInstance().getObject(ServiceName.FILTER_SERVICE, "")).getFilter();
        } else {
            ((ICategoryFacade) IOCContainer.getInstance().getObject(ServiceName.CATEGORY_SERVICE, "")).getCategoryList();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }


    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (response instanceof FilterResponse) {
            FilterResponse filterResponse = ((FilterResponse) response);
            filterType.addAll(filterResponse.getData());
            filterAdapter.notifyDataSetChanged();
        } else if (response instanceof CategoryListResponse) {
            filterAdapter = new CateoryFilterAdapter(getActivity(), (CategoryListResponse) response);
            filterRecyclerView.setAdapter(filterAdapter);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.applyButton:
                Query.getInstance().clearBackup();
                break;

            case R.id.resetButton:
                Query.getInstance().resetFilter();
                filterAdapter.notifyDataSetChanged();
                break;
        }

        BusProvider.getInstance().post(new FilterToggle());
    }


}
