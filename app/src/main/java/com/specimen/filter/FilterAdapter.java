package com.specimen.filter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.FilterType;
import com.specimen.core.model.FilterTypeOption;
import com.specimen.core.model.Query;
import com.specimen.util.CustomGridView;
import com.specimen.widget.AutoAdjustLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tejas on 7/1/2015.
 */
public class FilterAdapter extends BaseFilterAdapter {

    private List<FilterType> filters;

    public FilterAdapter(Context context, List<FilterType> filters) {
        super(context);
        this.filters = filters;
    }


    @Override
    public BaseFilterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_filter_row, null);

        return new FilterHolder(view);
    }


    @Override
    public void onBindViewHolder(BaseFilterHolder holder, int position) {
        FilterType filter = filters.get(position);
        holder.getView(filter);
    }

    @Override
    public int getItemCount() {
        return filters != null ? filters.size() : 0;
    }

    public class FilterHolder extends BaseFilterHolder implements View.OnClickListener {
        TextView lable;
        AutoAdjustLayout adjustLayout;
        FilterType filterType;

        public FilterHolder(View itemView) {
            super(itemView);
            lable = (TextView) itemView.findViewById(R.id.lable);
            adjustLayout = (AutoAdjustLayout) itemView.findViewById(R.id.autoAdjustLayout);
        }

        public void getView(Object filterType) {

            if (filterType == null) {
                return;
            }
            this.filterType = (FilterType) filterType;
            lable.setText(this.filterType.getLabel());

            adjustLayout.removeAllViews();

            LayoutInflater layoutInflater = LayoutInflater.from(context);


            ArrayList<String> values = Query.getInstance().getValues(this.filterType.getCode());

            if (null != ((FilterType) filterType).getOptions()) {

                for (FilterTypeOption filterTypeOption : this.filterType.getOptions()) {
                    final TextView btnFilterView = (TextView) layoutInflater.inflate(R.layout.filter_options_view, null);

                    btnFilterView.setTag(filterTypeOption);

                    Context context = btnFilterView.getContext();
                    int textColor;
                    Drawable backGroundSelector = null;

                    if (values != null && values.contains(filterTypeOption.getId())) {
                        textColor = context.getResources().getColor(android.R.color.holo_green_light);
                        backGroundSelector = context.getResources().getDrawable(R.drawable.filter_option_selected);
                    } else {
                        textColor = context.getResources().getColor(android.R.color.black);
                        backGroundSelector = context.getResources().getDrawable(R.drawable.filter_option);
                    }

                    GradientDrawable gradientDrawable = (GradientDrawable) backGroundSelector;
                    if (filterTypeOption.getHexcode() != null && !TextUtils.isEmpty(filterTypeOption.getHexcode())) {
                        assert gradientDrawable != null;
                        gradientDrawable.setColor(Color.parseColor(filterTypeOption.getHexcode()));
//                    btnFilterView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                        @Override
//                        public void onGlobalLayout() {
//                            if(btnFilterView.getHeight() > 0) {
//                                btnFilterView.setHeight(50);//getLayoutParams().height = 50;
//                                btnFilterView.setWidth(50);//.width = 50;
//                                btnFilterView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//                            }
//                        }
//                    });

//                    gradientDrawable.setColor(Color.parseColor("#00000000"));
                        btnFilterView.setBackground(gradientDrawable);
                    } else {
                        assert gradientDrawable != null;
                        gradientDrawable.setColor(Color.parseColor("#00000000"));
                        btnFilterView.setText(filterTypeOption.getLabel());
                        btnFilterView.setBackground(backGroundSelector);
                    }

                    btnFilterView.setTextColor(textColor);


                    btnFilterView.setOnClickListener(this);
                    adjustLayout.addView(btnFilterView);
                }
            }
        }

        @Override
        public void onClick(View v) {
            FilterTypeOption filterOptionState = (FilterTypeOption) v.getTag();

            if (!filterOptionState.isSelected()) {
                Query.getInstance().setValues(filterType.getCode(), filterOptionState.getId());
            } else {
                Query.getInstance().removeValue(filterType.getCode(), filterOptionState.getId());
            }

            filterOptionState.setIsSelected(!filterOptionState.isSelected());
            notifyDataSetChanged();
        }
    }
}
