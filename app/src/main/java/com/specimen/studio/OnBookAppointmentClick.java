package com.specimen.studio;

/**
 * Created by Swapnil on 9/2/2015.
 */
public interface OnBookAppointmentClick {

    void onBookAppointmentClick(String slot);
}
