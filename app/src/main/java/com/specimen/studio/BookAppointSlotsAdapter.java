package com.specimen.studio;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.StudioSlotsResponse.DataEntity.TimeslotsEntity;


import java.util.List;


public class BookAppointSlotsAdapter extends RecyclerView.Adapter<BookAppointSlotsAdapter.RowBookAppointSlotsViewHolder> {

    private List<TimeslotsEntity> mDataSet;

    private Context context;
    private LayoutInflater layoutInflater;
    int mSelectedSlot = -1;
    private OnBookAppointmentClick onBookAppointmentClick;

    public BookAppointSlotsAdapter(Context context, List<TimeslotsEntity> input) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.mDataSet = input;
        try {
            onBookAppointmentClick = (OnBookAppointmentClick) context;
        }catch (ClassCastException e){
            Log.e("Error :",e.getMessage());
        }
    }

    @Override
    public RowBookAppointSlotsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.studio_appointment_row_time_slots, null);
        RowBookAppointSlotsViewHolder holder = new RowBookAppointSlotsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RowBookAppointSlotsViewHolder holder, int position) {
        holder.setView(mDataSet.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class RowBookAppointSlotsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView btnTiming;
        private TextView btnStatus;

        public RowBookAppointSlotsViewHolder(View view) {
            super(view);
            btnTiming = (TextView) view.findViewById(R.id.btnTiming);
            btnStatus = (TextView) view.findViewById(R.id.btnStatus);

            btnStatus.setOnClickListener(this);
        }

        void setView(TimeslotsEntity object, int pos) {
            btnTiming.setText(object.getId());
            btnStatus.setPaintFlags(0);
            btnStatus.setBackgroundColor(context.getResources().getColor(R.color.specimen_grey_light));
            if (object.getStatus().equalsIgnoreCase("BOOKED")) {
                btnStatus.setTextColor(context.getResources().getColor(R.color.specimen_green));
                btnStatus.setPaintFlags(btnStatus.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                btnStatus.setTextColor(context.getResources().getColor(R.color.ninety_percent_transparent));
            }
            btnStatus.setText(object.getStatus());

            if(mSelectedSlot == pos) {
                btnStatus.setText("SELECTED");
                btnStatus.setBackgroundColor(context.getResources().getColor(R.color.edittexttheme_color));
            }
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.btnStatus) {
                if (!((TextView) view).getText().toString().equalsIgnoreCase("BOOKED")) {
                    mSelectedSlot = getAdapterPosition();
                    onBookAppointmentClick.onBookAppointmentClick(mDataSet.get(getAdapterPosition()).getSlot());
                    notifyDataSetChanged();
                }
            }
        }
    }

}
