package com.specimen.studio;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.model.BusyDatesResponse;
import com.specimen.core.response.APIResponse;
import com.specimen.core.studio.IStudioFacade;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Created by root on 25/8/15.
 */
public class StudioCalenderActivity extends BaseActivity implements IResponseSubscribe {

    private String studio_id;
    private String TAG = "StudioCalenderActivity";
    private CalendarView cv;
    private HashSet<Date> events;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studio_calender);
        getToolbar();
        Bundle bundle = getIntent().getExtras();
        studio_id = bundle.getString("studio_id");

        events = new HashSet<>();
        events.add(new Date());
        cv = ((CalendarView) findViewById(R.id.calendar_view));

        // assign event handler
        cv.setEventHandler(new CalendarView.EventHandler() {
            @Override
            public void onDayLongPress(Date date) {
                // show returned day
//                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                DateFormat df = SimpleDateFormat.getDateInstance();
                Toast.makeText(StudioCalenderActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(StudioCalenderActivity.this, StudioBookingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("studio_id", studio_id);
//                bundle.putString("date", df.format(date));
                bundle.putLong("date", date.getTime());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    public Toolbar getToolbar () {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.experience_studio_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        return toolbar;
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
        ((IStudioFacade) IOCContainer.getInstance().getObject(ServiceName.STUDIO_SERVICE, TAG)).getBusyDates(studio_id);
    }

    @Override
    protected void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (TAG.equals(tag)) {
            if (response instanceof BusyDatesResponse) {
                Log.e(TAG, response.toString());
                BusyDatesResponse busyDatesResponse = (BusyDatesResponse) response;
                events = getDatesHashSet(busyDatesResponse.getData().getBusyDates());
                cv.updateCalendar(events);
            }
        }
    }

    private HashSet<Date> getDatesHashSet(List<String> busyDates) {
        HashSet<Date> dates = new HashSet<>();
        for (int index = 0; index < busyDates.size(); index++) {
            Date expiredDate = stringToDate(busyDates.get(index), "yyyy-MM-dd");
            dates.add(expiredDate);
        }
        return dates;
    }

    private Date stringToDate(String aDate,String aFormat) {

        if(aDate==null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        return simpledateformat.parse(aDate, pos);

    }

    @Override
    public void onFailure(Exception error) {
    }
}