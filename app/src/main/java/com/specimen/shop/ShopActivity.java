package com.specimen.shop;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.specimen.BaseFragment;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.ServiceName;
import com.specimen.core.category.ICategoryFacade;
import com.specimen.core.model.TopCategory;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.CategoryListResponse;
import com.specimen.search.SearchFragment;
import com.specimen.widget.ProgressView;

import java.util.ArrayList;

public class ShopActivity extends BaseFragment {

    public final String TAG = "SHOP";
    ArrayList<TopCategory> tabTitles;
    PagerSlidingTabStrip tabs;
//    APIResponse response = null;
    private ViewPager viewPager;
    private ProgressView progressView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(getContentView(), container, false);
        callAfterInitialization(view);
        return view;
    }

    protected void callAfterInitialization(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.categoryViewPager);
        tabTitles = new ArrayList<>();

        // Bind the tabs to the ViewPager
        tabs = (PagerSlidingTabStrip) view.findViewById(R.id.categoryTabs);
        progressView = (ProgressView) view.findViewById(R.id.progressBar);

        view.findViewById(R.id.searchHolder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.switchFragment(new SearchFragment(),true,R.id.fragmentContainer);
            }
        });
    }

    protected int getContentView() {
        return R.layout.activity_shop;
    }


    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);

        fetchCategory();
    }

    private void fetchCategory() {
        if (tabTitles.size() == 0) {
            progressView.setVisibility(View.VISIBLE);
            ((ICategoryFacade) IOCContainer.getInstance().getObject(ServiceName.CATEGORY_SERVICE, TAG)).getCategoryList();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (!tag.equals(TAG)) {
            return;
        }
        progressView.setVisibility(View.GONE);
        if (response instanceof CategoryListResponse) {
            Log.d(TAG, ((CategoryListResponse) response).getData().toString());
            for (int i = 0; i < ((CategoryListResponse) response).getData().size(); i++) {
                tabTitles.add(((CategoryListResponse) response).getData().get(i));
            }
            Log.d(TAG, tabTitles.toString());
            //tabTitles = ((CategoryListResponse) response).getData().toString();
            SpecimenCategoryPagerAdapter specimenCategoryPagerAdapter = new SpecimenCategoryPagerAdapter(mContext, getChildFragmentManager(), tabTitles);
            viewPager.setAdapter(specimenCategoryPagerAdapter);

            tabs.setViewPager(viewPager);

            specimenCategoryPagerAdapter.notifyDataSetChanged();

        }

    }

    @Override
    public void onFailure(Exception error) {
//        Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
        Log.e(TAG, error.toString());
        progressView.setVisibility(View.GONE);
    }


}
