package com.specimen.shop;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.specimen.R;
import com.specimen.core.model.Explore;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jitendra Khetle on 29/06/15.
 */
public class ExploreAdapter extends ArrayAdapter<Explore> {
    private final DisplayMetrics matrix;
    //    Context context;
    LayoutInflater inflater;
    List<Explore> explores;
    int resourceLayout;
    String TAG = "SubCategoryAdapter";

    public ExploreAdapter(Context mContext, int resource, ArrayList<Explore> objects) {
        super(mContext, resource, objects);
//        this.context = mContext;
        this.resourceLayout = resource;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        explores = objects;
        matrix = mContext.getResources().getDisplayMetrics();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ExploreHolder exploreHolder = new ExploreHolder();
        if (convertView == null) {
            convertView = inflater.inflate(resourceLayout, null);
            exploreHolder.exploreImage = (ImageView) convertView.findViewById(R.id.exploreImage);
            convertView.setTag(exploreHolder);

        } else {
            exploreHolder = (ExploreHolder) convertView.getTag();
        }

        Picasso.with(getContext())
                .load(ImageUtils.getProductImageUrl(explores.get(position).getImage(), matrix))
                .placeholder(R.drawable.banner_explore_placeholder)
                .resize(matrix.widthPixels, (matrix.widthPixels / 4) * 3)
                 .centerInside()
                .into(exploreHolder.exploreImage);
        return convertView;
    }

    private class ExploreHolder {
        public ImageView exploreImage;
    }
}
