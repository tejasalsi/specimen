package com.specimen.shop;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.Subcategory;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Jitendra Khetle on 29/06/15.
 */
public class SubCategoryAdapter extends ArrayAdapter<Subcategory> {
    private final DisplayMetrics matrix;
    Context context;
    LayoutInflater inflater;
    List<Subcategory> subcategories;
    int resourceLayout;
    String TAG = "SubCategoryAdapter";

    public SubCategoryAdapter(Context mContext, int resource, ArrayList<Subcategory> objects) {
        super(mContext, resource, objects);
        this.context = mContext;
        this.resourceLayout = resource;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        subcategories = objects;
        matrix = context.getResources().getDisplayMetrics();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SubCategoryHolder subCategoryHolder = new SubCategoryHolder();
        if (convertView == null) {
            convertView = inflater.inflate(resourceLayout, null);
            subCategoryHolder.subCategoryImage = (ImageView) convertView.findViewById(R.id.gridImage);
            subCategoryHolder.subCategoryTitle = (TextView) convertView.findViewById(R.id.gridTitle);
            convertView.setTag(subCategoryHolder);

        } else {
            subCategoryHolder = (SubCategoryHolder) convertView.getTag();
        }


        Picasso.with(context)
                .load(ImageUtils.getProductImageUrl(subcategories.get(position).getImage(), matrix))
                .placeholder(R.drawable.product_list_placeholder)
//                .resize(matrix.widthPixels, matrix.heightPixels)
                //.resize(matrix.widthPixels, 100)
               // .centerInside()
                .into(subCategoryHolder.subCategoryImage);
        subCategoryHolder.subCategoryTitle.setText(subcategories.get(position).getName().toUpperCase());

        return convertView;
    }

    private class SubCategoryHolder {
        public ImageView subCategoryImage;
        public TextView subCategoryTitle;
    }
}
