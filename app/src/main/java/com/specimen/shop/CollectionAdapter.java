package com.specimen.shop;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.R;
import com.specimen.core.model.Collection;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jitendra Khetle on 29/06/15.
 */
public class CollectionAdapter extends ArrayAdapter<Collection> {
    private final DisplayMetrics matrix;
    //    Context context;
    LayoutInflater inflater;
    List<Collection> collections;
    int resourceLayout;
    String TAG = "CollectionAdapter";

    public CollectionAdapter(Context mContext, int resource, ArrayList<Collection> objects) {
        super(mContext, resource, objects);
//        this.context = mContext;
        this.resourceLayout = resource;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        collections = objects;
        matrix = mContext.getResources().getDisplayMetrics();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CollectionHolder collectionHolder = new CollectionHolder();
        if (convertView == null) {
            convertView = inflater.inflate(resourceLayout, null);
            collectionHolder.collectionImage = (ImageView) convertView.findViewById(R.id.gridImage);
            collectionHolder.collectionTitle = (TextView) convertView.findViewById(R.id.gridTitle);
            convertView.setTag(collectionHolder);

        } else {
            collectionHolder = (CollectionHolder) convertView.getTag();
        }
        Picasso.with(getContext())
                .load(ImageUtils.getProductImageUrl(collections.get(position).getImage(), matrix))
                .placeholder(R.drawable.banner_explore_placeholder)
               // .centerInside()
                .into(collectionHolder.collectionImage);
        collectionHolder.collectionTitle.setText(collections.get(position).getName().toUpperCase());
        return convertView;
    }

    private class CollectionHolder {
        public ImageView collectionImage;
        public TextView collectionTitle;
    }
}
