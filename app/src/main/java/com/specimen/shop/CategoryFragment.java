package com.specimen.shop;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.model.Collection;
import com.specimen.core.model.Explore;
import com.specimen.core.model.Query;
import com.specimen.core.model.Subcategory;
import com.specimen.core.model.TopCategory;
import com.specimen.product.ProductListingActivity;
import com.specimen.product.ProductSubCategoryListingActivity;

import java.util.ArrayList;


public class CategoryFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    public static boolean isPopupWindowDisplayed;

    private static final String TAG = "CategoryFragment";

    GridView subCategoryHolderGrid, collectionHolderGrid;
    ListView exploreList;
    ArrayList<Subcategory> subcategories;
    ArrayList<Collection> collections;
    ArrayList<Explore> explores;
    String topCategoryId;

    public static Fragment newInstance(TopCategory topCategory) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString("topCategory", "" + topCategory.getId());
        bundle.putParcelableArrayList("subcategories", (ArrayList<Subcategory>) topCategory.getSubcategory());
        bundle.putParcelableArrayList("collections", (ArrayList<Collection>) topCategory.getCollection());
        bundle.putParcelableArrayList("explores", (ArrayList<Explore>) topCategory.getExplore());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            subcategories = getArguments().getParcelableArrayList("subcategories");
            collections = getArguments().getParcelableArrayList("collections");
            explores = getArguments().getParcelableArrayList("explores");
            topCategoryId = getArguments().getString("topCategory");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        subCategoryHolderGrid = (GridView) view.findViewById(R.id.subCategoryHolderGrid);
        collectionHolderGrid = (GridView) view.findViewById(R.id.collectionHolderGrid);
        exploreList = (ListView) view.findViewById(R.id.exploreHolderList);

        if (subcategories != null && 0 < subcategories.size()) {
            SubCategoryAdapter subCategoryAdapter = new SubCategoryAdapter(getActivity(), R.layout.top_level_grid_item, subcategories);
            subCategoryHolderGrid.setAdapter(subCategoryAdapter);

        } else {
            subCategoryHolderGrid.setVisibility(View.GONE);
        }
        if (collections != null && 0 < collections.size()) {
            view.findViewById(R.id.collectionHeader).setVisibility(View.VISIBLE);
            CollectionAdapter collectionAdapter = new CollectionAdapter(getActivity(), R.layout.top_level_grid_item, collections);
            collectionHolderGrid.setAdapter(collectionAdapter);
        } else {
            view.findViewById(R.id.collectionHeader).setVisibility(View.GONE);
            collectionHolderGrid.setVisibility(View.GONE);
        }
        if (explores != null && 0 < explores.size()) {
            ExploreAdapter exploreAdapter = new ExploreAdapter(getActivity(), R.layout.explore_item_layout, explores);
            exploreList.setAdapter(exploreAdapter);
        } else {
            exploreList.setVisibility(View.GONE);
        }

        collectionHolderGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Query.getInstance().totalResetQuery();
                Query.getInstance().setTopCategoryId(topCategoryId);
                Query.getInstance().setCollection("" + collections.get(position).getId());

                Bundle bundle = new Bundle();
                bundle.putBoolean("isCategory", false);
                ProductListingActivity productListingActivity = new ProductListingActivity();
                mContext.switchFragment(productListingActivity, true, R.id.fragmentContainer);

            }
        });

        exploreList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Query.getInstance().totalResetQuery();
                Query.getInstance().setTopCategoryId(topCategoryId);
                Query.getInstance().setExplore("" + explores.get(position).getId());
                mContext.switchFragment(new ProductListingActivity(), true, R.id.fragmentContainer);


            }
        });

        Log.d(TAG, "on page load ");

        subCategoryHolderGrid.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "on Item click");
        Query.getInstance().totalResetQuery();
        Query.getInstance().setTopCategoryId(topCategoryId);

        Bundle intent = new Bundle();
        intent.putBoolean("isCategory", true);
        intent.putString("selectedSubCategory", subcategories.get(position).getName());
        intent.putInt("selectedSubCategoryPosition", position);
        intent.putParcelableArrayList("subcategories", subcategories);

        ProductSubCategoryListingActivity fragment = new ProductSubCategoryListingActivity();
        fragment.setArguments(intent);
        mContext.switchFragment(fragment, true, R.id.fragmentContainer);
    }
}
