package com.specimen.feedback;

import android.app.Activity;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.cart.ICartFacade;
import com.specimen.core.feedback.IFeedbackFacade;
import com.specimen.core.response.APIResponse;
import com.squareup.picasso.Picasso;

/**
 * Created by root on 8/10/15.
 */
public class FeedbackActivity extends BaseActivity implements IResponseSubscribe {

    private String ratingStyle, ratingFit, ratingFabric, ratingTailoring;
    private int STYLE_CONSTANT = 30, FIT_CONSTANT = 15, TAILORING_CONSTANT = 20, FABRIC_CONSTANT = 25;
    private String TAG = "FeedbackActivity";
    private String mProductName, mProductUrl, mProductId;
    private ImageView mProductImage;
    private String mProductPrice;
    private String mProductDiscountedPrice;
    private TextView tvProductName, tvSpecialPrice, tvPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_feedback);
        setToolbar();
        Bundle bundle = getIntent().getExtras();
        mProductName = bundle.getString("productName");
        mProductUrl = bundle.getString("imageUrl");
        mProductId = bundle.getString("productId");
        mProductPrice  = bundle.getString("productPrice");
        mProductDiscountedPrice  = bundle.getString("productDPrice");
        initUI();

    }

    void setToolbar () {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText(getResources().getString(R.string.str_feedback).toUpperCase());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initUI() {
        final RatingBar ratingBarStyle = (RatingBar) findViewById(R.id.ratingBarStyle);
        final RatingBar ratingBarFit = (RatingBar) findViewById(R.id.ratingBarFit);
        final RatingBar ratingBarFabric = (RatingBar) findViewById(R.id.ratingBarFabric);
        final RatingBar ratingBarTailoring = (RatingBar) findViewById(R.id.ratingBarTailoring);

        tvProductName = (TextView) findViewById(R.id.tvProductName);
        tvPrice = (TextView) findViewById(R.id.tvPrice);
        tvSpecialPrice = (TextView) findViewById(R.id.tvDPrice);

        tvProductName.setText(mProductName.toUpperCase());
        tvSpecialPrice.setText(getResources().getString(R.string.rupee_symbole)+" "+mProductPrice);

        if(!TextUtils.isEmpty(mProductDiscountedPrice) && !mProductDiscountedPrice.equals("0")) {
            tvPrice.setVisibility(View.VISIBLE);
            tvPrice.setText(getResources().getString(R.string.rupee_symbole)+" "+mProductDiscountedPrice);
            tvSpecialPrice.setText(getResources().getString(R.string.rupee_symbole)+" "+mProductDiscountedPrice);
            tvPrice.setPaintFlags(tvSpecialPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        ratingSetUp(ratingBarStyle);
        ratingSetUp(ratingBarFit);
        ratingSetUp(ratingBarFabric);
        ratingSetUp(ratingBarTailoring);

        mProductImage = (ImageView) findViewById(R.id.imgProduct);
        mProductImage.setVisibility(View.VISIBLE);
        Picasso.with(FeedbackActivity.this)
                .load(mProductUrl)
                .resize(300, 480)
                .centerInside()
                .into(mProductImage);

        Button btnSubmitFeedback = (Button) findViewById(R.id.btnSubmitFeedback);
        btnSubmitFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Getting the rating and displaying it on the toast
//                ratingStyle = String.valueOf(ratingBarStyle.getRating());
//                ratingFit = String.valueOf(ratingBarFit.getRating());
//                ratingFabric = String.valueOf(ratingBarFabric.getRating());
//                ratingTailoring = String.valueOf(ratingBarTailoring.getRating());
                ;
                ratingStyle = (STYLE_CONSTANT + ratingBarStyle.getProgress()) + "";
                ratingFit = (FIT_CONSTANT + ratingBarFit.getProgress()) + "";
                ratingFabric = (FABRIC_CONSTANT + ratingBarFabric.getProgress()) + "";
                ratingTailoring = (TAILORING_CONSTANT + ratingBarTailoring.getProgress()) + "";

                Log.d("Ratings", "Style :" + ratingStyle + " Fit : " + ratingFit + " Fabric : " + ratingFabric + " Tailering : " + ratingTailoring);
                if (ratingBarStyle.getProgress() == 0 || ratingBarFit.getProgress() == 0 || ratingBarFabric.getProgress() == 0 || ratingBarTailoring.getProgress() == 0) {
                    Toast.makeText(getApplicationContext(), "Please rate for all features", Toast.LENGTH_SHORT).show();
                } else {
                    ((IFeedbackFacade) IOCContainer.getInstance().getObject(ServiceName.FEEDBACK_SERVICE, TAG)).shareRating(mProductId, "", ratingFabric, ratingFit, ratingStyle, ratingTailoring);
                }

            }
        });
    }

    void ratingSetUp(RatingBar ratingBar) {
        ratingBar.setMax(5);
        ratingBar.setStepSize(1.0f);
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        if (!tag.equals(TAG)) {
            return;
        }

        if (null != response) {
            Toast.makeText(FeedbackActivity.this, "" + response.getMessage(), Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onFailure(Exception error) {

    }
}
