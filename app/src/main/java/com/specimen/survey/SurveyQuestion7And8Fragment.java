package com.specimen.survey;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.model.Questions;
import com.specimen.survey.callback.ISurveyFragmentChanger;
import com.specimen.widget.TouchImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 19/7/15.
 */
public class SurveyQuestion7And8Fragment extends BaseFragment {

    DisplayMetrics metrics;
    ISurveyFragmentChanger mISurveyFragmentChanger;

    final int SURVEY_QUESTION_7_AND_8 = 6;

    TextView lblQuestion_7_Option_1, lblQuestion_7_Option_2, lblQuestion_7_Option_3, lblQuestion_7_Option_4, lblQuestion_7_Option_5;
    ImageView imgOption_1, imgOption_2;
    View view_option_1, view_option_2;

    boolean isQuestion7Answered, isQuestion8Answered;

    Questions questions_1, questions_2;
    List<String> apiAnswerList_1, apiAnswerList_2;
    List<String> userSelectedAnswerList;

    public SurveyQuestion7And8Fragment(ISurveyFragmentChanger iSurveyFragmentChanger) {
        this.mISurveyFragmentChanger = iSurveyFragmentChanger;
    }

    @Override
    public void onResume() {
        super.onResume();
        userSelectedAnswerList = ((SurveyActivity) getActivity()).getSelectedAnswer(questions_1.getId());
        setQuestion_7_Answer();
        userSelectedAnswerList = ((SurveyActivity) getActivity()).getSelectedAnswer(questions_2.getId());
        setQuestion_8_Answer();

        ((SurveyActivity) getActivity()).visibleProgressbar(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_survey_question_7_8, container, false);

        questions_1 = ((SurveyActivity) getActivity()).getQuestionsModel(SURVEY_QUESTION_7_AND_8);
        questions_2 = ((SurveyActivity) getActivity()).getQuestionsModel(SURVEY_QUESTION_7_AND_8 + 1);

        apiAnswerList_1 = new ArrayList<>(1);
        apiAnswerList_1 = questions_1.getAnswer();

        apiAnswerList_2 = new ArrayList<>(1);
        apiAnswerList_2 = questions_2.getAnswer();

        userSelectedAnswerList = new ArrayList<>(1);


        if (!TextUtils.isEmpty(questions_1.getSizechart())) {
            view.findViewById(R.id.lblSurveyHeaderNormalText).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    RelativeLayout linearLayout = new RelativeLayout(mContext);
                    linearLayout.setPadding(5, 5, 5, 5);
                    TouchImageView imageView = new TouchImageView(mContext);

                    imageView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    linearLayout.addView(imageView);




                    AlertDialog.Builder alertDialogBuilder =
                            new AlertDialog.Builder(mContext, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setView(linearLayout);
                    Picasso.with(getActivity())
                            .load(questions_1.getSizechart())
                          //  .resize(metrics.widthPixels, metrics.heightPixels)
                            .placeholder(R.drawable.survey_img_placeholder)
                            //.centerInside()
                            .into(imageView);
                    alertDialog.show();
                }
            });
        }


        setOptionOverlayesView(view);

        setSurveyQuestionHeader_1(view);
        setQuestion_7_OptionValues(view);

        setSurveyQuestionHeader_2(view);
        setQuestion_8_OptionValues(view);
        setOptionImages(view);
        setQuestionsDesc(view);

        return view;
    }

    void setOptionOverlayesView(View view) {
        view_option_1 = view.findViewById(R.id.view_option_1);
        view_option_2 = view.findViewById(R.id.view_option_2);

        view_option_1.setVisibility(View.GONE);
        view_option_2.setVisibility(View.GONE);
    }

    void setOptionOverlayesViewVisibility(View view) {
        view_option_1.setVisibility(View.GONE);
        view_option_2.setVisibility(View.GONE);

        view.setVisibility(View.VISIBLE);
    }

    void setSurveyQuestionHeader_1(View view) {
        // set question 7
        ((TextView) view.findViewById(R.id.lblSurveyHeader1)).setText(questions_1.getQuestion());
    }

    void setQuestion_7_OptionValues(View view) {
        lblQuestion_7_Option_1 = (TextView) view.findViewById(R.id.lblQuestion_7_Option_1);
        lblQuestion_7_Option_2 = (TextView) view.findViewById(R.id.lblQuestion_7_Option_2);
        lblQuestion_7_Option_3 = (TextView) view.findViewById(R.id.lblQuestion_7_Option_3);
        lblQuestion_7_Option_4 = (TextView) view.findViewById(R.id.lblQuestion_7_Option_4);
        lblQuestion_7_Option_5 = (TextView) view.findViewById(R.id.lblQuestion_7_Option_5);

        lblQuestion_7_Option_1.setOnClickListener(onClickListener);
        lblQuestion_7_Option_2.setOnClickListener(onClickListener);
        lblQuestion_7_Option_3.setOnClickListener(onClickListener);
        lblQuestion_7_Option_4.setOnClickListener(onClickListener);
        lblQuestion_7_Option_5.setOnClickListener(onClickListener);

        lblQuestion_7_Option_1.setText(questions_1.getOptions().get(0).getValue());
        lblQuestion_7_Option_2.setText(questions_1.getOptions().get(1).getValue());
        lblQuestion_7_Option_3.setText(questions_1.getOptions().get(2).getValue());
        lblQuestion_7_Option_4.setText(questions_1.getOptions().get(3).getValue());
        lblQuestion_7_Option_5.setText(questions_1.getOptions().get(4).getValue());

        lblQuestion_7_Option_1.setTag(questions_1.getOptions().get(0).getId());
        lblQuestion_7_Option_2.setTag(questions_1.getOptions().get(1).getId());
        lblQuestion_7_Option_3.setTag(questions_1.getOptions().get(2).getId());
        lblQuestion_7_Option_4.setTag(questions_1.getOptions().get(3).getId());
        lblQuestion_7_Option_5.setTag(questions_1.getOptions().get(4).getId());

    }

    void setQuestion_7_Answer() {
        lblQuestion_7_Option_1.setBackgroundResource(R.drawable.survey_textview_border);
        lblQuestion_7_Option_2.setBackgroundResource(R.drawable.survey_textview_border);
        lblQuestion_7_Option_3.setBackgroundResource(R.drawable.survey_textview_border);
        lblQuestion_7_Option_4.setBackgroundResource(R.drawable.survey_textview_border);
        lblQuestion_7_Option_5.setBackgroundResource(R.drawable.survey_textview_border);

        if (null != userSelectedAnswerList && 0 < userSelectedAnswerList.size()) {
            if (lblQuestion_7_Option_1.getTag().toString().equals(userSelectedAnswerList.get(0))) {
                lblQuestion_7_Option_1.setBackgroundColor(getResources().getColor(R.color.specimen_green));
            } else if (lblQuestion_7_Option_2.getTag().toString().equals(userSelectedAnswerList.get(0))) {
                lblQuestion_7_Option_2.setBackgroundColor(getResources().getColor(R.color.specimen_green));
            } else if (lblQuestion_7_Option_3.getTag().toString().equals(userSelectedAnswerList.get(0))) {
                lblQuestion_7_Option_3.setBackgroundColor(getResources().getColor(R.color.specimen_green));
            } else if (lblQuestion_7_Option_4.getTag().toString().equals(userSelectedAnswerList.get(0))) {
                lblQuestion_7_Option_4.setBackgroundColor(getResources().getColor(R.color.specimen_green));
            } else if (lblQuestion_7_Option_5.getTag().toString().equals(userSelectedAnswerList.get(0))) {
                lblQuestion_7_Option_5.setBackgroundColor(getResources().getColor(R.color.specimen_green));
            }
        } else {
            if (null != apiAnswerList_1 && 0 < apiAnswerList_1.size()) {
                if (lblQuestion_7_Option_1.getTag().toString().equals(apiAnswerList_1.get(0))) {
                    lblQuestion_7_Option_1.setBackgroundColor(getResources().getColor(R.color.specimen_green));
                } else if (lblQuestion_7_Option_2.getTag().toString().equals(apiAnswerList_1.get(0))) {
                    lblQuestion_7_Option_2.setBackgroundColor(getResources().getColor(R.color.specimen_green));
                } else if (lblQuestion_7_Option_3.getTag().toString().equals(apiAnswerList_1.get(0))) {
                    lblQuestion_7_Option_3.setBackgroundColor(getResources().getColor(R.color.specimen_green));
                } else if (lblQuestion_7_Option_4.getTag().toString().equals(apiAnswerList_1.get(0))) {
                    lblQuestion_7_Option_4.setBackgroundColor(getResources().getColor(R.color.specimen_green));
                } else if (lblQuestion_7_Option_5.getTag().toString().equals(apiAnswerList_1.get(0))) {
                    lblQuestion_7_Option_5.setBackgroundColor(getResources().getColor(R.color.specimen_green));
                }
                isQuestion7Answered = true;
                areQuestion7And8Answered();
            }
        }
    }

    void setSurveyQuestionHeader_2(View view) {
        // set question 8
        ((TextView) view.findViewById(R.id.lblSurveyHeader2)).setText(questions_2.getQuestion());
    }

    void setQuestionsDesc(View view) {
        // set option values
        ((TextView) view.findViewById(R.id.lbl_option_1_desc)).setText(questions_2.getOptions().get(0).getDesc());
        ((TextView) view.findViewById(R.id.lbl_option_2_desc)).setText(questions_2.getOptions().get(1).getDesc());
    }

    void setQuestion_8_OptionValues(View view) {
        TextView lblQuestion_8_Option_1 = (TextView) view.findViewById(R.id.lbl_option_1_value);
        TextView lblQuestion_8_Option_2 = (TextView) view.findViewById(R.id.lbl_option_2_value);

        lblQuestion_8_Option_1.setText(questions_2.getOptions().get(0).getValue());
        lblQuestion_8_Option_2.setText(questions_2.getOptions().get(1).getValue());

    }

    void setOptionImages(View view) {
        metrics = getActivity().getResources().getDisplayMetrics();

        imgOption_1 = (ImageView) view.findViewById(R.id.imgOption1);
        imgOption_2 = (ImageView) view.findViewById(R.id.imgOption2);

        Picasso.with(getActivity()).
//                load(ImageUtils.getMediumImage(questions_2.getOptions().get(0).getImage(), metrics))
        load(questions_2.getOptions().get(0).getImage()).resize(metrics.widthPixels, metrics.heightPixels)
//                .placeholder(R.drawable.survey_product_placeholder)
                .placeholder(R.drawable.survey_img_placeholder)
                .resize(300, 300)
                .centerInside()
                .into(imgOption_1);

        Picasso.with(getActivity()).
//                load(ImageUtils.getMediumImage(questions_2.getOptions().get(1).getImage(), metrics))
        load(questions_2.getOptions().get(1).getImage()).resize(metrics.widthPixels, metrics.heightPixels)
//                .placeholder(R.drawable.survey_product_placeholder)
                .placeholder(R.drawable.survey_img_placeholder)
                .resize(300, 300)
                .centerInside()
                .into(imgOption_2);

        imgOption_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity) getActivity()).saveAnswers(questions_2.getId(), v.getTag().toString());
                isQuestion8Answered = true;
                areQuestion7And8Answered();
            }
        });

        imgOption_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyActivity) getActivity()).saveAnswers(questions_2.getId(), v.getTag().toString());
                isQuestion8Answered = true;
                areQuestion7And8Answered();
            }
        });

        imgOption_1.setTag(questions_2.getOptions().get(0).getId());
        imgOption_2.setTag(questions_2.getOptions().get(1).getId());
    }

    void setQuestion_8_Answer() {
        imgOption_1.setAlpha(1.0f);
        imgOption_2.setAlpha(1.0f);

        if (null != userSelectedAnswerList && 0 < userSelectedAnswerList.size()) {
            if (imgOption_1.getTag().toString().equals(userSelectedAnswerList.get(0))) {
                setOptionOverlayesViewVisibility(view_option_1);
            } else if (imgOption_2.getTag().toString().equals(userSelectedAnswerList.get(0))) {
                setOptionOverlayesViewVisibility(view_option_2);
            }
        } else {
            if (null != apiAnswerList_2 && 0 < apiAnswerList_2.size()) {
                if (imgOption_1.getTag().toString().equals(apiAnswerList_2.get(0))) {
                    setOptionOverlayesViewVisibility(view_option_1);
                } else if (imgOption_2.getTag().toString().equals(apiAnswerList_2.get(0))) {
                    setOptionOverlayesViewVisibility(view_option_2);
                }
                isQuestion8Answered = true;
                areQuestion7And8Answered();
            }
        }
    }

    // Question 5
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.lblQuestion_7_Option_1:
                    ((SurveyActivity) getActivity()).saveAnswers(questions_1.getId(), v.getTag().toString());
                    isQuestion7Answered = true;
                    areQuestion7And8Answered();
                    break;

                case R.id.lblQuestion_7_Option_2:
                    ((SurveyActivity) getActivity()).saveAnswers(questions_1.getId(), v.getTag().toString());
                    isQuestion7Answered = true;
                    areQuestion7And8Answered();
                    break;

                case R.id.lblQuestion_7_Option_3:
                    ((SurveyActivity) getActivity()).saveAnswers(questions_1.getId(), v.getTag().toString());
                    isQuestion7Answered = true;
                    areQuestion7And8Answered();
                    break;

                case R.id.lblQuestion_7_Option_4:
                    ((SurveyActivity) getActivity()).saveAnswers(questions_1.getId(), v.getTag().toString());
                    isQuestion7Answered = true;
                    areQuestion7And8Answered();
                    break;

                case R.id.lblQuestion_7_Option_5:
                    ((SurveyActivity) getActivity()).saveAnswers(questions_1.getId(), v.getTag().toString());
                    isQuestion7Answered = true;
                    areQuestion7And8Answered();
                    break;
            }
        }
    };

    void areQuestion7And8Answered() {
        if (isQuestion7Answered && isQuestion8Answered) {
            userSelectedAnswerList = ((SurveyActivity) getActivity()).getSelectedAnswer(questions_1.getId());
            setQuestion_7_Answer();
            userSelectedAnswerList = ((SurveyActivity) getActivity()).getSelectedAnswer(questions_2.getId());
            setQuestion_8_Answer();
            mISurveyFragmentChanger.changeSurveyFragment(SURVEY_QUESTION_7_AND_8);
        } else {
            userSelectedAnswerList = ((SurveyActivity) getActivity()).getSelectedAnswer(questions_1.getId());
            setQuestion_7_Answer();
            userSelectedAnswerList = ((SurveyActivity) getActivity()).getSelectedAnswer(questions_2.getId());
            setQuestion_8_Answer();
        }
    }

}
