package com.specimen.survey;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.specimen.R;
import com.specimen.core.model.Answer;
import com.specimen.core.model.SurveyOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 29/7/15.
 */
public class SurveyColorAdapter extends ArrayAdapter<SurveyOptions> {

    Context context;
    LayoutInflater inflater;
    List<SurveyOptions> colorOptionsHex;
    int resourceLayout;
    Map<String, String> mSelectColorMap;
    ISurveyColorSelection mISurveyColorSelection;

    private int mItemHeight = 0;
    private int mNumColumns = 0;
    private RelativeLayout.LayoutParams mImageViewLayoutParams;

    public interface ISurveyColorSelection {
        void getSelectedColor(Map<String, String> selectColorMap);
    }

    public SurveyColorAdapter(Context mContext, int resource, ArrayList<SurveyOptions> objects,
                              ISurveyColorSelection iSurveyColorSelection) {
        super(mContext, resource, objects);
        this.context = mContext;
        this.resourceLayout = resource;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.colorOptionsHex = objects;
        this.mSelectColorMap = new HashMap<>(20);
        this.mISurveyColorSelection = iSurveyColorSelection;
        this.mImageViewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);

    }

    @Override
    public SurveyOptions getItem(int position) {
        return colorOptionsHex.get(position);
    }

    @Override
    public int getCount() {
        return colorOptionsHex.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SurveyColorHolder surveyColorHolder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceLayout, null);
            surveyColorHolder = new SurveyColorHolder();
            surveyColorHolder.colorView = (ImageView) convertView.findViewById(R.id.imgSurveyColor);
         /*   surveyColorHolder.colorView.setLayoutParams(mImageViewLayoutParams);
            // Check the height matches our calculated column width
            if (surveyColorHolder.colorView.getLayoutParams().height != mItemHeight) {
                surveyColorHolder.colorView.setLayoutParams(mImageViewLayoutParams);
            }*/
            convertView.setTag(surveyColorHolder);

        } else {
            surveyColorHolder = (SurveyColorHolder) convertView.getTag();
        }

        if (!TextUtils.isEmpty(colorOptionsHex.get(position).getDesc())) {
            surveyColorHolder.colorView.
                    setBackgroundColor(Color.parseColor(colorOptionsHex.get(position).getDesc().trim()));

            if (isColorSelected(colorOptionsHex.get(position).getId()))
                surveyColorHolder.colorView.setImageResource(R.drawable.select_clr_arrow);
            else
                surveyColorHolder.colorView.setImageResource(R.drawable.select_transparent);

        } else {
            surveyColorHolder.colorView.setImageResource(R.drawable.logo);
            surveyColorHolder.colorView
                    .setBackgroundColor(surveyColorHolder.colorView.getResources()
                            .getColor(R.color.specimen_green));
        }

        selectMultiColorOptions(surveyColorHolder, colorOptionsHex.get(position).getId());

        return convertView;
    }

    // set numcols
    public void setNumColumns(int numColumns) {
        mNumColumns = numColumns;
    }

    public int getNumColumns() {
        return mNumColumns;
    }

    // set photo item height
    public void setItemHeight(int height) {
        if (height == mItemHeight) {
            return;
        }
        mItemHeight = height;
        mImageViewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, mItemHeight);
        notifyDataSetChanged();
    }

    void selectMultiColorOptions(final SurveyColorHolder surveyColorHolder, final String optionId) {
        surveyColorHolder.colorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isColorSelected(optionId)) {
                    mSelectColorMap.remove(optionId);
                    notifyDataSetChanged();
                } else {
                    mSelectColorMap.put(optionId, optionId);
                    notifyDataSetChanged();
                }

                mISurveyColorSelection.getSelectedColor(mSelectColorMap);
            }
        });
    }

    boolean isColorSelected(String optionId) {
        for (Object o : mSelectColorMap.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            if (null != pair && null != pair.getValue() && pair.getValue().toString().equals(optionId)) {
                return true;
            }
        }

        return false;
    }

    final static class SurveyColorHolder {
        ImageView colorView;
    }

    public void setPreviousAnswer(String colorAnswer) {
        mSelectColorMap.put(colorAnswer, colorAnswer);
        mISurveyColorSelection.getSelectedColor(mSelectColorMap);
//        notifyDataSetChanged();
    }
}
