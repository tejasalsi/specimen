package com.specimen.survey;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.model.Answer;
import com.specimen.core.model.SurveyOptions;
import com.specimen.core.model.Questions;
import com.specimen.core.response.APIResponse;
import com.specimen.core.survey.ISurveyFacade;
import com.specimen.core.survey.SurveyFacade;
import com.specimen.survey.SurveyColorAdapter.ISurveyColorSelection;
import com.specimen.survey.callback.ISurveyFragmentChanger;
import com.specimen.util.ExpandableHeightGridView;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by mohanish on 3/7/15.
 */
public class SurveyQuestion9ColorSelectFragment extends BaseFragment implements ISurveyColorSelection {

    ISurveyFragmentChanger mISurveyFragmentChanger;

    final int SURVEY_QUESTION_9 = 7;

    Questions questions;
    Map<String, String> selectColorMap;
    ExpandableHeightGridView gridView;
    SurveyColorAdapter surveyColorAdapter;

    List<String> apiAnswerList;

    private int mPhotoSize;

    public SurveyQuestion9ColorSelectFragment(ISurveyFragmentChanger iSurveyFragmentChanger) {
        this.mISurveyFragmentChanger = iSurveyFragmentChanger;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_9_color_select, container, false);

        // get the photo size and spacing
        mPhotoSize = getResources().getDimensionPixelSize(R.dimen.photo_size);

        selectColorMap = new Hashtable<>();
        questions = ((SurveyActivity) getActivity()).getQuestionsModel(SURVEY_QUESTION_9 + 1);

        setSurveyQuestionHeader(view);
        setColorOptions(view);

        view.findViewById(R.id.btnEndSurvey).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != selectColorMap && 0 < selectColorMap.size() && selectColorMap.size() > 2) {
                    ((SurveyActivity) getActivity()).saveAnswers(questions.getId(), createCommaSeparatedString());
                    mISurveyFragmentChanger.changeSurveyFragment(SURVEY_QUESTION_9);
                } else {
                    Toast.makeText(getActivity(), "Please select at least 3 colors.", Toast.LENGTH_LONG).show();
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((SurveyActivity) getActivity()).visibleProgressbar(false);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    void setSurveyQuestionHeader(View view) {
        // set question 1
        ((TextView) view.findViewById(R.id.lblSurveyHeader1)).setText(questions.getQuestion());
    }

    void setColorOptions(View view) {

        gridView = (ExpandableHeightGridView) view.findViewById(R.id.gridColor);
        gridView.setExpanded(true);
        gridView.setNumColumns(5);
        gridView.setGravity(Gravity.CENTER_HORIZONTAL);
        gridView.setVerticalSpacing(0);
        gridView.setHorizontalSpacing(0);
        surveyColorAdapter = new SurveyColorAdapter(getActivity(),
                R.layout.survey_color_view, (ArrayList<SurveyOptions>) questions.getOptions(), this);

        gridView.setAdapter(surveyColorAdapter);

/*
        final LinearLayout linearGrid = (LinearLayout) view.findViewById(R.id.linearGrid);
        // get the view tree observer of the grid and set the height and numcols dynamically
        gridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (surveyColorAdapter.getNumColumns() == 0) {
                    final int numColumns = (int) Math.floor(gridView.getWidth() / (mPhotoSize));
                    if (numColumns > 0) {
//                        final int columnWidth = (gridView.getWidth() / numColumns);
                        surveyColorAdapter.setNumColumns(numColumns);
                        surveyColorAdapter.setItemHeight((linearGrid.getHeight() / 5));
                        Log.i("$$$$$$$$$$$$$$$$$$$",""+linearGrid.getHeight());
                    }
                }
            }
        });
*/

        if (null != questions.getAnswer() && 0 < questions.getAnswer().size()) {
            apiAnswerList = new ArrayList<>(questions.getAnswer().size());
            apiAnswerList.addAll(questions.getAnswer());

            for (String colorAnswer : apiAnswerList) {
                surveyColorAdapter.setPreviousAnswer(colorAnswer);
//            surveyColorAdapter.setPreviousAnswer(apiAnswerList.get(0));
                surveyColorAdapter.notifyDataSetChanged();
//            surveyColorAdapter.setPreviousAnswer(apiAnswerList.get(1));

            }


//            surveyColorAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void getSelectedColor(Map<String, String> selectColorMap) {
        this.selectColorMap = selectColorMap;
    }

    String createCommaSeparatedString() {
        String commaSeparatedString = selectColorMap.values().toString();
        commaSeparatedString = commaSeparatedString.replace("[", "");
        commaSeparatedString = commaSeparatedString.replace("]", "");
        return commaSeparatedString;
    }

}
