package com.specimen.survey;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.specimen.R;

/**
 * Created by root on 20/7/15.
 */
public class SurveySkipFragment extends DialogFragment {

    public static SurveySkipFragment newInstance() {
        return new SurveySkipFragment();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Translucent_NoTitleBar);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_skip_survey, container, false);
        TextView textView5 = (TextView) view.findViewById(R.id.textView5);
        view.findViewById(R.id.btnContinueSurvey).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dismiss();
            }
        });

        view.findViewById(R.id.btnEnterStore).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((SurveyActivity) getActivity()).postAnswerList();
            }
        });

        view.findViewById(R.id.imgClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

//        View lineView = view.findViewById(R.id.viewline);
//        lineView.setLayoutParams(
//                new RelativeLayout.LayoutParams(textView5.getWidth(), 1));
//        lineView.setBackgroundColor(getResources().getColor(R.color.specimen_green));
        return view;
    }
}
