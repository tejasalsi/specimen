package com.specimen.survey;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.specimen.BaseFragment;
import com.specimen.R;
import com.specimen.core.model.Answer;
import com.specimen.core.model.ImageEntity;
import com.specimen.core.model.Questions;
import com.specimen.survey.callback.ISurveyFragmentChanger;
import com.specimen.util.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 19/7/15.
 */
public class SurveyQuestion3Fragment extends BaseFragment {

    DisplayMetrics metrics;
    ISurveyFragmentChanger mISurveyFragmentChanger;

    ImageView imgOption_1, imgOption_2, imgOption_3, imgOption_4;
    View view_option_1, view_option_2, view_option_3, view_option_4;

    final int SURVEY_QUESTION_3 = 3;

    Questions questions;
    List<String> apiAnswerList;
    List<String> userSelectedAnswerList;

    public SurveyQuestion3Fragment(ISurveyFragmentChanger iSurveyFragmentChanger) {
        this.mISurveyFragmentChanger = iSurveyFragmentChanger;
    }

    @Override
    public void onResume() {
        super.onResume();
        userSelectedAnswerList = ((SurveyActivity) getActivity()).getSelectedAnswer(questions.getId());
        setAnswer();
        ((SurveyActivity) getActivity()).visibleProgressbar(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_survey_question_1_2_3_4, container, false);

        questions = ((SurveyActivity) getActivity()).getQuestionsModel(SURVEY_QUESTION_3 - 1);
        apiAnswerList = new ArrayList<>(1);
        apiAnswerList = questions.getAnswer();
        userSelectedAnswerList = new ArrayList<>(1);

        setOptionOverlayesView(view);
        setSurveyQuestionHeader(view);
        setOptionsValue(view);
        setOptionImages(view);
        setQuestionsDesc(view);


        return view;
    }

    void setOptionOverlayesView(View view) {
        view_option_1 = view.findViewById(R.id.view_option_1);
        view_option_2 = view.findViewById(R.id.view_option_2);
        view_option_3 = view.findViewById(R.id.view_option_3);
        view_option_4 = view.findViewById(R.id.view_option_4);

        view_option_1.setVisibility(View.GONE);
        view_option_2.setVisibility(View.GONE);
        view_option_3.setVisibility(View.GONE);
        view_option_4.setVisibility(View.GONE);
    }

    void setOptionOverlayesViewVisibility(View view) {
        view_option_1.setVisibility(View.GONE);
        view_option_2.setVisibility(View.GONE);
        view_option_3.setVisibility(View.GONE);
        view_option_4.setVisibility(View.GONE);

        view.setVisibility(View.VISIBLE);
    }

    void setSurveyQuestionHeader(View view) {
        // set question
        ((TextView) view.findViewById(R.id.lblSurveyHeader1)).setText(questions.getQuestion());

    }

    void setAnswer() {
        if (null != userSelectedAnswerList && 0 < userSelectedAnswerList.size()) {
            if (imgOption_1.getTag().toString().equals(userSelectedAnswerList.get(0))) {
                setOptionOverlayesViewVisibility(view_option_1);
            } else if (imgOption_2.getTag().toString().equals(userSelectedAnswerList.get(0))) {
                setOptionOverlayesViewVisibility(view_option_2);
            } else if (imgOption_3.getTag().toString().equals(userSelectedAnswerList.get(0))) {
                setOptionOverlayesViewVisibility(view_option_3);
            } else if (imgOption_4.getTag().toString().equals(userSelectedAnswerList.get(0))) {
                setOptionOverlayesViewVisibility(view_option_4);
            }
        } else {
            if (null != apiAnswerList && 0 < apiAnswerList.size()) {
                if (imgOption_1.getTag().toString().equals(apiAnswerList.get(0))) {
                    setOptionOverlayesViewVisibility(view_option_1);
                } else if (imgOption_2.getTag().toString().equals(apiAnswerList.get(0))) {
                    setOptionOverlayesViewVisibility(view_option_2);
                } else if (imgOption_3.getTag().toString().equals(apiAnswerList.get(0))) {
                    setOptionOverlayesViewVisibility(view_option_3);
                } else if (imgOption_4.getTag().toString().equals(apiAnswerList.get(0))) {
                    setOptionOverlayesViewVisibility(view_option_4);
                }
            }
        }
    }

    void setQuestionsDesc(View view) {
        // set option values
        String[] str_option_1_desc = questions.getOptions().get(0).getDesc().split("\\|");
        String[] str_option_2_desc = questions.getOptions().get(1).getDesc().split("\\|");
        String[] str_option_3_desc = questions.getOptions().get(2).getDesc().split("\\|");
        String[] str_option_4_desc = questions.getOptions().get(3).getDesc().split("\\|");

        ((TextView) view.findViewById(R.id.lbl_option_1_desc)).setText(str_option_1_desc[1].trim());
        ((TextView) view.findViewById(R.id.lbl_option_2_desc)).setText(str_option_2_desc[1].trim());
        ((TextView) view.findViewById(R.id.lbl_option_3_desc)).setText(str_option_3_desc[1].trim());
        ((TextView) view.findViewById(R.id.lbl_option_4_desc)).setText(str_option_4_desc[1].trim());

        TextView lbl_option_1_extra_text = (TextView)view.findViewById(R.id.lbl_option_1_extra_text);
        TextView lbl_option_2_extra_text = (TextView)view.findViewById(R.id.lbl_option_2_extra_text);
        TextView lbl_option_3_extra_text = (TextView)view.findViewById(R.id.lbl_option_3_extra_text);
        TextView lbl_option_4_extra_text = (TextView)view.findViewById(R.id.lbl_option_4_extra_text);

        lbl_option_1_extra_text.setVisibility(View.VISIBLE);
        lbl_option_2_extra_text.setVisibility(View.VISIBLE);
        lbl_option_3_extra_text.setVisibility(View.VISIBLE);
        lbl_option_4_extra_text.setVisibility(View.VISIBLE);

        ((TextView) view.findViewById(R.id.lbl_option_1_extra_text)).setText(str_option_1_desc[0]);
        ((TextView) view.findViewById(R.id.lbl_option_2_extra_text)).setText(str_option_2_desc[0]);
        ((TextView) view.findViewById(R.id.lbl_option_3_extra_text)).setText(str_option_3_desc[0]);
        ((TextView) view.findViewById(R.id.lbl_option_4_extra_text)).setText(str_option_4_desc[0]);
    }

    void setOptionsValue(View view) {
        // set option values
        ((TextView) view.findViewById(R.id.lbl_option_1_value)).setText(questions.getOptions().get(0).getValue());
        ((TextView) view.findViewById(R.id.lbl_option_2_value)).setText(questions.getOptions().get(1).getValue());
        ((TextView) view.findViewById(R.id.lbl_option_3_value)).setText(questions.getOptions().get(2).getValue());
        ((TextView) view.findViewById(R.id.lbl_option_4_value)).setText(questions.getOptions().get(3).getValue());
    }

    void setOptionImages(View view) {
        metrics = getActivity().getResources().getDisplayMetrics();

        imgOption_1 = (ImageView) view.findViewById(R.id.imgOption1);
        imgOption_2 = (ImageView) view.findViewById(R.id.imgOption2);
        imgOption_3 = (ImageView) view.findViewById(R.id.imgOption3);
        imgOption_4 = (ImageView) view.findViewById(R.id.imgOption4);

        imgOption_1.setTag(questions.getOptions().get(0).getId());
        imgOption_2.setTag(questions.getOptions().get(1).getId());
        imgOption_3.setTag(questions.getOptions().get(2).getId());
        imgOption_4.setTag(questions.getOptions().get(3).getId());

        setPicassoImage(imgOption_1, questions.getOptions().get(0).getImage());
        setPicassoImage(imgOption_2, questions.getOptions().get(1).getImage());
        setPicassoImage(imgOption_3, questions.getOptions().get(2).getImage());
        setPicassoImage(imgOption_4, questions.getOptions().get(3).getImage());

        imgOption_1.setOnClickListener(onClickListener);
        imgOption_2.setOnClickListener(onClickListener);
        imgOption_3.setOnClickListener(onClickListener);
        imgOption_4.setOnClickListener(onClickListener);

    }

    //    void setPicassoImage(ImageView imageView, ImageEntity imageEntity) {
    void setPicassoImage(ImageView imageView, String imageEntity) {
        Picasso.with(getActivity()).
//                load(ImageUtils.getProductImageUrl(imageEntity, metrics))
        load(imageEntity).resize(metrics.widthPixels, metrics.heightPixels)
//                .placeholder(R.drawable.survey_product_placeholder)
                .placeholder(R.drawable.survey_img_placeholder)
                .resize(300, 300)
                .centerInside()
                .into(imageView);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((SurveyActivity) getActivity()).saveAnswers(questions.getId(), v.getTag().toString());
            userSelectedAnswerList = ((SurveyActivity) getActivity()).getSelectedAnswer(questions.getId());
            setAnswer();
            mISurveyFragmentChanger.changeSurveyFragment(SURVEY_QUESTION_3);
        }
    };


}
