package com.specimen.survey;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.specimen.BaseActivity;
import com.specimen.MainActivity;
import com.specimen.R;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.model.IsSurveyCompleteResponse;
import com.specimen.core.model.Questions;
import com.specimen.core.model.SurveyResponseData;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.SurveyResponse;
import com.specimen.core.survey.ISurveyFacade;
import com.specimen.survey.callback.ISurveyFragmentChanger;
import com.specimen.widget.ProgressView;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

@SuppressWarnings("ALL")
public class SurveyActivity extends BaseActivity implements ISurveyFragmentChanger, IResponseSubscribe {

    final String TAG = "SURVEY";
    final String SURVEY_SUBMIT_TAG = "SURVEY_SUBMIT";
    ProgressBar pbSurvey;
    final int SURVEY_FRAGMENT_COUNT = 8;
    int fragmentCount;

    List<Fragment> mFragmentList;
    public SurveyResponseData surveyResponseData;
    public Map<String, List<String>> mSurveyAnswersMap;
    boolean areAllAnswersGiven;
    ProgressView mProgressView;
    TextView lbl_skip_survey;
    boolean isSurveyComplete, isNavigationFromPreviousAnswers;
    String mBundleTagValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        mSurveyAnswersMap = new Hashtable<>();
        mFragmentList = new ArrayList<>();

        initUI();
        getToolbar();

        ((ISurveyFacade) IOCContainer.getInstance().getObject(ServiceName.SURVEY_SERVICE, TAG)).getSurveyData();

        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            mBundleTagValue = bundle.getString("TAG");
//            if (mBundleTagValue.equals("ProfileActivity")) {
//                boolean isSurveyComplete = bundle.getBoolean("SURVEY_COMPLETE", false);
//                if (isSurveyComplete) {
////                    finishSurvey();
//                    ((ISurveyFacade) IOCContainer.getInstance().getObject(ServiceName.SURVEY_SERVICE, TAG)).getSurveyData();
//                } else {
//                    ((ISurveyFacade) IOCContainer.getInstance().getObject(ServiceName.SURVEY_SERVICE, TAG)).getSurveyData();
//                }
//            }
        }
//        else {
//            ((ISurveyFacade) IOCContainer.getInstance().getObject(ServiceName.SURVEY_SERVICE, TAG)).isSurveyCompleted();
//        }

    }

    void initUI() {
        mFragmentList.add(new SurveyStartupFragment(this));
        mFragmentList.add(new SurveyQuestion1Fragment(this));
        mFragmentList.add(new SurveyQuestion2Fragment(this));
        mFragmentList.add(new SurveyQuestion3Fragment(this));
        mFragmentList.add(new SurveyQuestion4Fragment(this));
        mFragmentList.add(new SurveyQuestion5And6Fragment(this));
        mFragmentList.add(new SurveyQuestion7And8Fragment(this));
        mFragmentList.add(new SurveyQuestion9ColorSelectFragment(this));

        mProgressView = (ProgressView) findViewById(R.id.progressView);
        mProgressView.setVisibility(View.VISIBLE);

        pbSurvey = (ProgressBar) findViewById(R.id.pbSurvey);
        pbSurvey.setMax(8);

        lbl_skip_survey = (TextView) findViewById(R.id.lblSkipSurvey);
        lbl_skip_survey.setTypeface(null, Typeface.BOLD);
        SpannableString content = new SpannableString(lbl_skip_survey.getText());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        lbl_skip_survey.setText(content);
        lbl_skip_survey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = SurveySkipFragment.newInstance();
                dialogFragment.show(getSupportFragmentManager().beginTransaction(), "");
            }
        });
        progressBarSetup();
        getToolbar();
    }

    void progressBarSetup() {
        final float[] roundedCorners = new float[]{5, 5, 5, 5, 5, 5, 5, 5};
        ShapeDrawable pgDrawable = new ShapeDrawable(new RoundRectShape(roundedCorners, null, null));
        pgDrawable.getPaint().setColor(getResources().getColor(R.color.specimen_green));
        ClipDrawable progress1 = new ClipDrawable(pgDrawable, Gravity.LEFT, ClipDrawable.HORIZONTAL);
        pbSurvey.setProgressDrawable(progress1);
        pbSurvey.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.progress_horizontal));
    }

    public Toolbar getToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn, null));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_btn));
        }
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.survey_title));
//        if(!TextUtils.isEmpty(mBundleTagValue) && mBundleTagValue.equals("ProfileActivity")) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        }
        return toolbar;
    }

    @Override
    public void onBackPressed() {
        Fragment frag = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (frag instanceof SurveyEndFragment || getSupportFragmentManager().getBackStackEntryCount() == 0
                || frag instanceof SurveyStartupFragment) {
            if(!TextUtils.isEmpty(mBundleTagValue)) {
                finish();
            } else {
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }
        }
//        else if (isNavigationFromPreviousAnswers) {
//            replaceFragments(mFragmentList.get(fragmentCount));
//            fragmentCount = (fragmentCount - 1);
//            if (0 == fragmentCount || fragmentCount == (-1)) fragmentCount = 0;
//    }

        else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    public void visibleProgressbar(boolean flag) {
        if (flag) {
            lbl_skip_survey.setVisibility(View.VISIBLE);
            pbSurvey.setVisibility(View.VISIBLE);
            setProgress();
        } else {
            lbl_skip_survey.setVisibility(View.GONE);
            pbSurvey.setVisibility(View.GONE);
        }
    }

    void replaceFragments(Fragment newFragment) {
        switchFragment(newFragment, true, R.id.fragment_container, R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
    }

    public Questions getQuestionsModel(int position) {
        return surveyResponseData.getQuestions().get(position);
    }

    public Map<String, List<String>> getSurveyAnswerMap() {
        return mSurveyAnswersMap;
    }

    public List<String> getSelectedAnswer(String questionId) {
        return mSurveyAnswersMap.get(questionId);
    }

    public void saveAnswers(String questionId, String optionsId) {
        List<String> listAnswer = new ArrayList<>();
        listAnswer.add(optionsId);
        mSurveyAnswersMap.put(questionId, listAnswer);
    }

    public int getTotalAnswers() {
        return mSurveyAnswersMap.size();
    }

    public List<String> getAnswers(String optionsId) {
        return mSurveyAnswersMap.get(optionsId);
    }

    void setPreviousAnswers() {
        int questDataSize = surveyResponseData.getQuestions().size();

        if (null != mSurveyAnswersMap) {
            visibleProgressbar(true);

            for (int i = 0; i < questDataSize; i++) {
                if (null != surveyResponseData.getQuestions().get(i).getAnswer() && 0 < surveyResponseData.getQuestions().get(i).getAnswer().size()) {
                    isNavigationFromPreviousAnswers = true;
                    fragmentCount = i;
                    mSurveyAnswersMap.put(surveyResponseData.getQuestions().get(i).getId(),
                            surveyResponseData.getQuestions().get(i).getAnswer());
                }
            }

//            if (0 == mSurveyAnswersMap.size()) {
            replaceFragments(mFragmentList.get(0));
            visibleProgressbar(false);
//            } else if (questDataSize == mSurveyAnswersMap.size()) {
//                Log.d(TAG, "FINISHING SURVEY");
////                finishSurvey();
//                setProgress();
////                replaceFragments(mFragmentList.get(mSurveyAnswersMap.size()));
//                replaceFragments(mFragmentList.get(0));
//            } else {
//                setProgress();
////                replaceFragments(mFragmentList.get(mSurveyAnswersMap.size()));
//                replaceFragments(mFragmentList.get(0));
//            }
        }
    }

    void finishSurvey() {
        mSurveyAnswersMap.clear();
        mFragmentList.clear();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
        replaceFragments(new SurveyEndFragment(this));

    }

    void postAnswerList() {
        mProgressView.setVisibility(View.VISIBLE);
        ((ISurveyFacade) IOCContainer.getInstance().getObject(ServiceName.SURVEY_SERVICE, SURVEY_SUBMIT_TAG)).submitSurveyData(mSurveyAnswersMap);
    }

    @Override
    public void changeSurveyFragment(int fragmentReplaceWith) {
        fragmentCount = fragmentReplaceWith;
        if (SURVEY_FRAGMENT_COUNT == (fragmentReplaceWith + 1)) {
            areAllAnswersGiven = true;
            postAnswerList();
        } else {
            if (fragmentReplaceWith + 1 == SURVEY_FRAGMENT_COUNT - 1) {
                visibleProgressbar(false);
            } else {
                visibleProgressbar(true);
            }
            replaceFragments(mFragmentList.get(fragmentReplaceWith + 1));
            setProgress();
        }
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        mProgressView.setVisibility(View.GONE);
        if (response instanceof IsSurveyCompleteResponse) {
            surveyContinueResponse(response, tag);
        } else if (response instanceof SurveyResponse) {
            surveyCompleteCheckResponse(response, tag);
        }
//        else if (response instanceof SurveyStyleProfileResponseData) {
//            finishSurvey();
//        }
    }

    @Override
    public void onFailure(Exception error) {
        super.onFailure(error);
        mProgressView.setVisibility(View.GONE);
    }

    void surveyContinueResponse(APIResponse response, String tag) {
        mProgressView.setVisibility(View.VISIBLE);
        if (null != response && ((IsSurveyCompleteResponse) response).getIs_survey_completed().equals("true")) {
            if (isSurveyComplete) {
                finishSurvey();
            } else {
                startActivity(new Intent(SurveyActivity.this, MainActivity.class));
                finish();
            }
        } else {
            ((ISurveyFacade) IOCContainer.getInstance().getObject(ServiceName.SURVEY_SERVICE, TAG)).getSurveyData();
        }
    }

    void surveyCompleteCheckResponse(APIResponse response, String tag) {
        if (tag.equals(TAG)) {
            surveyResponseData = ((SurveyResponse) response).getData();
            setPreviousAnswers();
        } else if (tag.equals(SURVEY_SUBMIT_TAG)) {
            if (areAllAnswersGiven) {
                Toast.makeText(SurveyActivity.this, "Thanks for taking survey.", Toast.LENGTH_SHORT).show();
                finishSurvey();
            } else {
                startActivity(new Intent(this, MainActivity.class));
                finish();
            }
        }
    }

    void setProgress() {
        int progress = 0;
        if (null != mSurveyAnswersMap && mSurveyAnswersMap.size() == 9) {
            progress = mSurveyAnswersMap.size() - 1;
        } else {
            progress = getTotalAnswers();
        }
        pbSurvey.setProgress(progress);
    }
}