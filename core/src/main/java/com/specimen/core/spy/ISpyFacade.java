package com.specimen.core.spy;

import com.specimen.core.IBaseFacade;
import com.specimen.core.model.PageRequest;

/**
 * Created by Intelliswift on 7/10/2015.
 */
public interface ISpyFacade extends IBaseFacade {

    void setSpied(String productId);//,PageRequest pageRequest);

    void removeSpied(String productId);

    void spiedProductList();

}
