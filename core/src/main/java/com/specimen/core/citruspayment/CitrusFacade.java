package com.specimen.core.citruspayment;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.deal.IDealFacade;
import com.specimen.core.model.CreateOrderResponse;
import com.specimen.core.request.CitrusRequestBuilder;
import com.specimen.core.request.DealRequestBuilder;
import com.specimen.core.response.CitrusPaymentResponse;
import com.specimen.core.response.DealResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by root on 3/10/15.
 */
public class CitrusFacade extends BaseFacade implements ICitrusFacade {

    private CitrusRequestBuilder citrusRequestBuilder;

    public CitrusFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        citrusRequestBuilder = new CitrusRequestBuilder();
    }

    @Override
    public void createOrder(String shipping_address_id, String paymentMethod) {
        HashMap<String, Object> createOrder = getRequestBody("createOrder");
        createOrder.put("shipping_address_id",shipping_address_id);
        createOrder.put("payment_method",paymentMethod);

        citrusRequestBuilder.getService().citrusPaymentCOD(createOrder, new Callback<CreateOrderResponse>() {
            @Override
            public void success(CreateOrderResponse citrusPaymentResponse, Response response) {
                mResponseSubscribe.onSuccess(citrusPaymentResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void paymentByDebitCard() {

    }

    @Override
    public void paymentByCreditCard() {

    }
}

