package com.specimen.core.askastylist;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.request.AskStylistRequestBuilder;
import com.specimen.core.response.AskStylistImagesData;
import com.specimen.core.response.AskStylistResponse;

import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by root on 5/9/15.
 */
public class AskAStylistFacade extends BaseFacade implements IAskAStylistFacade {

    private AskStylistRequestBuilder askStylistRequestBuilder;

    public AskAStylistFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        askStylistRequestBuilder = new AskStylistRequestBuilder();
    }

    @Override
    public void askastylistOccasion() {
        askStylistRequestBuilder.getService().askastylistOccasion(getRequestBody("askastylistOccasion"), new Callback<AskStylistResponse>() {
            /**
             * Successful HTTP response.
             *
             * @param askStylistResponse
             * @param response
             */
            @Override
            public void success(AskStylistResponse askStylistResponse, Response response) {
                mResponseSubscribe.onSuccess(askStylistResponse, TAG);
            }

            /**
             * Unsuccessful HTTP response due to network failure, non-2XX status code, or unexpected
             * exception.
             *
             * @param error
             */
            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void askastylist(String occassion, List<AskStylistImagesData.AskStylistImagesEntity> stylistImagesDataList, String question, String size) {

        HashMap<String, Object> bodyParameter = getRequestBody("askastylist");
        bodyParameter.put("question", question);
        bodyParameter.put("occassion", occassion);
        bodyParameter.put("size", size);
        bodyParameter.put("images", stylistImagesDataList);

        askStylistRequestBuilder.getService().askastylist(bodyParameter, new Callback<AskStylistResponse>() {
            /**
             * Successful HTTP response.
             *
             * @param askStylistResponse
             * @param response
             */
            @Override
            public void success(AskStylistResponse askStylistResponse, Response response) {
                mResponseSubscribe.onSuccess(askStylistResponse, TAG);
            }

            /**
             * Unsuccessful HTTP response due to network failure, non-2XX status code, or unexpected
             * exception.
             *
             * @param error
             */
            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }
}
