package com.specimen.core.application;

public interface IApplicationPersistence {


    String getOfflineUser();

    void saveUserResponse(Object response);

    String getOfflineResponse(String key);



    void setOfflineResponse(String key, String value);
}