package com.specimen.core.productdetails;

import com.specimen.core.IBaseFacade;

/**
 * Created by root on 6/8/15.
 */
public interface IProductDetailsFacade extends IBaseFacade {

    void getProductDetailsData(String productId);
}
