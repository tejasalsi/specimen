package com.specimen.core.survey;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.Answer;
import com.specimen.core.model.IsSurveyCompleteResponse;
import com.specimen.core.model.SubmitSurveyQuery;
import com.specimen.core.model.SurveyStyleProfileResponseData;
import com.specimen.core.request.SurveyRequestBuilder;
import com.specimen.core.response.SurveyResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by root on 26/7/15.
 */
public class SurveyFacade extends BaseFacade implements ISurveyFacade {

    private SurveyRequestBuilder surveyRequestBuilder;

    public SurveyFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        surveyRequestBuilder = new SurveyRequestBuilder();
    }

    @Override
    public void getSurveyData() {
        surveyRequestBuilder.getService().getSurveyQuestion(getRequestBody("getSurveyQuestion"), new Callback<SurveyResponse>() {
            /**
             * Successful HTTP response.
             *
             * @param surveyResponse
             * @param response
             */
            @Override
            public void success(SurveyResponse surveyResponse, Response response) {
                mResponseSubscribe.onSuccess(surveyResponse, TAG);
            }

            /**
             * Unsuccessful HTTP response due to network failure, non-2XX status code, or unexpected
             * exception.
             *
             * @param error
             */
            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void submitSurveyData(Map<String, List<String>> surveyAnswersMap) {
        HashMap<String,Object> objectHashMap = getRequestBody("submitSurvey");

        SubmitSurveyQuery submitSurveyQuery = new SubmitSurveyQuery();
        submitSurveyQuery.convertMapInList(surveyAnswersMap);

        objectHashMap.put("survey", submitSurveyQuery.getSurvey());

        surveyRequestBuilder.getService().submitSurvey(objectHashMap, new Callback<SurveyResponse>() {
            /**
             * Successful HTTP response.
             *
             * @param surveyResponse
             * @param response
             */
            @Override
            public void success(SurveyResponse surveyResponse, Response response) {
                mResponseSubscribe.onSuccess(surveyResponse, TAG);
            }

            /**
             * Unsuccessful HTTP response due to network failure, non-2XX status code, or unexpected
             * exception.
             *
             * @param error
             */
            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void getstyleprofiledata() {
        // Todo: temporary. remove this hashmap object
        HashMap<String,Object> objectHashMap = getRequestBody("getstyleprofiledata");
        objectHashMap.put("customer_id", objectHashMap.get("cust_id"));

        surveyRequestBuilder.getService().getstyleprofiledata(objectHashMap, new Callback<SurveyStyleProfileResponseData>() {
            /**
             * Successful HTTP response.
             *
             * @param surveyResponse
             * @param response
             */
            @Override
            public void success(SurveyStyleProfileResponseData surveyResponse, Response response) {
                mResponseSubscribe.onSuccess(surveyResponse, TAG);
            }

            /**
             * Unsuccessful HTTP response due to network failure, non-2XX status code, or unexpected
             * exception.
             *
             * @param error
             */
            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void isSurveyCompleted() {
        surveyRequestBuilder.getService().isSurveyCompleted(getRequestBody("isSurveyCompleted"), new Callback<IsSurveyCompleteResponse>() {
            /**
             * Successful HTTP response.
             *
             * @param surveyResponse
             * @param response
             */
            @Override
            public void success(IsSurveyCompleteResponse surveyResponse, Response response) {
                mResponseSubscribe.onSuccess(surveyResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

}