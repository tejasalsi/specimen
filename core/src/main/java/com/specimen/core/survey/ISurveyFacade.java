package com.specimen.core.survey;

import com.specimen.core.IBaseFacade;
import com.specimen.core.model.Answer;

import java.util.List;
import java.util.Map;

/**
 * Created by root on 26/7/15.
 */
public interface ISurveyFacade extends IBaseFacade {
    void getSurveyData();

    void submitSurveyData(Map<String, List<String>> mSurveyAnswersMap);

    void getstyleprofiledata();

    void isSurveyCompleted();
}
