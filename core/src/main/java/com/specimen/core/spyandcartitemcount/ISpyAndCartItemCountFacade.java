package com.specimen.core.spyandcartitemcount;

import com.specimen.core.IBaseFacade;

/**
 * Created by DhirajK on 11/28/2015.
 */
public interface ISpyAndCartItemCountFacade extends IBaseFacade {

    void getTotalSpyCartCount();
}
