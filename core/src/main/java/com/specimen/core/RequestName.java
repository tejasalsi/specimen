package com.specimen.core;

/**
 * Created by Intelliswift on 7/31/2015.
 */
public enum RequestName {
    WITH_TOP_CATEGORY_FETCH, WITH_SUBCATEGORY_FETCH
}
