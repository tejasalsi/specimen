package com.specimen.core.feedback;

import com.specimen.core.IBaseFacade;

/**
 * Created by Swapnil on 10/20/2015.
 */
public interface IFeedbackFacade extends IBaseFacade{

    void shareRating(String productId, String comment, String fabric, String fit, String style, String tailoring);
}
