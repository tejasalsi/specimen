package com.specimen.core.feedback;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.FeedbackRatings;
import com.specimen.core.request.FeedbackRequestBuilder;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.DealResponse;

import java.util.HashMap;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Swapnil on 10/20/2015.
 */
public class FeedbackFacade extends BaseFacade implements IFeedbackFacade {

    private FeedbackRequestBuilder feedbackRequestBuilder;

    public FeedbackFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        feedbackRequestBuilder = new FeedbackRequestBuilder();
    }

    @Override
    public void shareRating(String productId, String comment, String fabric, String fit, String style, String tailoring) {
        HashMap<String, Object> map = getRequestBody("shareFeedback");
        map.put("product_id",productId);
        map.put("comments",comment);
        FeedbackRatings feedbackRatings = new FeedbackRatings(fabric,fit,style,tailoring);
        map.put("ratings", feedbackRatings);

        feedbackRequestBuilder.getService().shareFeedback(map, new Callback<APIResponse>() {
            @Override
            public void success(APIResponse dealResponse, Response response) {
                mResponseSubscribe.onSuccess(dealResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }
}
