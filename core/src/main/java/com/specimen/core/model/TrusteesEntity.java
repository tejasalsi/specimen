package com.specimen.core.model;

import com.google.gson.annotations.Expose;

public class TrusteesEntity {
    /**
     * trustee_id : 31
     * trustee_name : Jitu
     * trustee_email : null
     * trustee_mobileno : 097 73 407267
     */

    private String trustee_id;
    private String trustee_name;
    private String trustee_email;
    private String trustee_mobileno;

    private String feedback_given;

    public void setTrustee_id(String trustee_id) {
        this.trustee_id = trustee_id;
    }

    public void setTrustee_name(String trustee_name) {
        this.trustee_name = trustee_name;
    }

    public void setTrustee_email(String trustee_email) {
        this.trustee_email = trustee_email;
    }

    public void setTrustee_mobileno(String trustee_mobileno) {
        this.trustee_mobileno = trustee_mobileno;
    }

    public String getTrustee_id() {
        return trustee_id;
    }

    public String getTrustee_name() {
        return trustee_name;
    }

    public String getTrustee_email() {
        return trustee_email;
    }

    public String getTrustee_mobileno() {
        return trustee_mobileno;
    }


    public String getFeedback_given() {
        return feedback_given;
    }

    public void setFeedback_given(String feedback_given) {
        this.feedback_given = feedback_given;
    }

}