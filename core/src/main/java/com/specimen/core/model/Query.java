package com.specimen.core.model;

import com.specimen.core.RequestName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Intelliswift on 7/9/2015.
 */
public class Query implements Cloneable {

    private static Query qurInstance;
    private final String TAG = "tag", SEARCH_STRING = "searchString", CATEGORY_ID = "categoryId", SORT_BY = "sorter";
    private Query backUpInstance;
    private HashMap<String, ArrayList<String>> queryParameter;
    private String mSort_by;
    private String topCategoryId;
    private String subCategoryId;

    private Query() {

        queryParameter = new HashMap<>();
    }

    public static Query getInstance() {

        if (qurInstance == null) {
            qurInstance = new Query();
        }

        return qurInstance;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getTopCategoryId() {
        return topCategoryId;
    }

    public void setTopCategoryId(String topCategoryId) {
        this.topCategoryId = topCategoryId;
    }

    public String getCategoryId() {

        if (queryParameter.get(CATEGORY_ID) != null && queryParameter.get(CATEGORY_ID).size() > 0) {
            return queryParameter.get(CATEGORY_ID).get(0);
        }
        return null;
    }

    public void setCategoryId(String categoryId) {

        /**
         *  Considered as no multiple category ID for single query.
         */

        ArrayList<String> arrayList = queryParameter.get(CATEGORY_ID);
        if (arrayList != null) {
            arrayList.clear();
        } else {
            arrayList = new ArrayList<>();
        }

        if (categoryId != null) {
            arrayList.add(0, categoryId);
        }


//        arrayList.clear();
//        arrayList.add(0, categoryId);
        queryParameter.put(CATEGORY_ID, arrayList);
    }

    public String getSearchString() {
        if (queryParameter.get(SEARCH_STRING) != null && queryParameter.get(SEARCH_STRING).size() > 0) {
            return queryParameter.get(SEARCH_STRING).get(0);
        }
        return null;
    }

    public void setSearchString(String searchString) {
        ArrayList<String> arrayList = queryParameter.get(SEARCH_STRING);
        if (arrayList != null) {
            arrayList.clear();
        } else {
            arrayList = new ArrayList<>();
        }
        arrayList.add(0, searchString);
        queryParameter.put(SEARCH_STRING, arrayList);
    }

    public ArrayList<String> getTag() {
        return getValues(TAG);
    }

    public void setTag(String tag) {
        setValues(TAG, tag);
    }

    public ArrayList<String> getValues(String key) {
        return queryParameter.get(key);
    }

    public void setValues(String key, String value) {
        ArrayList<String> arrayList = queryParameter.get(key);
        if (arrayList == null) {
            arrayList = new ArrayList<>();
        }
        arrayList.add(value);
        queryParameter.put(key, arrayList);
    }

    public void totalResetQuery() {
        resetFilter();
        mSort_by = null;
        topCategoryId = null;
        subCategoryId = null;
    }

    public void resetFilter() {
        if (queryParameter != null)
            queryParameter.clear();
    }

    public void resetSorter() {
        mSort_by = null;
    }

    public ArrayList<String> getCollections() {
        return getValues(TAG);
    }

    public void setCollection(String collectionId) {
        setValues(TAG, collectionId);
    }

    public ArrayList<String> getExplore() {
        return getValues(TAG);
    }

    public void setExplore(String exploreId) {
        setValues(TAG, exploreId);
    }

    private void makeRequest(HashMap queryParameterMap) {
        Set<String> keys = queryParameter.keySet();

        if (keys != null && keys.size() > 0) {
            ArrayList<Filter> _queryParameterList = new ArrayList<>();
            for (String key : keys) {
                if (queryParameter.get(key) == null) {
                    continue;
                }
                Filter filter = new Filter();
                filter.setAttr_code(key);
                filter.setAttr_value(queryParameter.get(key));
                _queryParameterList.add(filter);
            }
            queryParameterMap.put("filter", _queryParameterList);
        }
        if (mSort_by != null) {
            queryParameterMap.put(SORT_BY, mSort_by);
        }
    }

    /**
     * @param requestName Request method name and accordingly add parameter
     * @return return arrayList of filters
     */

    public void addRequestParameter(RequestName requestName, HashMap queryParameterMap) {

        //TODO: Need to configure conditional code segment to requestName

        switch (requestName) {
            case WITH_TOP_CATEGORY_FETCH:
                setCategoryId(topCategoryId);
                makeRequest(queryParameterMap);
//                setCategoryId("");
                break;
            case WITH_SUBCATEGORY_FETCH:
                setCategoryId(subCategoryId);
                makeRequest(queryParameterMap);
//                setCategoryId("");
                break;
        }


    }

    public String getSorter() {
        return mSort_by;
    }

    public void setSorter(String sort_by) {
        this.mSort_by = sort_by;
    }

    public void removeValue(String key, String value) {
        ArrayList<String> arrayList = queryParameter.get(key);
        if (arrayList == null) {
            return;
        }
        arrayList.remove(value);
        if (arrayList.size() == 0) {
            queryParameter.remove(key);
        } else {
            queryParameter.put(key, arrayList);
        }
    }

    public void backupQuery() {
        try {
            backUpInstance = (Query) qurInstance.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    public void clearBackup() {
        backUpInstance = null;
    }

    public void initaliseWithBackup() {
        qurInstance = backUpInstance;
    }

    @Override
    protected final Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
