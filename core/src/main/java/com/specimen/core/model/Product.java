package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Product implements Parcelable {

    private String id;

    private boolean isSpied;
    private List<ImageEntity> images;
    private int spied_count;
    private List<ProductSizesEntity> sizes;
    private List<ProductSizesEntity> size;
    private String price;
    private String name;
    private String specialprice;

    private boolean feedback;

    private String color;//, qauntity; //use in ordersInfo

    private int qauntity;
    int quantity; // used in order module
    private List<Attribute> attribute;

    public boolean getFeedback() {
        return feedback;
    }

    public void setFeedback(boolean feedback) {
        this.feedback = feedback;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

/*    public int getQauntity() {
        return qauntity;
    }*/

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

 /*   public void setQauntity(int qauntity) {
        this.qauntity = qauntity;
    }*/

    public List<Attribute> getAttribute() {
        return attribute;
    }

    public void setAttribute(List<Attribute> attribute) {
        this.attribute = attribute;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getIsSpied() {
        return isSpied;
    }

    public void setIsSpied(boolean isSpied) {
        this.isSpied = isSpied;
    }

    public List<ImageEntity> getImage() {
        return images;
    }

    public void setImage(List<ImageEntity> image) {
        this.images = image;
    }

    public int getSpied_count() {
        return spied_count;
    }

    public void setSpied_count(int spied_count) {
        this.spied_count = spied_count;
    }

    ///////////////////////////////// CART /////////////////////////////////////

    public List<ProductSizesEntity> getSizes() {
        return sizes;
    }

    public void setSizes(List<ProductSizesEntity> sizes) {
        this.sizes = sizes;
    }

    ////////////////////////////////// SPY //////////////////////////////////////

    public List<ProductSizesEntity> getSize() {
        return size;
    }

    public void setSize(List<ProductSizesEntity> size) {
        this.size = size;
    }

    /////////////////////////////////////////////////////////////////////////////

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialprice() {
        return specialprice;
    }

    public void setSpecialprice(String specialprice) {
        this.specialprice = specialprice;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeByte(isSpied ? (byte) 1 : (byte) 0);
        dest.writeTypedList(images);
        dest.writeInt(this.spied_count);
        dest.writeTypedList(sizes);
        dest.writeTypedList(size);
        dest.writeString(this.price);
        dest.writeString(this.name);
        dest.writeString(this.specialprice);
    }

    public Product() {
    }

    class Attribute {
        String label, value;
    }

    protected Product(Parcel in) {
        this.id = in.readString();
        this.isSpied = in.readByte() != 0;
        this.images = in.createTypedArrayList(ImageEntity.CREATOR);
        this.spied_count = in.readInt();
        this.sizes = in.createTypedArrayList(ProductSizesEntity.CREATOR);
        this.size = in.createTypedArrayList(ProductSizesEntity.CREATOR);
        this.price = in.readString();
        this.name = in.readString();
        this.specialprice = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}