package com.specimen.core.model;

import com.google.gson.annotations.Expose;

public class CartProductSize {

    @Expose
    private String lable;
    @Expose
    private String value;

    @Expose
    private String stock_count;

    /**
     * @return The lable
     */
    public String getLable() {
        return lable;
    }

    /**
     * @param lable The lable
     */
    public void setLable(String lable) {
        this.lable = lable;
    }

    /**
     * @return The value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    public String getStock_count() {
        return stock_count;
    }

    public void setStock_count(String stock_count) {
        this.stock_count = stock_count;
    }


}