package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jitendra Khetle on 08/07/15.
 */
public class ImageSize implements Parcelable {
    /**
     * sizeName : Small
     * actualWidth : 31
     * width : 32
     * actualHeight : 40
     * url : http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg
     * height : 40
     */
    private String sizeName;
    private int actualWidth;
    private int width;
    private int actualHeight;
    private String url;
    private int height;

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public int getActualWidth() {
        return actualWidth;
    }

    public void setActualWidth(int actualWidth) {
        this.actualWidth = actualWidth;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getActualHeight() {
        return actualHeight;
    }

    public void setActualHeight(int actualHeight) {
        this.actualHeight = actualHeight;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    protected ImageSize(Parcel in) {
        sizeName = in.readString();
        actualWidth = in.readInt();
        width = in.readInt();
        actualHeight = in.readInt();
        url = in.readString();
        height = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sizeName);
        dest.writeInt(actualWidth);
        dest.writeInt(width);
        dest.writeInt(actualHeight);
        dest.writeString(url);
        dest.writeInt(height);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ImageSize> CREATOR = new Parcelable.Creator<ImageSize>() {
        @Override
        public ImageSize createFromParcel(Parcel in) {
            return new ImageSize(in);
        }

        @Override
        public ImageSize[] newArray(int size) {
            return new ImageSize[size];
        }
    };
}
