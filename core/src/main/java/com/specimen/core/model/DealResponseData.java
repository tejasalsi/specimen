package com.specimen.core.model;

import java.util.List;

/**
 * Created by root on 19/8/15.
 */
public class DealResponseData {

    private List<DealsEntity> deals;

    public void setDeals(List<DealsEntity> deals) {
        this.deals = deals;
    }

    public List<DealsEntity> getDeals() {
        return deals;
    }
}
