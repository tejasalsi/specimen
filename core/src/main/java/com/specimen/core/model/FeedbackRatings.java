package com.specimen.core.model;

/**
 * Created by Swapnil on 10/20/2015.
 */
public class FeedbackRatings {

    /**
     * fabric : 29
     * fit : 16
     * style : 32
     * tailoring : 24
     */

    private String fabric;
    private String fit;
    private String style;

    public FeedbackRatings(String fabric, String fit, String style, String tailoring) {
        this.fabric = fabric;
        this.fit = fit;
        this.style = style;
        this.tailoring = tailoring;
    }

    private String tailoring;

    public void setFabric(String fabric) {
        this.fabric = fabric;
    }

    public void setFit(String fit) {
        this.fit = fit;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public void setTailoring(String tailoring) {
        this.tailoring = tailoring;
    }

    public String getFabric() {
        return fabric;
    }

    public String getFit() {
        return fit;
    }

    public String getStyle() {
        return style;
    }

    public String getTailoring() {
        return tailoring;
    }
}
