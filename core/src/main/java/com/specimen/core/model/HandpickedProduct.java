package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swapnil on 8/4/2015.
 */
@SuppressWarnings("ALL")
public class HandpickedProduct implements Parcelable {

    private String id;

    private String isSpied;
    private List<ImageEntity> images;
    private int spied_count;
    private List<ProductSizesEntity> sizes;
    private String price;
    private String name;
    private String specialprice;
    private String attribute_id;

    public String getAttribute_id() {
        return attribute_id;
    }

    public void setAttribute_id(String attribute_id) {
        this.attribute_id = attribute_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsSpied() {
        return isSpied;
    }

    public void setIsSpied(String isSpied) {
        this.isSpied = isSpied;
    }

    public List<ImageEntity> getImage() {
        return images;
    }

    public void setImage(List<ImageEntity> image) {
        this.images = image;
    }

    public int getSpied_count() {
        return spied_count;
    }

    public void setSpied_count(int spied_count) {
        this.spied_count = spied_count;
    }

    public List<ProductSizesEntity> getSizes() {
        return sizes;
    }

    public void setSizes(List<ProductSizesEntity> sizes) {
        this.sizes = sizes;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialprice() {
        return specialprice;
    }

    public void setSpecialprice(String specialprice) {
        this.specialprice = specialprice;
    }



    protected HandpickedProduct(Parcel in) {
        id = in.readString();
        isSpied = in.readString();
        images = (List<ImageEntity>) in.readValue(ImageEntity.class.getClassLoader());
        spied_count = in.readInt();
        if (in.readByte() == 0x01) {
            sizes = new ArrayList<>();
            in.readList(sizes, ProductSizesEntity.class.getClassLoader());
        } else {
            sizes = null;
        }
        price = in.readString();
        name = in.readString();
        specialprice = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(isSpied);
        dest.writeValue(images);
        dest.writeInt(spied_count);
        if (sizes == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(sizes);
        }
        dest.writeString(price);
        dest.writeString(name);
        dest.writeString(specialprice);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<HandpickedProduct> CREATOR = new Parcelable.Creator<HandpickedProduct>() {
        @Override
        public HandpickedProduct createFromParcel(Parcel in) {
            return new HandpickedProduct(in);
        }

        @Override
        public HandpickedProduct[] newArray(int size) {
            return new HandpickedProduct[size];
        }
    };
}