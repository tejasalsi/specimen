package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Intelliswift on 7/8/2015.
 */
public class Category implements Parcelable {


    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
    private ImageEntity image;
    private String name;
    private int id;

    protected Category(Parcel in) {
        image = (ImageEntity) in.readValue(ImageEntity.class.getClassLoader());
        name = in.readString();
        id = in.readInt();
    }

    public ImageEntity getImage() {
        return image;
    }

    public void setImage(ImageEntity image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(image);
        dest.writeString(name);
        dest.writeInt(id);
    }
}