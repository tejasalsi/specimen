package com.specimen.core.model;

import java.util.List;

public class FavoriteResponseData {

    private List<Product> products;
    private List<Look> looks;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Look> getLooks() {
        return looks;
    }

    public void setLooks(List<Look> looks) {
        this.looks = looks;
    }
}