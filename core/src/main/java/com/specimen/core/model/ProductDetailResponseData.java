package com.specimen.core.model;

import java.util.List;

public class ProductDetailResponseData {

    private String notes;
    private List<ImageEntity> images;
    private String spied_count;
    private String color;
    private Rating rating;
    private String feedback_count;
    private String description;
    private Look look;
    private String isSpied;
    private Product identical;
    private List<ProductSizesEntity> sizes;
    private String specialprice;
    private String low_price;
    private String high_price;
    private String current_price;
    private String price;
    private String qty;
    private String name;
    private String details;
    private String id;
    private String view_count;

    private String sku;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String note) {
        this.notes = note;
    }

    public List<ImageEntity> getImages() {
        return images;
    }

    public void setImages(List<ImageEntity> images) {
        this.images = images;
    }

    public String getSpied_count() {
        return spied_count;
    }

    public void setSpied_count(String spied_count) {
        this.spied_count = spied_count;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public String getFeeabackCount() {
        return feedback_count;
    }

    public void setFeedbackCount(String feedbackCount) {
        this.feedback_count = feedbackCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Look getLook() {
        return look;
    }

    public void setLook(Look look) {
        this.look = look;
    }

    public String getIsSpied() {
        return isSpied;
    }

    public void setIsSpied(String isSpied) {
        this.isSpied = isSpied;
    }

    public Product getIdentical() {
        return identical;
    }

    public void setIdentical(Product identical) {
        this.identical = identical;
    }

    public List<ProductSizesEntity> getSizes() {
        return sizes;
    }

    public void setSizes(List<ProductSizesEntity> sizes) {
        this.sizes = sizes;
    }

    public String getCurrent_price() {
        return current_price;
    }

    public void setCurrent_price(String current_price) {
        this.current_price = current_price;
    }

    public String getSpecialprice() {
        return specialprice;
    }

    public void setSpecialprice(String specialprice) {
        this.specialprice = specialprice;
    }

    public String getLow_price() {
        return low_price;
    }

    public void setLow_price(String low_price) {
        this.low_price = low_price;
    }

    public String getHigh_price() {
        return high_price;
    }

    public void setHigh_price(String high_price) {
        this.high_price = high_price;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getView_count() {
        return view_count;
    }

    public void setView_count(String view_count) {
        this.view_count = view_count;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }


}