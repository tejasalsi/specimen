package com.specimen.core.model;

/**
 * Created by Swapnil on 8/19/2015.
 */
public class CartMetadata {



    private String trunk_total;
    private String total_tax;
    private String trunk_discount;
    private String total;
    private String coupon_code;
    private String subtotal;
   // private String totalsavings;

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getTotal_tax() {
        return total_tax;
    }

    public void setTotal_tax(String total_tax) {
        this.total_tax = total_tax;
    }

    public String getTotal_discount() {
        return trunk_discount;
    }

    public void setTotal_discount(String total_discount) {
        this.trunk_discount = total_discount;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }


   /* public String getTotalSaving() {
        return totalsavings;
    }

    public void setTotalSaving(String totalSaving) {
        this.totalsavings = totalSaving;
    }*/

    public String getTrunk_total() {
        return trunk_total;
    }

    public void setTrunk_total(String trunk_total) {
        this.trunk_total = trunk_total;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

}
