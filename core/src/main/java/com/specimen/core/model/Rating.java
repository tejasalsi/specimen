package com.specimen.core.model;

public class Rating {

            private int fit;
            private int tailoring;
            private int fabric;
            private int style;

            public void setFit(int fit) {
                this.fit = fit;
            }

            public void setTailoring(int tailoring) {
                this.tailoring = tailoring;
            }

            public void setFabric(int fabric) {
                this.fabric = fabric;
            }

            public void setStyle(int style) {
                this.style = style;
            }

            public int getFit() {
                return fit;
            }

            public int getTailoring() {
                return tailoring;
            }

            public int getFabric() {
                return fabric;
            }

            public int getStyle() {
                return style;
            }
        }