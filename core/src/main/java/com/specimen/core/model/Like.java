package com.specimen.core.model;

import java.util.HashMap;
import java.util.Map;

public class Like {

    private String name;
    private String like;
    private String id;

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The like
     */
    public String getLike() {
        return like;
    }

    /**
     * @param like The like
     */
    public void setLike(String like) {
        this.like = like;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }


}