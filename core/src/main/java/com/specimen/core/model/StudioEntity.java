package com.specimen.core.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class StudioEntity {
    /**
     * image : {"Small":{"sizeName":"Small","actualWidth":31,"width":32,"actualHeight":40,"url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":40},"XLarge":{"sizeName":"XLarge","actualWidth":264,"width":328,"actualHeight":344,"url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":410},"Medium":{"sizeName":"Medium","actualWidth":107,"width":112,"actualHeight":140,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","height":140},"Large":{"sizeName":"Large","actualWidth":157,"width":164,"actualHeight":205,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","height":205},"IPhoneSmall":{"sizeName":"IPhoneSmall","actualWidth":96,"width":100,"actualHeight":125,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":125},"Best":{"sizeName":"Best","actualWidth":264,"width":720,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","height":900},"Original":{"sizeName":"Original","actualWidth":264,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg"},"IPhone":{"sizeName":"IPhone","actualWidth":264,"width":288,"actualHeight":344,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":360}}
     * is_offer : false
     * id : 11
     * timeslots : [{"id":"10-11"},{"id":"11-12"}]
     */
//    private ImageEntity image;
//    private String is_offer;
    private String id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    //    private String title;
    private String image;

//    public void setImage(ImageEntity image) {
//        this.image = image;
//    }

//    public void setIs_offer(String is_offer) {
//        this.is_offer = is_offer;
//    }

    public void setId(String id) {
        this.id = id;
    }

//    public ImageEntity getImage() {
//        return image;
//    }

//    public String isIs_offer() {
//        return is_offer;
//    }

    public String getId() {
        return id;
    }

//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }


    ////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////

//    /**
//     * image : {"Small":{"sizeName":"Small","actualWidth":31,"width":32,"actualHeight":40,"url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":40},"XLarge":{"sizeName":"XLarge","actualWidth":264,"width":328,"actualHeight":344,"url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":410},"Medium":{"sizeName":"Medium","actualWidth":107,"width":112,"actualHeight":140,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","height":140},"Large":{"sizeName":"Large","actualWidth":157,"width":164,"actualHeight":205,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","height":205},"IPhoneSmall":{"sizeName":"IPhoneSmall","actualWidth":96,"width":100,"actualHeight":125,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":125},"Best":{"sizeName":"Best","actualWidth":264,"width":720,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","height":900},"Original":{"sizeName":"Original","actualWidth":264,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg"},"IPhone":{"sizeName":"IPhone","actualWidth":264,"width":288,"actualHeight":344,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":360}}
//     * id : 11
//*///    private ImageEntity image;
//    private String id;


//    public ImageEntity getImage() {
//        return image;
//    }
//
//    public void setImage(ImageEntity image) {
//        this.image = image;
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }



}