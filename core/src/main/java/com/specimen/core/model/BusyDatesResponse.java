package com.specimen.core.model;

import com.specimen.core.response.APIResponse;

public class BusyDatesResponse extends APIResponse {

    private BusyDatesData data;
//    private String status;

    /**
     * @return The data
     */
    public BusyDatesData getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(BusyDatesData data) {
        this.data = data;
    }

//    /**
//     * @return The status
//     */
//    public String getStatus() {
//        return status;
//    }
//
//    /**
//     * @param status The status
//     */
//    public void setStatus(String status) {
//        this.status = status;
//    }

}

