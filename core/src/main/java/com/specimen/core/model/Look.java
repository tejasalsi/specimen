package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Look implements Parcelable {

    /**
     * isSpied : true
     * image : {"Small":{"sizeName":"Small","actualWidth":31,"width":32,"actualHeight":40,"url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":40},"XLarge":{"sizeName":"XLarge","actualWidth":264,"width":328,"actualHeight":344,"url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":410},"Medium":{"sizeName":"Medium","actualWidth":107,"width":112,"actualHeight":140,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","height":140},"Large":{"sizeName":"Large","actualWidth":157,"width":164,"actualHeight":205,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","height":205},"IPhoneSmall":{"sizeName":"IPhoneSmall","actualWidth":96,"width":100,"actualHeight":125,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":125},"Best":{"sizeName":"Best","actualWidth":264,"width":720,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","height":900},"Original":{"sizeName":"Original","actualWidth":264,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg"},"IPhone":{"sizeName":"IPhone","actualWidth":264,"width":288,"actualHeight":344,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":360}}
     * spied_count : 222
     * name : Fernbuckles washed-chinos
     * id : 36
     * view_count : 2020
     */
    private boolean isSpied;
    private List<ImageEntity> images;
    private int spied_count;
    private String name;
    private String id;
    private int view_count;
    private String description;

    public Look() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsSpied() {
        return isSpied;
    }

    public void setIsSpied(boolean isSpied) {
        this.isSpied = isSpied;
    }

    public List<ImageEntity> getImage() {
        return images;
    }

    public void setImage(List<ImageEntity> image) {
        this.images = image;
    }

    public int getSpied_count() {
        return spied_count;
    }

    public void setSpied_count(int spied_count) {
        this.spied_count = spied_count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(isSpied ? (byte) 1 : (byte) 0);
        dest.writeTypedList(images);
        dest.writeInt(this.spied_count);
        dest.writeString(this.name);
        dest.writeString(this.id);
        dest.writeInt(this.view_count);
    }

    protected Look(Parcel in) {
        this.isSpied = in.readByte() != 0;
        this.images = in.createTypedArrayList(ImageEntity.CREATOR);
        this.spied_count = in.readInt();
        this.name = in.readString();
        this.id = in.readString();
        this.view_count = in.readInt();
    }

    public static final Parcelable.Creator<Look> CREATOR = new Parcelable.Creator<Look>() {
        public Look createFromParcel(Parcel source) {
            return new Look(source);
        }

        public Look[] newArray(int size) {
            return new Look[size];
        }
    };
}