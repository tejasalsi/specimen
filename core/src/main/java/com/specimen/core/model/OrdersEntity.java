package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
public class OrdersEntity implements Parcelable {

    private String orderId;
    private String orderNo;
    private String orderStatus;
    private String orderTotalValue;
    private String orderDate;
    private List<Product> orderItems;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderTotalValue() {
        return orderTotalValue;
    }

    public void setOrderTotalValue(String orderTotalValue) {
        this.orderTotalValue = orderTotalValue;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public List<Product> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<Product> orderItems) {
        this.orderItems = orderItems;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.orderId);
        dest.writeString(this.orderStatus);
        dest.writeString(this.orderTotalValue);
        dest.writeString(this.orderDate);
        dest.writeString(this.orderNo);
        dest.writeTypedList(orderItems);
    }

    public OrdersEntity() {
    }

    protected OrdersEntity(Parcel in) {
        this.orderId = in.readString();
        this.orderStatus = in.readString();
        this.orderTotalValue = in.readString();
        this.orderDate = in.readString();
        this.orderNo = in.readString();
        this.orderItems = in.createTypedArrayList(Product.CREATOR);
    }

    public static final Parcelable.Creator<OrdersEntity> CREATOR = new Parcelable.Creator<OrdersEntity>() {
        public OrdersEntity createFromParcel(Parcel source) {
            return new OrdersEntity(source);
        }

        public OrdersEntity[] newArray(int size) {
            return new OrdersEntity[size];
        }
    };

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}