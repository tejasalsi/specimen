package com.specimen.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WishedProductList {

    @SerializedName("product_id")
    @Expose
    private List<String> productId = new ArrayList<>();

    /**
     * @return The productId
     */
    public List<String> getProductId() {
        return productId;
    }

    /**
     * @param productId The product_id
     */
    public void setProductId(List<String> productId) {
        this.productId = productId;
    }

}