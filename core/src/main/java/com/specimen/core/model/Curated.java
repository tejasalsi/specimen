package com.specimen.core.model;

public class Curated {

        /**
         * id : 48
         * name : Smart Casual
         * product_count : 6
         */

        private String id;
        private String name;
        private int product_count;
    /**
     * description : These are smart casuals
     */

    private String description;

    public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setCount(int count) {
            this.product_count = count;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getCount() {
            return product_count;
        }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}