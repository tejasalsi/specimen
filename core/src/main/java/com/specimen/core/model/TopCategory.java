package com.specimen.core.model;

import android.os.Parcel;

import java.util.List;

public class TopCategory extends Category {

    //    private ImageEntity image;
    private List<Explore> explore;
    //    private String name;
//    private int id;
    private List<Collection> collection;
    private List<Subcategory> subcategory;

    protected TopCategory(Parcel in) {
        super(in);
    }

//    public ImageEntity getImage() {
//        return image;
//    }
//
//    public void setImage(ImageEntity image) {
//        this.image = image;
//    }

    public List<Explore> getExplore() {
        return explore;
    }

    public void setExplore(List<Explore> explore) {
        this.explore = explore;
    }

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }

    public List<Collection> getCollection() {
        return collection;
    }

    public void setCollection(List<Collection> collection) {
        this.collection = collection;
    }

    public List<Subcategory> getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(List<Subcategory> subcategory) {
        this.subcategory = subcategory;
    }


}