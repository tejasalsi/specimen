package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductSizesEntity implements Parcelable {
    private int stock_count;
    private String label;
    //    private String id;
    private String attr_option_id;
    private String value;
    private String child_product_id;
    private String attribute_id;
    private String size_chart;

    public String getSize_chart() {
        return size_chart;
    }

    public void setSize_chart(String size_chart) {
        this.size_chart = size_chart;
    }

    public String getAttribute_id() {
        return attribute_id;
    }

    public void setAttribute_id(String attribute_id) {
        this.attribute_id = attribute_id;
    }

    public String getChild_product_id() {
        return child_product_id;
    }

    public void setChild_product_id(String child_product_id) {
        this.child_product_id = child_product_id;
    }

    public int getStock_count() {
        return stock_count;
    }

    public void setStock_count(int stock_count) {
        this.stock_count = stock_count;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getId() {
        return attr_option_id;
    }

    public void setId(String id) {
        this.attr_option_id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    protected ProductSizesEntity(Parcel in) {
        stock_count = in.readInt();
        label = in.readString();
        attr_option_id = in.readString();
        value = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(stock_count);
        dest.writeString(label);
        dest.writeString(attr_option_id);
        dest.writeString(value);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ProductSizesEntity> CREATOR = new Parcelable.Creator<ProductSizesEntity>() {
        @Override
        public ProductSizesEntity createFromParcel(Parcel in) {
            return new ProductSizesEntity(in);
        }

        @Override
        public ProductSizesEntity[] newArray(int size) {
            return new ProductSizesEntity[size];
        }
    };
}