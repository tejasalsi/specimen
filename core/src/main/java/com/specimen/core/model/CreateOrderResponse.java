package com.specimen.core.model;

import com.specimen.core.response.APIResponse;

public class CreateOrderResponse extends APIResponse{

    private CreateOrder data;

    /**
     * @return The data
     */
    public CreateOrder getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(CreateOrder data) {
        this.data = data;
    }


}