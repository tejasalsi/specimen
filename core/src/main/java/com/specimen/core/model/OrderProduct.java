package com.specimen.core.model;

/**
 * Created by Swapnil on 8/20/2015.
 */
public class OrderProduct extends Product{

    int quantity;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


}
