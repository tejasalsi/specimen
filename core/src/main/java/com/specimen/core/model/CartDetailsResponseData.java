package com.specimen.core.model;

import java.util.List;

public class CartDetailsResponseData {

    private List<CartProduct> products;
    private CartMetadata metadata;

    private int shoppingcart_count;

    private String productId;



    private String productName;
    private String stock;

//    public String getTotalValue() {
//        return totalValue;
//    }
//
//    public void setTotalValue(String totalValue) {
//        this.totalValue = totalValue;
//    }
//
//    public String getTotalTax() {
//        return totalTax;
//    }
//
//    public void setTotalTax(String totalTax) {
//        this.totalTax = totalTax;
//    }
//
//    public String getTotalDiscount() {
//        return totalDiscount;
//    }
//
//    public void setTotalDiscount(String totalDiscount) {
//        this.totalDiscount = totalDiscount;
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }

    public List<CartProduct> getProducts() {
        return products;
    }

    public void setProducts(List<CartProduct> products) {
        this.products = products;
    }

    public CartMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(CartMetadata metadata) {
        this.metadata = metadata;
    }

    public void setshoppingcart_count(int shoppingcart_count) {
        this.shoppingcart_count = shoppingcart_count;
    }

    public int getshoppingcart_count() {
        return shoppingcart_count;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

}