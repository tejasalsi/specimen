package com.specimen.core.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class ProductListResponseData {

@Expose
private List<Product> products = new ArrayList<>();
@Expose
private List<Filter> filters = new ArrayList<>();

/**
* 
* @return
* The products
*/
public List<Product> getProducts() {
return products;
}

/**
* 
* @param products
* The products
*/
public void setProducts(List<Product> products) {
this.products = products;
}

/**
* 
* @return
* The filters
*/
public List<Filter> getFilters() {
return filters;
}

/**
* 
* @param filters
* The filters
*/
public void setFilters(List<Filter> filters) {
this.filters = filters;
}

}