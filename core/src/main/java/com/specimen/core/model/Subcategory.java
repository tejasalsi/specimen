package com.specimen.core.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Intelliswift on 7/8/2015.
 */
public class Subcategory extends Category{
    protected Subcategory(Parcel in) {
        super(in);
    }
    public static final Parcelable.Creator<Subcategory> CREATOR = new Parcelable.Creator<Subcategory>() {
        @Override
        public Subcategory createFromParcel(Parcel in) {
            return new Subcategory(in);
        }

        @Override
        public Subcategory[] newArray(int size) {
            return new Subcategory[size];
        }
    };
}
