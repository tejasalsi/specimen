package com.specimen.core.home.handpicked;

import com.specimen.core.IBaseFacade;

/**
 * Created by Swapnil on 7/27/2015.
 */
public interface IHandpickedFacade  extends IBaseFacade {

    void getHandpickedProducts();

    void removeHandpickedProduct(String productId);

    void setSpied(String id);

    int y=5;
}
