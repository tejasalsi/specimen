package com.specimen.core.home.handpicked;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.HandpickedProductsResponse;
import com.specimen.core.model.HandpickedRemovedResponse;
import com.specimen.core.model.IsSurveyCompleteResponse;
import com.specimen.core.model.OnAddToSpiedResponse;
import com.specimen.core.request.HandpickedReuestBuilder;
import com.specimen.core.response.AddOrRemoveFromWishListResponse;
import com.specimen.core.response.FavoriteListResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Swapnil on 7/27/2015.
 */
public class HandpickedFacade extends BaseFacade implements IHandpickedFacade{

    private HandpickedReuestBuilder handpickedRequestBuilder;

    public HandpickedFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        handpickedRequestBuilder = new HandpickedReuestBuilder();
    }


    @Override
    public void getHandpickedProducts() {
        handpickedRequestBuilder.getService().getHandpickedProducts(getRequestBody("handpicked"), new Callback<HandpickedProductsResponse>() {
            @Override
            public void success(HandpickedProductsResponse handpickedProductsResponse, Response response) {
                mResponseSubscribe.onSuccess(handpickedProductsResponse,TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void removeHandpickedProduct(String productId) {
        HashMap<String, Object> map = getRequestBody("removeFromHandpicked");
        map.put("product_id", productId);
        handpickedRequestBuilder.getService().removeHandpickedProduct(map, new Callback<HandpickedRemovedResponse>() {
            @Override
            public void success(HandpickedRemovedResponse favoriteListResponse, Response response) {
                mResponseSubscribe.onSuccess(favoriteListResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void setSpied(String productId){//}, PageRequest pageRequest) {

        HashMap<String, Object> bodyParameter = getRequestBody("savetoWishlist");
        // pageRequest.addParameters(bodyParameter);
        bodyParameter.put("product_id", productId);//: 1
//        handpickedRequestBuilder.getService().setSpied(bodyParameter, new Callback<OnAddToSpiedResponse>() {
//            @Override
//            public void success(OnAddToSpiedResponse favoriteListResponse, Response response) {
//                mResponseSubscribe.onSuccess(favoriteListResponse, TAG);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                mResponseSubscribe.onFailure(error);
//            }
//        });

        handpickedRequestBuilder.getService().setSpied(bodyParameter, new Callback<AddOrRemoveFromWishListResponse>() {
            @Override
            public void success(AddOrRemoveFromWishListResponse favoriteListResponse, Response response) {
                mResponseSubscribe.onSuccess(favoriteListResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

}
