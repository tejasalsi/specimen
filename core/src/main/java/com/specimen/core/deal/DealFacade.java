package com.specimen.core.deal;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.request.DealRequestBuilder;
import com.specimen.core.response.DealResponse;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by root on 19/8/15.
 */
public class DealFacade extends BaseFacade implements IDealFacade {

    private DealRequestBuilder productDetailsRequestBuilder;

    public DealFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        productDetailsRequestBuilder = new DealRequestBuilder();
    }

    @Override
    public void deal() {
        productDetailsRequestBuilder.getService().deal(getRequestBody("deal"), new Callback<DealResponse>() {
            @Override
            public void success(DealResponse dealResponse, Response response) {
                mResponseSubscribe.onSuccess(dealResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }
}
