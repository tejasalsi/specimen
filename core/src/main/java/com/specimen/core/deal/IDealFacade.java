package com.specimen.core.deal;

import com.specimen.core.IBaseFacade;

/**
 * Created by root on 19/8/15.
 */
public interface IDealFacade extends IBaseFacade {

    void deal();
}

