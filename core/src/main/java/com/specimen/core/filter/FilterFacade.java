package com.specimen.core.filter;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.RequestName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.Query;
import com.specimen.core.request.FilterRequestBuilder;
import com.specimen.core.response.FilterResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Intelliswift on 7/10/2015.
 */
public class FilterFacade extends BaseFacade implements IFilterFacade {

    private FilterRequestBuilder filterRequestBuilder;

    public FilterFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        filterRequestBuilder = new FilterRequestBuilder();
    }

    @Override
    public void getFilter() {
        FilterResponse filterResponse = (FilterResponse) mApplicationFacade.getResponse(FilterResponse.class.getSimpleName());

        if (filterResponse != null) {
            mResponseSubscribe.onSuccess(filterResponse, TAG);
            return;
        }

        HashMap<String, Object> bodyRequestParameter = getRequestBody("categoryFilters");
        Query.getInstance().addRequestParameter(RequestName.WITH_TOP_CATEGORY_FETCH, bodyRequestParameter);
        filterRequestBuilder.getService().getFilter(bodyRequestParameter, new Callback<FilterResponse>() {
            @Override
            public void success(FilterResponse filterResponse, Response response) {
                mApplicationFacade.setResponse(FilterResponse.class.getSimpleName(), filterResponse);
                mResponseSubscribe.onSuccess(filterResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void clearFilterResponse() {
        mApplicationFacade.setResponse(FilterResponse.class.getSimpleName(), null);
    }
}
