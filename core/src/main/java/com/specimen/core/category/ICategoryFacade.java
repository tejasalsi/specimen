package com.specimen.core.category;

import com.specimen.core.IBaseFacade;

/**
 * Created by Intelliswift on 6/17/2015.
 */
public interface ICategoryFacade extends IBaseFacade {

//     void getCategoryInfo(String catId);
//
//    void getTopCategories();

    void getCategoryList();
}
