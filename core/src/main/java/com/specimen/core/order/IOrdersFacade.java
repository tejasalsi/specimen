package com.specimen.core.order;

/**
 * Created by Swapnil on 8/20/2015.
 */
public interface IOrdersFacade {

    void getOrderInfo();

    void getOrderDetail(String orderId);

    void trackShipment(String orderId);
}
