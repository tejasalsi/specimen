package com.specimen.core.order;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.OrderHistoryResponse;
import com.specimen.core.request.OrderRequestBuilder;
import com.specimen.core.response.OrderTrackingResponse;
import com.specimen.core.response.OrderedDetailsResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Swapnil on 8/20/2015.
 */
public class OrdersFacade extends BaseFacade implements IOrdersFacade {

    OrderRequestBuilder mOrderRequestBuilder;

    public OrdersFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        this.mOrderRequestBuilder = new OrderRequestBuilder();
    }

    @Override
    public void getOrderInfo() {
        mOrderRequestBuilder.getService().getOrderInfo(getRequestBody("getOrderInfo"), new Callback<OrderHistoryResponse>() {
            @Override
            public void success(OrderHistoryResponse orderHistoryResponse, Response response) {
                mResponseSubscribe.onSuccess(orderHistoryResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void getOrderDetail(String orderId) {
        HashMap<String, Object> map = getRequestBody("getOrderDetails");
        map.put("orderId", orderId);
        mOrderRequestBuilder.getService().getOrderDetail(map, new Callback<OrderedDetailsResponse>() {

            @Override
            public void success(OrderedDetailsResponse orderedDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(orderedDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void trackShipment(String orderNo) {
        HashMap<String, Object> map = getRequestBody("trackShipment");
        map.put("order_no", orderNo);
        mOrderRequestBuilder.getService().trackShipment(map, new Callback<OrderTrackingResponse>() {

            @Override
            public void success(OrderTrackingResponse orderedDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(orderedDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }
}
