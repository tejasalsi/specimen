package com.specimen.core.studio;

import com.specimen.core.IBaseFacade;

/**
 * Created by root on 23/8/15.
 */
public interface IStudioFacade extends IBaseFacade {

    void getStudios ();

    void setAppointment (String studioId, String slotId, String date);

    void getTimeSlots(String studio_id, String date);

    void getBusyDates(String studioId);

}
