package com.specimen.core.look;

import com.specimen.core.IBaseFacade;
import com.specimen.core.model.Query;

/**
 * Created by Intelliswift on 8/20/2015.
 */
public interface ILookFacade extends IBaseFacade {

    void fetchLooks(Query query);

    void fetchLookDetail(String lookId);
}
