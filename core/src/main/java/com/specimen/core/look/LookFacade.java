package com.specimen.core.look;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.RequestName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.Query;
import com.specimen.core.request.LookRequestBuilder;
import com.specimen.core.response.LookDetailsResponse;
import com.specimen.core.response.LookResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Intelliswift on 8/20/2015.
 */
public class LookFacade extends BaseFacade implements ILookFacade {

    private final LookRequestBuilder mLookRequestBuilder;

    public LookFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        mLookRequestBuilder = new LookRequestBuilder();
    }

    @Override
    public void fetchLooks(Query query) {

        HashMap<String, Object> map = getRequestBody("getLooks");
        Query.getInstance().addRequestParameter(RequestName.WITH_SUBCATEGORY_FETCH, map);

        mLookRequestBuilder.getService().getLooks(map, new Callback<LookResponse>() {
            @Override
            public void success(LookResponse lookResponse, Response response) {
                mResponseSubscribe.onSuccess(lookResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void fetchLookDetail(String lookId) {
        HashMap<String, Object> map = getRequestBody("looksDetails");
        map.put("look_id", lookId);

        mLookRequestBuilder.getService().getLookDetails(map, new Callback<LookDetailsResponse>() {
            @Override
            public void success(LookDetailsResponse lookDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(lookDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });

    }
}
