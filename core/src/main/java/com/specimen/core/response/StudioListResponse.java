package com.specimen.core.response;

import com.specimen.core.model.StudioResponseData;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public class StudioListResponse extends APIResponse {


    /**
     * data : {"styler":[{"image":{"Small":{"sizeName":"Small","actualWidth":31,"width":32,"actualHeight":40,"url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":40},"XLarge":{"sizeName":"XLarge","actualWidth":264,"width":328,"actualHeight":344,"url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":410},"Medium":{"sizeName":"Medium","actualWidth":107,"width":112,"actualHeight":140,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","height":140},"Large":{"sizeName":"Large","actualWidth":157,"width":164,"actualHeight":205,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","height":205},"IPhoneSmall":{"sizeName":"IPhoneSmall","actualWidth":96,"width":100,"actualHeight":125,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":125},"Best":{"sizeName":"Best","actualWidth":264,"width":720,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","height":900},"Original":{"sizeName":"Original","actualWidth":264,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg"},"IPhone":{"sizeName":"IPhone","actualWidth":264,"width":288,"actualHeight":344,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":360}},"id":"11"},{"image":{"Small":{"sizeName":"Small","actualWidth":31,"width":32,"actualHeight":40,"url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":40},"XLarge":{"sizeName":"XLarge","actualWidth":264,"width":328,"actualHeight":344,"url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":410},"Medium":{"sizeName":"Medium","actualWidth":107,"width":112,"actualHeight":140,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","height":140},"Large":{"sizeName":"Large","actualWidth":157,"width":164,"actualHeight":205,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","height":205},"IPhoneSmall":{"sizeName":"IPhoneSmall","actualWidth":96,"width":100,"actualHeight":125,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":125},"Best":{"sizeName":"Best","actualWidth":264,"width":720,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","height":900},"Original":{"sizeName":"Original","actualWidth":264,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg"},"IPhone":{"sizeName":"IPhone","actualWidth":264,"width":288,"actualHeight":344,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":360}},"id":"12"},{"image":{"Small":{"sizeName":"Small","actualWidth":31,"width":32,"actualHeight":40,"url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":40},"XLarge":{"sizeName":"XLarge","actualWidth":264,"width":328,"actualHeight":344,"url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":410},"Medium":{"sizeName":"Medium","actualWidth":107,"width":112,"actualHeight":140,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","height":140},"Large":{"sizeName":"Large","actualWidth":157,"width":164,"actualHeight":205,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","height":205},"IPhoneSmall":{"sizeName":"IPhoneSmall","actualWidth":96,"width":100,"actualHeight":125,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":125},"Best":{"sizeName":"Best","actualWidth":264,"width":720,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","height":900},"Original":{"sizeName":"Original","actualWidth":264,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg"},"IPhone":{"sizeName":"IPhone","actualWidth":264,"width":288,"actualHeight":344,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":360}},"id":"13"}]}
     * message : fetched successfully
     * status : Success
     */
    private StudioResponseData data;

    public StudioResponseData getData() {
        return data;
    }

    public void setData(StudioResponseData data) {
        this.data = data;
    }


}
