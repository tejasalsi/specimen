package com.specimen.core.response;

import com.specimen.core.model.OrderHistoryResponseData;

/**
 * Created by Intelliswift on 7/14/2015.
 */
public class OrderedHistoryResponse extends APIResponse {


    private OrderHistoryResponseData data;

    public OrderHistoryResponseData getData() {
        return data;
    }

    public void setData(OrderHistoryResponseData data) {
        this.data = data;
    }


}
