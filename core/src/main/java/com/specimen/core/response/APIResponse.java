package com.specimen.core.response;

import com.google.gson.annotations.Expose;

/**
 * Created by Intelliswift on 6/16/2015.
 */
public class APIResponse {
    @Expose
    private String status;
    @Expose
    private String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String result) {
        this.status = result;
    }


}
