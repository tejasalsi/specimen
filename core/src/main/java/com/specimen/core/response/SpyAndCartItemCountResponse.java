package com.specimen.core.response;

/**
 * Created by DhirajK on 11/28/2015.
 */
public class SpyAndCartItemCountResponse extends APIResponse {
    /**
     * spy_count : 3
     * shoppingcart_count : 1
     */

    private DataEntity data;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public DataEntity getData() {
        return data;
    }

    public static class DataEntity {
        private int spy_count;
        private String shoppingcart_count;

        public void setSpy_count(int spy_count) {
            this.spy_count = spy_count;
        }

        public void setShoppingcart_count(String shoppingcart_count) {
            this.shoppingcart_count = shoppingcart_count;
        }

        public int getSpy_count() {
            return spy_count;
        }

        public String getShoppingcart_count() {
            return shoppingcart_count;
        }
    }
}
