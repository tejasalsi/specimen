package com.specimen.core.response;

import com.google.gson.annotations.Expose;
import com.specimen.core.model.User;

public class UserResponse extends APIResponse {

        @Expose
        private User data ;

        /**
         * @return The data
         */
        public User getData() {
            return data;
        }

        /**
         * @param data The data
         */
        public void setData(User data) {
            this.data = data;
        }


    }