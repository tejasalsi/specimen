package com.specimen.core.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.specimen.core.model.Address;
import com.specimen.core.model.ProductSizesEntity;

import java.util.ArrayList;
import java.util.List;

public class AddressResponse extends APIResponse implements Parcelable {

    @Expose
    private List<Address> data = new ArrayList<>();

    /**
     * @return The data
     */
    public List<Address> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<Address> data) {
        this.data = data;
    }

    public AddressResponse (List<Address> list) {
        this.data = list;
    }


    protected AddressResponse(Parcel in) {
        if (in.readByte() == 0x01) {
            data = new ArrayList<>();
            in.readList(data, Address.class.getClassLoader());
        } else {
            data = null;
        }
    }

    public final static Creator<AddressResponse> CREATOR = new Creator<AddressResponse>() {
        @Override
        public AddressResponse createFromParcel(Parcel in) {
            return new AddressResponse(in);
        }

        @Override
        public AddressResponse[] newArray(int size) {
            return new AddressResponse[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (data == null) {
            parcel.writeByte((byte) (0x00));
        } else {
            parcel.writeByte((byte) (0x01));
            parcel.writeList(data);
        }
    }
}