package com.specimen.core.response;

import com.specimen.core.model.FilterType;

import java.util.List;

/**
 * Created by Intelliswift on 7/10/2015.
 */
public class FilterResponse extends APIResponse {

    private List<FilterType> data;

    public List<FilterType> getData() {
        return data;
    }

    public void setData(List<FilterType> data) {
        this.data = data;
    }

}
