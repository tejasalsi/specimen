package com.specimen.core.response;

import com.specimen.core.model.LookResponseData;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public class LookResponse extends APIResponse {

    /**
     * data : {"metadata":{"filter":[{"attr_code":"categoryId","attr_value":["10"]}],"total":24,"limit":4,"page":0},"looks":[{"isSpied":"true","image":{"Small":{"sizeName":"Small","actualWidth":31,"width":32,"actualHeight":40,"url":"http://resources.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":40},"XLarge":{"sizeName":"XLarge","actualWidth":264,"width":328,"actualHeight":344,"url":"http://resources.shopstyle.com/xim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":410},"Medium":{"sizeName":"Medium","actualWidth":107,"width":112,"actualHeight":140,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926_medium/burberry-infant-s-cashmere-dress.jpg","height":140},"Large":{"sizeName":"Large","actualWidth":157,"width":164,"actualHeight":205,"url":"http://resources.shopstyle.com/sim/36/30/3630ea5625adac7abc61f61e8cf7b926/burberry-infant-s-cashmere-dress.jpg","height":205},"IPhoneSmall":{"sizeName":"IPhoneSmall","actualWidth":96,"width":100,"actualHeight":125,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926_small.jpg","height":125},"Best":{"sizeName":"Best","actualWidth":264,"width":720,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg","height":900},"Original":{"sizeName":"Original","actualWidth":264,"actualHeight":344,"url":"http://bim.shopstyle.com/pim/36/30/3630ea5625adac7abc61f61e8cf7b926_best.jpg"},"IPhone":{"sizeName":"IPhone","actualWidth":264,"width":288,"actualHeight":344,"url":"http://resources.shopstyle.com/mim/36/30/3630ea5625adac7abc61f61e8cf7b926.jpg","height":360}},"spied_count":222,"name":"Fernbuckles washed-chinos","id":"36","view_count":2020}]}
     * message : Looks Data.
     * status : Success
     */
    private LookResponseData data;

    public void setData(LookResponseData data) {
        this.data = data;
    }


    public LookResponseData getData() {
        return data;
    }


}
