package com.specimen.core.response;

import com.specimen.core.model.BulletinEntity;

import java.util.List;

/**
 * Created by root on 20/10/15.
 */
public class BulletinResponseData extends APIResponse {
    List<BulletinEntity> data;

    public void setData(List<BulletinEntity> data) {
        this.data = data;
    }

    public List<BulletinEntity> getData() {
        return data;
    }
}
