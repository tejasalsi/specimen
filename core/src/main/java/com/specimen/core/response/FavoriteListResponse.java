package com.specimen.core.response;

import com.specimen.core.model.FavoriteResponseData;

public class FavoriteListResponse extends APIResponse {

    private FavoriteResponseData data;

    public FavoriteResponseData getFavoriteResponseData() {
        return data;
    }

    public void setFavoriteResponseData(FavoriteResponseData data) {
        this.data = data;
    }


}