package com.specimen.core.response;

import com.specimen.core.model.SuggestionEntity;

import java.util.List;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public class AutoCompleteResponse extends APIResponse {

    /**
     * data : [{"id":"5","string":"Chinos","type":"category"},
     * {"id":3,"string":"Chinos IN Bottoms","type":"category"},
     * {"id":5,"string":"Chinos IN Chinos","type":"category"},
     * {"id":"11","string":"Chinos khakis Washed Chinos","type":"product"},
     * {"id":"17","string":"Chinos Product","type":"product"}]
     */

    private List<SuggestionEntity> data;

    public void setData(List<SuggestionEntity> data) {
        this.data = data;
    }

    public List<SuggestionEntity> getData() {
        return data;
    }

}
