package com.specimen.core.response;

import java.util.ArrayList;

/**
 * Created by Rohit on 9/4/2015.
 */
public class AddOrRemoveFromWishListResponse extends APIResponse {
    /**
     * spy_count : 4
     */

    private DataEntity data;

//    private ArrayList<String> data;


//    public  ArrayList<String> getFavoriteResponseData() {
//        return data;
//    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public DataEntity getData() {
        return data;
    }

    public static class DataEntity {
        private int spy_count;

        public void setSpy_count(int spy_count) {
            this.spy_count = spy_count;
        }

        public int getSpy_count() {
            return spy_count;
        }
    }
}
