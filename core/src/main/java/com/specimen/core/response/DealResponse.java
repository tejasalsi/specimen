package com.specimen.core.response;

import com.specimen.core.model.DealResponseData;

/**
 * Created by root on 19/8/15.
 */
public class DealResponse extends APIResponse{

    private DealResponseData data;

    public void setData(DealResponseData data) {
        this.data = data;
    }

    public DealResponseData getData() {
        return data;
    }


}
