package com.specimen.core.bulletin;

import com.specimen.core.IBaseFacade;

/**
 * Created by root on 20/10/15.
 */
public interface IBulletinFacade extends IBaseFacade {

    void bulletin();
}
