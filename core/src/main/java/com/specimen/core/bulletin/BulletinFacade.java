package com.specimen.core.bulletin;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.request.BulletinRequestBuilder;
import com.specimen.core.response.BulletinResponseData;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by root on 20/10/15.
 */
public class BulletinFacade extends BaseFacade implements IBulletinFacade {

    private BulletinRequestBuilder bulletinRequestBuilder;

    public BulletinFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        bulletinRequestBuilder = new BulletinRequestBuilder();
    }

    @Override
    public void bulletin() {
        bulletinRequestBuilder.getService().bulletin(getRequestBody("bulletin"), new Callback<BulletinResponseData>() {
            @Override
            public void success(BulletinResponseData dealResponse, Response response) {
                mResponseSubscribe.onSuccess(dealResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }
}

