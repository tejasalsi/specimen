package com.specimen.core.request;

import com.specimen.core.IOCContainer;

import java.lang.reflect.Field;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;

public abstract class RetroRequestBuilder {

    //    String MOCK = "http://private-0cf37-specimen1.apiary-mock.com";
    //    String STAGE=" http://staging.intelliswift.co.in/sdt/specimen/trunk/webservices";
//    String STAGE="http://staging.intelliswift.co.in/sdt/specimennew/Specimen/trunk/index.php/webservices";
//    String STAGE = "http://specimen.intelliswift.in/webservices";
//    String TESTING = "http://specimenqa.intelliswift.in/webservices";
    private static RestAdapter restAdapter = null;

    protected RestAdapter build() {

        if (restAdapter == null) {
            restAdapter = new RestAdapter.Builder()
                    .setEndpoint(IOCContainer.BaseURL)
                    .setLogLevel(LogLevel.FULL)
                    .build();
        }
        return restAdapter;
    }
}
