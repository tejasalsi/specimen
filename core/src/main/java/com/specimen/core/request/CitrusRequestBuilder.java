package com.specimen.core.request;

import com.specimen.core.model.CreateOrderResponse;
import com.specimen.core.response.CitrusPaymentResponse;
import com.specimen.core.response.DealResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by root on 3/10/15.
 */
public class CitrusRequestBuilder extends RetroRequestBuilder {

    public CitrusPaymentService getService() {

        return super.build().create(CitrusPaymentService.class);
    }

    public interface CitrusPaymentService {
        //        @POST("/deal")
        @POST("/")
        void citrusPaymentCOD(@Body HashMap<String, Object> bodyParameter,
                  Callback<CreateOrderResponse> callback);

    }
}
