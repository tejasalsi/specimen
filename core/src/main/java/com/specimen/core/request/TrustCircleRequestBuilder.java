package com.specimen.core.request;

import com.specimen.core.model.LikeNotificationResponse;
import com.specimen.core.response.TrustCircleResponse;

import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public class TrustCircleRequestBuilder extends RetroRequestBuilder {

    public TrustCircleService getService() {

        return super.build().create(TrustCircleService.class);
    }

    public interface TrustCircleService {

        //        @POST("/customerTrustCircle")
        @POST("/")
        void getTrustCircle(@Body HashMap<String, Object> bodyParameter,
                            Callback<TrustCircleResponse> callback);


        //        @POST("/addTrustee")
        @POST("/")
        void addTrustee(@Body HashMap<String, Object> bodyParameter,
                        Callback<TrustCircleResponse> callback);


        //        @POST("/deleteTrustee")
        @POST("/")
        void deleteTrustee(@Body HashMap<String, Object> bodyParameter,
                           Callback<TrustCircleResponse> callback);

        @POST("/")
        void sendProducttoTrustCircle(@Body HashMap<String, Object> bodyParameter,
                                      Callback<TrustCircleResponse> callback);

        @POST("/")
        void trusteeProductLikeNotification(@Body HashMap<String, Object> bodyParameter,
                                            Callback<LikeNotificationResponse> callback);

    }

}
