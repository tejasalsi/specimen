package com.specimen.core.request;

import com.specimen.core.response.FilterResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 7/10/2015.
 */
public class FilterRequestBuilder extends RetroRequestBuilder {
    public FilterService getService() {
        return super.build().create(FilterService.class);
    }

    public interface FilterService {

//        @POST("/categoryFilters")
@POST("/")
        void getFilter(@Body HashMap<String, Object> bodyParameter,
                       Callback<FilterResponse> callback);


    }
}
