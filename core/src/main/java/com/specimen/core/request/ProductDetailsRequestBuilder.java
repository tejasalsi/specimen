package com.specimen.core.request;

import com.specimen.core.response.ProductDetailsResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by root on 7/8/15.
 */
public class ProductDetailsRequestBuilder extends RetroRequestBuilder {

    public ProductDetailsService getService() {

        return super.build().create(ProductDetailsService.class);
    }

    public interface ProductDetailsService {
//                @POST("/getProductDetails")
        @POST("/")
        void getProductDetails(@Body HashMap<String, Object> bodyParameter,
                               Callback<ProductDetailsResponse> callback);

    }
}