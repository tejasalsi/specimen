package com.specimen.core.request;

import com.specimen.core.model.HandpickedProductsResponse;
import com.specimen.core.model.HandpickedRemovedResponse;
import com.specimen.core.model.IsSurveyCompleteResponse;
import com.specimen.core.model.OnAddToSpiedResponse;
import com.specimen.core.response.AddOrRemoveFromWishListResponse;
import com.specimen.core.response.FavoriteListResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Swapnil on 7/27/2015.
 */
public class HandpickedReuestBuilder extends RetroRequestBuilder {


    public HandpickedService getService() {
        return super.build().create(HandpickedService.class);
    }

    public interface HandpickedService {

//                @POST("/savetoWishlist")
        @POST("/")
        void setSpied(@Body HashMap<String, Object> bodyParameter,
//                      Callback<OnAddToSpiedResponse> callback);
                      Callback<AddOrRemoveFromWishListResponse> callback);

//                @POST("/handPicked")
        @POST("/")
        void getHandpickedProducts(@Body HashMap<String, Object> bodyParameter,
                                   Callback<HandpickedProductsResponse> callback);

//                @POST("/removeHandpickedProduct")
        @POST("/")
        void removeHandpickedProduct(@Body HashMap<String, Object> bodyParameter,
                                     Callback<HandpickedRemovedResponse> callback);


    }
}
