package com.specimen.core.request;


import com.specimen.core.response.LoginBannerResponse;
import com.specimen.core.response.UserResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

public class AuthenticationRequestBuilder extends
        RetroRequestBuilder {

    public AuthenticationService getService() {

        return super.build().create(AuthenticationService.class);
    }

    public interface AuthenticationService {

//        @POST("/login")
        @POST("/")
        void login(
                @Body HashMap<String, Object> bodyParameter,
                Callback<UserResponse> callback);

//                @POST("/socialLogin")
        @POST("/")
        void socialLogin(
                @Body HashMap<String, Object> bodyParameter,
                Callback<UserResponse> callback);

//                               @POST("/register")
        @POST("/")
        void signUp(@Body HashMap<String, Object> bodyParameter,
                    Callback<UserResponse> callback);

//                @POST("/forgotPassword")
        @POST("/")
        void forgotPassword(
                @Body HashMap<String, Object> bodyParameter,
                Callback<UserResponse> callback);

//        @POST("/loginBannerImages")
        @POST("/")
        void loginBannerImages(
                @Body HashMap<String, Object> bodyParameter,
                Callback<LoginBannerResponse> callback);

        @POST("/")
        void changePassword(
                @Body HashMap<String, Object> bodyParameter,
                Callback<UserResponse> callback);
    }

}
