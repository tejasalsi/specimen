package com.specimen.core.request;

import com.specimen.core.response.APIResponse;
import com.specimen.core.response.DealResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Swapnil on 10/20/2015.
 */
public class FeedbackRequestBuilder extends RetroRequestBuilder {

    public FeedbackShareService getService() {

        return super.build().create(FeedbackShareService.class);
    }

    public interface FeedbackShareService {
        //        @POST("/shareFeedback")
        @POST("/")
        void shareFeedback(@Body HashMap<String, Object> bodyParameter,
                  Callback<APIResponse> callback);

    }
}
