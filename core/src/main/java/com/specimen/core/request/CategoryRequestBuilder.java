package com.specimen.core.request;

import com.specimen.core.response.CategoryListResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Intelliswift on 6/17/2015.
 */
public class CategoryRequestBuilder extends
        RetroRequestBuilder {

    public CategoryService getService() {

        return super.build().create(CategoryService.class);
    }

    public interface CategoryService {

//        @POST("/categoryList")
        @POST("/")
        void getCategoryList(
                @Body HashMap<String, Object> bodyParameter,
                Callback<CategoryListResponse> callback);

    }

}
