package com.specimen.core.authentication;

import com.specimen.core.IBaseFacade;

/**
 * Created by Intelliswift on 6/16/2015.
 */
public interface IAuthenticationFacade extends IBaseFacade {

    void signIn(String email, String password, String device_id);

    void resetPassword(String emailId);

    void socialLogin(String login_type, String email, String firstName, String accessToken, String device_id);

    void signUp(String name, String email, String password, String mobileNumber, String device_id);

    void loginBanner();

    void changePassword(String oldPwd, String newPwd);

    void logout();

    void getUserId();
}
