package com.specimen.core.authentication;

import com.specimen.core.BaseFacade;
import com.specimen.core.IOCContainer;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.ServiceName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.request.AuthenticationRequestBuilder;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.LoginBannerResponse;
import com.specimen.core.response.UserResponse;
import com.specimen.core.util.SharedPreferenceHelper;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Rohit on 6/16/2015.
 */
public class AuthenticationFacade extends BaseFacade implements IAuthenticationFacade, IResponseSubscribe {

    private final AuthenticationRequestBuilder authenticationRequestBuilder;


    public AuthenticationFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        this.authenticationRequestBuilder = new AuthenticationRequestBuilder();
    }


    @Override
    public void signIn(String email, String password, String device_id) {

        UserResponse userResponse = mApplicationFacade.getUserResponse();

        if (userResponse == null) {
//            HashMap<String, Object> bodyParameter = getBodyParameterMap("login");
            HashMap<String, Object> bodyParameter = getRequestBody("login");
            bodyParameter.put("email", email);
            bodyParameter.put("password", password);
            bodyParameter.put("device_id", device_id);
            bodyParameter.put("device_type", "1");

            authenticationRequestBuilder.getService().login(bodyParameter, new Callback<UserResponse>() {


                @Override
                public void success(UserResponse userResponse, Response response) {
                    onSuccess(userResponse, TAG);
                }

                @Override
                public void failure(RetrofitError error) {
                    onFailure(error);
                }
            });
        } else {
            onSuccess(userResponse, TAG);
        }
    }

    @Override
    public void getUserId(){
        UserResponse userResponse = mApplicationFacade.getUserResponse();
        if (userResponse != null) {
            onSuccess(userResponse, TAG);
        }
    }


    @Override
    public void resetPassword(String emailId) {
        HashMap<String, Object> bodyParameter = getRequestBody("forgotPassword");
        bodyParameter.put("email", emailId);
        authenticationRequestBuilder.getService().forgotPassword(bodyParameter, new Callback<UserResponse>() {
            @Override
            public void success(UserResponse userResponse, Response response) {
                 //TODO: Need to implement
                 //                responseSubscribe.onSuccess(userResponse, TAG);
                onSuccess(userResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                onFailure(error);
            }
        });

    }

    @Override
    public void socialLogin(String login_type, String email, String firstName, String accessToken, String deviceId) {
        HashMap<String, Object> bodyParameter = getRequestBody("socialLogin");
        bodyParameter.put("email", email);
        bodyParameter.put("login_type", login_type);
        bodyParameter.put("firstname", firstName);
        bodyParameter.put("login_token", accessToken);
        bodyParameter.put("device_id", deviceId);
        bodyParameter.put("device_type", "1");

        authenticationRequestBuilder.getService().socialLogin(bodyParameter, new Callback<UserResponse>() {
            @Override
            public void success(UserResponse userResponse, Response response) {
                //TODO: Need to implement
                onSuccess(userResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                onFailure(error);
            }
        });
    }

    @Override
    public void signUp(String name, String email, String password, String mobileNumber, String deviceId) {
        HashMap<String, Object> bodyParameter = getRequestBody("register");
        bodyParameter.put("firstname", name);
        bodyParameter.put("email", email);
        bodyParameter.put("password", password);
        bodyParameter.put("device_id", deviceId);
        bodyParameter.put("device_type", "1");

        authenticationRequestBuilder.getService().signUp(bodyParameter, new Callback<UserResponse>() {
            @Override
            public void success(UserResponse userResponse, Response response) {
                //TODO: Need to implement
                onSuccess(userResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                onFailure(error);
            }
        });

    }

    @Override
    public void loginBanner() {
        HashMap<String, Object> bodyParameter = getRequestBody("loginBannerImages");
        authenticationRequestBuilder.getService().loginBannerImages(bodyParameter, new Callback<LoginBannerResponse>() {
            @Override
            public void success(LoginBannerResponse userResponse, Response response) {
                mResponseSubscribe.onSuccess(userResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void changePassword(String oldPwd, String newPwd) {
        HashMap<String, Object> bodyParameter = getRequestBody("changepassword");
        bodyParameter.put("old_password", oldPwd);
        bodyParameter.put("new_password", newPwd);

        authenticationRequestBuilder.getService().signUp(bodyParameter, new Callback<UserResponse>() {
            @Override
            public void success(UserResponse userResponse, Response response) {
                //TODO: Need to implement
                mResponseSubscribe.onSuccess(userResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void logout() {
        SharedPreferenceHelper.clear(SharedPreferenceHelper.APP_PROPERTIES);
        SharedPreferenceHelper.clear(SharedPreferenceHelper.CACHED_RESPONSES);
        SharedPreferenceHelper.clear(SharedPreferenceHelper.LOCAL);
        ((IApplicationFacade) IOCContainer.getInstance().getObject(ServiceName.APPLICATION_SERVICE, "")).setOfflineResponse("fbFriendsCount", "0");
    }


    @Override
    public void onSuccess(APIResponse response, String tag) {

        mApplicationFacade.setUserResponse((UserResponse) response);
        mResponseSubscribe.onSuccess(response, tag);
    }

    @Override
    public void onFailure(Exception error) {
        mResponseSubscribe.onFailure(error);
    }


}
