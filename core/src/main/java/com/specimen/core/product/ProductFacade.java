package com.specimen.core.product;

import android.util.Log;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.RequestName;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.model.PageRequest;
import com.specimen.core.model.Product;
import com.specimen.core.model.ProductResponseData;
import com.specimen.core.model.Query;
import com.specimen.core.request.ProductRequestBuilder;
import com.specimen.core.response.APIResponse;
import com.specimen.core.response.ProductDetailsResponse;
import com.specimen.core.response.ProductListResponse;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Intelliswift on 7/9/2015.
 */

public class ProductFacade extends BaseFacade implements IProductFacade, IResponseSubscribe{


    private final ProductRequestBuilder mProductRequestBuilder;

    public ProductFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        this.mProductRequestBuilder = new ProductRequestBuilder();

    }

    @Override
    public void getProduct(PageRequest pageRequest) {

        HashMap<String, Object> map = getRequestBody("getProduct");
        Query.getInstance().addRequestParameter(RequestName.WITH_SUBCATEGORY_FETCH, map);
        pageRequest.addParameters(map);
        mProductRequestBuilder.getService().getProduct(map, new Callback<ProductListResponse>() {
            @Override
            public void success(ProductListResponse productResponse, Response response) {
                onSuccess(productResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                onFailure(error);
            }
        });

    }


    @Override
    public synchronized void getProduct(PageRequest pageRequest, String categoryId, final String tag) {

        synchronized (mProductRequestBuilder) {
            HashMap<String, Object> map = getRequestBody("getProduct");
            Query.getInstance().setSubCategoryId(categoryId);
            // map.put("categoryId", categoryId);
            Query.getInstance().addRequestParameter(RequestName.WITH_SUBCATEGORY_FETCH, map);
            pageRequest.addParameters(map);
            Log.e("Request Tag -->" + tag, map.toString());
            mProductRequestBuilder.getService().getProduct((HashMap) map.clone(), new Callback<ProductListResponse>() {
                @Override
                public void success(ProductListResponse productResponse, Response response) {
                    onSuccess(productResponse, tag);
                }

                @Override
                public void failure(RetrofitError error) {
                    onFailure(error);
                }
            });
        }
    }

    @Override
    public synchronized void getProduct(PageRequest pageRequest, Query query, final String tag) {

        HashMap<String, Object> map = getRequestBody("getProduct");
//        Query.getInstance().setSubCategoryId(categoryId);
        // map.put("categoryId", categoryId);
        query.addRequestParameter(RequestName.WITH_TOP_CATEGORY_FETCH, map);
        pageRequest.addParameters(map);
        Log.e("Request Tag --" + tag, map.toString());
        mProductRequestBuilder.getService().getProduct((HashMap) map.clone(), new Callback<ProductListResponse>() {
            @Override
            public void success(ProductListResponse productResponse, Response response) {
                onSuccess(productResponse, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onFailure(error);
            }
        });

    }


    private void appendResponse(ProductListResponse arg0) {

        ProductListResponse currentResponse = arg0;

        ProductResponseData currentProductResponseData = currentResponse.getProductResponseData();
        if (currentProductResponseData.getMetadata() != null) {

            ArrayList<Product> currentProductList = (ArrayList<Product>) currentProductResponseData.getProducts();
//                    .getProducts();


            // find out in object pool
            ProductListResponse oldResponse = (ProductListResponse) mApplicationFacade.getResponse(
                    ProductListResponse.class.getSimpleName());

            // Check for previous response
            if (oldResponse != null) {

                ProductResponseData productResponseData = oldResponse.getProductResponseData();
                productResponseData.getProducts().addAll(currentProductList);
                productResponseData.setMetadata(currentProductResponseData.getMetadata());
                // Update response for publish
                currentResponse = oldResponse;
            }

            // Added to object pool
            mApplicationFacade.setResponse(
                    ProductListResponse.class.getSimpleName(),
                    currentResponse);
        }

    }

    @Override
    public void searchProducts(PageRequest pageRequest) {
        HashMap<String, Object> map = getRequestBody("getProduct");
        Query.getInstance().addRequestParameter(RequestName.WITH_SUBCATEGORY_FETCH, map);
        pageRequest.addParameters(map);
        mProductRequestBuilder.getService().searchProduct(map, new Callback<ProductListResponse>() {
            @Override
            public void success(ProductListResponse productResponse, Response response) {
                onSuccess(productResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                onFailure(error);
            }
        });
    }

    @Override
    public void getProductDetail(String productID) {
        HashMap<String, Object> map = getRequestBody("getProductDetails");
        map.put("product_id", productID);

        mProductRequestBuilder.getService().getProductDetails(map, new Callback<ProductDetailsResponse>() {
            @Override
            public void success(ProductDetailsResponse productDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(productDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                onFailure(error);
            }
        });

    }

    @Override
    public void resetProductCache() {
        mApplicationFacade.setResponse(ProductListResponse.class.getSimpleName(), null);
    }

    @Override
    public void productsharecount(String productID) {
        HashMap<String, Object> map = getRequestBody("productsharecount");
        map.put("product_id", productID);

        mProductRequestBuilder.getService().getProductDetails(map, new Callback<ProductDetailsResponse>() {
            @Override
            public void success(ProductDetailsResponse productDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(productDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                onFailure(error);
            }
        });
    }

    @Override
    public void onSuccess(APIResponse response, String tag) {
        appendResponse((ProductListResponse) response);
        mResponseSubscribe.onSuccess(response, tag);
    }

    @Override
    public void onFailure(Exception error) {
        mResponseSubscribe.onFailure(error);
    }


}
