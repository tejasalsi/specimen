package com.specimen.core.cart;

import com.specimen.core.IBaseFacade;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public interface ICartFacade extends IBaseFacade {

    void getCart();

    void addToCart(String productId, String qty, String sizeId, String attributeId);

    void deleteFromCart(String productId, String parentProductId);


    void updateQuantity(String productId, String quantity, String childId);

    void applyPromoCode(String promoCode);

    void getCartStock();
}
