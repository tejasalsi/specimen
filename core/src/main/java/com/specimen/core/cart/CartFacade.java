package com.specimen.core.cart;

import com.specimen.core.BaseFacade;
import com.specimen.core.IResponseSubscribe;
import com.specimen.core.application.IApplicationFacade;
import com.specimen.core.request.CartRequestBuilder;
import com.specimen.core.response.CartDetailsResponse;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Intelliswift on 7/20/2015.
 */
public class CartFacade extends BaseFacade implements ICartFacade {

    private CartRequestBuilder cartRequestBuilder;

    public CartFacade(IResponseSubscribe responseSubscribe, IApplicationFacade applicationFacade) {
        super(responseSubscribe, applicationFacade);
        cartRequestBuilder = new CartRequestBuilder();
    }

    @Override
    public void getCart() {
        cartRequestBuilder.getService().getCart(getRequestBody("getCart"), new Callback<CartDetailsResponse>() {
            @Override
            public void success(CartDetailsResponse cartDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(cartDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });

    }

    @Override
    public void addToCart(String productId, String qty, String sizeId, String attributeId) {
        HashMap<String, Object> savetoCart = getRequestBody("savetoCart");
        savetoCart.put("product_id",productId);
        savetoCart.put("qty",qty);
        savetoCart.put("size_id",sizeId);
        savetoCart.put("attribute_id",attributeId);

        cartRequestBuilder.getService().addToCart(savetoCart, new Callback<CartDetailsResponse>() {
            @Override
            public void success(CartDetailsResponse cartDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(cartDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void deleteFromCart(String productId, String childId) {
        HashMap<String, Object> deleteFromCart = getRequestBody("deleteFromCart");
//        deleteFromCart.put("parent_product_id",parentProductId);
//        deleteFromCart.put("product_id",productId);
        deleteFromCart.put("parent_product_id",productId);
        deleteFromCart.put("product_id",childId);
        cartRequestBuilder.getService().deleteFromCart(deleteFromCart, new Callback<CartDetailsResponse>() {
            @Override
            public void success(CartDetailsResponse cartDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(cartDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });

    }

    @Override
    public void updateQuantity(String productId, String quantity, String childId) {
        HashMap<String, Object> getCartInfo = getRequestBody("quantityUpdate");
//        getCartInfo.put("productId", productId);
//        getCartInfo.put("quantity", quantity);
        getCartInfo.put("parent_product_id",productId);
        getCartInfo.put("product_id",childId);
        getCartInfo.put("qty", quantity);
        cartRequestBuilder.getService().getQuantityUpdate(getCartInfo, new Callback<CartDetailsResponse>() {
            @Override
            public void success(CartDetailsResponse cartDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(cartDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void applyPromoCode(String promoCode) {
        HashMap<String, Object> applyPromoCode = getRequestBody("applyPromoCode");
        applyPromoCode.put("promoCode", promoCode);
        cartRequestBuilder.getService().applyPromoCode(applyPromoCode, new Callback<CartDetailsResponse>() {
            @Override
            public void success(CartDetailsResponse cartDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(cartDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }

    @Override
    public void getCartStock() {

        cartRequestBuilder.getService().getCart(getRequestBody("getCartStock"), new Callback<CartDetailsResponse>() {
            @Override
            public void success(CartDetailsResponse cartDetailsResponse, Response response) {
                mResponseSubscribe.onSuccess(cartDetailsResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                mResponseSubscribe.onFailure(error);
            }
        });
    }
}
